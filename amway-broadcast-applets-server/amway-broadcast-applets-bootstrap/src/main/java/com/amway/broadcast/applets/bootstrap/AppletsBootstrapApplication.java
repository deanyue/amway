package com.amway.broadcast.applets.bootstrap;

import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.ServletComponentScan;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import org.springframework.tsf.annotation.EnableTsf;

@Slf4j
@EnableTransactionManagement
@ServletComponentScan
@EnableScheduling
@SpringBootApplication(scanBasePackages = {"com.amway"})
@EnableTsf
public class AppletsBootstrapApplication {
    public static void main(String[] args) {
        SpringApplication.run(AppletsBootstrapApplication.class,args);
    }
}
