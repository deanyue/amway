package com.amway.broadcast.applets.bootstrap.config;

import com.amway.broadcast.applets.service.utils.CheckUtils;
import org.apache.tomcat.util.threads.ThreadPoolExecutor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;

import java.util.concurrent.Executor;

/**
 * 线程池配置类
 */
@Configuration
@EnableAsync
public class ExecutorConfig {
    //@Value("${async.executor.thread.core_pool_size}")
    private int corePoolSize = Runtime.getRuntime().availableProcessors()*2;
    //@Value("${async.executor.thread.max_pool_size}")
    private int maxPoolSize = Runtime.getRuntime().availableProcessors()*2;
    @Value("${async.executor.thread.queue_capacity}")
    private int queueCapacity;
    @Value("${async.executor.thread.name.prefix}")
    private String namePrefix;

    @Bean
    public Executor asyncServiceExecutor() {
        System.out.println(Runtime.getRuntime().availableProcessors());
        if(CheckUtils.isEmpty (corePoolSize))
            corePoolSize =8;
        if (CheckUtils.isEmpty (maxPoolSize))
            maxPoolSize = 8;
        ThreadPoolTaskExecutor executor = new ThreadPoolTaskExecutor();
        //配置核心线程数
        executor.setCorePoolSize(corePoolSize);
        //配置最大线程数
        executor.setMaxPoolSize(maxPoolSize);
        //配置队列大小
        executor.setQueueCapacity(queueCapacity);
        //配置线程池中的线程的名称前缀
        executor.setThreadNamePrefix(namePrefix);
        // 设置是否等待计划任务在关闭时完成
        executor.setWaitForTasksToCompleteOnShutdown(true);
        // 设置此执行器应该阻止的最大秒数
        executor.setAwaitTerminationSeconds(60);

        // 线程池对拒绝任务的处理策略
        executor.setRejectedExecutionHandler(new ThreadPoolExecutor.CallerRunsPolicy());
        // 初始化
        executor.initialize();
        return executor;
    }
}
