package com.amway.broadcast.applets.bootstrap.config;
import com.alibaba.fastjson.support.spring.FastJsonRedisSerializer;
import com.amway.broadcast.applets.bootstrap.properties.CacheManagerProperties;
import org.springframework.boot.SpringBootConfiguration;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.cache.CacheManager;
import org.springframework.cache.annotation.CachingConfigurerSupport;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.context.annotation.Bean;
import org.springframework.data.redis.cache.RedisCacheConfiguration;
import org.springframework.data.redis.cache.RedisCacheManager;
import org.springframework.data.redis.cache.RedisCacheWriter;
import org.springframework.data.redis.connection.RedisConnectionFactory;
import org.springframework.data.redis.serializer.RedisSerializationContext;
import org.springframework.data.redis.serializer.StringRedisSerializer;

import javax.annotation.Resource;
import java.time.Duration;


@EnableCaching
@SpringBootConfiguration
@EnableConfigurationProperties(CacheManagerProperties.class)
public class CacheManagerConfig extends CachingConfigurerSupport {

    @Resource
    private CacheManagerProperties properties;

    @Bean
    public CacheManager cacheManager(RedisConnectionFactory redisConnectionFactory) {

        FastJsonRedisSerializer fastJsonRedisSerializer = new FastJsonRedisSerializer(Object.class);

        RedisCacheConfiguration redisCacheConfiguration = RedisCacheConfiguration.defaultCacheConfig()
                .serializeKeysWith(RedisSerializationContext.SerializationPair.fromSerializer(new StringRedisSerializer()))
                .serializeValuesWith(RedisSerializationContext.SerializationPair.fromSerializer(fastJsonRedisSerializer))
                // 设置缓存有效期一小时版本1.0
                .entryTtl(Duration.ofHours(properties.getEntryTtl()));
        return RedisCacheManager
                .builder(RedisCacheWriter.nonLockingRedisCacheWriter(redisConnectionFactory))
                .cacheDefaults(redisCacheConfiguration).build();
    }


}
