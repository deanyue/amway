package com.amway.broadcast.applets.service.api.service;


import com.amway.broadcast.applets.service.dao.entity.AmwayUser;
import com.amway.broadcast.common.base.Result;

public interface DepartmentMemberBindService {
    //（业务）用户绑定department部门表唯一数据将数据添加到departmentMember表中
    //（被使用地址）AmwayUserLoginServiceImpl的wx_login微信登录方法中使用
    Result memberBind(AmwayUser amwayUser);
}
