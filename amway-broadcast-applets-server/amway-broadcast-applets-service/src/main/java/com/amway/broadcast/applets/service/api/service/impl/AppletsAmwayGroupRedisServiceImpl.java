//package com.amway.broadcast.applets.service.api.service.impl;
//
//import com.alibaba.fastjson.JSONObject;
//import com.amway.broadcast.applets.service.dao.entity.AmwayGroup;
//import com.amway.broadcast.common.service.AmwayGroupService;
//import com.amway.broadcast.common.util.RedisUtil;
//import jodd.util.StringUtil;
//import org.springframework.stereotype.Service;
//import org.springframework.transaction.annotation.Transactional;
//
//import java.util.ArrayList;
//import java.util.List;
//import java.util.Set;
//import java.util.stream.Collectors;
//
//import static com.amway.broadcast.common.constant.RedisKeyPrefixAndSuffix.AMWAY_GROUP_PREFIX;
//
///**
// * @auther 薛晨
// * @date 2020/12/12
// * @time 11:37
// * @description redis实现
// */
//@Service
//@Transactional(rollbackFor = Exception.class)
//public class AppletsAmwayGroupRedisServiceImpl implements AmwayGroupService<AmwayGroup> {
//
//    @Override
//    public int insert(AmwayGroup baseAmwayGroup) {
//        RedisUtil.set(AMWAY_GROUP_PREFIX + baseAmwayGroup.getId() + "-" + baseAmwayGroup.getImGroupId(), JSONObject.toJSONString(baseAmwayGroup));
//        return 0;
//    }
//
//    @Override
//    public int delete(String groupId) {
//        return 0;
//    }
//
//    @Override
//    public int update(AmwayGroup baseAmwayGroup) {
//        RedisUtil.set(AMWAY_GROUP_PREFIX + baseAmwayGroup.getId() + "-" + baseAmwayGroup.getImGroupId(), JSONObject.toJSONString(baseAmwayGroup));
//        return 0;
//    }
//
//    @Override
//    public List<AmwayGroup> getListByCondition(AmwayGroup condition) {
//        List<AmwayGroup> result = new ArrayList<>();
//        if (condition.getId() != null) {
//            String s = RedisUtil.get(AMWAY_GROUP_PREFIX + condition.getId());
//            AmwayGroup group = JSONObject.parseObject(s, AmwayGroup.class);
//            result.add(group);
//        }
//        return result;
//    }
//
//    @Override
//    public AmwayGroup getByGroupId(String groupId) {
//        String s = RedisUtil.get(AMWAY_GROUP_PREFIX + groupId);
//        if (StringUtil.isNotBlank(s)) {
//            AmwayGroup group = JSONObject.parseObject(s, AmwayGroup.class);
//            return group;
//        }
//        return null;
//    }
//
//    @Override
//    public AmwayGroup getByImGroupId(String imGroupId) {
//        Set<String> keys = RedisUtil.keys(AMWAY_GROUP_PREFIX + "*" + imGroupId);
//        if (!keys.isEmpty()) {
//            return keys.stream().findFirst().map(s -> JSONObject.parseObject(s, AmwayGroup.class)).orElse(null);
//        }
//        return null;
//    }
//
//    @Override
//    public List<AmwayGroup> getByMemberId(Long memberId) {
//        List<AmwayGroup> all = getAll();
//        return all.stream().filter(amwayGroup -> amwayGroup.getMemberId().equals(memberId)).collect(Collectors.toList());
//    }
//
//    private List<AmwayGroup> getAll() {
//        List<AmwayGroup> list = new ArrayList<>();
//        Set<String> keys = RedisUtil.keys(AMWAY_GROUP_PREFIX + "*");
//        keys.forEach(key -> {
//            AmwayGroup group = JSONObject.parseObject(key, AmwayGroup.class);
//            list.add(group);
//        });
//        return list;
//    }
//}
