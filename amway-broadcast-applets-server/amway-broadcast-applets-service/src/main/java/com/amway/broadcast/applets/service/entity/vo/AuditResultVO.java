package com.amway.broadcast.applets.service.entity.vo;

import lombok.Data;

@Data
public class AuditResultVO {
    private String status;
    private String fileId;
    private String fileUrl;
    private Long errCode;
    private String publishResult;
    private String message;
}
