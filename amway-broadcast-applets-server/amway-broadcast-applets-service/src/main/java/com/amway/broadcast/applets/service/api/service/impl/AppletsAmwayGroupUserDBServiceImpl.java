//package com.amway.broadcast.applets.service.api.service.impl;
//
//import com.amway.broadcast.applets.service.dao.entity.AmwayGroupUser;
//import com.amway.broadcast.applets.service.dao.mapper.AmwayGroupUserMapper;
//import com.amway.broadcast.common.service.AmwayGroupUserService;
//import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.stereotype.Service;
//import org.springframework.transaction.annotation.Transactional;
//
//import java.util.List;
//
///**
// * @auther 薛晨
// * @date 2020/12/12
// * @time 13:51
// * @description
// */
//@Service
//@Transactional
//public class AppletsAmwayGroupUserDBServiceImpl implements AmwayGroupUserService<AmwayGroupUser> {
//    @Autowired
//    private AmwayGroupUserMapper groupUserMapper;
//
//    @Override
//    public int insert(AmwayGroupUser baseAmwayGroupUser) {
//        return 0;
//    }
//
//    @Override
//    public int update(AmwayGroupUser baseAmwayGroupUser) {
//        return 0;
//    }
//
//    @Override
//    public List<AmwayGroupUser> getListByCondition(AmwayGroupUser condition) {
//        return null;
//    }
//
//    @Override
//    public AmwayGroupUser getOne(String groupId, String userId) {
//        return null;
//    }
//
//    @Override
//    public List<AmwayGroupUser> getByGroupIdAndStatus(String groupId, Integer quitStatus) {
//        return groupUserMapper.selectList(new LambdaQueryWrapper<AmwayGroupUser>().eq(AmwayGroupUser::getGroupId, groupId).eq(AmwayGroupUser::getIsQuit, quitStatus));
//    }
//}
