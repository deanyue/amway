package com.amway.broadcast.applets.service.service;

import com.amway.broadcast.applets.service.dao.entity.NewAmwayGroup;
import com.amway.broadcast.common.service.BaseService;

/**
 * @auther 薛晨
 * @date 2020/12/14
 * @time 10:12
 * @description
 */
public interface AmwayGroupService extends BaseService<NewAmwayGroup> {
}
