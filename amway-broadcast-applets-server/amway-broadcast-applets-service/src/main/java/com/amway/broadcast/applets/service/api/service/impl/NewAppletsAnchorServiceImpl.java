package com.amway.broadcast.applets.service.api.service.impl;

import com.alibaba.fastjson.JSONObject;
import com.amway.broadcast.applets.service.api.service.AmwayUserEntryService;
import com.amway.broadcast.applets.service.api.service.AppletsAnchorService;
import com.amway.broadcast.applets.service.api.service.UserCacheService;
import com.amway.broadcast.applets.service.dao.entity.*;
import com.amway.broadcast.applets.service.dao.mapper.AmwayDepartmentMemberMapper;
import com.amway.broadcast.applets.service.dao.mapper.AmwayGroupMapper;
import com.amway.broadcast.applets.service.dao.mapper.AmwayGroupUserMapper;
import com.amway.broadcast.applets.service.dao.mapper.AmwayUserMapper;
import com.amway.broadcast.applets.service.entity.vo.AmwayGroupVO;
import com.amway.broadcast.applets.service.paramsEntity.*;
import com.amway.broadcast.applets.service.redis.RedisUtils;
import com.amway.broadcast.applets.service.service.AmwaySettingService;
import com.amway.broadcast.applets.service.utils.*;
import com.amway.broadcast.common.base.Result;
import com.amway.broadcast.common.base.ResultUtils;
import com.amway.broadcast.common.constant.WXApiConstant;
import com.amway.broadcast.common.enumeration.ResultCodeEnum;
import com.amway.broadcast.common.exception.user.parm.ContainsContrabandSensitiveWordsException;
import com.amway.broadcast.common.util.DateUtils;
import com.amway.broadcast.common.util.JwtUtils;
import com.amway.broadcast.common.util.RedisUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.vdurmont.emoji.EmojiParser;
import jodd.util.StringUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.client.RestTemplate;

import java.io.*;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.stream.Collectors;

import static com.amway.broadcast.common.enumeration.ResultCodeEnum.A0301;
import static com.amway.broadcast.common.enumeration.ResultCodeEnum.A0503;

/**
 * @auther 薛晨
 * @date 2020/12/14
 * @time 14:30
 * @description
 */
@Service
@Slf4j
public class NewAppletsAnchorServiceImpl implements AppletsAnchorService {

    @Autowired
    private AmwayUserEntryService userEntryService;
    @Autowired
    private AmwayGroupMapper groupMapper;
    @Autowired
    private AmwayGroupUserMapper groupUserMapper;
    @Autowired
    private AmwayDepartmentMemberMapper departmentMemberMapper;
    @Autowired
    private UserCacheService userService;
    @Autowired
    private AmwayUserMapper userMapper;
    @Autowired
    private AmwaySettingService settingService;

    @Value("${TencentConfig.weChatAppid}")
    private String weChatAppid;

    @Value("${TencentConfig.weChatSecret}")
    private String weChatSecret;

    @Autowired
    private RedisUtils redisUtils;

    //背景图文件名
    private static final String BACKGROUND_NAME = "background.jpg";

    //服务器上存储合成图片后的路径
    @Value("${ImageConfig.saveImagePath}")
    private String SAVE_IMAGE_PATH;

    @Value("${ImageConfig.backgroundUrl}")
    private String BACKGROUND_URL;

    /**
     * 直播间状态
     * 1：待开始 2：进行中 3：已解散
     */
    private static final String STATUS_TO_BEGIN = "1";
    private static final String STATUS_PROCESSING = "2";
    private static final String STATUS_DISSOLVED = "3";

    private static final String IS_TODAY = "1";
    private static final String IS_NOT_TODAY = "2";

    /**
     * all_time:0 1 2(所有 一周 一个月)
     */
    private static final String IS_ALL = "0";
    private static final String IS_WEEK = "1";
    private static final String IS_MOTH = "2";

    @Value("${Pressure.Test}")
    private Boolean isDebug;

    @Override
    @Transactional(rollbackFor = Exception.class)
    public Result liveIndex(String groupId) {
        NewAmwayGroup exist = groupMapper.selectById(Long.parseLong(groupId));
        if (exist == null) {
            return ResultUtils.error("0", "当前畅聊间不存在");
        }
        if (exist.getMemberId() == null) {
            return ResultUtils.error("0", "未绑定memberId");
        }
        //创建畅聊人头像、创建畅聊人昵称，根据返回的member_id去查询department_member表获得
        AmwayDepartmentMember amwayDepartmentMember = departmentMemberMapper.selectById(exist.getMemberId());
        String avatarUrl = "";
        String nickname = "";
        if (amwayDepartmentMember != null) {
            avatarUrl = amwayDepartmentMember.getAvatar();
            nickname = EmojiParser.parseToUnicode(amwayDepartmentMember.getName());
        }
        AmwayGroupVO amwayGroup = new AmwayGroupVO();
        amwayGroup.setExpire(exist.getExpire());
        amwayGroup.setImGroupId(exist.getImGroupId());
        amwayGroup.setLimitType(exist.getLimitType());
        amwayGroup.setMemberId(exist.getMemberId());
        amwayGroup.setId(exist.getId());
        amwayGroup.setMemberAvatar(avatarUrl);
        amwayGroup.setMemberNickname(nickname);
        //畅聊开始时间-时间戳
        amwayGroup.setStartTimeTimestamp(exist.getStartTime().getTime());
        log.info("畅聊间首页live_index--> 查询创建畅聊人头像、昵称数据完成");

        //统计畅聊间人数 根据id去查询group_user表有多少条记录
        // 如果为开播提醒（未开始）与im对应都为1人
        Integer count = 1;
        Date now = new Date();
        if (exist.getStartTime().before(now)) {
            count = groupUserMapper.selectCount(new LambdaQueryWrapper<NewAmwayGroupUser>().eq(NewAmwayGroupUser::getGroupId, exist.getId()));
        }

        amwayGroup.setMemberNum(count);
        //查询畅聊间头像列表 （最多取3个，结构：['', '', '']）
        List<String> groupAvatarsList = queryGroupAvatars(groupId);
        amwayGroup.setGroupAvatars(groupAvatarsList);
        //名称emoji解码
        amwayGroup.setName(EmojiParser.parseToUnicode(exist.getName()));
        //简介remark解码
        amwayGroup.setRemark(EmojiParser.parseToUnicode(exist.getRemark()));
        //统计管理员人数  只统计进入直播间的  是否群管理员(0为否,1为是,默认为1)
        int managerNum = groupUserMapper
                .selectCount(new LambdaQueryWrapper<NewAmwayGroupUser>()
                        .eq(NewAmwayGroupUser::getGroupId, exist.getId())
                        .eq(NewAmwayGroupUser::getIsManager, 1));
        amwayGroup.setManagerNum(managerNum);
        amwayGroup.setStartTime(exist.getStartTime());
        amwayGroup.setStatusAm(judgeStatus(exist.getStartTime(), exist.getEndTime()));
        //返回的畅聊间首页对象
        AnchorIndexRepose anchorIndexRepose = new AnchorIndexRepose();
        anchorIndexRepose.setInfoList(amwayGroup);
        return ResultUtils.success(anchorIndexRepose);
    }

    /**
     * 首页-全部直播间
     *
     * @param status 状态 1未开始 2进行中（废弃）
     * @return 返回值说明：
     * 1。直播列表集合，永远只返回两个直播间数据，分为直播中和待开始两类的数据
     * 直播中的直播（以结束时间正序）优先级高于待开始的直播数据（以开始时间正序），
     * 缺的一类数据用另一类数据补全，集合中直播中的排在前面
     * 2。该人创建的所有直播间
     */
    @Override
    public Result index(String wxAppApiToken, String status) {
        //通过token查询用户唯一标识openid
        String openId = JwtUtils.getUserNameFromToken(wxAppApiToken);
        AmwayUser amwayUser = userService.getUserInCacheByOpenId(openId);
        if (amwayUser == null) {
            return ResultUtils.error(ResultCodeEnum.A0201.value(), ResultCodeEnum.A0201.getName());
        }
        //判断是否授权、未授权则返回
//        if (amwayUser.getIsAuth() == 0L) {
//            return ResultUtils.error("999", "未授权");
//        }

        Date now = new Date();

        //返回前端实体对象
        AnchorIndexRepose appletsAnchorIndexRepose = new AnchorIndexRepose();
        //装载畅聊间列表信息list，返回给前端
        List<AmwayIndex> amwayIndexList = new ArrayList<>();
        //查询当前自己创建的畅聊间 表amway_group 未开始的两条数据
//        List<NewAmwayGroup> amwayGroupList = groupMapper.selectList(new LambdaQueryWrapper<NewAmwayGroup>().eq(NewAmwayGroup::getStatusAm, Integer.valueOf(status)).eq(NewAmwayGroup::getMemberId, amwayUser.getMemberId()).orderByAsc(NewAmwayGroup::getStartTime));
        LambdaQueryWrapper<NewAmwayGroup> notStartedLambdaQueryWrapper = new LambdaQueryWrapper<NewAmwayGroup>()
                .eq(NewAmwayGroup::getMemberId, amwayUser.getMemberId())
                .ge(NewAmwayGroup::getStartTime, now)
                .orderByAsc(NewAmwayGroup::getStartTime)
                .last("limit 2");
        List<NewAmwayGroup> notStartedAmwayGroupList = groupMapper.selectList(notStartedLambdaQueryWrapper);

        // 开始的两条数据
        LambdaQueryWrapper<NewAmwayGroup> startedLambdaQueryWrapper = new LambdaQueryWrapper<NewAmwayGroup>()
                .eq(NewAmwayGroup::getMemberId, amwayUser.getMemberId())
                .lt(NewAmwayGroup::getStartTime, now)
                .ge(NewAmwayGroup::getEndTime, now)
                .orderByAsc(NewAmwayGroup::getEndTime)
                .last("limit 2");
        List<NewAmwayGroup> startedAmwayGroupList = groupMapper.selectList(startedLambdaQueryWrapper);

        if (startedAmwayGroupList != null && startedAmwayGroupList.size() > 1) {
            startedAmwayGroupList.forEach(newAmwayGroup -> {
                amwayIndexList.add(getAmwayIndex2(STATUS_PROCESSING, newAmwayGroup));
            });
        } else if (startedAmwayGroupList != null && startedAmwayGroupList.size() > 0) {
            amwayIndexList.add(getAmwayIndex2(STATUS_PROCESSING, startedAmwayGroupList.get(0)));
            if (notStartedAmwayGroupList != null && notStartedAmwayGroupList.size() > 0) {
                amwayIndexList.add(getAmwayIndex2(STATUS_TO_BEGIN, notStartedAmwayGroupList.get(0)));
            }
        } else {

            if (notStartedAmwayGroupList != null) {
                notStartedAmwayGroupList.forEach(newAmwayGroup -> {
                    amwayIndexList.add(getAmwayIndex2(STATUS_TO_BEGIN, newAmwayGroup));
                });
            }
        }

        //畅聊间数量
        int joinCount = groupUserMapper.selectCount(new LambdaQueryWrapper<NewAmwayGroupUser>().eq(NewAmwayGroupUser::getUserId, amwayUser.getId()));
        appletsAnchorIndexRepose.setCount(joinCount);
        //畅聊间列表
        appletsAnchorIndexRepose.setGroupList(amwayIndexList);
        return ResultUtils.success(appletsAnchorIndexRepose);
    }

    private AmwayIndex getAmwayIndex(String status, NewAmwayGroup amwayGroup) {
        AmwayIndex amwayIndex = new AmwayIndex();
        amwayIndex.setId(amwayGroup.getId());
        amwayIndex.setImGroupId(amwayGroup.getImGroupId());
        amwayIndex.setName(EmojiParser.parseToUnicode(amwayGroup.getName()));
        SimpleDateFormat sdf = new SimpleDateFormat("HH:mm");
        amwayIndex.setStartTime(sdf.format(amwayGroup.getStartTime()));
        amwayIndex.setStatus(Integer.valueOf(status));
        amwayIndex.setLimitType(amwayGroup.getLimitType());
        amwayIndex.setStartDate3(sdf.format(amwayGroup.getStartTime()));
        return amwayIndex;
    }

    private AmwayIndex getAmwayIndex2(String status, NewAmwayGroup amwayGroup) {
        AmwayIndex amwayIndex = new AmwayIndex();
        amwayIndex.setId(amwayGroup.getId());
        amwayIndex.setImGroupId(amwayGroup.getImGroupId());
        amwayIndex.setName(EmojiParser.parseToUnicode(amwayGroup.getName()));
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm");
        amwayIndex.setStartTime(sdf.format(amwayGroup.getStartTime()));
        amwayIndex.setStatus(Integer.valueOf(status));
        amwayIndex.setStartDate3(sdf.format(amwayGroup.getStartTime()));
        amwayIndex.setLimitType(amwayGroup.getLimitType());
        return amwayIndex;
    }

    /**
     * 什么鬼逻辑
     * 开始的，结束时间肯定是大于当前时间的，当天的条件没有任何意义
     *
     * @param wxAppApiToken 小程序端请求服务器端的凭证
     * @param status        状态 1：待开始 2：进行中 3：已过期
     * @param isToday       0：全部 1：当天 2：非当天
     * @return
     */
    @Override
    public Result liveList(String wxAppApiToken, String status, String allTime, String isToday) {
        //通过token查询用户唯一标识openid
        String openId = JwtUtils.getUserNameFromToken(wxAppApiToken);
        //通过openid查询用户信息
        AmwayUser amwayUser = userService.getUserInCacheByOpenId(openId);
        if (amwayUser == null) {
            return ResultUtils.error(ResultCodeEnum.A0201.value(), ResultCodeEnum.A0201.getName());
        }
        if (amwayUser.getIsAuth() == 0L) {
            return ResultUtils.error(A0301.value(), A0301.getName());
        }
        if (amwayUser.getMemberId() == null) {
            return ResultUtils.error("0", "未绑定memberId");
        }
        Date now = new Date();
        LambdaQueryWrapper<NewAmwayGroup> condition = new LambdaQueryWrapper<NewAmwayGroup>().eq(NewAmwayGroup::getMemberId, amwayUser.getMemberId());
        getCondition(condition, status, isToday, allTime, now);
        //该主播自己创建的直播间
        List<NewAmwayGroup> createGroups = groupMapper.selectList(condition);
        List<Long> createGroupIds = createGroups.stream().map(NewAmwayGroup::getId).collect(Collectors.toList());
        //该主播所有参与和创建的直播间
        List<NewAmwayGroupUser> list = groupUserMapper.selectList(new LambdaQueryWrapper<NewAmwayGroupUser>().eq(NewAmwayGroupUser::getUserId, amwayUser.getId()));
        List<Long> allGroupIds = list.stream().map(NewAmwayGroupUser::getGroupId).collect(Collectors.toList());
        List<Long> joinList = allGroupIds.stream().filter(groupId -> !createGroupIds.contains(groupId)).collect(Collectors.toList());
        List<NewAmwayGroup> joinGroups = new ArrayList<>();
        if (!joinList.isEmpty()) {
            LambdaQueryWrapper<NewAmwayGroup> condition2 = new LambdaQueryWrapper<NewAmwayGroup>().in(NewAmwayGroup::getId, joinList);
            getCondition(condition2, status, isToday, allTime, now);
//            该主播参与的直播间
            joinGroups = groupMapper.selectList(condition2);
        }
        List<AmwayIndex> amwayIndexList = new ArrayList<>();
        List<AmwayIndex> joinGroupList = new ArrayList<>();
        encapsulate(status, createGroups, amwayIndexList);
        encapsulate(status, joinGroups, joinGroupList);
        //畅聊间列表
        AnchorIndexRepose appletsAnchorIndexRepose = new AnchorIndexRepose();
        appletsAnchorIndexRepose.setGroupList(amwayIndexList);
        appletsAnchorIndexRepose.setJoinGroupList(joinGroupList);
        return ResultUtils.success(appletsAnchorIndexRepose);
    }

    private void encapsulate(String status, List<NewAmwayGroup> createGroups, List<AmwayIndex> amwayIndexList) {
        createGroups.forEach(group -> {
            AmwayIndex amwayIndex = getAmwayIndex(status, group);
            amwayIndex.setStartTime2(group.getStartTime());
            int memberNum = groupUserMapper.selectCount(new LambdaQueryWrapper<NewAmwayGroupUser>().eq(NewAmwayGroupUser::getGroupId, group.getId()));
            amwayIndex.setMemberNum(memberNum);
            amwayIndex.setLimitType(group.getLimitType());
            amwayIndexList.add(amwayIndex);
        });
    }

    /**
     * 设置查询条件
     * 如果是直播中，是获取直播结束时间，一周内是取结束时间在12/24 11:38 前的列表；
     * 如果是待开始，是获取直播开始时间，一周内取开始时间在12/24 11:38 前的列表。
     *
     * @param condition
     * @param status
     * @param isToday
     * @param allTime
     * @param now
     */
    static void getCondition(LambdaQueryWrapper<NewAmwayGroup> condition, String status, String isToday, String allTime, Date now) {

        Date timeConditionTime = null;
        if (!IS_ALL.equals(allTime)) {
            Calendar cal = Calendar.getInstance();
            cal.setTime(now);
            if (IS_WEEK.equals(allTime)) {
                cal.add(Calendar.DATE, +7);
            } else if (IS_MOTH.equals(allTime)) {
                cal.add(Calendar.MONTH, +1);
            }
            timeConditionTime = cal.getTime();
        }


        switch (status) {
            //待开始
            case STATUS_TO_BEGIN:
                // 当天未开始的直播
                if (IS_TODAY.equals(isToday)) {
                    condition.between(NewAmwayGroup::getStartTime, now, DateUtils.currentMax(now));
                    // 非当天未开始的直播
                } else if (IS_NOT_TODAY.equals(isToday)) {
                    condition.gt(NewAmwayGroup::getStartTime, DateUtils.currentMax(now));
                } else {
                    condition.ge(NewAmwayGroup::getStartTime, now);
                }

                if (timeConditionTime != null && !IS_TODAY.equals(isToday)) {
                    condition.lt(NewAmwayGroup::getStartTime, timeConditionTime);
                }

                break;
            //进行中
            case STATUS_PROCESSING:
                // 当天开始的直播
                if (IS_TODAY.equals(isToday)) {
                    condition.between(NewAmwayGroup::getStartTime, DateUtils.currentMin(now), now);
                    // 非当天开始的直播
                } else if (IS_NOT_TODAY.equals(isToday)) {
                    condition.lt(NewAmwayGroup::getStartTime, DateUtils.currentMin(now)).ge(NewAmwayGroup::getEndTime, now);
                } else {
                    condition.lt(NewAmwayGroup::getStartTime, now).ge(NewAmwayGroup::getEndTime, now);
                }

                if (timeConditionTime != null) {
                    condition.lt(NewAmwayGroup::getEndTime, timeConditionTime);
                }

                break;
            default:
                break;
        }
        condition.orderByAsc(NewAmwayGroup::getStartTime);
    }

    @Override
    public Result liveShare(String groupId, String path) {
        //二维码文件名
        long currentTime = System.currentTimeMillis();
        String codeName = currentTime + ".jpg";
        //合成图片后的文件名
        String fileName = currentTime + "image.jpg";
        //校验是否存在指定 按?分割
        String[] pathPam = CheckUtils.checkSplit(path, "?", "\\?");
        if (CheckUtils.isEmpty(pathPam)) {
            log.error("畅聊分享live_share-->畅聊间分享页路径不正确path{}", path);
            return ResultUtils.error("0", "畅聊间分享页路径格式不正确");
        }
        NewAmwayGroup amwayGroup = groupMapper.selectById(Long.parseLong(groupId));
        if (CheckUtils.isEmpty(amwayGroup)) {
            log.info("畅聊分享live_share--> 未查询到畅聊间信息，当前畅聊间id=[{}]不存在", groupId);
            return ResultUtils.error("0", "当前畅聊间不存在");
        }

        if (isDebug) {
            AnchorLiveShareRepose anchorLiveShareRepose = new AnchorLiveShareRepose();
            //分享的路径暂时用传参的，后续调整------
            anchorLiveShareRepose.setShareUrl(path + "/" + groupId);
            anchorLiveShareRepose.setShareImg("");
            log.info("分享live_share-->压测分享图片等信息生成成功");
            return ResultUtils.success(anchorLiveShareRepose);
        }

        //微信的接口，获取token
        String accessToken = new AccessTokenUtils().getAccessToken(weChatAppid, weChatSecret, redisUtils);
        if (accessToken == null) {
            log.error("分享live_share-->未获取到所需的token");
            return ResultUtils.error("0", "当前人数过多，请重新获取分享码");
        }
        //获取二维码
        try {
            String shareImg = picImage(accessToken, codeName, fileName, pathPam);
            if (StringUtil.isNotBlank(shareImg)) {
                AnchorLiveShareRepose anchorLiveShareRepose = new AnchorLiveShareRepose();
                //分享的路径暂时用传参的，后续调整------
                anchorLiveShareRepose.setShareUrl(path + "/" + groupId);
                anchorLiveShareRepose.setShareImg(shareImg);
                log.info("分享live_share-->分享图片等信息生成成功");
                return ResultUtils.success(anchorLiveShareRepose);
            }
        } catch (Exception e) {
            log.error("生成海报失败，请重新获取{}", e.getMessage());
            return ResultUtils.error("0", "当前人数过多，请重新获取分享码");
        }
        log.info("畅聊分享live_share-->分享失败");
        return ResultUtils.error("0", "当前人数过多，请重新获取分享码");
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public Result liveCreate(AnchorLiveCreateReq anchorLiveCreateReq, String wxAppApiToken) {
        //通过token查询用户唯一标识openid
        String openId = JwtUtils.getUserNameFromToken(wxAppApiToken);
        //通过openid查询用户信息
        AmwayUser amwayUser = userService.getUserInCacheByOpenId(openId);
        if (amwayUser == null) {
            return ResultUtils.error(ResultCodeEnum.A0201.value(), ResultCodeEnum.A0201.getName());
        }
        if (!isDebug) {
            checkSensitive(anchorLiveCreateReq);
        }
        String imGroupId = anchorLiveCreateReq.getIm_group_id();
        Boolean tryLock = RedisUtil.setNxAndExpireTime("CREATE_GROUP_" + imGroupId, 3);
        if (tryLock) {
            Integer count = groupMapper.selectCount(new LambdaQueryWrapper<NewAmwayGroup>().eq(NewAmwayGroup::getImGroupId, imGroupId));
            if (count > 0) {
                RedisUtil.del("CREATE_GROUP_" + imGroupId);
                return ResultUtils.error("0", "当前畅聊间已存在");
            } else {
                NewAmwayGroup amwayGroup = new NewAmwayGroup();
                amwayGroup.setName(EmojiParser.parseToHtmlDecimal(anchorLiveCreateReq.getName()));
                amwayGroup.setStartTime(DateUtils.getDateHhMm(anchorLiveCreateReq.getStart_time()));
                amwayGroup.setExpire(Integer.parseInt(anchorLiveCreateReq.getExpire()));
                amwayGroup.setLimitType(Integer.parseInt(anchorLiveCreateReq.getLimit_type()));
                amwayGroup.setRemark(EmojiParser.parseToHtmlDecimal(anchorLiveCreateReq.getRemark()));
                //根据openid查询user表获得创建人的畅聊ID
                amwayGroup.setMemberId(amwayUser.getMemberId());
                //------取值待定， 一直在变---------
                //状态  1：待开始  2：进行中  3：已解散
                if (DateUtils.getUntilSecond(new Date(), DateUtils.getDateHhMm(anchorLiveCreateReq.getStart_time())) <= 0) {
                    //立即开播的意思，状态置为2 进行中
                    amwayGroup.setStatusAm(2);
                } else {
                    amwayGroup.setStatusAm(1);
                }
                //目前该字段无实际含义 是否删除 0：否  1：是
                amwayGroup.setIsDelete(0);
                amwayGroup.setCreateTime(new Date());
                amwayGroup.setEndTime();
                amwayGroup.setImGroupId(anchorLiveCreateReq.getIm_group_id());
                int amwayFlag = groupMapper.insert(amwayGroup);
                //新增成功
                if (amwayFlag > 0) {
                    //新增自己的人数
                    NewAmwayGroupUser amwayGroupUser = new NewAmwayGroupUser();
                    amwayGroupUser.setId(0L);
                    amwayGroupUser.setUserId(amwayUser.getId());
                    amwayGroupUser.setCreateTime(new Date());
                    amwayGroupUser.setGroupId(amwayGroup.getId());
                    amwayGroupUser.setShareUserId(0L);
                    groupUserMapper.saveOrUpdate(amwayGroupUser);
                    //自己创建，则畅聊间数量初始化为1
                    AmwayGroupVO vo = new AmwayGroupVO();
                    BeanUtils.copyProperties(amwayGroup, vo);
                    vo.setMemberNum(1);
                    //返回前端实体对象
                    AnchorIndexRepose appletsAnchorIndexRepose = new AnchorIndexRepose();
                    appletsAnchorIndexRepose.setInfoList(vo);
                    RedisUtil.del("CREATE_GROUP_" + imGroupId);
                    return ResultUtils.success(appletsAnchorIndexRepose);
                } else {
                    RedisUtil.del("CREATE_GROUP_" + imGroupId);
                    return ResultUtils.error("0", "新建畅聊间失败");
                }
            }
        } else {
            return ResultUtils.error(A0503.value(), "操作太频繁请稍后重试");
        }
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public Result liveDisband(AnchorLiveDisbandReq anchorLiveDisbandReq) {
        NewAmwayGroup amwayGroup = groupMapper.selectById(Long.parseLong(anchorLiveDisbandReq.getGroup_id()));
        if (amwayGroup == null) {
            return ResultUtils.error("0", "当前畅聊群不存在");
        }
        int resultAmwayGroup = groupMapper.deleteById(amwayGroup.getId());
        if (resultAmwayGroup > 0) {
            log.info("手动解散畅聊间live_disband-->手动解散畅聊群成功，标识id{}", amwayGroup.getId());
            groupUserMapper.delete(new LambdaQueryWrapper<NewAmwayGroupUser>().eq(NewAmwayGroupUser::getGroupId, amwayGroup.getId()));
            return ResultUtils.success("手动解散畅聊群成功");
        } else {
            return ResultUtils.error("0", "手动解散畅聊群成功");
        }
    }

    @Override
    public Result liveSetLimit(String groupId, String limitType) {
        NewAmwayGroup amwayGroup = groupMapper.selectById(Long.parseLong(groupId));
        if (amwayGroup == null) {
            log.info("修改畅聊间转发权限live_setLimit-->当前畅聊间id{}不存在", groupId);
            return ResultUtils.error("0", "当前畅聊间id不存在");
        }
        //根据id更新amway_group表权限limit_type
        amwayGroup.setLimitType(Integer.parseInt(limitType));
        int resultAmwayGroup = groupMapper.updateById(amwayGroup);
        if (resultAmwayGroup > 0) {
            log.info("修改畅聊间转发权限live_setLimit-->修改畅聊间转发权限成功id{}", amwayGroup.getId());
            AnchorLiveSetLimitRepose anchorLiveSetLimitRepose = new AnchorLiveSetLimitRepose();
            anchorLiveSetLimitRepose.setLimit_type(amwayGroup.getLimitType());
            //有病
            return ResultUtils.success(anchorLiveSetLimitRepose);
        } else {
            return ResultUtils.error("0", "操作失败");
        }
    }

    @Override
    public Result getActivityId() {
        //微信的接口
        String accessToken = new AccessTokenUtils().getAccessToken(weChatAppid, weChatSecret, redisUtils);
        if (accessToken == null) {
            log.error("获取分享需要的getActivity_id-->未获取到所需的token");
            return ResultUtils.error("0", "获取分享需要的activity_id失败");
        }
        String url = WXApiConstant.CREATE_ACTIVITY + accessToken;
        RestTemplate restTemplate = new RestTemplate();
        //进行网络请求
        ResponseEntity<String> responseEntity = restTemplate.exchange(url, HttpMethod.GET, null, String.class);
        if (responseEntity != null && responseEntity.getStatusCode() == HttpStatus.OK) {
            String ActivityData = responseEntity.getBody();
            log.info("获取分享需要的activity_id-->返回的ActivityData：" + ActivityData);
            JSONObject retJson = JSONObject.parseObject(ActivityData);
            Object activityId = retJson.get("activity_id");
            AnchorActivityIdRepose AnchorActivityIdRepose = new AnchorActivityIdRepose();
            if (activityId != null) {
                AnchorActivityIdRepose.setActivityId(activityId.toString());
            }
            return ResultUtils.success(AnchorActivityIdRepose);
        }
        log.info("获取分享需要的activity_id-->获取分享需要的activity_id失败");
        return ResultUtils.error("0", "当前人数过多，请重试");
    }

    @Override
    public Result liveCountLimit(String wxAppApiToken) {
        //通过token查询用户唯一标识openid
        String openId = JwtUtils.getUserNameFromToken(wxAppApiToken);
        //通过openid查询用户信息
        AmwayUser amwayUser = userService.getUserInCacheByOpenId(openId);
        if (amwayUser == null) {
            return ResultUtils.error(ResultCodeEnum.A0201.value(), ResultCodeEnum.A0201.getName());
        }
        //获取AmwaySetting数量
        AmwaySetting amwaySetting = settingService.getOne();
        if (amwaySetting == null) {
            return ResultUtils.error("0", "系统畅聊场数未设置");
        }
        //根据openid查询user表获得创建人的畅聊ID //进行中的
        Date now = new Date();
        int dayGroupNum = groupMapper.selectCount(new LambdaQueryWrapper<NewAmwayGroup>().eq(NewAmwayGroup::getMemberId, amwayUser.getMemberId())
                .ge(NewAmwayGroup::getEndTime, now).le(NewAmwayGroup::getStartTime, now));
        //若每人每天最多开多少场畅聊数小于 当日主播创建的畅聊
        if (dayGroupNum >= amwaySetting.getMaxGroupPerDay()) {
            return ResultUtils.error("0", "您今日创建畅聊间数量已达上限，先解散一些进行中的畅聊");
        }
        //若每人最多开启多少场畅聊 小于 主播创建的畅聊
        int total = groupMapper.selectCount(new LambdaQueryWrapper<NewAmwayGroup>().eq(NewAmwayGroup::getMemberId, amwayUser.getMemberId()));
        int expire = groupMapper.selectCount(new LambdaQueryWrapper<NewAmwayGroup>().eq(NewAmwayGroup::getMemberId, amwayUser.getMemberId())
                .le(NewAmwayGroup::getEndTime, now));
        if (total - expire >= amwaySetting.getMaxGroupTotal()) {
            return ResultUtils.error("0", "您总创建畅聊间数量已达上限，先解散一些进行中的畅聊");
        }
        return ResultUtils.success("畅聊间数量限制成功");
    }

    @Override
    public Result msgCheck(String wxAppApiToken, String words) {

        if (isDebug) {
            return ResultUtils.success("畅聊间消息敏感词过滤校验成功");
        }

        String accessToken = new AccessTokenUtils().getAccessToken(weChatAppid, weChatSecret, redisUtils);
        PrintWriter printWriter = null;
        BufferedInputStream bufferedInputStream = null;
        if (accessToken == null) {
            log.error("校验敏感词msgCheck-->未获取到所需的token");
            return ResultUtils.error(ResultCodeEnum.A0431.value(), ResultCodeEnum.A0431.getName());
        }
        try {
            URL url = new URL(WXApiConstant.MSG_SEC_CHECK + accessToken);
            HttpURLConnection httpURLConnection = (HttpURLConnection) url.openConnection();
            httpURLConnection.setRequestMethod("POST");
            // 发送POST请求必须设置如下两行
            httpURLConnection.setDoOutput(true);
            httpURLConnection.setDoInput(true);
            // 获取URLConnection对象对应的输出流
            printWriter = new PrintWriter(httpURLConnection.getOutputStream());
            // 发送请求参数
            JSONObject paramJson = new JSONObject();
            //对请求参数进行处理，请求最终格式xx:xx,xx:xx
            paramJson.put("content", words);
            printWriter.write(paramJson.toString());
            // flush输出流的缓冲
            printWriter.flush();
            //开始获取数据
            bufferedInputStream = new BufferedInputStream(httpURLConnection.getInputStream());
            int len = 0;
            byte[] arr = new byte[1024];
            String retData = null;
            while ((len = bufferedInputStream.read(arr)) != -1) {
                retData = new String(arr, 0, len);
            }

            JSONObject retJson = JSONObject.parseObject(retData);
            Object errCode = retJson.get("errcode");
            if (!"0".equals(errCode.toString())) {
                log.info("校验敏感词msgCheck-->{}:", retData);
                return ResultUtils.error(ResultCodeEnum.A0431.value(), ResultCodeEnum.A0431.getName());
            }
        } catch (Exception e) {
            log.error("敏感词获取失败：{}", e.getMessage());
            return ResultUtils.error(ResultCodeEnum.A0431.value(), ResultCodeEnum.A0431.getName());
        } finally {
            printWriter.close();
            try {
                bufferedInputStream.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return ResultUtils.success("畅聊间消息敏感词过滤校验成功");
    }

    @Override
    public Result pageSetting(String wxAppApiToken) {
        AmwaySetting amwaySetting = settingService.getOne();
        if (amwaySetting == null) {
            return ResultUtils.error("0", "系统管理员参数未设置");
        }
        pageSettingRepose pageSettingRepose = new pageSettingRepose();
        pageSettingRepose.setMaxAdminNum(amwaySetting.getMaxAdminNun());
        pageSettingRepose.setOpenMsgCheck(amwaySetting.getOpenMsgCheck());
        pageSettingRepose.setOpenVideoUpload(amwaySetting.getOpenVideoUpload());
        pageSettingRepose.setVideoText(amwaySetting.getVideoText());
        return ResultUtils.success(pageSettingRepose);
    }

    public List<String> queryGroupAvatars(String groupId) {
        //返回的头像list
        List<String> avatarUrlList = new ArrayList<>();
        List<NewAmwayGroupUser> list = groupUserMapper.selectList(new LambdaQueryWrapper<NewAmwayGroupUser>().eq(NewAmwayGroupUser::getGroupId, Long.valueOf(groupId)).last("limit 0, 3"));
        if (!list.isEmpty()) {
            //根据group_user的user_id去匹配user表的id （最多取3个，结构：['', '', '']）
            List<AmwayUser> amwayUserList = userMapper.selectList(new LambdaQueryWrapper<AmwayUser>().in(com.amway.broadcast.applets.service.dao.entity.AmwayUser::getId, list.stream().map(NewAmwayGroupUser::getUserId).collect(Collectors.toList())));
            for (AmwayUser amwayUsers : amwayUserList) {
                avatarUrlList.add(amwayUsers.getAvatarUrl());
            }
        }
        return avatarUrlList;
    }

    public void getShareImage(String accessToken, String codeName, String fileName, String[] pathPam) {
        Map<String, String> pathMap = checkImage(codeName, fileName);
        PrintWriter printWriter = null;
        OutputStream outputStream = null;
        BufferedInputStream bufferedInputStream = null;

        try {
            URL url = new URL(WXApiConstant.CODE_UN_LIMIT + accessToken);
            HttpURLConnection httpURLConnection = (HttpURLConnection) url.openConnection();
            httpURLConnection.setRequestMethod("POST");
            // 发送POST请求必须设置如下两行
            httpURLConnection.setDoOutput(true);
            httpURLConnection.setDoInput(true);
            // 获取URLConnection对象对应的输出流
            printWriter = new PrintWriter(httpURLConnection.getOutputStream());
            // 发送请求参数
            JSONObject paramJson = new JSONObject();
            //对请求参数进行处理，请求最终格式xx:xx,xx:xx
            paramJson.put("scene", CheckUtils.getJsonValue(pathPam[1]));
            paramJson.put("page", CheckUtils.checkStr(pathPam[0], 1, "/"));
            paramJson.put("width", "280px");
            paramJson.put("is_hyaline", true);
            printWriter.write(paramJson.toString());
            // flush输出流的缓冲
            printWriter.flush();
            //开始获取数据
            bufferedInputStream = new BufferedInputStream(httpURLConnection.getInputStream());
            outputStream = new FileOutputStream(pathMap.get("codePath"));
            int len = 0;
            byte[] arr = new byte[1024];
            while ((len = bufferedInputStream.read(arr)) != -1) {
                outputStream.write(arr, 0, len);
                outputStream.flush();
            }

        } catch (Exception e) {
            log.error("获取分享二维码失败:" + e.getMessage());
        } finally {
            printWriter.close();
            try {
                outputStream.close();
                bufferedInputStream.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * 压缩二维码图片，合成二维码图片，并返回base64流
     *
     * @return
     */
    public String picImage(String accessToken, String codeName, String fileName, String[] pathPam) {
        //调用微信小程序api接口获取二维码，保存本地
        getShareImage(accessToken, codeName, fileName, pathPam);

        Map<String, String> pathMap = checkImage(codeName, fileName);
        //添加随机数
        int randomNum = (int) (1 + Math.random() * 10);
        //背景图片保存路径
        String backgroup = ImageUtils.createImage(BACKGROUND_URL + "?" + randomNum, SAVE_IMAGE_PATH + BACKGROUND_NAME);
        //二维码压缩
        ImageUtil.reduceImgCode(pathMap.get("codePath"), pathMap.get("codePath"));
        ImageUtil.drawString("\\", pathMap.get("imagePath"), backgroup, pathMap.get("codePath"));
        //图片压缩
        //ImageUtil.reduceImg(pathMap.get("imagePath"), pathMap.get("imagePath"));
        String base64Image = ImageUtil.readImage(pathMap.get("imagePath"));
        //本地临时文件删除
        if (FilesUtils.deleteFile(pathMap.get("imagePath"))) log.info("本地生成的临时分享图片删除成功");
        if (FilesUtils.deleteFile(pathMap.get("codePath"))) log.info("本地生成的临时code分享图片删除成功");
        return base64Image;
    }

    private Map<String, String> checkImage(String codeName, String fileName) {
        Map<String, String> pathMap = new HashMap<String, String>();
        //二维码文件
        File codeFile = new File(SAVE_IMAGE_PATH);
        if (!codeFile.exists()) {
            codeFile.mkdirs();
        }
        String codePath = codeFile.getPath() + File.separator + codeName;
        codePath = CheckUtils.checkUrl(codePath, "\\", "/");

        String imagePath = codeFile.getPath() + File.separator + fileName;
        imagePath = CheckUtils.checkUrl(imagePath, "\\", "/");

        pathMap.put("codePath", codePath);
        pathMap.put("imagePath", imagePath);
        return pathMap;
    }

    private void checkSensitive(AnchorLiveCreateReq anchorLiveCreateReq) {
        if (CheckUtils.isEmpty(anchorLiveCreateReq)) {
            log.info("校验敏感词-->入参对象为空");
            throw new ContainsContrabandSensitiveWordsException("包含违禁敏感词");
        }
        String accessToken = new AccessTokenUtils().getAccessToken(weChatAppid, weChatSecret, redisUtils);
        if (accessToken == null) {
            log.error("校验敏感词-->未获取到所需的token");
            throw new ContainsContrabandSensitiveWordsException("包含违禁敏感词");
        }
        PrintWriter printWriter = null;
        BufferedInputStream bufferedInputStream = null;
        try {
            URL url = new URL(WXApiConstant.MSG_SEC_CHECK + accessToken);
            HttpURLConnection httpURLConnection = (HttpURLConnection) url.openConnection();
            httpURLConnection.setRequestMethod("POST");
            // 发送POST请求必须设置如下两行
            httpURLConnection.setDoOutput(true);
            httpURLConnection.setDoInput(true);
            // 获取URLConnection对象对应的输出流
            printWriter = new PrintWriter(httpURLConnection.getOutputStream());
            // 发送请求参数
            JSONObject paramJson = new JSONObject();
            //对请求参数进行处理，请求最终格式xx:xx,xx:xx
            paramJson.put("content", anchorLiveCreateReq.getName() + anchorLiveCreateReq.getRemark());
            printWriter.write(paramJson.toString());
            // flush输出流的缓冲
            printWriter.flush();
            //开始获取数据
            bufferedInputStream = new BufferedInputStream(httpURLConnection.getInputStream());
            int len = 0;
            byte[] arr = new byte[1024];
            String retData = null;
            while ((len = bufferedInputStream.read(arr)) != -1) {
                retData = new String(arr, 0, len);
            }
            printWriter.close();
            bufferedInputStream.close();

            JSONObject retJson = JSONObject.parseObject(retData);
            Object errCode = retJson.get("errcode");
            if (!"0".equals(errCode.toString())) {
                log.info("校验敏感词msgCheck-->{}:", retData);
                throw new ContainsContrabandSensitiveWordsException("包含违禁敏感词");
            }
        } catch (Exception e) {
            log.error("敏感词获取失败：{}", e.getMessage());
            throw new ContainsContrabandSensitiveWordsException("包含违禁敏感词");
        } finally {
            if (printWriter != null) {
                printWriter.close();
            }
            try {
                if (bufferedInputStream != null) {
                    bufferedInputStream.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * 判断直播的状态是未开始1 还是进行中2
     * 当前时间小于开始时间 1
     * 当前时间在开始时间及失效期以内 2
     *
     * @param startTime
     * @param endTime
     * @return
     */
    public static Integer judgeStatus(Date startTime, Date endTime) {
        if (DateUtils.getUntilSecond(new Date(), startTime) >= 0) {
            return 1;
        } else {
            if (DateUtils.getUntilSecond(new Date(), endTime) >= 0) {
                return 2;
            } else {
                return 3;
            }
        }
    }

    public static void main(String[] args) throws ParseException {
        SimpleDateFormat sdf = new SimpleDateFormat("HH:mm");
        System.out.println(sdf.format(new Date()));
        Date date = DateUtils.currentMin(new Date());
        System.out.println(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(date));
        //测试进行中
        Integer integer = judgeStatus(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse("2020-12-10 00:00:00"), new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse("2020-12-28 00:00:00"));
        System.out.println(integer); //预测 2
        //测试未开始
        Integer integer2 = judgeStatus(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse("2020-12-19 00:00:00"), new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse("2020-12-28 00:00:00"));
        System.out.println(integer2); //预测 1
        //测试已结束
        Integer integer3 = judgeStatus(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse("2020-12-10 00:00:00"), new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse("2020-12-15 00:00:00"));
        System.out.println(integer3); //预测 3
    }
}
