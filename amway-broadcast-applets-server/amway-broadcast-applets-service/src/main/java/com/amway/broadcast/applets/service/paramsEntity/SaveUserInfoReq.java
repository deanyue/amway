package com.amway.broadcast.applets.service.paramsEntity;

import lombok.Data;

import javax.validation.constraints.NotBlank;

/**
 *  用户授权用户信息 (服务端保存用户信息)  请求对象
 */
@Data
public class SaveUserInfoReq {

    /**
     *  不包括敏感信息的原始数据字符串，用于计算签名
     */
    @NotBlank(message = "rawData不能为空")
    private String rawData;

    /**
     * 使用 sha1( rawData + sessionkey ) 得到字符串，用于校验用户信息
     */
    @NotBlank(message = "signature不能为空")
    private String signature;

    /**
     * 包括敏感数据在内的完整用户信息的加密数据
     */
    @NotBlank(message = "encryptedData不能为空")
    private String encryptedData;

    /**
     * 加密算法的初始向量
     */
    @NotBlank(message = "iv不能为空")
    private String iv;
}
