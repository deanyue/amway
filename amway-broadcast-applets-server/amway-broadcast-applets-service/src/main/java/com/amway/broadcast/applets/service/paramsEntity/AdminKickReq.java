package com.amway.broadcast.applets.service.paramsEntity;

import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;

/**
 * 管理员踢人（包括主播踢人） req对象
 */
@Data
public class AdminKickReq {

    /**
     * 账号
     */
   @NotBlank(message = "直播间id不能为空")
   private String group_id;
    /**
     * 密码
     */
   @NotBlank(message = "用户id不能为空")
   private String user_id;

}
