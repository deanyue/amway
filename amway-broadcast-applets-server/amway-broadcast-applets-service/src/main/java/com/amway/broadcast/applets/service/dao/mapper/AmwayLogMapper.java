package com.amway.broadcast.applets.service.dao.mapper;

import com.amway.broadcast.applets.service.dao.entity.AmwayLog;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

public interface AmwayLogMapper extends BaseMapper<AmwayLog> {
}
