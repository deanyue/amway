package com.amway.broadcast.applets.service.dao.entity;

import lombok.Data;

/**
 * @author WCX
 * @date 2020/11/3
 */
@Data
public class WeChatSession {
    public String openid;
    public String session_key;
}
