package com.amway.broadcast.applets.service.api.service.impl;

import com.amway.broadcast.applets.service.api.service.AmwayUserEntryService;
import com.amway.broadcast.applets.service.api.service.UserCacheService;
import com.amway.broadcast.applets.service.common.constant.AdminOperationTypeEnum;
import com.amway.broadcast.applets.service.dao.entity.*;
import com.amway.broadcast.applets.service.dao.mapper.AmwayGroupMapper;
import com.amway.broadcast.applets.service.dao.mapper.AmwayGroupUserLogMapper;
import com.amway.broadcast.applets.service.dao.mapper.AmwayGroupUserMapper;
import com.amway.broadcast.applets.service.dao.mapper.AmwayGroupUserPremiereMapper;
import com.amway.broadcast.applets.service.paramsEntity.AnchorIndexRepose;
import com.amway.broadcast.applets.service.paramsEntity.SearchUserRepose;
import com.amway.broadcast.applets.service.paramsEntity.UserEntryReq;
import com.amway.broadcast.applets.service.paramsEntity.UserQuitReq;
import com.amway.broadcast.applets.service.service.AmwayGroupService;
import com.amway.broadcast.applets.service.service.AmwayGroupUserService;
import com.amway.broadcast.common.base.Result;
import com.amway.broadcast.common.base.ResultUtils;
import com.amway.broadcast.common.enumeration.ResultCodeEnum;
import com.amway.broadcast.common.util.JwtUtils;
import com.amway.broadcast.common.util.RedisUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.vdurmont.emoji.EmojiParser;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import static com.amway.broadcast.common.enumeration.ResultCodeEnum.A0503;

/**
 * @auther 薛晨
 * @date 2020/12/14
 * @time 10:53
 * @description
 */
@Service
@Slf4j
public class NewAmwayUserEntryServiceImpl implements AmwayUserEntryService {
    @Autowired
    private AmwayGroupService groupService;
    @Autowired
    private AmwayGroupMapper groupMapper;
    @Autowired
    private AmwayGroupUserService groupUserService;
    @Autowired
    private AmwayGroupUserMapper groupUserMapper;
    @Autowired
    private UserCacheService userService;
    @Autowired
    private AmwayGroupUserLogMapper amwayGroupUserLogMapper;
    @Autowired
    private AmwayGroupUserPremiereMapper amwayGroupUserPremiereMapper;

    private static final int USER_IS_ADMIN = 1;

    private static final int USER_IS_NOT_ADMIN = 0;

    /**
     * 用户进入直播间标识位，1代表待进入
     */
    private static final String ENTRY_TYPE = "1";

    /**
     * 直播间状态
     * 1：待开始 2：进行中 3：已解散
     */
    private static final String STATUS_TO_BEGIN = "1";
    private static final String STATUS_PROCESSING = "2";
    private static final String STATUS_DISSOLVED = "3";

    private static final String IS_TODAY = "1";
    private static final String IS_NOT_TODAY = "2";

    /**
     * all_time:0 1 2(所有 一周 一个月)
     */
    private static final String IS_ALL = "0";
    private static final String IS_WEEK = "1";
    private static final String IS_MOTH = "2";

    /**
     * 前端会判定好是否为开播提醒的状态的页面（通过前端传入的entryType是否为1）（不计直播是否开始）
     * 开播提醒不计入人数，所以不计入用户关联日志表中
     * 业务上说明：
     * 1。若是开播提醒状态就会，先存入开播提醒的表中
     * 2。开播提醒的用户，进入直播的时候，前端会再次调用进入直播的接口，
     * 保存到直播用户关联表中，保存到日志表中
     *
     * @param userEntryReq
     * @param wxAppApiToken
     * @return
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public Result liveJoin(UserEntryReq userEntryReq, String wxAppApiToken) {
        NewAmwayGroup amwayGroup = groupMapper.selectById(Long.valueOf(userEntryReq.getGroup_id()));
        if (amwayGroup == null) {
            return ResultUtils.error("0", "当前畅聊间id不存在");
        }
        if (NewAppletsAnchorServiceImpl.judgeStatus(amwayGroup.getStartTime(), amwayGroup.getEndTime()) == 3) {
            return ResultUtils.error("0", "当前畅聊间已解散");
        }
        //通过token获取openid
        String openId = JwtUtils.getUserNameFromToken(wxAppApiToken);
        AmwayUser user = userService.getUserInCacheByOpenId(openId);
        String entryType = userEntryReq.getEntry_type();
        if (user == null) {
            return ResultUtils.error(ResultCodeEnum.A0201.value(), ResultCodeEnum.A0201.getName());
        }
        Boolean tryLock = RedisUtil.setNxAndExpireTime("JOIN_GROUP_" + userEntryReq.getGroup_id() + "_" + openId, 3);
        if (tryLock) {

            // 创建开播提醒记录
            if (ENTRY_TYPE.equals(entryType)) {
                AmwayGroupUserPremiere amwayGroupUserLog = new AmwayGroupUserPremiere();
                setAmwayGroupUserLog(amwayGroupUserLog, userEntryReq, amwayGroup, user);
                amwayGroupUserPremiereMapper.saveOrUpdate(amwayGroupUserLog);
//                RedisUtil.del("JOIN_GROUP_" + userEntryReq.getGroup_id() + "_" + openId);
//                return ResultUtils.success("进入畅聊间成功");
            }

//        List<NewAmwayGroupUser> list = groupUserMapper.selectList(new LambdaQueryWrapper<NewAmwayGroupUser>().eq(NewAmwayGroupUser::getGroupId, Long.valueOf(userEntryReq.getGroup_id())).eq(NewAmwayGroupUser::getUserId, user.getId()));
//        if (list.isEmpty()) {

            // 真正进入直播
            NewAmwayGroupUser amwayGroupUser = new NewAmwayGroupUser();
            amwayGroupUser.setUserId(user.getId());
            amwayGroupUser.setGroupId(Long.valueOf(userEntryReq.getGroup_id()));
            amwayGroupUser.setCreateTime(new Date());
            amwayGroupUser.setShareUserId(Long.valueOf(userEntryReq.getShare_user_id()));
            int count = groupUserMapper.saveOrUpdate(amwayGroupUser);
            log.info("中间表{}:{}",amwayGroupUser,count);
            // 记录日志
            AmwayGroupUserLog amwayGroupUserLog = new AmwayGroupUserLog();
            setAmwayGroupUserLog(amwayGroupUserLog, userEntryReq, amwayGroup, user);
            int count2 = amwayGroupUserLogMapper.saveOrUpdate(amwayGroupUserLog);
            log.info("日志表{}:{}",amwayGroupUserLog,count2);
//      }
            RedisUtil.del("JOIN_GROUP_" + userEntryReq.getGroup_id() + "_" + openId);
            return ResultUtils.success("进入畅聊间成功");
        } else {
            return ResultUtils.error(A0503.value(), "您的操作频率太频繁，请一分钟之后再试");
        }
    }

    private void setAmwayGroupUserLog(AmwayGroupUserLog amwayGroupUserLog, UserEntryReq userEntryReq, NewAmwayGroup amwayGroup, AmwayUser user) {

        amwayGroupUserLog.setUserId(user.getId());
        amwayGroupUserLog.setGroupId(Long.valueOf(userEntryReq.getGroup_id()));
        amwayGroupUserLog.setGroupInfoStr(amwayGroup);
        amwayGroupUserLog.setCreateTime(new Date());
        amwayGroupUserLog.setShareUserId(Long.valueOf(userEntryReq.getShare_user_id()));
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public Result liveQuit(UserQuitReq userQuitReq, String wxAppApiToken) {
        NewAmwayGroup newAmwayGroup = groupMapper.selectById(Long.valueOf(userQuitReq.getGroup_id()));
        if (newAmwayGroup == null) {
            return ResultUtils.error("0", "当前畅聊间id不存在");
        }
        String openId = JwtUtils.getUserNameFromToken(wxAppApiToken);
        AmwayUser amwayUser = userService.getUserInCacheByOpenId(openId);
        if (amwayUser == null) {
            return ResultUtils.error(ResultCodeEnum.A0201.value(), ResultCodeEnum.A0201.getName());
        }
        LambdaQueryWrapper<NewAmwayGroupUser> condition = new LambdaQueryWrapper<NewAmwayGroupUser>().eq(NewAmwayGroupUser::getGroupId, Long.valueOf(userQuitReq.getGroup_id())).eq(NewAmwayGroupUser::getUserId, amwayUser.getId());
        List<NewAmwayGroupUser> list = groupUserMapper.selectList(condition);
        if (list.isEmpty()) {
            return ResultUtils.error("0", "当前用户未进入该畅聊间,无法退出");
        } else {
            int count = groupUserMapper.delete(condition);
            if (count > 0) {
                return ResultUtils.success("退出畅聊间成功");
            } else {
                return ResultUtils.error("0", "退出畅聊间失败，请重试");
            }
        }
    }

    @Override
    public Result userLiveList(String wxAppApiToken, String status,String allTime, String isToday) {
        //通过token查询用户唯一标识openid
        String openId = JwtUtils.getUserNameFromToken(wxAppApiToken);
        //通过openid查询用户信息
        AmwayUser amwayUser = userService.getUserInCacheByOpenId(openId);
        if (amwayUser == null) {
            return ResultUtils.error(ResultCodeEnum.A0201.value(), ResultCodeEnum.A0201.getName());
        }
        //装载畅聊间列表信息list，返回给前端
        List<AmwayIndex> amwayIndexList = new ArrayList<>();
        List<NewAmwayGroupUser> list = groupUserMapper.selectList(new LambdaQueryWrapper<NewAmwayGroupUser>().eq(NewAmwayGroupUser::getUserId, amwayUser.getId()));
        if (!list.isEmpty()) {
            List<Long> groupIdList = list.stream().map(NewAmwayGroupUser::getGroupId).collect(Collectors.toList());
            LambdaQueryWrapper<NewAmwayGroup> condition = new LambdaQueryWrapper<NewAmwayGroup>().in(NewAmwayGroup::getId, groupIdList);
            Date now = new Date();
            NewAppletsAnchorServiceImpl.getCondition(condition, status, isToday, allTime, now);
            List<NewAmwayGroup> allGroups = groupMapper.selectList(condition);
            SimpleDateFormat sdf = new SimpleDateFormat("HH:mm");
            allGroups.forEach(group -> {
                AmwayIndex amwayIndex = new AmwayIndex();
                amwayIndex.setId(group.getId());
                amwayIndex.setImGroupId(group.getImGroupId());
                amwayIndex.setName(EmojiParser.parseToUnicode(group.getName()));
                amwayIndex.setStartTime(sdf.format(group.getStartTime()));
                amwayIndex.setStatus(Integer.valueOf(status));
                amwayIndex.setStartTime2(group.getStartTime());
                int memberNum = groupUserMapper.selectCount(new LambdaQueryWrapper<NewAmwayGroupUser>().eq(NewAmwayGroupUser::getGroupId, group.getId()));
                amwayIndex.setMemberNum(memberNum);
                amwayIndexList.add(amwayIndex);
            });
            //畅聊间列表
            AnchorIndexRepose appletsAnchorIndexRepose = new AnchorIndexRepose();
            appletsAnchorIndexRepose.setGroupList(amwayIndexList);
            return ResultUtils.success(appletsAnchorIndexRepose);
        } else {
            return ResultUtils.success(new AnchorIndexRepose());
        }
    }

    /**
     * 直播群搜索用户
     * @param wxAppApiToken
     * @param nickName
     * @return
     */
    @Override
    public Result searchUser(String wxAppApiToken, String nickName) {
        SearchUserRepose searchUserRepose = new SearchUserRepose();
        List<String> userIdList = new ArrayList<>();
        List<AmwayUser> amwayUserList = userService.getUserByNickName(nickName);

        log.info("直播群搜索用户查询id条数{}", amwayUserList.size());
        for (AmwayUser userids : amwayUserList){
            userIdList.add(userids.getId().toString());
        }
        searchUserRepose.setUserIds(userIdList);
        return ResultUtils.success(searchUserRepose);
    }
}
