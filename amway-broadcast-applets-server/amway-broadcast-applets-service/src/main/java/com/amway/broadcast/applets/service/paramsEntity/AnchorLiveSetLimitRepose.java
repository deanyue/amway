package com.amway.broadcast.applets.service.paramsEntity;

import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;

/**
 * 修改直播间转发权限 返回参数
 */
@Data
public class AnchorLiveSetLimitRepose {


    /**
     * 收听权限 1：转发可收听 2：转发不可收听
     */
    private Integer limit_type;
}
