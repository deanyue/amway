package com.amway.broadcast.applets.service.service;


import com.amway.broadcast.applets.service.dao.entity.AmwayDepartmentMember;
import com.amway.broadcast.applets.service.dao.entity.AmwayUser;
import com.amway.broadcast.common.service.BaseService;


/**
 * @author Dean
 */
public interface AmwayDepartmentMemberService extends BaseService<AmwayDepartmentMember> {

}