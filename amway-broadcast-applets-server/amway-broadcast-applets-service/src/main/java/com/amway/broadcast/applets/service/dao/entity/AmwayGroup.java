//package com.amway.broadcast.applets.service.dao.entity;
//
//import com.amway.broadcast.common.base.BaseAmwayGroup;
//import com.baomidou.mybatisplus.annotation.IdType;
//import com.baomidou.mybatisplus.annotation.TableField;
//import com.baomidou.mybatisplus.annotation.TableId;
//import com.baomidou.mybatisplus.annotation.TableName;
//import com.fasterxml.jackson.annotation.JsonFormat;
//import com.fasterxml.jackson.annotation.JsonIgnore;
//import com.fasterxml.jackson.annotation.JsonInclude;
//import com.fasterxml.jackson.annotation.JsonProperty;
//import lombok.Getter;
//import lombok.Setter;
//
//import java.io.Serializable;
//import java.util.Date;
//import java.util.List;
//
///**
// *  直播间列表
// */
//@Getter
//@Setter
//@TableName(value = "amway_group")
//public class AmwayGroup extends BaseAmwayGroup implements Serializable {
//
//    private static final long serialVersionUID = -5245396336670127078L;
//
//    @TableId(value = "id", type= IdType.AUTO)
//    private Long id;
//
//    //创建直播人ID号
//    @JsonProperty("member_id")
//    @TableField(value = "member_id")
//    private Long memberId;
//
//    //直播群ID号
//    @JsonProperty("im_group_id")
//    @TableField(value = "im_group_id")
//    private String imGroupId;
//
//    //直播间名称
//    @TableField(value = "name")
//    private String name;
//
//    //直播开始时间
//    @JsonProperty("start_time")
//    @TableField(value = "start_time")
//    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone="GMT+8")
//    private Date startTime;
//
//    //有效时长，单位：天
//    @TableField(value = "expire")
//    private Integer expire;
//
//    //权限  1：转发可收听  2：转发不可收听
//    @JsonProperty("limit_type")
//    @TableField(value = "limit_type")
//    private Integer limitType;
//
//    //简介
//    @TableField(value = "remark")
//    private String remark;
//
//    //状态   1：待开始  2：进行中  3：已过期
//    @JsonProperty("status")
//    @TableField(value = "status_am")
//    private Integer statusAm;
//
//    //是否解散 0：否  1：是
//    @JsonIgnore
//    private Integer isDelete;
//
//    @JsonIgnore
//    @TableField(value = "create_time")
//    private Date createTime;
//
//    // 表中待新增字段，或联查字段
//    //创建直播人头像
//    @TableField(exist=false)
//    @JsonProperty("member_avatar")
//    private String memberAvatar;
//
//    //创建直播人昵称
//    @TableField(exist=false)
//    @JsonProperty("member_nickname")
//    private String memberNickname;
//
//    /**
//     * 直播间首页接口返回 直播开始时间-时间戳
//     */
//    @TableField(exist=false)
//    @JsonProperty("start_time_timestamp")
//    private Long startTimeTimestamp;
//
//    /**
//     * 直播间首页接口返回 直播间头像列表（最多取3个，结构：['', '', '']）
//     */
//    @TableField(exist=false)
//    @JsonProperty("group_avatars")
//    private List<?> groupAvatars;
//
//    /**
//     * 直播间首页接口返回  直播间人数
//     */
//    @TableField(exist=false)
//    @JsonProperty("member_num")
//    private Integer memberNum;
//
//    /**
//     * 是否允许管理员设置全体禁言(0为否,1为是,默认为1)
//     */
//    @TableField(value = "is_all_mute")
//    private String isAllMute;
//
//    /**
//     * 是否允许管理员踢走成员(0为否,1为是,默认为1)
//     */
//    @TableField(value = "is_kick")
//    private String isKick;
//
//    /**
//     * 直播间管理员人数
//     */
//    @TableField(exist=false)
//    @JsonProperty("manager_num")
//    private Integer managerNum;
//}