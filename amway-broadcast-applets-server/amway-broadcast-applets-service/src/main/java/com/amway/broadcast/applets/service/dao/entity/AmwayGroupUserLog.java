package com.amway.broadcast.applets.service.dao.entity;

import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;

import java.util.Date;

/**
 * 直播群-用户 关系表（用户加入直播间记录）
 */
@Getter
@Setter
@Slf4j
@TableName(value = "amway_group_user_log")
public class AmwayGroupUserLog {

    private static final long serialVersionUID = 7883992849173226701L;

    @TableId(value = "id")
    private Long id;

    //直播群ID号
    @TableField(value = "group_id")
    private Long groupId;

    //用户id号
    @TableField(value = "user_id")
    private Long userId;

    //分享者id号
    @TableField(value = "share_user_id")
    private Long shareUserId;

    //创建时间
    @TableField(value = "create_time")
    private Date createTime;

    /**
     * 直播间json字符串
     */
    @TableField(value = "group_info_str")
    private String groupInfoStr;

    public NewAmwayGroup getAmwayGroup() {
        NewAmwayGroup amwayGroup = null;
        try {
            amwayGroup = JSONObject.parseObject(groupInfoStr,NewAmwayGroup.class);
        } catch (Exception e) {
            log.error("直播间json字符串，转换异常",e);
        }
        return amwayGroup;
    }

    public void setGroupInfoStr(NewAmwayGroup groupInfoStr) {
        this.groupInfoStr = JSONObject.toJSONString(groupInfoStr);
    }

    @Override
    public String toString() {
        return "AmwayGroupUserLog{" +
                "id=" + id +
                ", groupId=" + groupId +
                ", userId=" + userId +
                ", shareUserId=" + shareUserId +
                ", createTime=" + createTime +
                ", groupInfoStr='" + groupInfoStr + '\'' +
                '}';
    }
}
