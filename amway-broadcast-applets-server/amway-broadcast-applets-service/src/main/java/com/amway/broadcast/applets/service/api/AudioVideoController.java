package com.amway.broadcast.applets.service.api;

import com.amway.broadcast.applets.service.api.service.UserCacheService;
import com.amway.broadcast.applets.service.common.annotation.ServiceTokenRequired;
import com.amway.broadcast.applets.service.entity.vo.AuditResultVO;
import com.amway.broadcast.applets.service.service.AudioVideoService;
import com.amway.broadcast.common.base.Result;
import com.amway.broadcast.common.base.ResultUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

/**
 * @auther 薛晨
 * @date 2020/12/16
 * @time 10:27
 * @description
 */
@RestController
@RequestMapping
@Validated
@Slf4j
public class AudioVideoController {
    @Autowired
    private AudioVideoService audioVideoService;
    @Autowired
    private UserCacheService userCacheService;

    /**
     * 一句话识别 语音转中文
     *
     * @param audio_url
     * @return
     */
    @PostMapping("/audio_recognition")
    @ServiceTokenRequired
    public Result recognition(@NotBlank(message = "audio_url不能为空") String audio_url) {
        String data = audioVideoService.recognition(audio_url);
        return ResultUtils.success(data);
    }

    /**
     * 合并多条语音
     *
     * @param taskId
     * @return
     */
    @GetMapping("/merge_media")
    @ServiceTokenRequired
    public Result<AuditResultVO> mergeMedia(@NotNull(message = "taskId不能为空") Integer taskId) {
        AuditResultVO vo = audioVideoService.mergeMedia(taskId);
        return ResultUtils.success(vo);
    }

    /**
     * 语音批量上传到VOD
     *
     * @param media_strs
     * @return
     */
    @GetMapping("/uploadAudio")
    @ServiceTokenRequired
    public Result uploadAudio(@NotBlank(message = "media_strs不能为空") String media_strs) {
        int taskId = audioVideoService.uploadAudio(media_strs);
        return ResultUtils.success(taskId);
    }

    /**
     * 视频上传
     * 上传改为前端直接上传
     *
     * @param video_url
     * @return
     */
    @Deprecated
    @GetMapping("/save_video")
    @ServiceTokenRequired
    public Result saveVideo(@NotBlank(message = "video_url不能为空") String video_url) {
        String taskId = audioVideoService.saveVideo(video_url);
        return ResultUtils.success(taskId);
    }

    /**
     * 获取签名
     *
     * @return
     */
    @GetMapping("/getSignature")
    @ServiceTokenRequired
    public Result getSignature() {
        String taskId = audioVideoService.getSignature();
        return ResultUtils.success(taskId);
    }

    /**
     * 上传视频 审核
     *
     * @param fileId
     * @return
     */
    @GetMapping("/audit")
    @ServiceTokenRequired
    public Result audit(@NotBlank(message = "fileId不能为空") String fileId) {
        String taskId = audioVideoService.audit(fileId);
        return ResultUtils.success(taskId);
    }

    /**
     * 获取任务结果
     *
     * @param taskId
     * @return
     */
    @GetMapping("/auditResult")
    @ServiceTokenRequired
    public Result<AuditResultVO> auditResult(@NotBlank(message = "taskId不能为空") String taskId) {
        AuditResultVO result = audioVideoService.auditResult(taskId);
        return ResultUtils.success(result);
    }

    /**
     * 发布
     *
     * @param fileId
     * @return
     */
    @GetMapping("/publish")
    @ServiceTokenRequired
    public Result publish(@NotBlank(message = "fileId不能为空") String fileId) {
        String taskId = audioVideoService.publish(fileId);
        return ResultUtils.success(taskId);
    }
}
