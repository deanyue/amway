package com.amway.broadcast.applets.service.utils;

import com.vdurmont.emoji.EmojiParser;

public class EmojiUtil {
    public static void main (String[] args) {

        String str = "\uD83D\uDE24yuhang\uD83D\uDE08";
        String str1 ="1你！，";
        System.out.println("原始字符为：\n" + str);


        //将表情转换成对应别名字符（to aliases）
        System.out.println("to aliases 之后：");
        System.out.println(EmojiParser.parseToAliases(str));
        System.out.println(EmojiParser.parseToAliases(str, EmojiParser.FitzpatrickAction.PARSE));
        System.out.println(EmojiParser.parseToAliases(str, EmojiParser.FitzpatrickAction.REMOVE));
        System.out.println(EmojiParser.parseToAliases(str, EmojiParser.FitzpatrickAction.IGNORE));


        //将表情转换成html（to html）
        System.out.println("to html：");
        System.out.println(EmojiParser.parseToHtmlDecimal(str));
        System.out.println(EmojiParser.parseToHtmlDecimal(str, EmojiParser.FitzpatrickAction.PARSE));
        System.out.println(EmojiParser.parseToHtmlDecimal(str, EmojiParser.FitzpatrickAction.REMOVE));
        System.out.println(EmojiParser.parseToHtmlDecimal(str, EmojiParser.FitzpatrickAction.IGNORE));

        System.out.println("to html：");
        String s = EmojiParser.parseToHtmlDecimal(str);
        System.out.println(s);

        System.out.println("还原：");
        System.out.println(EmojiParser.parseToUnicode(s));

    }
}
