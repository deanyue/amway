//package com.amway.broadcast.applets.service.api.service.impl;
//
//import com.amway.broadcast.applets.service.api.service.AmwayAdminService;
//import com.amway.broadcast.applets.service.api.service.SimpleGenericDataBaseService;
//import com.amway.broadcast.applets.service.dao.entity.AmwayGroupUser;
//import com.amway.broadcast.applets.service.paramsEntity.AdminKickReq;
//import com.amway.broadcast.applets.service.utils.CheckUtils;
//import com.amway.broadcast.common.base.Result;
//import com.amway.broadcast.common.base.ResultUtils;
//import com.amway.broadcast.common.service.AmwayGroupService;
//import com.amway.broadcast.mybatis.autoconfigure.util.WrapperUtils;
//import lombok.extern.slf4j.Slf4j;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.stereotype.Service;
//
//import javax.annotation.Resource;
//
///**
// * 管理员功能
// */
//@Slf4j
//@Service
//public class AmwayAdminServiceImpl implements AmwayAdminService {
//    @Autowired
//    private AppletsAmwayGroupUserServiceImpl amwayGroupService;
//    /**
//     * 是否退出 0否  1是  2待进入
//     */
//    private static final Integer NO_QUIT = 0;
//    private static final Integer YES_QUIT = 1;
//    @Resource
//    private SimpleGenericDataBaseService simpleGenericDataBaseService;
//
//    /*@Override
//    public Result adminKick(AdminKickReq adminKickReq) {
//        AmwayGroupUser amwayGroupUser = new AmwayGroupUser();
//        amwayGroupUser.setUserId(adminKickReq.getUser_id());
//        amwayGroupUser.setGroupId(adminKickReq.getGroup_id());
//        //若根据userid和group_id查询group_user表，判断是否存在，不存在，则用户不在直播间。
//        AmwayGroupUser newAmwayGroupUser = simpleGenericDataBaseService.getOne(WrapperUtils.query(amwayGroupUser));
//        if(CheckUtils.isEmpty(newAmwayGroupUser)){
//            log.error("管理员踢人adminKick-->未查询到当前直播间下的用户UserId{}，GroupId{}",adminKickReq.getUser_id(), adminKickReq.getGroup_id());
//            return ResultUtils.error("0", "当前踢出的用户不存在");
//        }
//        //若为0未退出，则置为1-退出
//        if(NO_QUIT.equals(newAmwayGroupUser.getIsQuit())){
//            newAmwayGroupUser.setIsQuit(YES_QUIT);
//            simpleGenericDataBaseService.updateById(newAmwayGroupUser);
//            log.error("管理员踢人adminKick-->踢出成功UserId{}，GroupId{}",adminKickReq.getUser_id(), adminKickReq.getGroup_id());
//            return ResultUtils.success("踢出成功");
//        }
//        if(YES_QUIT.equals(newAmwayGroupUser.getIsQuit())){
//            return ResultUtils.error("0","已踢出该成员，请勿重复操作");
//        }
//        return ResultUtils.error("0","该用户不在畅聊间，踢出失败");
//    }*/
//    //refactor
//    @Override
//    public Result adminKick(AdminKickReq adminKickReq) {
////        AmwayGroupUser amwayGroupUser = new AmwayGroupUser();
////        amwayGroupUser.setUserId(adminKickReq.getUser_id());
////        amwayGroupUser.setGroupId(adminKickReq.getGroup_id());
//        //若根据userid和group_id查询group_user表，判断是否存在，不存在，则用户不在直播间。
////        AmwayGroupUser newAmwayGroupUser = simpleGenericDataBaseService.getOne(WrapperUtils.query(amwayGroupUser));
//        AmwayGroupUser newAmwayGroupUser = amwayGroupService.getOne(adminKickReq.getGroup_id(), adminKickReq.getUser_id());
//        if(CheckUtils.isEmpty(newAmwayGroupUser)){
//            log.error("管理员踢人adminKick-->未查询到当前直播间下的用户UserId{}，GroupId{}",adminKickReq.getUser_id(), adminKickReq.getGroup_id());
//            return ResultUtils.error("0", "当前踢出的用户不存在");
//        }
//        //若为0未退出，则置为1-退出
//        if(NO_QUIT.equals(newAmwayGroupUser.getIsQuit())){
//            newAmwayGroupUser.setIsQuit(YES_QUIT);
//            amwayGroupService.update(newAmwayGroupUser);
//            log.error("管理员踢人adminKick-->踢出成功UserId{}，GroupId{}",adminKickReq.getUser_id(), adminKickReq.getGroup_id());
//            return ResultUtils.success("踢出成功");
//        }
//        if(YES_QUIT.equals(newAmwayGroupUser.getIsQuit())){
//            return ResultUtils.error("0","已踢出该成员，请勿重复操作");
//        }
//        return ResultUtils.error("0","该用户不在畅聊间，踢出失败");
//    }
//}
