package com.amway.broadcast.applets.service.api.service.impl;

import com.alibaba.fastjson.JSONObject;
import com.amway.broadcast.applets.service.api.service.SimpleGenericDataBaseService;
import com.amway.broadcast.applets.service.api.service.UserCacheService;
import com.amway.broadcast.applets.service.dao.entity.AmwayUser;
import com.amway.broadcast.applets.service.redis.RedisUtils;
import com.amway.broadcast.applets.service.service.AmwayUserService;
import com.amway.broadcast.common.enumeration.RedisKeyEnum;
import com.amway.broadcast.common.util.RedisUtil;
import com.amway.broadcast.mybatis.autoconfigure.util.WrapperUtils;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.vdurmont.emoji.EmojiParser;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections4.CollectionUtils;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import javax.annotation.Resource;
import java.util.List;

/**
 * 管理员功能
 * @author Dean
 */
@Slf4j
@Service
public class UserCacheServiceImpl implements UserCacheService {

    @Resource
    private AmwayUserService amwayUserService;

    /**
     * 缓存中获取用户信息
     *
     * @param openId
     * @return
     */
    @Override
    public AmwayUser getUserInCacheByOpenId(String openId) {
        String userCache = RedisUtil.get(RedisKeyEnum.CACHE_USER.getPrefix() + openId);
        if (!StringUtils.isEmpty(userCache)){
            AmwayUser amwayUser = null;
            try {
                amwayUser = JSONObject.parseObject(userCache, AmwayUser.class);
            }catch (Exception e){
                log.error("缓存转换异常:{},{}",openId,e.getMessage());
                amwayUser = getUserByOpenId(openId);
            }
            return amwayUser;
        }
        return getUserByOpenId(openId);
    }

    /**
     * 获取用户信息
     *
     * @param openId
     * @return
     */
    @Override
    public AmwayUser getUserByOpenId(String openId) {
        AmwayUser amwayUser = null;
        LambdaQueryWrapper<AmwayUser> wrapper = new LambdaQueryWrapper<AmwayUser>().eq(AmwayUser::getOpenId, openId);
        List<AmwayUser> list = amwayUserService.list(wrapper);
        if (CollectionUtils.isNotEmpty(list)){
            amwayUser = list.get(0);
            RedisUtil.set(RedisKeyEnum.CACHE_USER.getPrefix() + openId,JSONObject.toJSONString(amwayUser),RedisKeyEnum.CACHE_USER.getExpiredTime());
        }
        return amwayUser;
    }

    /**
     * 删除用户缓存
     *
     * @param amwayUser
     * @return
     */
    @Override
    public void deleteCacheByAmwayUser(AmwayUser amwayUser) {
        if (!StringUtils.isEmpty(amwayUser)){
            RedisUtil.del(RedisKeyEnum.CACHE_USER.getPrefix() + amwayUser.getOpenId());
        }
    }

    /**
     * 存放用户缓存
     *
     * @param amwayUser
     * @return
     */
    @Override
    public void setCacheByAmwayUser(AmwayUser amwayUser) {
        if (!StringUtils.isEmpty(amwayUser)){
            RedisUtil.set(RedisKeyEnum.CACHE_USER.getPrefix() + amwayUser.getOpenId(),JSONObject.toJSONString(amwayUser),RedisKeyEnum.CACHE_USER.getExpiredTime());
        }
    }

    @Override
    public List<AmwayUser> getUserByNickName(String nickName) {
        LambdaQueryWrapper<AmwayUser> wrapper = new LambdaQueryWrapper<AmwayUser>().eq(AmwayUser::getNickname, EmojiParser.parseToHtmlDecimal(nickName));
        return amwayUserService.list(wrapper);
    }
}
