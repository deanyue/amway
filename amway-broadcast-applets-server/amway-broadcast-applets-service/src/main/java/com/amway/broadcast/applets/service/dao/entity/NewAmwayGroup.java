package com.amway.broadcast.applets.service.dao.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Builder;
import lombok.Data;

import java.util.Calendar;
import java.util.Date;

/**
 * @auther 薛晨
 * @date 2020/12/14
 * @time 10:27
 * @description 完成后注释AmwayGroup
 */
@Data
@TableName(value = "amway_group")
public class NewAmwayGroup {
    //主键
    @TableId(value = "id",type = IdType.AUTO)
    private Long id;

    //创建直播人ID号
    private Long memberId;

    //直播群ID号
    private String imGroupId;

    //直播间名称
    private String name;

    //直播开始时间
    private Date startTime;

    //直播结束时间
    private Date endTime;

    //有效时长，单位：天
    private Integer expire;

    //权限  1：转发可收听  2：转发不可收听
    private Integer limitType;

    //管理员人数
//    private String groupManagerNum;

    //简介
    private String remark;

    //状态   1：待开始  2：进行中  3：已过期
    private Integer statusAm;

    //是否解散 0：否  1：是
    private Integer isDelete;

    //创建时间
    private Date createTime;

    //是否允许管理员设置全体禁言(0为否,1为是,默认为1)
    private String isAllMute;

    //是否允许管理员踢走成员(0为否,1为是,默认为1)
    private String isKick;

    public void setEndTime() {
        if (startTime != null && expire != null) {
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(startTime);
            calendar.add(Calendar.DATE,expire);
            this.setEndTime(calendar.getTime());
        }
    }
}
