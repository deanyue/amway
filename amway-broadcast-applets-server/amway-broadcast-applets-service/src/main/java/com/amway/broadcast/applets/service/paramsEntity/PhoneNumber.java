package com.amway.broadcast.applets.service.paramsEntity;

import lombok.Data;

/**
 * @author WCX
 * @date 2020/11/3
 */
@Data
public class PhoneNumber {
    private String phone_number;
}
