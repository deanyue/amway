//package com.amway.broadcast.applets.service.task;
//
//import com.amway.broadcast.applets.service.api.service.AmwayTaskService;
//import org.springframework.scheduling.annotation.Scheduled;
//import org.springframework.stereotype.Component;
//
//import javax.annotation.Resource;
///**
// * 定时器
// */
////主要用于标记配置类，兼备Component的效果
//@Component
//public class TaskAmway {
//
//    @Resource
//    private AmwayTaskService amwayTaskService;
//
//    //每5秒钟执行一次
////    @Scheduled(cron = "*/5 * * * * ?")
////    public void configureTasks() {
////        amwayTaskService.updateAnchorStatus();
////    }
//
//}
