//package com.amway.broadcast.applets.service.api.service.impl;
//
//import com.amway.broadcast.applets.service.api.service.AmwayTaskService;
//import com.amway.broadcast.common.util.DateUtils;
//import com.amway.broadcast.mybatis.autoconfigure.util.WrapperUtils;
//import lombok.extern.slf4j.Slf4j;
//import org.springframework.stereotype.Service;
//
//import javax.annotation.Resource;
//import java.util.Date;
//
///**
// * @author cjj
// */
//@Slf4j
//@Service
//public class AmwayTaskServiceImpl implements AmwayTaskService {
//
//    @Resource
//    private SimpleGenericDataBaseServiceImpl simpleGenericDataBaseService;
//
//
//    /**
//     * 直播间状态
//     * 1：待开始 2：进行中 3：已解散
//     */
//    private static final int STATUS_TO_BEGIN = 1;
//    private static final int STATUS_PROCESSING = 2;
//    private static final int STATUS_DISSOLVED = 3;
//
//    /**
//     * 更新直播间数据
//     *
//     * @return
//     */
//    @Override
//    public void updateAnchorStatus() {
//        AmwayGroup amwayGroup = new AmwayGroup();
//        try {
//            //查询比当前时间小的数据,且状态不为3
//            List<AmwayGroup> amwayGroupList = simpleGenericDataBaseService.list(WrapperUtils.query(amwayGroup).le("start_time", new Date()).notIn("status_am", "3"));
//            int processingCount = 0;
//            int dissolvedCount = 0;
//            for (AmwayGroup amwayGroups : amwayGroupList) {
//                //1、若当前时间比 直播开始时间小或等于0 且过期时间没过，并且等于1-待开始 说明直播时间到了，修改状态为开始
//                if (DateUtils.getUntilSecond(new Date(), amwayGroups.getStartTime()) <= 0
//                        && DateUtils.getUntilSecond(new Date(), DateUtils.getDateAdd(amwayGroups.getStartTime(), amwayGroups.getExpire())) > 0
//                        && STATUS_TO_BEGIN == amwayGroups.getStatusAm()) {
//                    amwayGroups.setStatusAm(STATUS_PROCESSING);
//                    simpleGenericDataBaseService.updateById(amwayGroups);
//                }
//                //2、若当前时间小或等于 直播开始时间+有效时间，且状态不等于3  则判定解散
//                else if (DateUtils.getUntilSecond(new Date(), DateUtils.getDateAdd(amwayGroups.getStartTime(), amwayGroups.getExpire())) <= 0 & STATUS_DISSOLVED != amwayGroups.getStatusAm()) {
//                    amwayGroups.setStatusAm(STATUS_DISSOLVED);
//                    simpleGenericDataBaseService.updateById(amwayGroups);
//                }
//            }
//        }catch (Exception e){
//            log.error("更新直播间数据task出现异常{}",e.getMessage());
//        }
//    }
//
//}
