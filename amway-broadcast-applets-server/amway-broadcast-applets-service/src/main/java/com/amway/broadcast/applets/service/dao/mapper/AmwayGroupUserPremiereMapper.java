package com.amway.broadcast.applets.service.dao.mapper;

import com.amway.broadcast.applets.service.dao.entity.AmwayGroupUserPremiere;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Insert;

/**
 * @author liujch
 * @description
 * @Package com.amway.broadcast.applets.service.dao.mapper
 * @date 2020年12月14日 15:30
 */
public interface AmwayGroupUserPremiereMapper extends BaseMapper<AmwayGroupUserPremiere> {

    @Insert("insert into amway_group_user_premiere (group_id,user_id,share_user_id,create_time,group_info_str) values(#{groupId},#{userId},#{shareUserId},#{createTime},#{groupInfoStr}) on duplicate key update group_info_str = #{groupInfoStr}")
    int saveOrUpdate(AmwayGroupUserPremiere amwayGroupUserPremiere);
}
