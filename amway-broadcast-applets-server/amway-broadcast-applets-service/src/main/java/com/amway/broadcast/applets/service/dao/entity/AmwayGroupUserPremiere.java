package com.amway.broadcast.applets.service.dao.entity;

import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;

import java.util.Date;

/**
 * 直播群-用户 开播提醒表（用户加入直播间记录）
 */
@Getter
@Setter
@Slf4j
@TableName(value = "amway_group_user_premiere")
public class AmwayGroupUserPremiere extends AmwayGroupUserLog {

    private static final long serialVersionUID = 7883992849173226701L;

}
