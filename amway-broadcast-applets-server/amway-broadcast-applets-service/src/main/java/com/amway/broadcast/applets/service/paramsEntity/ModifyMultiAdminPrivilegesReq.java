package com.amway.broadcast.applets.service.paramsEntity;

import lombok.Data;

import javax.validation.constraints.NotBlank;
import java.io.Serializable;

/**
 * @author liujch
 * @description 多人修改权限
 * @Package com.amway.broadcast.applets.service.paramsEntity
 * @date 2020年12月11日 09:45
 */
@Data
public class ModifyMultiAdminPrivilegesReq implements Serializable {

    private static final long serialVersionUID = -4438522731225655841L;
    /**
     * 直播间id
     */
    @NotBlank(message = "直播间id不能为空")
    private String group_id;

    /**
     * 用户id（多个） 例：1,2,3
     */
    @NotBlank(message = "用户id不能为空")
    private String user_ids;
}
