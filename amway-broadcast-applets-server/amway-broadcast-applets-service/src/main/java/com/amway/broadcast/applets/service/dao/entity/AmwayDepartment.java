package com.amway.broadcast.applets.service.dao.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

/**
 * 组织架构部门表
 */
@Getter
@Setter
@TableName(value = "amway_department")
public class AmwayDepartment implements Serializable {

    private static final long serialVersionUID = -4235442820529178601L;

    @TableId(value = "id")
    private Long id;

    @TableField(value = "department_id")
    private Integer departmentId;

    @TableField(value = "name")
    private String name;//直播间名称

    @TableField(value = "parent_id")
    private Integer parentId;//父部门id。根部门为1

    @TableField(value = "order_am")
    private Integer orderAm;//在父部门中的次序值
}
