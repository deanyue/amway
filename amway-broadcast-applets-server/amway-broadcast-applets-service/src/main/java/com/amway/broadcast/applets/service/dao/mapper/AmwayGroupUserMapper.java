package com.amway.broadcast.applets.service.dao.mapper;

import com.amway.broadcast.applets.service.dao.entity.NewAmwayGroupUser;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Insert;

public interface AmwayGroupUserMapper extends BaseMapper<NewAmwayGroupUser> {

    @Insert("insert into amway_group_user (group_id,user_id,share_user_id,create_time,is_manager) values(#{groupId},#{userId},#{shareUserId},#{createTime},#{isManager}) on duplicate key update create_time = #{createTime}")
    int saveOrUpdate(NewAmwayGroupUser newAmwayGroupUser);

}
