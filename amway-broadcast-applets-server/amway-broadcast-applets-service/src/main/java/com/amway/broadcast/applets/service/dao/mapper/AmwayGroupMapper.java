package com.amway.broadcast.applets.service.dao.mapper;

import com.amway.broadcast.applets.service.dao.entity.NewAmwayGroup;
import com.amway.broadcast.common.base.BaseAmwayGroup;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

public interface AmwayGroupMapper extends BaseMapper<NewAmwayGroup> {
}
