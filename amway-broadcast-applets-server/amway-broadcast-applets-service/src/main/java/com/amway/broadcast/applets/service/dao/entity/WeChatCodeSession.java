package com.amway.broadcast.applets.service.dao.entity;

import lombok.Data;

/**
 * code2Session 返回的数据
 */
@Data
public class WeChatCodeSession {
    private String corpid;
    private String userid;
    private String sessionKey;
}
