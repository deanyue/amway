package com.amway.broadcast.applets.service.dao.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.*;

import java.io.Serializable;
import java.util.Date;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@TableName(value = "amway_user")
@JsonIgnoreProperties(ignoreUnknown = true)
public class AmwayUser implements Serializable {
    private static final long serialVersionUID = 3470881553030536328L;
    @TableId(value = "id", type= IdType.AUTO)
    private Long id;

    @TableField(value = "nickname")
    private String nickname;

    @TableField(value = "avatar_url")
    private String avatarUrl;

    @TableField(value = "mobile")
    private String mobile;

    @TableField(value = "gender")
    private Boolean gender;

    @TableField(value = "open_id")
    private String openId;

    @TableField(value = "is_auth")
    private Long isAuth;

    @TableField(value = "member_id")
    private Long memberId;

    @TableField(value = "create_time")
    private Date createTime;

    @TableField(value = "unionId")
    private String unionId;
}
