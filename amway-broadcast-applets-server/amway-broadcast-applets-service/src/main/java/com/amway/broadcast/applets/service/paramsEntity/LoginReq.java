package com.amway.broadcast.applets.service.paramsEntity;

import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;

/**
 * 模拟登陆请求req对象
 */
@Data
public class LoginReq {

    /**
     * 账号
     */
   @NotBlank(message = "账号不能为空")
   @Pattern(regexp = "^1[3456789]\\d{9}$", message = "登录账号输入有误，请核实")
   private String account;
    /**
     * 密码
     */
   @NotBlank(message = "密码不能为空")
   private String password;

}
