package com.amway.broadcast.applets.service.api.service.impl;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.amway.broadcast.applets.service.api.service.AmwayUserLoginService;
import com.amway.broadcast.applets.service.api.service.UserCacheService;
import com.amway.broadcast.applets.service.dao.entity.*;
import com.amway.broadcast.applets.service.paramsEntity.*;
import com.amway.broadcast.applets.service.redis.RedisUtils;
import com.amway.broadcast.applets.service.service.*;
import com.amway.broadcast.applets.service.utils.AccessTokenUtils;
import com.amway.broadcast.applets.service.utils.CheckUtils;
import com.amway.broadcast.applets.service.utils.TLSSigAPIv2;
import com.amway.broadcast.applets.service.utils.WechatUtils;
import com.amway.broadcast.common.base.Result;
import com.amway.broadcast.common.base.ResultUtils;
import com.amway.broadcast.common.constant.WXApiConstant;
import com.amway.broadcast.common.enumeration.*;
import com.amway.broadcast.common.util.JwtUtils;
import com.amway.broadcast.common.util.RedisUtil;
import com.amway.broadcast.common.util.SHA256;
import com.amway.broadcast.mybatis.autoconfigure.util.WrapperUtils;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.google.common.util.concurrent.ThreadFactoryBuilder;
import com.google.gson.Gson;
import com.vdurmont.emoji.EmojiParser;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.http.impl.client.DefaultUserTokenHandler;
import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.codehaus.xfire.util.Base64;
import org.redisson.api.RedissonClient;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.*;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import org.springframework.web.client.RestTemplate;

import javax.annotation.Resource;
import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;
import java.io.UnsupportedEncodingException;
import java.security.*;
import java.security.spec.InvalidParameterSpecException;
import java.util.*;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

@Slf4j
@Service
public class AmwayUserLoginServiceImpl implements AmwayUserLoginService {

    @Resource
    private RedisUtils redisUtils;
    @Resource
    private RedissonClient redissonClient;
    @Resource
    private UserCacheService userCacheService;
    @Resource
    private AmwayLogService amwayLogService;
    @Resource
    private AmwayUserService amwayUserService;
    @Resource
    private AmwayDepartmentService amwayDepartmentService;
    @Resource
    private AmwayDepartmentMemberService amwayDepartmentMemberService;
    @Resource
    private AmwayGroupUserService amwayGroupUserService;

    @Value("${Pressure.Test}")
    private Boolean isDebug;
    /**
     * case 1：用户不存在，需要添加
     * case 2: 用户存在且已被授权，直接返回用户信息
     * case 3：用户存在且未授权，update后返回最新数据
     */
    private static final int USER_NO_EXISTED = 0;
    private static final int USER_EXISTED_AND_UNAUTHORZIED = 1;
    private static final int USER_EXISTED_AND_AUTHORZIED = 2;

//    private static ThreadFactory namedThreadFactory = new ThreadFactoryBuilder()
//            .setNameFormat("save-login-log--pool-%d").build();
//
//    // 创建线程池
//    private static ThreadPoolExecutor threadPool = new ThreadPoolExecutor(4, 8, 1000L, TimeUnit.MILLISECONDS,
//            new LinkedBlockingQueue<>(2048), namedThreadFactory,
//            new ThreadPoolExecutor.DiscardOldestPolicy());

    @Value("${TencentConfig.weChatAppid}")
    private String weChatAppid;

    @Value("${TencentConfig.weChatSecret}")
    private String weChatSecret;

    @Value("${TencentConfig.sdkappid}")
    private long sdkappid;

    @Value("${TencentConfig.key}")
    private String key;

    @Value("${QyTencentConfig.corpid}")
    private String corpid;

    @Value("${QyTencentConfig.corpsecret}")
    private String corpsecret;

    /**
     * LoginCenter相关
     */
    @Value("${LoginCenterConfig.loginAppId}")
    private String loginAppId;

    @Value("${LoginCenterConfig.loginAppSecret}")
    private String loginAppSecret;

    @Value("${LoginCenterConfig.loginAhannelId}")
    private String loginAhannelId;

    @Value("${LoginCenterConfig.loginAppUrl}")
    private String loginAppUrl;
    /**
     * 安利LoginCenter相关角色权限
     */
    private static final String LOGIN_ROLE_ABO = "ABO";
    /**
     * redis过期时间 1000秒
     */
    private static final long EXPIRE = 1000;

    /**
     * 账号密码模拟登录
     *
     * @param loginReq
     * @return
     */
    @Override
    public Result login(LoginReq loginReq, String wxAppApiToken) {
        Long memberId = 0L;
        //根据amway_department_member表中手机号和密码进行account和password的验证
        if (!isDebug){
            LambdaQueryWrapper<AmwayDepartmentMember> queryWrapper = new LambdaQueryWrapper<AmwayDepartmentMember>().eq(AmwayDepartmentMember::getMobile, loginReq.getAccount());
            List<AmwayDepartmentMember> memberList = amwayDepartmentMemberService.list(queryWrapper);
            if (CollectionUtils.isEmpty(memberList)) {
                //用户名校验失败
                return ResultUtils.error("0", "用户不存在");
            }
            AmwayDepartmentMember amwayDepartmentMember = memberList.get(0);
            //密码比对
            String passwordmd = SHA256.encrypt(loginReq.getPassword());
            if (!passwordmd.equals(amwayDepartmentMember.getPasswordAm())) {
                //密码校验失败
                return ResultUtils.error("0", "密码错误");
            }
            log.info("账号密码模拟登录login-->模拟登录账号密码比对通过.");
            //获取department_member中id信息
            memberId = amwayDepartmentMember.getId();
        }
        //通过token查询用户唯一标识openid
        String openId = JwtUtils.getUserNameFromToken(wxAppApiToken);
        //通过openid查询用户信息
        AmwayUser amwayUser = userCacheService.getUserInCacheByOpenId(openId);
        //判断用户是否绑定department_member表,未绑定就进行绑定操作
        if (CheckUtils.isEmpty(amwayUser)) {
            saveUser(openId, memberId);
        }else {
            if (!isDebug){
                //校验memberId是否存在,若存在，则代表有用户绑定了memberid；校验当前openid与memberid是否存在
                List<AmwayUser> amwayUserMembers = amwayUserService.list(new LambdaQueryWrapper<AmwayUser>().eq(AmwayUser::getMemberId, memberId));
                if (amwayUserMembers.size() > 0) {
                    //若当前memberid存在，则判断是否和当前openid绑定
                    List<AmwayUser> amwayUsers = amwayUserMembers.stream().filter(a -> a.getOpenId().equals(openId)).collect(Collectors.toList());
                    if (CollectionUtils.isEmpty(amwayUsers)){
                        log.error("账号密码模拟登录login-->当前memberId[{}],当前openid[{}]，绑定openid[{}]用户绑定了", memberId, openId, amwayUserMembers.get(0).getOpenId());
                        return ResultUtils.error("0", "此账号已被绑定");
                    }
                }
                checkUserMemberId(amwayUser, memberId);
            }
        }
        return ResultUtils.success("登录成功！");
    }

    /**
     * 微信小程序登录
     *
     * @param code
     * @return
     */
    @Override
    public Result getOpenId(String code) {
        String wxAppApiToken, openId, SessionKey;
        if (isDebug) {
            openId = System.currentTimeMillis() + UUID.randomUUID().toString();
            SessionKey = openId;
            wxAppApiToken = JwtUtils.generateOperationToken(SessionKey, openId, openId);
        } else {
            WeChatSession weChatSession = new WeChatSession();
            try {
                weChatSession = getWeChatSessionFromAPI(code, weChatSession);
            }catch (Exception e){
                log.error("api获取信息失败:",e);
                ResultUtils.error (ResultCodeEnum.A0200.value (), ResultCodeEnum.A0200.getName ());
            }
            openId = weChatSession.getOpenid();
            SessionKey = weChatSession.getSession_key();
            wxAppApiToken = JwtUtils.generateOperationToken(SessionKey, openId, openId);
        }

        //设置是否是主播是否进入过直播间等信息
        LoginRepose loginRepose = setUserLiveIanchorInfo(openId);
        loginRepose.setWxapp_api_token(wxAppApiToken);
        redissonClient.getBucket(openId).set(wxAppApiToken, JwtUtils.getExpirationDateFromToken(wxAppApiToken).getTime(), TimeUnit.MILLISECONDS);
        return ResultUtils.success(loginRepose);
    }

    /**
     * 用户授权获取手机号
     *
     * @param wxGetPhoneNumberReq
     * @return 废弃
     */
    @Override
    public Result wxGetPhoneNumber(WxGetPhoneNumberReq wxGetPhoneNumberReq, String wxAppApiToken) {
        //解析wxapp_api_token获取Session_key信息
        String SessionKey = JwtUtils.getMerchantUserIdFromToken(wxAppApiToken);
        JSONObject userInfo = getPhoneNumber(SessionKey, wxGetPhoneNumberReq.getEncryptedData(), wxGetPhoneNumberReq.getIv());
        log.info("--->返回手机号接口的userInfojson：" + userInfo);
        //组装phoneNumber返回信息
        PhoneNumber phoneNumber = new PhoneNumber();
        phoneNumber.setPhone_number(null);
        return ResultUtils.success(phoneNumber);
    }

    /**
     * 获取用户登录即时通信IM的UserSig
     *
     * @param wxapp_api_token
     * @return
     */
    @Override
    public Result imGetUserSig(String wxapp_api_token) {
        //通过token获取openid
        String openId = JwtUtils.getUserNameFromToken(wxapp_api_token);
        //通过openid唯一标识查询user表中用户id
        AmwayUser amwayUser = userCacheService.getUserInCacheByOpenId(openId);
        if (StringUtils.isEmpty(amwayUser)) {
            amwayUser = AmwayUser.builder().build();
            amwayUser.setOpenId (openId);
            amwayUser.setIsAuth (UserAuthEnum.IS_AUTH.getCode());
            amwayUser.setCreateTime (new Date ());
            amwayUserService.save(amwayUser);
        }
        log.info("获取用户登录即时通信:{},{}",openId,amwayUser);
        String userId = amwayUser.getId().toString();
        //通过用户id和过期时间生成usersig,设置默认时间31天
        String userSig = TLSSigAPIv2.genUserSig(userId, 86400 * 31, sdkappid, key);
        GetUserSig getUserSig = new GetUserSig();
        getUserSig.setUser_id(userId);
        getUserSig.setUser_sig(userSig);
        return ResultUtils.success(getUserSig);
    }

    /**
     * 用户授权用户信息 (服务端保存用户信息)
     *
     * @param saveUserInfoReq
     * @return
     */
    @Override
    public Result wxSaveUserInfo(SaveUserInfoReq saveUserInfoReq, String wxAppApiToken) {
        if (!isDebug){
            //1、解析wxapp_api_token获取Session_key、获取openid信息
            String sessionKey = JwtUtils.getMerchantUserIdFromToken(wxAppApiToken);
            //2、用SHA-1算法计算签名
            String signature = WechatUtils.useSHA1(saveUserInfoReq.getRawData() + sessionKey);
            if (!signature.equals(saveUserInfoReq.getSignature())) {
                log.info("用户授权用户信息wx_save_user_info-->签名不一致 req入参 signature=" + saveUserInfoReq.getSignature() + "\n\t\n" + " SHA-1算法计算后的签名 signature=" + signature);
                return ResultUtils.error(ResultCodeEnum.A0340.value(), ResultCodeEnum.A0340.getName(),false);
            }
        }
        String openid = JwtUtils.getUserNameFromToken(wxAppApiToken);
        //3、比对不一致，则校验不通过
        //4、根据openid查询amway_user表，校验用户是否存在.不存在，则返回
        AmwayUser amwayUser = userCacheService.getUserInCacheByOpenId(openid);
        //5、根据用户存续状态和授权状态选择相应策略
        if (StringUtils.isEmpty(amwayUser)){
            return ResultUtils.error(ResultCodeEnum.A0.value(), ResultCodeEnum.A0.getName(),false);
        }
        isNeedUpdateUser(saveUserInfoReq, amwayUser);
        if (!StringUtils.isEmpty(amwayUser.getNickname())){
            amwayUser.setNickname(EmojiParser.parseToUnicode(amwayUser.getNickname()));
        }
        return ResultUtils.success(SaveUserInfoRepose.builder().amwayUserInfo(amwayUser).build());
    }

    /**
     * 企业微信自动登录
     *
     * @param code
     * @return
     */
    @Override
    public Result getQyUserId(String wxAppApiToken, String code) {
        String userId = org.apache.commons.lang.StringUtils.EMPTY;
        //获取user表的信息
        String openId = JwtUtils.getUserNameFromToken(wxAppApiToken);
        AmwayUser amwayUser = userCacheService.getUserInCacheByOpenId(openId);
        Long memberId = 0L;
        //企业微信的接口
        if (isDebug) {
            //debug模式userid无用
            List<AmwayDepartmentMember> allMemberList = new ArrayList<>();
            String debugDepUserList = RedisUtil.get("debugDepUserList");
            if (!StringUtils.isEmpty(debugDepUserList)){
                allMemberList = JSONObject.parseArray(debugDepUserList, AmwayDepartmentMember.class);
            }else {
                allMemberList = amwayDepartmentMemberService.list(new LambdaQueryWrapper<AmwayDepartmentMember>().orderByAsc(AmwayDepartmentMember::getId));
                if (CollectionUtils.isNotEmpty(allMemberList)){
                    RedisUtil.set("debugDepUserList",JSONObject.toJSONString(allMemberList),120);
                    RedisUtil.set("debugDepUserListId",(allMemberList.size()-1)+"",120);
                }
            }
            String debugDepUserListId = RedisUtil.get("debugDepUserListId");
            if (!StringUtils.isEmpty(debugDepUserListId)){
                int id = Integer.parseInt(debugDepUserListId);
                if (id>=0){
                    if (CollectionUtils.isNotEmpty(allMemberList)&& (allMemberList.size()-1) >= id){
                        AmwayDepartmentMember member = allMemberList.get(id);
                        userId = member.getUserId();
                        memberId = member.getId();
                        RedisUtil.set("debugDepUserListId",(id-1)+"",120);
                    }
                }
            }
            if (memberId==0L){
                log.error("debug测试模式depMember人数不够啦");
                return ResultUtils.error("0", "debug测试模式depMember人数不够啦");
            }
        }else {
            String accessToken = new AccessTokenUtils().getQyAccessToken(corpid, corpsecret, redisUtils);
            if (accessToken == null) {
                log.error("企业微信自动登录getQyUserId-->未获取到所需的token");
                return ResultUtils.error("0", "企业微信自动登录失败，请稍后重试");
            }
            userId = getUserIdFromCode(code, accessToken);
            log.info("开始调用企业微信自动登录getQyUserId--> userId: {},{}",userId,code);
            if (CheckUtils.isEmpty(userId)) {
                log.info("开始调用企业微信自动登录getQyUserId--> 返回的userId不存在");
                return ResultUtils.error(ResultCodeEnum.A0200.value(), ResultCodeEnum.A0200.getName());
            }
            LambdaQueryWrapper<AmwayDepartmentMember> wrapper = new LambdaQueryWrapper<AmwayDepartmentMember>().eq(AmwayDepartmentMember::getUserId, userId);
            List<AmwayDepartmentMember> amwayDepartmentMemberList = amwayDepartmentMemberService.list(wrapper);
            if (CollectionUtils.isEmpty(amwayDepartmentMemberList)) {
                log.info("开始调用企业微信自动登录getQyUserId--> DepartmentMember表中userId不存在");
                return ResultUtils.error("0", "组织架构中未找到该用户");
            }
            AmwayDepartmentMember amwayDepartmentMember = amwayDepartmentMemberList.get(0);
            memberId = amwayDepartmentMember.getId();
        }
        if (CheckUtils.isEmpty(amwayUser)) {
            AmwayUser user = saveUser(openId, memberId);
            if (!StringUtils.isEmpty(user)){
                log.info("开始调用企业微信自动登录getQyUserId-->用户ID{}小程序登录成功", memberId);
                return ResultUtils.success(SaveUserInfoRepose.builder().amwayUserInfo(user).build());
            }
        }else {
            Long userMemberId = amwayUser.getMemberId();
            if (!CheckUtils.isEmpty(userMemberId) && !userMemberId.equals(memberId)) {
                log.error("开始调用企业微信自动登录getQyUserId-->此用户:{}已被绑定，memberId为{} ,DepartmentMemberid为{}", amwayUser.getId(), userMemberId, memberId);
                return ResultUtils.error("0", "此用户已绑定其它账号信息");
            }
            //校验memberId是否存在,若存在，则代表有用户绑定了memberid
            List<AmwayUser> amwayUserMembers = amwayUserService.getUserListByMemberId(memberId);
            if (CollectionUtils.isNotEmpty(amwayUserMembers)) {
                log.info("查询出来的user信息size{}，企业微信登录code{}，企业微信userid{}，通过userid查询到的memberId{}，通过memberId查询的openId{}，" +
                        "token传进来的openId{}，如果openId一致则属于相同用户，如果不一致则被其他用户绑定", amwayUserMembers.size(), code, userId, memberId, amwayUserMembers.get(0).getOpenId(), openId);
                //若当前memberid存在，则判断是否和当前openid绑定
                List<AmwayUser> amwayUsers = amwayUserMembers.stream().filter(u -> u.getOpenId().equals(openId)).collect(Collectors.toList());
                if (CollectionUtils.isEmpty(amwayUsers)) {
                    log.error("开始调用企业微信自动登录getQyUserId-->当前userId[{}]、memberId[{}]、openid[{}]，绑定openid[{}]用户绑定了", userId, memberId, openId, amwayUserMembers.get(0).getOpenId());
                    return ResultUtils.error("0", "此账号已被绑定");
                }
            }
            checkUserMemberId(amwayUser, memberId);
            return ResultUtils.success(SaveUserInfoRepose.builder().amwayUserInfo(amwayUser).build());
        }
        return ResultUtils.error("0", "企业微信自动登录失败，请稍后重试");
    }

    private String getUserIdFromCode(String code, String accessToken) {
        String userId;
        ResponseEntity<String> responseEntity = getStringResponseEntity(code, accessToken);
        if (responseEntity.getStatusCode() != HttpStatus.OK) {
            log.info("开始调用企业微信自动登录getQyUserId-->调用微信登录API接口失败。");
            ResultUtils.error(ResultCodeEnum.A0200.value(), ResultCodeEnum.A0200.getName());
        }
        String jscodeSession = responseEntity.getBody();
        Gson gson = new Gson();
        //从微信服务器获得的数据并进行解析
        WeChatCodeSession weChatCodeSession = gson.fromJson(jscodeSession, WeChatCodeSession.class);
        userId = weChatCodeSession.getUserid();
        return userId;
    }

    private LoginRepose setUserLiveIanchorInfo( String openId) {
        LoginRepose loginRepose = LoginRepose.builder().build();
        AmwayUser amwayUser = userCacheService.getUserInCacheByOpenId(openId);
        Long isAuth = UserAuthEnum.NO_AUTH.getCode();
        if (StringUtils.isEmpty(amwayUser)){
            //用户身份
            loginRepose.setUserIdentity(UserAnchorEnum.USER.getCode());
            //未进入直播间
            loginRepose.setUserInto(InLiveRoomEnum.NO_IN_LIVEROOM.getCode());
        }else {
            isAuth = amwayUser.getIsAuth();
            //根据memberid判断是否为主播，存在则为直播，不存在则为用户   //0:用户 1：主播
            if(StringUtils.isEmpty(amwayUser.getMemberId())){
                log.info("开始调用微信登录接口wxLogin-->用户身份");
                //用户标识用户标识
                loginRepose.setUserIdentity(UserAnchorEnum.USER.getCode());
                //未进入直播间
                loginRepose.setUserInto(InLiveRoomEnum.NO_IN_LIVEROOM.getCode());
                try {
                    LambdaQueryWrapper<NewAmwayGroupUser> queryWrapper = new LambdaQueryWrapper<NewAmwayGroupUser>()
                            .eq(NewAmwayGroupUser::getUserId, amwayUser.getId().toString());
                    //若查询直播间记录  存在记录，则代表进入了直播间
                    List<NewAmwayGroupUser> amwayGroupUserList = amwayGroupUserService.list(queryWrapper);
                    if(CollectionUtils.isNotEmpty(amwayGroupUserList)){
                        loginRepose.setUserInto(InLiveRoomEnum.IN_LIVEROOM.getCode());
                    }
                }catch (Exception e){
                    log.error("用户群组信息查询失败:",e);
                }
            }else{
                //主播身份
                log.info("开始调用微信登录接口wxLogin-->主播身份");
                loginRepose.setUserIdentity(UserAnchorEnum.IANCHOR.getCode());
            }
        }
        loginRepose.setIs_auth(isAuth);
        return loginRepose;
    }

    private WeChatSession getWeChatSessionFromAPI(String code, WeChatSession weChatSession) {
        RestTemplate restTemplate = new RestTemplate();
        String url = WXApiConstant.SNS_JSCODE2SESSION + weChatAppid +
                "&secret=" + weChatSecret + "&js_code=" + code + "&grant_type=authorization_code";
        //进行网络请求
        ResponseEntity<String> responseEntity = restTemplate.exchange(url, HttpMethod.GET, null, String.class);
        if (responseEntity.getStatusCode() == HttpStatus.OK) {
            String sessionData = responseEntity.getBody();
            Gson gson = new Gson();
            //解析从微信服务器获得的数据并进行解析
            weChatSession = gson.fromJson(sessionData, WeChatSession.class);
            //获取用户的openid
        }
        return weChatSession;
    }

    //获取手机号信息
    public JSONObject getPhoneNumber(String sessionKey, String encryptedData, String iv) {
        // 被加密的数据
        byte[] dataByte = Base64.decode(encryptedData);
        // 加密秘钥
        byte[] keyByte = Base64.decode(sessionKey);
        // 偏移量
        byte[] ivByte = Base64.decode(iv);
        try {
            // 如果密钥不足16位，那么就补足.  这个if 中的内容很重要
            int base = 16;
            if (keyByte.length % base != 0) {
                int groups = keyByte.length / base + (keyByte.length % base != 0 ? 1 : 0);
                byte[] temp = new byte[groups * base];
                Arrays.fill(temp, (byte) 0);
                System.arraycopy(keyByte, 0, temp, 0, keyByte.length);
                keyByte = temp;
            }
            // 初始化
            Security.addProvider(new BouncyCastleProvider());
            Cipher cipher = Cipher.getInstance("AES/CBC/PKCS7Padding", "BC");
            SecretKeySpec spec = new SecretKeySpec(keyByte, "AES");
            AlgorithmParameters parameters = AlgorithmParameters.getInstance("AES");
            parameters.init(new IvParameterSpec(ivByte));
            cipher.init(Cipher.DECRYPT_MODE, spec, parameters);
            byte[] resultByte = cipher.doFinal(dataByte);
            if (null != resultByte && resultByte.length > 0) {
                String result = new String(resultByte, "UTF-8");
                return JSONObject.parseObject(result);
            }
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (NoSuchPaddingException e) {
            e.printStackTrace();
        } catch (InvalidParameterSpecException e) {
            e.printStackTrace();
        } catch (IllegalBlockSizeException e) {
            e.printStackTrace();
        } catch (BadPaddingException e) {
            e.printStackTrace();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (InvalidKeyException e) {
            e.printStackTrace();
        } catch (InvalidAlgorithmParameterException e) {
            e.printStackTrace();
        } catch (NoSuchProviderException e) {
            e.printStackTrace();
        }
        return null;
    }

    private void isNeedUpdateUser(SaveUserInfoReq saveUserInfoReq, AmwayUser amwayUser) {
        if (StringUtils.isEmpty(amwayUser.getAvatarUrl())||StringUtils.isEmpty(amwayUser.getNickname())){
            if(isDebug){
                String headPic = "https://thirdwx.qlogo.cn/mmopen/vi_32/GWRialrxplkPjiaibU2p338zZzWjQJVU2AlMHVRX9XBJvAaebnFsLxM7FQEFvtKq0Z2Ct2J0qV2LWTVLdwXb10hSg/132";
                amwayUser.setNickname(EmojiParser.parseToHtmlDecimal("测试模式昵称"));
                amwayUser.setGender(true);
                amwayUser.setAvatarUrl(headPic);
                amwayUser.setUnionId("000000");
                amwayUser.setCreateTime(new Date());
                try {
                    updateUser(amwayUser);
                }catch (Exception e){
                    log.error("更新用户信息失败:{},{}",amwayUser,e);
                }
            }else {
                AmwayUser amwayUserJson = AmwayUser.builder().build();
                try{
                    String userInfoStr = saveUserInfoReq.getRawData();
                    amwayUserJson = JSONObject.parseObject(userInfoStr, AmwayUser.class);
                }catch (Exception e){
                    log.error("解析用户失败:",e);
                }
                amwayUser.setNickname(EmojiParser.parseToHtmlDecimal(amwayUserJson.getNickname()));
                amwayUser.setGender(amwayUserJson.getGender());
                amwayUser.setAvatarUrl(amwayUserJson.getAvatarUrl());
                amwayUser.setUnionId(amwayUserJson.getUnionId());
                amwayUser.setCreateTime(new Date());
                try {
                    updateUser(amwayUser);
                }catch (Exception e){
                    log.error("更新用户信息失败:{},{}",amwayUser,e);
                }
            }

        }
    }

    private void updateUser(AmwayUser amwayUser) {
        //6、解析用户信息，以id进行更新amway_user表、更新成功，则返回对象，否则，为null
        boolean amwayUserFlag = amwayUserService.updateById(amwayUser);
        if (amwayUserFlag) {
            userCacheService.setCacheByAmwayUser(amwayUser);
        }
    }

    private Result addUser(SaveUserInfoReq saveUserInfoReq, String sessionKey, String openid, AmwayUser amwayUser) {
        //6、解析用户信息，以id进行更新amway_user表、更新成功，则返回对象，否则，为null
        String userInfoStr = WechatUtils.wxDecrypt(saveUserInfoReq.getEncryptedData(), sessionKey, saveUserInfoReq.getIv());
        AmwayUser amwayUserJson = JSONObject.parseObject(userInfoStr, AmwayUser.class);
        amwayUser.setOpenId(openid);
        amwayUser.setCreateTime(new Date());
        amwayUser.setNickname(EmojiParser.parseToHtmlDecimal(amwayUserJson.getNickname()));
        amwayUser.setGender(amwayUserJson.getGender());
        amwayUser.setAvatarUrl(amwayUserJson.getAvatarUrl());
        amwayUser.setIsAuth(UserAuthEnum.IS_AUTH.getCode());
        amwayUser.setUnionId(amwayUserJson.getUnionId());
        boolean amwayUserFlag = amwayUserService.save(amwayUser);
        if (amwayUserFlag) {
            RedisUtil.set(RedisKeyEnum.CACHE_USER.getPrefix() + openid, JSONObject.toJSONString(amwayUser), RedisKeyEnum.CACHE_USER.getExpiredTime());
            log.info("用户授权用户信息wx_save_user_info-->用户授权信息更新成功");
            //返回报文info节点
            amwayUser.setNickname(EmojiParser.parseToUnicode(amwayUser.getNickname()));
            return ResultUtils.success(SaveUserInfoRepose.builder().amwayUserInfo(amwayUser).build());
        } else {
            return ResultUtils.error("0", "授权信息更新失败，请稍后重试");
        }
    }

    private int getAuthorizedStatus(AmwayUser amwayUser) {
        if (StringUtils.isEmpty(amwayUser)) {
            return USER_NO_EXISTED;
        }
        if (!StringUtils.isEmpty(amwayUser) && amwayUser.getIsAuth().equals(UserAuthEnum.IS_AUTH.getCode())) {
            return USER_EXISTED_AND_AUTHORZIED;
        }
        return USER_EXISTED_AND_UNAUTHORZIED;
    }

    private void checkUserMemberId(AmwayUser amwayUser, Long memberId) {
        if (CheckUtils.isEmpty(amwayUser.getMemberId())) {
            //将department_member的id绑定进user表中
            amwayUser.setMemberId(memberId);
            updateUser(amwayUser);
            log.info ("开始调用企业微信自动登录getQyUserId-->将department_member的id绑定进user表中member_id绑定成功");
        }
    }

    private ResponseEntity<String> getStringResponseEntity(String code, String accessToken) {
        String url = WXApiConstant.MINIPROGRAM_JSCODE2SESSION + accessToken + "&js_code=" + code + "&grant_type=authorization_code";
        RestTemplate restTemplate = new RestTemplate();
        //进行网络请求
        return restTemplate.exchange(url, HttpMethod.GET, null, String.class);
    }


    private AmwayUser saveUser(String openId, Long memberId) {
        AmwayUser amwayUser = AmwayUser.builder().build();
        amwayUser.setOpenId(openId);
        amwayUser.setCreateTime(new Date());
        amwayUser.setIsAuth(UserAuthEnum.IS_AUTH.getCode());
        amwayUser.setMemberId(memberId);
        if (amwayUserService.save(amwayUser)){
            return amwayUser;
        }
        return null;
    }

    /**
     * 安利LoginCenter校验
     *
     * @return
     */
    public String amwayLoginCenter(String unionId) {
        String token = null;
        try {
            token = org.apache.commons.codec.binary.Base64.encodeBase64String((loginAppId + ":" + loginAppSecret).getBytes("UTF-8"));
        } catch (Exception e) {
            log.info("安利LoginCenter校验-->base64出现异常{}", e.getMessage());
        }
        Map<String, Object> reqDate = new HashMap<String, Object>();
        reqDate.put("channelId", loginAhannelId);
        reqDate.put("unionId", unionId);

        HttpHeaders httpHeaders = new HttpHeaders();
        MediaType type = MediaType.parseMediaType(MediaType.APPLICATION_JSON_UTF8_VALUE);
        httpHeaders.setContentType(type);
        httpHeaders.set("Authorization", "Basic " + token);
        HttpEntity<Map> httpEntity = new HttpEntity<>(reqDate, httpHeaders);
        RestTemplate restTemplate = new RestTemplate();
        ResponseEntity<String> responseEntity = restTemplate.postForEntity(loginAppUrl, httpEntity, String.class);
        if (responseEntity != null && responseEntity.getStatusCode() == HttpStatus.OK) {
            String retData = responseEntity.getBody();
            JSONObject retJson = JSONObject.parseObject(retData);
            //若为成功,解析data获取role信息
            if ("0".equals(retJson.get("code").toString())) {
                String data = retJson.getString("data");
                JSONObject dataJson = JSONObject.parseObject(data);
                String role = dataJson.getString("role");
                String ada = dataJson.getString("ada");
                log.info("安利LoginCenter校验成功-->返回的role[{}],ada[{}]", role, ada);
                return role;
            }
        }
        return null;
    }

}
