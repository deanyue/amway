package com.amway.broadcast.applets.service.service;


import com.amway.broadcast.applets.service.dao.entity.AmwayUser;
import com.amway.broadcast.common.service.BaseService;

import java.util.List;


/**
 * @author Dean
 */
public interface AmwayUserService extends BaseService<AmwayUser> {

    AmwayUser getByOpenId(String openId);

    List<AmwayUser> getUserListByMemberId(Long memberId);
}