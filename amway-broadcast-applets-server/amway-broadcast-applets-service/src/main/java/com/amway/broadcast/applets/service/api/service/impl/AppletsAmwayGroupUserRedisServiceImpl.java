//package com.amway.broadcast.applets.service.api.service.impl;
//
//import com.alibaba.fastjson.JSONObject;
//import com.amway.broadcast.applets.service.dao.entity.AmwayGroupUser;
//import com.amway.broadcast.common.service.AmwayGroupUserService;
//import com.amway.broadcast.common.util.RedisUtil;
//import jodd.util.StringUtil;
//import org.springframework.stereotype.Service;
//
//import java.util.ArrayList;
//import java.util.List;
//import java.util.Set;
//import java.util.stream.Collectors;
//
///**
// * @auther 薛晨
// * @date 2020/12/12
// * @time 13:53
// * @description
// */
//@Service
//public class AppletsAmwayGroupUserRedisServiceImpl implements AmwayGroupUserService<AmwayGroupUser> {
//    @Override
//    public int insert(AmwayGroupUser baseAmwayGroupUser) {
//        return 0;
//    }
//
//    @Override
//    public int update(AmwayGroupUser baseAmwayGroupUser) {
//        return 0;
//    }
//
//    @Override
//    public List<AmwayGroupUser> getListByCondition(AmwayGroupUser condition) {
//        return null;
//    }
//
//    @Override
//    public AmwayGroupUser getOne(String groupId, String userId) {
//        String s = RedisUtil.get(groupId + userId);
//        if (StringUtil.isNotBlank(s)) {
//            AmwayGroupUser groupUser = JSONObject.parseObject(s, AmwayGroupUser.class);
//            return groupUser;
//        }
//        return null;
//    }
//
//    @Override
//    public List<AmwayGroupUser> getByGroupIdAndStatus(String groupId, Integer quitStatus) {
//        List<AmwayGroupUser> list = new ArrayList<>();
//        Set<String> keys = RedisUtil.keys(groupId + "*");
//        if (!keys.isEmpty()) {
//            for (String key : keys) {
//                String s = RedisUtil.get(key);
//                AmwayGroupUser groupUser = JSONObject.parseObject(s, AmwayGroupUser.class);
//                list.add(groupUser);
//            }
//        }
//        return list.stream().filter(amwayGroupUser -> amwayGroupUser.getIsQuit().equals(quitStatus)).collect(Collectors.toList());
//    }
//}
