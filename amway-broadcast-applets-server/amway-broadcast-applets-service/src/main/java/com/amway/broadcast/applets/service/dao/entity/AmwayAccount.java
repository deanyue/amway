package com.amway.broadcast.applets.service.dao.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.util.Date;

/**
 * 系统日志表
 */
@Getter
@Setter
@TableName(value = "amway_account")
public class AmwayAccount implements Serializable {

    private static final long serialVersionUID = -3793736476903477440L;

    @TableId(value = "id")
    private Long id;

    @TableField(value = "username")
    private String username;

    @TableField(value = "password_am")
    private String passwordAm;

    @TableField(value = "realname")
    private String realname;

    @TableField(value = "last_login_time")
    private String lastLoginTime;

    @TableField(value = "enabled")
    private Integer enabled;

    @TableField(value = "create_time")
    private Date createTime;
}
