package com.amway.broadcast.applets.service.api.service;

public interface AmwayTaskService {

    /**
     * 定时任务调度，更新直播间状态
     */
     void updateAnchorStatus();
}
