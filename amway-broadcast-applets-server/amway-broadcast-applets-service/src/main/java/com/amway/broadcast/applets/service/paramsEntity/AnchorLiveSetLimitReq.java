package com.amway.broadcast.applets.service.paramsEntity;

import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;

/**
 * 修改畅聊间转发权限 req入参
 */
@Data
public class AnchorLiveSetLimitReq {


    /**
     * 小程序端请求服务器端的凭证
     */
    @NotBlank(message = "畅聊间id不能为空")
    @Pattern(regexp = "^\\d+$", message = "畅聊间id格式不正确")
    private String group_id;


    /**
     * 收听权限 1：转发可收听 2：转发不可收听
     */
    @NotBlank(message = "转发权限类型不能为空")
    @Pattern(regexp = "^[12]$", message = "转发权限类型输入有误,请核实")
    private String limit_type;
}
