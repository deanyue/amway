package com.amway.broadcast.applets.service.paramsEntity;

import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;

/**
 * 用户退出畅聊间
 */
@Data
public class UserQuitReq {

    /**
     * 畅聊间id
     */
    @NotBlank(message = "畅聊间id编号不能为空")
    @Pattern(regexp = "^\\d+$", message = "畅聊间id格式不正确")
    private String group_id;

}
