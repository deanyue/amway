package com.amway.broadcast.applets.service.common.constant;

import lombok.Getter;

/**
 * @author liujch
 * @description 操作类型
 * @Package com.amway.broadcast.applets.service.common.constant
 * @date 2020年12月11日 10:49
 */

@Getter
public enum AdminOperationTypeEnum {

    /**
     * 增加管理员
     */
    ADD_ADMIN("1","增加管理员")

    /*
      取消管理员
     */
    ,CANCEL_ADMIN("0","取消管理员");

    private String operateType;

    private String operateName;

    AdminOperationTypeEnum(String operateType,String operateName) {
        this.operateType = operateType;
        this.operateName = operateName;
    }

    public static AdminOperationTypeEnum getAdminOperationTypeEnum(String operateType) {
        AdminOperationTypeEnum[] values = AdminOperationTypeEnum.values();
        for (int i = 0; i < values.length; i++) {
            if (operateType.equals(values[i].getOperateType())) {
                return values[i];
            }
        }
        return null;
    }
}
