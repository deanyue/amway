package com.amway.broadcast.applets.service.service.impl;


import com.amway.broadcast.applets.service.dao.entity.AmwayLog;
import com.amway.broadcast.applets.service.dao.entity.AmwayUser;
import com.amway.broadcast.applets.service.dao.mapper.AmwayLogMapper;
import com.amway.broadcast.applets.service.dao.mapper.AmwayUserMapper;
import com.amway.broadcast.applets.service.service.AmwayLogService;
import com.amway.broadcast.applets.service.service.AmwayUserService;
import com.amway.broadcast.common.service.impl.BaseServiceImpl;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


/**
 * @author Dean
 */
@Service
@Slf4j
public class AmwayLogServiceImpl extends BaseServiceImpl<AmwayLogMapper, AmwayLog> implements AmwayLogService {

    @Autowired
    private AmwayLogMapper amwayLogMapper;

}