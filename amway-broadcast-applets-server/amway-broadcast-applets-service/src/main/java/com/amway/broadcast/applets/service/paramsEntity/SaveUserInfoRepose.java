package com.amway.broadcast.applets.service.paramsEntity;

import com.amway.broadcast.applets.service.dao.entity.AmwayUser;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Builder;
import lombok.Data;

/**
 * 用户授权用户信息返回信息repose对象
 */
@Data
@Builder
public class SaveUserInfoRepose {
    /**
     * 用户信息
     */
    @JsonProperty("info")
    private AmwayUser amwayUserInfo;
}
