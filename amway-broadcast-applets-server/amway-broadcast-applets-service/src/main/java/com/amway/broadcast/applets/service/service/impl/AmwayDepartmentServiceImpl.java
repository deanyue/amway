package com.amway.broadcast.applets.service.service.impl;


import com.amway.broadcast.applets.service.dao.entity.AmwayDepartment;
import com.amway.broadcast.applets.service.dao.entity.AmwayUser;
import com.amway.broadcast.applets.service.dao.mapper.AmwayDepartmentMapper;
import com.amway.broadcast.applets.service.dao.mapper.AmwayDepartmentMemberMapper;
import com.amway.broadcast.applets.service.dao.mapper.AmwayUserMapper;
import com.amway.broadcast.applets.service.service.AmwayDepartmentService;
import com.amway.broadcast.applets.service.service.AmwayUserService;
import com.amway.broadcast.common.service.impl.BaseServiceImpl;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


/**
 * @author Dean
 */
@Service
@Slf4j
public class AmwayDepartmentServiceImpl extends BaseServiceImpl<AmwayDepartmentMapper, AmwayDepartment> implements AmwayDepartmentService {

    @Autowired
    private AmwayDepartmentMapper amwayDepartmentMapper;
}