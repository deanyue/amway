package com.amway.broadcast.applets.service.entity.vo;

import com.amway.broadcast.applets.service.dao.entity.NewAmwayGroup;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.util.Date;
import java.util.List;

/**
 * @auther 薛晨
 * @date 2020/12/14
 * @time 14:44
 * @description
 */
@Data
public class AmwayGroupVO extends NewAmwayGroup {
    //    private String memberAvatar;
//    private String memberNickname;
//    private long startTimeTimestamp;
//    private Integer memberNum;
//    private List<String> groupAvatars;
//    private int managerNum;
    //服了
    @JsonProperty("member_id")
    private Long memberId;

    //直播群ID号
    @JsonProperty("im_group_id")
    private String imGroupId;


    //直播开始时间
    @JsonProperty("start_time")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date startTime;

    //权限  1：转发可收听  2：转发不可收听
    @JsonProperty("limit_type")
    private Integer limitType;


    //状态   1：待开始  2：进行中  3：已过期
    @JsonProperty("status")
    private Integer statusAm;


    // 表中待新增字段，或联查字段
    //创建直播人头像
    @JsonProperty("member_avatar")
    private String memberAvatar;

    //创建直播人昵称
    @JsonProperty("member_nickname")
    private String memberNickname;

    /**
     * 直播间首页接口返回 直播开始时间-时间戳
     */
    @JsonProperty("start_time_timestamp")
    private Long startTimeTimestamp;

    /**
     * 直播间首页接口返回 直播间头像列表（最多取3个，结构：['', '', '']）
     */
    @JsonProperty("group_avatars")
    private List<String> groupAvatars;

    /**
     * 直播间首页接口返回  直播间人数
     */
    @JsonProperty("member_num")
    private Integer memberNum;
    /**
     * 直播间管理员人数
     */
    @JsonProperty("manager_num")
    private Integer managerNum;
}
