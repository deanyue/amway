package com.amway.broadcast.applets.service.paramsEntity;

import lombok.Data;

import javax.validation.constraints.NotBlank;
/**
 * 微信登录 req对象
 */
@Data
public class LoginWxReq {

    /**
     * 用户登录凭证（有效期五分钟）。开发者需要在开发者服务器后台调用auth.code2Session，使用 code 换取 openid 和 session_key信息等
     */
    @NotBlank(message = "用户登录凭证不能为空")
    private String code;


}
