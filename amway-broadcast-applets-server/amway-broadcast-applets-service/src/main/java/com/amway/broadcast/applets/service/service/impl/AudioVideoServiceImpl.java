package com.amway.broadcast.applets.service.service.impl;

import cn.hutool.core.util.HashUtil;
import com.alibaba.fastjson.JSONArray;
import com.amway.broadcast.applets.service.config.other.Signature;
import com.amway.broadcast.applets.service.config.thread.AudioVideoThreadPool;
import com.amway.broadcast.applets.service.entity.vo.AuditResultVO;
import com.amway.broadcast.applets.service.service.AudioVideoService;
import com.amway.broadcast.common.exception.user.overall.OverallException;
import com.amway.broadcast.common.util.RedisUtil;
import com.tencentcloudapi.asr.v20190614.AsrClient;
import com.tencentcloudapi.asr.v20190614.models.SentenceRecognitionRequest;
import com.tencentcloudapi.asr.v20190614.models.SentenceRecognitionResponse;
import com.tencentcloudapi.common.Credential;
import com.tencentcloudapi.common.exception.TencentCloudSDKException;
import com.tencentcloudapi.common.profile.ClientProfile;
import com.tencentcloudapi.common.profile.HttpProfile;
import com.tencentcloudapi.vod.v20180717.VodClient;
import com.tencentcloudapi.vod.v20180717.models.*;
import jodd.util.StringUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;

;
;
;import static com.amway.broadcast.applets.service.redis.AudioVideoRedisKeyStore.MERGE_MEDIA_PREFIX;
import static com.amway.broadcast.applets.service.redis.AudioVideoRedisKeyStore.MERGE_MEDIA_TASK_PREFIX;

/**
 * @auther 薛晨
 * @date 2020/12/16
 * @time 10:37
 * @description
 */
@Service
@Slf4j
public class AudioVideoServiceImpl implements AudioVideoService {

    @Value("${auditVideo.secretId}")
    private String secretId;

    @Value("${auditVideo.secretKey}")
    private String secretKey;

    @Value("${auditVideo.subAppId}")
    private String subAppId;

    @Value("${auditVideo.cosshDomain}")
    private String cosshDomain;

    private static final String SPLIT = "com/";
    private static final String HTTPS = "https://";

    /**
     * 一句话 语音转中文
     *
     * @param mp3Url
     * @return
     */
    @Override
    public String recognition(String mp3Url) {
        try {
            Credential cred = new Credential(secretId, secretKey);
            HttpProfile httpProfile = new HttpProfile();
            httpProfile.setEndpoint("asr.tencentcloudapi.com");
            ClientProfile clientProfile = new ClientProfile();
            clientProfile.setHttpProfile(httpProfile);
            AsrClient client = new AsrClient(cred, "", clientProfile);
            SentenceRecognitionRequest req = new SentenceRecognitionRequest();
            req.setProjectId(0L);
            req.setSubServiceType(2L);
            req.setEngSerViceType("16k_zh");
            req.setSourceType(0L);
            req.setVoiceFormat("mp3");
            req.setUrl(mp3Url);
            req.setUsrAudioKey("amway-recognition-" + System.currentTimeMillis());
            SentenceRecognitionResponse resp = client.SentenceRecognition(req);
            return resp.getResult();
        } catch (TencentCloudSDKException e) {
            log.info(e.toString());
            throw new OverallException("语音识别出错");
        }
    }

    @Override
    public String merge(String media_strs) {
        String[] sourceUrls = media_strs.split(",");
        log.info("原始sourceUrls{}", sourceUrls);
        try {
            Credential cred = new Credential(secretId, secretKey);
            HttpProfile httpProfile = new HttpProfile();
            httpProfile.setEndpoint("vod.tencentcloudapi.com");
            ClientProfile clientProfile = new ClientProfile();
            clientProfile.setHttpProfile(httpProfile);
            VodClient client = new VodClient(cred, "", clientProfile);
            ComposeMediaRequest req = new ComposeMediaRequest();
            MediaTrack[] mediaTracks1 = new MediaTrack[1];
            MediaTrack mediaTrack1 = new MediaTrack();
            MediaTrackItem[] mediaTrackItems1 = new MediaTrackItem[sourceUrls.length];
            for (int i = 0; i < sourceUrls.length; i++) {
                MediaTrackItem mediaTrackItem1 = new MediaTrackItem();
                AudioTrackItem audioTrackItem1 = new AudioTrackItem();
                audioTrackItem1.setSourceMedia(sourceUrls[i]);
                mediaTrackItem1.setAudioItem(audioTrackItem1);
                mediaTrackItem1.setType("Audio");
                mediaTrackItems1[i] = mediaTrackItem1;
            }
            mediaTrack1.setTrackItems(mediaTrackItems1);
            mediaTrack1.setType("Audio");
            mediaTracks1[0] = mediaTrack1;
            req.setTracks(mediaTracks1);
            ComposeMediaOutput composeMediaOutput1 = new ComposeMediaOutput();
            composeMediaOutput1.setFileName("amway-merge-" + System.currentTimeMillis());
            req.setOutput(composeMediaOutput1);
            req.setSubAppId(Long.valueOf(subAppId));
            log.info("请求参数{}", req);
            ComposeMediaResponse resp = client.ComposeMedia(req);
            return resp.getTaskId();
        } catch (TencentCloudSDKException e) {
            log.error("获取语音拼接ID出错", e);
            throw new OverallException("获取语音拼接ID出错");
        }
    }

    @Override
    public String saveVideo(String video_url) {
        try {
            Credential cred = new Credential(secretId, secretKey);
            HttpProfile httpProfile = new HttpProfile();
            httpProfile.setEndpoint("vod.tencentcloudapi.com");
            ClientProfile clientProfile = new ClientProfile();
            clientProfile.setHttpProfile(httpProfile);
            VodClient client = new VodClient(cred, "", clientProfile);
            PullUploadRequest req = new PullUploadRequest();
            req.setMediaUrl(transform(video_url));
            req.setSubAppId(Long.valueOf(subAppId));
            PullUploadResponse resp = client.PullUpload(req);
            log.info("视频上传{}", PullUploadResponse.toJsonString(resp));
            return resp.getTaskId();
        } catch (TencentCloudSDKException e) {
            log.error("视频上传失败", e);
            throw new OverallException("视频上传出错");
        }
    }


    private String transform(String media_strs) {
        String substring = media_strs.substring(media_strs.substring(0, media_strs.indexOf(SPLIT)).length() + SPLIT.length());
        String str3 = substring.substring(0, substring.indexOf('/'));
        String str4 = substring.substring(str3.length());
        String changeUrl = HTTPS + str3 + "." + cosshDomain + str4;
        log.info("url地址转换:{},-> {}", media_strs, changeUrl);
        return changeUrl;
    }

    @Override
    public String getSignature() {
        Signature sign = new Signature();
        // 设置 App 的云 API 密钥
        sign.setSecretId(secretId);
        sign.setSecretKey(secretKey);
        sign.setCurrentTime(System.currentTimeMillis() / 1000);
        sign.setRandom(new Random().nextInt(java.lang.Integer.MAX_VALUE));
        sign.setSignValidDuration(3600 * 24 * 2); // 签名有效期：2天
        sign.setSubAppId(Long.parseLong(subAppId));
        try {
            String signature = sign.getUploadSignature();
            log.info("signature : " + signature);
            return signature;
        } catch (Exception e) {
            throw new OverallException("获取签名出错");
        }
    }

    @Override
    public String audit(String fileId) {
        try {
            Credential cred = new Credential(secretId, secretKey);
            HttpProfile httpProfile = new HttpProfile();
            httpProfile.setEndpoint("vod.tencentcloudapi.com");
            ClientProfile clientProfile = new ClientProfile();
            clientProfile.setHttpProfile(httpProfile);
            VodClient client = new VodClient(cred, "", clientProfile);
            ProcessMediaRequest req = new ProcessMediaRequest();
            AiContentReviewTaskInput aiContentReviewTaskInput1 = new AiContentReviewTaskInput();
            aiContentReviewTaskInput1.setDefinition(getDefinitionId());
            req.setAiContentReviewTask(aiContentReviewTaskInput1);
            req.setSubAppId(Long.valueOf(subAppId));
            req.setFileId(fileId);
            ProcessMediaResponse resp = client.ProcessMedia(req);
            log.info("审核{}", ProcessMediaResponse.toJsonString(resp));
            return resp.getTaskId();
        } catch (TencentCloudSDKException e) {
            log.info("审核{}", e.toString());
            throw new OverallException("审核出错");
        }
    }

    @Override
    public AuditResultVO auditResult(String taskId) {
        try {
            Credential cred = new Credential(secretId, secretKey);
            HttpProfile httpProfile = new HttpProfile();
            httpProfile.setEndpoint("vod.tencentcloudapi.com");
            ClientProfile clientProfile = new ClientProfile();
            clientProfile.setHttpProfile(httpProfile);
            VodClient client = new VodClient(cred, "", clientProfile);
            DescribeTaskDetailRequest req = new DescribeTaskDetailRequest();
            req.setTaskId(taskId);
            req.setSubAppId(Long.valueOf(subAppId));
            DescribeTaskDetailResponse resp = client.DescribeTaskDetail(req);
            log.info("task结果{}", DescribeTaskDetailResponse.toJsonString(resp));
            String taskType = resp.getTaskType();
            String status = resp.getStatus();
            AuditResultVO vo = new AuditResultVO();
            if ("WAITING".equals(status)) {
                vo.setStatus("WAITING");
                return vo;
            }
            //语音拼接
            if ("ComposeMedia".equals(taskType)) {
                ComposeMediaTask composeMediaTask = resp.getComposeMediaTask();
                vo.setErrCode(composeMediaTask.getErrCode());
                vo.setStatus(composeMediaTask.getStatus());
                ComposeMediaTaskOutput output = composeMediaTask.getOutput();
                if (output != null) {
                    vo.setFileId(output.getFileId());
                    vo.setFileUrl(output.getFileUrl());
                }
                //视频上传
            } else if ("Procedure".equals(taskType)) {
                ProcedureTask composeMediaTask = resp.getProcedureTask();
                vo.setErrCode(composeMediaTask.getErrCode());
                vo.setStatus(composeMediaTask.getStatus());
                vo.setFileId(composeMediaTask.getFileId());
                vo.setFileUrl(composeMediaTask.getFileUrl());
            } else if ("WechatMiniProgramPublish".equals(taskType)) {
                WechatMiniProgramPublishTask wechatMiniProgramPublishTask = resp.getWechatMiniProgramPublishTask();
                vo.setErrCode(wechatMiniProgramPublishTask.getErrCode());
                vo.setStatus(wechatMiniProgramPublishTask.getStatus());
                vo.setFileId(wechatMiniProgramPublishTask.getFileId());
                vo.setPublishResult(wechatMiniProgramPublishTask.getPublishResult());
            } else if ("PullUpload".equals(taskType)) {
                PullUploadTask pullUploadTask = resp.getPullUploadTask();
                vo.setErrCode(pullUploadTask.getErrCode());
                vo.setStatus(pullUploadTask.getStatus());
                vo.setFileId(pullUploadTask.getFileId());
                vo.setFileUrl(pullUploadTask.getFileUrl());
            } else if ("EditMedia".equals(taskType)) {
                EditMediaTask pullUploadTask = resp.getEditMediaTask();
                vo.setErrCode(pullUploadTask.getErrCode());
                vo.setStatus(pullUploadTask.getStatus());
                EditMediaTaskOutput output = pullUploadTask.getOutput();
                if (output != null) {
                    vo.setFileId(output.getFileId());
                    vo.setFileUrl(output.getFileUrl());
                }
            }
            return vo;
        } catch (TencentCloudSDKException e) {
            log.info("task结果{}", e.toString());
            throw new OverallException("获取结果失败");
        }
    }

    @Override
    public String publish(String fileId) {
        try {
            Credential cred = new Credential(secretId, secretKey);
            HttpProfile httpProfile = new HttpProfile();
            httpProfile.setEndpoint("vod.tencentcloudapi.com");
            ClientProfile clientProfile = new ClientProfile();
            clientProfile.setHttpProfile(httpProfile);
            VodClient client = new VodClient(cred, "", clientProfile);
            WeChatMiniProgramPublishRequest req = new WeChatMiniProgramPublishRequest();
            req.setSubAppId(Long.valueOf(subAppId));
            req.setFileId(fileId);
            WeChatMiniProgramPublishResponse resp = client.WeChatMiniProgramPublish(req);
            log.info("微信小程序视频publish{}", WeChatMiniProgramPublishResponse.toJsonString(resp));
            return resp.getTaskId();
        } catch (TencentCloudSDKException e) {
            log.info(e.toString());
            throw new OverallException("发布出错");
        }
    }

    /**
     * 返回自定义taskId
     *
     * @param media_strs
     * @return
     */
    @Override
    public Integer uploadAudio(String media_strs) {
        try {
            String[] sourceUrls = media_strs.split(",");
            log.info("sourceUrls:{}", sourceUrls);
            String[] targetTaskId = new String[sourceUrls.length];
            for (int i = 0; i < sourceUrls.length; i++) {
                String taskId = saveVideo(sourceUrls[i]);
                targetTaskId[i] = taskId;
            }
            String taskIds = String.join(",", targetTaskId);
            log.info("taskIds:{}", taskIds);
            int hashTaskId = HashUtil.javaDefaultHash(taskIds);
            //1:n
            RedisUtil.set(MERGE_MEDIA_PREFIX + hashTaskId, taskIds);
            return hashTaskId;
        } catch (Exception e) {
            log.error("上传音频失败", e);
            throw new OverallException("上传音频出错");
        }
    }

    /**
     * * 1、任意一个子taskId未完成 则总体PROCESSING
     * * 2、任意一个子taskId有问题，则总体FINISH
     * 判断音频上传是否成功
     * true代表已经拿到merge的taskId
     *
     * @param hashTaskId
     */
    private boolean buildMergeTaskId(int hashTaskId) {
        String taskIdStr = RedisUtil.get(MERGE_MEDIA_PREFIX + hashTaskId);
        String[] taskIds = taskIdStr.split(",");
        String[] fileUrls = new String[taskIds.length];
        for (int i = 0; i < taskIds.length; i++) {
            String originTaskId = taskIds[i];
            AuditResultVO vo = auditResult(originTaskId);
            if (vo != null) {
                Long errCode = vo.getErrCode();
                if (errCode != null && errCode.intValue() != 0) {
                    throw new OverallException("merge error");
                } else {
                    String status = vo.getStatus();
                    if (!"FINISH".equals(status)) {
                        return false;
                    }
                    String fileUrl = vo.getFileId();
                    fileUrls[i] = fileUrl;
                }
            } else {
                throw new OverallException("merge error");
            }
        }
        //至此则表示所有音频已全部上传至vod
        String mergeTaskId = editMedia(String.join(",", fileUrls));
        if (StringUtil.isNotEmpty(mergeTaskId)) {
            RedisUtil.set(MERGE_MEDIA_TASK_PREFIX + hashTaskId, mergeTaskId);
        } else {
            log.error("merge有问题{}", hashTaskId);
            throw new OverallException("merge error");
        }
        return true;
    }

    /**
     * @param hashTaskId
     * @return
     */
    @Override
    public AuditResultVO mergeMedia(Integer hashTaskId) {
        AuditResultVO result = new AuditResultVO();
        String mergeTaskId = RedisUtil.get(MERGE_MEDIA_TASK_PREFIX + hashTaskId);
        if (StringUtil.isNotEmpty(mergeTaskId)) {
            return auditResult(mergeTaskId);
        } else {
            boolean canILoop = buildMergeTaskId(hashTaskId);
            if (canILoop) {
                mergeTaskId = RedisUtil.get(MERGE_MEDIA_TASK_PREFIX + hashTaskId);
                AuditResultVO vo = auditResult(mergeTaskId);
                if ("FINISH".equals(vo.getStatus())) {
                    RedisUtil.del(MERGE_MEDIA_TASK_PREFIX + hashTaskId);
                    RedisUtil.del(MERGE_MEDIA_PREFIX + hashTaskId);
                    log.info("终于完成了");
                }
                return vo;
            } else {
                result.setStatus("PROCESSING");
                return result;
            }
        }
    }

    @Override
    public String editMedia(String fileIds) {
        String[] sourceUrls = fileIds.split(",");
        try {
            Credential cred = new Credential(secretId, secretKey);
            HttpProfile httpProfile = new HttpProfile();
            httpProfile.setEndpoint("vod.tencentcloudapi.com");
            ClientProfile clientProfile = new ClientProfile();
            clientProfile.setHttpProfile(httpProfile);
            VodClient client = new VodClient(cred, "", clientProfile);
            EditMediaRequest req = new EditMediaRequest();
            EditMediaFileInfo[] editMediaFileInfos1 = new EditMediaFileInfo[sourceUrls.length];
            for (int i = 0; i < sourceUrls.length; i++) {
                EditMediaFileInfo editMediaFileInfo1 = new EditMediaFileInfo();
                editMediaFileInfo1.setFileId(sourceUrls[i]);
                editMediaFileInfos1[i] = editMediaFileInfo1;
            }
            req.setSubAppId(Long.parseLong(subAppId));
            req.setFileInfos(editMediaFileInfos1);
            EditMediaOutputConfig editMediaOutputConfig1 = new EditMediaOutputConfig();
            editMediaOutputConfig1.setMediaName("amway-merge-" + System.currentTimeMillis());
            req.setOutputConfig(editMediaOutputConfig1);
            req.setInputType("File");
            EditMediaResponse resp = client.EditMedia(req);
            return resp.getTaskId();
        } catch (TencentCloudSDKException e) {
            throw new OverallException("视频编辑出错");
        }
    }

    private Long getDefinitionId() {
        try {
            Credential cred = new Credential(secretId, secretKey);
            HttpProfile httpProfile = new HttpProfile();
            httpProfile.setEndpoint("vod.tencentcloudapi.com");
            ClientProfile clientProfile = new ClientProfile();
            clientProfile.setHttpProfile(httpProfile);
            VodClient client = new VodClient(cred, "", clientProfile);
            DescribeContentReviewTemplatesRequest req = new DescribeContentReviewTemplatesRequest();
            req.setSubAppId(Long.valueOf(subAppId));
            DescribeContentReviewTemplatesResponse resp = client.DescribeContentReviewTemplates(req);
            log.info("获取模板id{}", DescribeContentReviewTemplatesResponse.toJsonString(resp));
            ContentReviewTemplateItem[] contentReviewTemplateSet = resp.getContentReviewTemplateSet();
            if (contentReviewTemplateSet != null && contentReviewTemplateSet.length > 0) {
                for (int i = 0; i < contentReviewTemplateSet.length; i++) {
                    ContentReviewTemplateItem contentReviewTemplateItem = contentReviewTemplateSet[i];
                    String name = contentReviewTemplateItem.getName();
                    if ("安利视频审核模板".equals(name)) {
                        return contentReviewTemplateItem.getDefinition();
                    }
                }

            }
            return createDefinition();
        } catch (TencentCloudSDKException e) {
            log.info("获取模板id{}", e.toString());
        }
        return null;
    }

    /**
     * 创建视频内容智能识别模版
     *
     * @return
     */
    private Long createDefinition() {
        try {
            Credential cred = new Credential(secretId, secretKey);
            HttpProfile httpProfile = new HttpProfile();
            httpProfile.setEndpoint("vod.tencentcloudapi.com");
            ClientProfile clientProfile = new ClientProfile();
            clientProfile.setHttpProfile(httpProfile);
            VodClient client = new VodClient(cred, "", clientProfile);
            CreateContentReviewTemplateRequest req = new CreateContentReviewTemplateRequest();
            req.setReviewWallSwitch("OFF");
            req.setName("安利视频审核模板");
            CreateContentReviewTemplateResponse resp = client.CreateContentReviewTemplate(req);
            log.info("创建模板{}", CreateContentReviewTemplateResponse.toJsonString(resp));
            return resp.getDefinition();
        } catch (TencentCloudSDKException e) {
            log.info("创建模板", e.toString());
        }
        return null;
    }

    // 全部开启的审核模板
    public static void main(String[] args) {
        try {

            Credential cred = new Credential("AKIDzyhBMQqjy0mLfGGjdIqKci3B9xXgEWMU", "5uZ0HIFATXM3d6JG5pJXWHmGVbagW6wY");

            HttpProfile httpProfile = new HttpProfile();
            httpProfile.setEndpoint("vod.tencentcloudapi.com");

            ClientProfile clientProfile = new ClientProfile();
            clientProfile.setHttpProfile(httpProfile);

            VodClient client = new VodClient(cred, "", clientProfile);

            CreateContentReviewTemplateRequest req = new CreateContentReviewTemplateRequest();
            PornConfigureInfo pornConfigureInfo1 = new PornConfigureInfo();
            PornImgReviewTemplateInfo pornImgReviewTemplateInfo1 = new PornImgReviewTemplateInfo();

            String[] labelSet1 = {"porn", "vulgar", "intimacy", "sexy"};
            pornImgReviewTemplateInfo1.setLabelSet(labelSet1);

            pornImgReviewTemplateInfo1.setSwitch("ON");
            pornConfigureInfo1.setImgReviewInfo(pornImgReviewTemplateInfo1);

            PornAsrReviewTemplateInfo pornAsrReviewTemplateInfo1 = new PornAsrReviewTemplateInfo();
            pornAsrReviewTemplateInfo1.setSwitch("ON");
            pornConfigureInfo1.setAsrReviewInfo(pornAsrReviewTemplateInfo1);

            PornOcrReviewTemplateInfo pornOcrReviewTemplateInfo1 = new PornOcrReviewTemplateInfo();
            pornOcrReviewTemplateInfo1.setSwitch("ON");
            pornConfigureInfo1.setOcrReviewInfo(pornOcrReviewTemplateInfo1);

            req.setPornConfigure(pornConfigureInfo1);

            TerrorismConfigureInfo terrorismConfigureInfo1 = new TerrorismConfigureInfo();
            TerrorismImgReviewTemplateInfo terrorismImgReviewTemplateInfo1 = new TerrorismImgReviewTemplateInfo();

            String[] labelSet2 = {"guns", "crowd", "bloody", "police", "banners", "militant", "explosion", "terrorists"};
            terrorismImgReviewTemplateInfo1.setLabelSet(labelSet2);

            terrorismImgReviewTemplateInfo1.setSwitch("ON");
            terrorismConfigureInfo1.setImgReviewInfo(terrorismImgReviewTemplateInfo1);

            TerrorismOcrReviewTemplateInfo terrorismOcrReviewTemplateInfo1 = new TerrorismOcrReviewTemplateInfo();
            terrorismOcrReviewTemplateInfo1.setSwitch("ON");
            terrorismConfigureInfo1.setOcrReviewInfo(terrorismOcrReviewTemplateInfo1);

            req.setTerrorismConfigure(terrorismConfigureInfo1);

            PoliticalConfigureInfo politicalConfigureInfo1 = new PoliticalConfigureInfo();
            PoliticalImgReviewTemplateInfo politicalImgReviewTemplateInfo1 = new PoliticalImgReviewTemplateInfo();

            String[] labelSet3 = {"violation_photo", "politician", "entertainment", "sport", "entrepreneur", "scholar", "celebrity", "military"};
            politicalImgReviewTemplateInfo1.setLabelSet(labelSet3);

            politicalImgReviewTemplateInfo1.setSwitch("ON");
            politicalConfigureInfo1.setImgReviewInfo(politicalImgReviewTemplateInfo1);

            PoliticalAsrReviewTemplateInfo politicalAsrReviewTemplateInfo1 = new PoliticalAsrReviewTemplateInfo();
            politicalAsrReviewTemplateInfo1.setSwitch("ON");
            politicalConfigureInfo1.setAsrReviewInfo(politicalAsrReviewTemplateInfo1);

            PoliticalOcrReviewTemplateInfo politicalOcrReviewTemplateInfo1 = new PoliticalOcrReviewTemplateInfo();
            politicalOcrReviewTemplateInfo1.setSwitch("ON");
            politicalConfigureInfo1.setOcrReviewInfo(politicalOcrReviewTemplateInfo1);

            req.setPoliticalConfigure(politicalConfigureInfo1);

            ProhibitedConfigureInfo prohibitedConfigureInfo1 = new ProhibitedConfigureInfo();
            ProhibitedAsrReviewTemplateInfo prohibitedAsrReviewTemplateInfo1 = new ProhibitedAsrReviewTemplateInfo();
            prohibitedAsrReviewTemplateInfo1.setSwitch("ON");
            prohibitedConfigureInfo1.setAsrReviewInfo(prohibitedAsrReviewTemplateInfo1);

            ProhibitedOcrReviewTemplateInfo prohibitedOcrReviewTemplateInfo1 = new ProhibitedOcrReviewTemplateInfo();
            prohibitedOcrReviewTemplateInfo1.setSwitch("ON");
            prohibitedConfigureInfo1.setOcrReviewInfo(prohibitedOcrReviewTemplateInfo1);

            req.setProhibitedConfigure(prohibitedConfigureInfo1);

            req.setSubAppId(1500003094L);
            req.setReviewWallSwitch("OFF");
            req.setName("安利视频审核模板");
            req.setComment("安利视频审核模板");

            CreateContentReviewTemplateResponse resp = client.CreateContentReviewTemplate(req);

            log.info("创建模板{}", CreateContentReviewTemplateResponse.toJsonString(resp));
        } catch (TencentCloudSDKException e) {
            log.info("创建模板{}", e.toString());
        }

    }
}
