package com.amway.broadcast.applets.service.api;

import com.amway.broadcast.applets.service.api.service.AppletsAnchorService;
import com.amway.broadcast.applets.service.common.annotation.ServiceTokenRequired;
import com.amway.broadcast.applets.service.paramsEntity.AnchorLiveCreateReq;
import com.amway.broadcast.applets.service.paramsEntity.AnchorLiveDisbandReq;
import com.amway.broadcast.common.base.Result;
import com.amway.broadcast.common.constant.HeadTokenConstant;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;

/**
 * 小程序-->主播端
 */
@RestController
@CrossOrigin
@Slf4j
@Api(value = "小程序", tags = {"主播端"})
@Validated
public class ApiAppletsAnchorController {

    @Resource
    private AppletsAnchorService appletsAnchorService;
    /**
     * 畅聊间首页
     * @param groupId 	畅聊间id
     * @return
     */
    @ApiOperation(value = "畅聊间首页", response = Result.class)
    @GetMapping("live_index")
    @ServiceTokenRequired
    public Result liveIndex(@NotBlank(message = "畅聊间id不能为空")  @RequestParam("group_id") String groupId){
        return  appletsAnchorService.liveIndex(groupId);
    }


    /**
     * 首页-全部畅聊间
     * @return
     */
    @ApiOperation(value = "全部畅聊间", response = Result.class)
    @GetMapping("index")
    @ServiceTokenRequired
    public Result index(@RequestHeader(HeadTokenConstant.WX_APP_API_TOKEN) String wxAppApiToken,
                        @NotBlank(message = "畅聊间状态不能为空")   @Pattern(regexp = "^[12]$", message = "畅聊间状态有误") @RequestParam("status") String status){
        return appletsAnchorService.index(wxAppApiToken, status);
    }


    /**
     * 畅聊间列表
     * @param wxAppApiToken 小程序端请求服务器端的凭证
     * @param status 	状态 1：待开始 2：进行中
     * @return
     */
    @ApiOperation(value = "畅聊间列表", response = Result.class)
    @GetMapping("live_list")
    @ServiceTokenRequired
    public Result liveList(@RequestHeader(HeadTokenConstant.WX_APP_API_TOKEN) String wxAppApiToken,
                            @NotBlank(message = "畅聊间状态不能为空")   @Pattern(regexp = "^[12]$", message = "畅聊间状态有误") @RequestParam("status") String status,
                           @NotBlank(message = "时间区域选择不能为空")   @Pattern(regexp = "^[012]$", message = "时间区域选择状态有误") @RequestParam("all_time") String allTime,
                            @NotBlank(message = "时间选择不能为空")   @Pattern(regexp = "^[012]$", message = "时间选择状态有误") @RequestParam("is_today") String isToday){
        return appletsAnchorService.liveList(wxAppApiToken, status,allTime,isToday);
    }


    /**
     * 畅聊间分享
     * @param groupId 	畅聊间id
     * @param path 	路径
     * @return
     */
    @ApiOperation(value = "畅聊间分享", response = Result.class)
    @GetMapping("live_share")
    @ServiceTokenRequired
    //@RepeatSubmit(type = RepeatSubmit.Type.NORMAL)
    public Result liveShare(@NotBlank(message = "畅聊间id不能为空") @Pattern(regexp = "^\\d+$", message = "畅聊间id格式不正确") @RequestParam("group_id")String groupId,
                             @NotBlank(message = "分享页路径不能为空") @RequestParam("path")String path){
        return appletsAnchorService.liveShare(groupId, path);
    }

    /**
     * 发起畅聊间
     * @param anchorLiveCreateReq 入参实体类
     * @return
     */
    @ApiOperation(value = "发起畅聊", response = Result.class)
    @PostMapping("live_create")
    @ServiceTokenRequired
    public Result liveCreate(@Validated AnchorLiveCreateReq anchorLiveCreateReq, @RequestHeader(HeadTokenConstant.WX_APP_API_TOKEN)String wxAppApiToken){
        return appletsAnchorService.liveCreate(anchorLiveCreateReq, wxAppApiToken);
    }

    /**
     * 手动解散畅聊群
     * @param anchorLiveDisbandReq
     * @return
     */
    @ApiOperation(value = "手动解散畅聊群", response = Result.class)
    @PostMapping("live_disband")
    @ServiceTokenRequired
    //@RepeatSubmit(type = RepeatSubmit.Type.NORMAL)
    public Result liveDisband(@Validated AnchorLiveDisbandReq anchorLiveDisbandReq){
        return appletsAnchorService.liveDisband(anchorLiveDisbandReq);
    }

    /**
     * 修改畅聊间转发权限
     * @param
     * @return
     */
    @ApiOperation(value = "修改畅聊间转发权限", response = Result.class)
    @GetMapping("live_setLimit")
    @ServiceTokenRequired
    //@RepeatSubmit(type = RepeatSubmit.Type.NORMAL)
    public Result live_setLimit(@NotBlank(message = "畅聊间id不能为空")  @Pattern(regexp = "^\\d+$", message = "畅聊间id格式不正确") @RequestParam("group_id") String groupId,
                                @NotBlank(message = "转发权限类型不能为空")  @Pattern(regexp = "^[12]$", message = "转发权限类型输入有误") @RequestParam("limit_type") String limitType){
        return appletsAnchorService.liveSetLimit(groupId, limitType);
    }
    /**
     * 获取分享需要的activity_id
     * @param
     * @return
     */
    @ApiOperation(value = "获取分享需要的activity_id", response = Result.class)
    @GetMapping("getActivity_id")
    @ServiceTokenRequired
   // @RepeatSubmit(type = RepeatSubmit.Type.NORMAL)
    public Result getActivity_id(){
        return appletsAnchorService.getActivityId();
    }

    /**
     * 畅聊间数量限制
     * @param
     * @return
     */
    @ApiOperation(value = "播间数量限制", response = Result.class)
    @GetMapping("live_count_limit")
    @ServiceTokenRequired
    //@RepeatSubmit(type = RepeatSubmit.Type.NORMAL)
    public Result liveCountLimit(@RequestHeader(HeadTokenConstant.WX_APP_API_TOKEN)String wxAppApiToken){
        return appletsAnchorService.liveCountLimit(wxAppApiToken);
    }

    /**
     * 畅聊间消息敏感词过滤
     * @param
     * @return
     */
    @ApiOperation(value = "畅聊间消息敏感词过滤", response = Result.class)
    @GetMapping("msg_check")
    @ServiceTokenRequired
    //@RepeatSubmit(type = RepeatSubmit.Type.NORMAL)
    public Result msgCheck(@RequestHeader(HeadTokenConstant.WX_APP_API_TOKEN)String wxAppApiToken, @NotBlank(message = "消息内容不能为空") @RequestParam("words")String words){
        return appletsAnchorService.msgCheck(wxAppApiToken, words);
    }

    /**
     * 获取全局配置校验
     * @return
     */
    @ApiOperation(value = "全局配置", response = Result.class)
    @GetMapping("page_setting")
    @ServiceTokenRequired
    public Result pageSetting(@RequestHeader(HeadTokenConstant.WX_APP_API_TOKEN)String wxAppApiToken){
        return appletsAnchorService.pageSetting(wxAppApiToken);
    }

}
