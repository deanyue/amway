//package com.amway.broadcast.applets.service.api.service.impl;
//
//import com.amway.broadcast.applets.service.dao.entity.AmwayGroupUser;
//import com.amway.broadcast.common.service.AmwayGroupUserService;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.stereotype.Service;
//
//import java.util.List;
//
///**
// * @auther 薛晨
// * @date 2020/12/12
// * @time 13:55
// * @description
// */
//@Service
//public class AppletsAmwayGroupUserServiceImpl implements AmwayGroupUserService<AmwayGroupUser> {
//
//    @Autowired
//    private AppletsAmwayGroupUserDBServiceImpl dbService;
//
//    @Autowired
//    private AppletsAmwayGroupUserRedisServiceImpl redisService;
//
//    @Override
//    public int insert(AmwayGroupUser baseAmwayGroupUser) {
//        return 0;
//    }
//
//    @Override
//    public int update(AmwayGroupUser baseAmwayGroupUser) {
//        redisService.update(baseAmwayGroupUser);
//        dbService.update(baseAmwayGroupUser);
//        return 0;
//    }
//
//    @Override
//    public List<AmwayGroupUser> getListByCondition(AmwayGroupUser condition) {
//        return null;
//    }
//
//    @Override
//    public AmwayGroupUser getOne(String groupId, String userId) {
//        AmwayGroupUser one = redisService.getOne(groupId, userId);
//        if (one != null) {
//            return one;
//        } else {
//            return dbService.getOne(groupId, userId);
//        }
//    }
//
//    @Override
//    public List<AmwayGroupUser> getByGroupIdAndStatus(String groupId, Integer quitStatus) {
//        List<AmwayGroupUser> list = redisService.getByGroupIdAndStatus(groupId, quitStatus);
//        if (list.isEmpty()) {
//            return dbService.getByGroupIdAndStatus(groupId, quitStatus);
//        }
//        return list;
//    }
//}
