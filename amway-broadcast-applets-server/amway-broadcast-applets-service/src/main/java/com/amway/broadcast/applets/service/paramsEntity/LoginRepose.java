package com.amway.broadcast.applets.service.paramsEntity;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Builder;
import lombok.Data;

/**
 * @author WCX
 * @date 2020/11/3
 */
@Data
@Builder
public class LoginRepose {

    //小程序端请求服务器端的凭证
    private String wxapp_api_token;

    //用户是否已授权 0否1是
    private long is_auth;

    /**
     * 用户身份
     * 0：用户
     * 1：主播
     */
    @JsonProperty("user_identity")
    private String userIdentity;

    /**
     * 用户进入直播间标识
     * 0：未进入
     * 1：已进入
     */
    @JsonProperty("user_into")
    private String userInto;
}
