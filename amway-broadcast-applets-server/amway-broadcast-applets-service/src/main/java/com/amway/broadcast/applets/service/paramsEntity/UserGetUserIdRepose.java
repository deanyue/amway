package com.amway.broadcast.applets.service.paramsEntity;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

/**
 * 企业微信自动登录 返回repose
 */
@Data
public class UserGetUserIdRepose {
    /**
     * 成员id号，department_member表中的id
     */
    @JsonProperty("member_id")
    private Long memberId;
}
