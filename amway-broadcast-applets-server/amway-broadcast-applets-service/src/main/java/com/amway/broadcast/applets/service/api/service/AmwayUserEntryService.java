package com.amway.broadcast.applets.service.api.service;
import com.amway.broadcast.applets.service.paramsEntity.UserEntryReq;
import com.amway.broadcast.applets.service.paramsEntity.UserQuitReq;
import com.amway.broadcast.common.base.Result;

public interface AmwayUserEntryService {

    /**
     * 用户进入直播间
     * @param userEntryReq
     * @return
     */
    Result liveJoin(UserEntryReq userEntryReq, String wxAppApiToken);

    /**
     * 用户退出直播间
     * @param userQuitReq
     * @return
     */
    Result liveQuit(UserQuitReq userQuitReq,String wxAppApiToken);

    /**
     * 直播间列表
     * @param wxAppApiToken
     * @param status
     * @param isToday
     * @return
     */
    Result userLiveList(String wxAppApiToken, String status,String allTime, String isToday);

    /**
     * 直播群搜索用户
     * @param wxAppApiToken
     * @param nickName
     * @return
     */
    Result searchUser(String wxAppApiToken, String nickName);
}
