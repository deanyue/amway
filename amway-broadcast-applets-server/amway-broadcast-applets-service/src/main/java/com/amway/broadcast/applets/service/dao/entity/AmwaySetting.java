package com.amway.broadcast.applets.service.dao.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

/**
 * 系统设置表
 */
@Getter
@Setter
@TableName(value = "amway_setting")
public class AmwaySetting implements Serializable {

    private static final long serialVersionUID = 5838009308045908934L;

    @TableId(value = "id")
    private Long id;

    //每人每天最多开多少场直播
    @TableField(value = "max_group_per_day")
    private Integer maxGroupPerDay;

    //每人最多开启多少场直播
    @TableField(value = "max_group_total")
    private Integer maxGroupTotal;

    //群管理员上限
    @TableField(value = "max_admin_num")
    private Integer maxAdminNun;

    //是否开启敏感词校验 0：否 1：是
    @TableField(value = "open_msg_check")
    private Integer openMsgCheck;

    //是否开启视频上传 0：否 1：是
    @TableField(value = "open_video_upload")
    private Integer openVideoUpload;

    /**
     * 上传视频说明
     */
    @TableField(value = "video_text")
    private String videoText;
}
