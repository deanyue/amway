package com.amway.broadcast.applets.service.paramsEntity;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

/**
 * 获取分享需要的activity_id  reopse参数
 */
@Data
public class AnchorActivityIdRepose {

    @JsonProperty("activity_id")
    private String activityId;
}
