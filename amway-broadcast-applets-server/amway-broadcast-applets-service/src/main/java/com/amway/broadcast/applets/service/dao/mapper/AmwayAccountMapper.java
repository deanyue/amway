package com.amway.broadcast.applets.service.dao.mapper;

import com.amway.broadcast.applets.service.dao.entity.AmwayAccount;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

public interface AmwayAccountMapper extends BaseMapper<AmwayAccount> {
}
