package com.amway.broadcast.applets.service.api.service.impl;

import com.amway.broadcast.applets.service.api.service.AmwayManagerService;
import com.amway.broadcast.applets.service.common.constant.AdminOperationTypeEnum;
import com.amway.broadcast.applets.service.dao.entity.*;
import com.amway.broadcast.applets.service.dao.mapper.AmwayGroupUserLogMapper;
import com.amway.broadcast.applets.service.paramsEntity.AdminAuthDetailsRepose;
import com.amway.broadcast.applets.service.paramsEntity.AdminListRepose;
import com.amway.broadcast.applets.service.paramsEntity.AdminSetAuthReq;
import com.amway.broadcast.applets.service.service.AmwayGroupService;
import com.amway.broadcast.applets.service.service.AmwayGroupUserService;
import com.amway.broadcast.applets.service.paramsEntity.*;
import com.amway.broadcast.applets.service.utils.CheckUtils;
import com.amway.broadcast.common.base.Result;
import com.amway.broadcast.common.base.ResultUtils;
import com.amway.broadcast.mybatis.autoconfigure.util.WrapperUtils;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections4.CollectionUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.*;
import java.util.stream.Collectors;

/**
 * @author WCX
 * @date 2020/12/10
 *
 * 管理员列表功能
 * 设置直播间管理员权限
 * 添加管理员权限
 * 添加/取消管理员权限（单人）
 */
@Slf4j
@Service
public class AmwayManagerServiceImpl implements AmwayManagerService {
    @Resource
    private AmwayGroupService amwayGroupService;
    @Resource
    private AmwayGroupUserService amwayGroupUserService;
    @Resource
    private AmwayGroupUserLogMapper amwayGroupUserLogMapper;
    /**
     * 是否退出 0否  1是  2待进入
     */
    private static final Integer NO_QUIT = 0;
    private static final Integer YES_QUIT = 1;

    /**
     * 管理员权限详情
     * @param groupId
     * @return
     *
     * 1.通过groupId获取group表中禁言权限is_all_mute和踢人权限is_kick
     */
    @Override
    public Result getAdminAuthDetails(String groupId) {
        NewAmwayGroup amwayGroup = amwayGroupService.getById(groupId);
        if(CheckUtils.isEmpty(amwayGroup)){
            log.error("通过groupId：{}，查询的直播间不存在",groupId);
            return ResultUtils.error("0","直播间不存在");
        }
        return ResultUtils.success(AdminAuthDetailsRepose.builder()
                .is_all_mute(amwayGroup.getIsAllMute())
                .is_kick(amwayGroup.getIsKick())
                .build());
    }

    /**
     * 管理员列表功能
     * @param groupId
     * @return
     *
     * 1.通过groupId获取group表中禁言权限is_all_mute和踢人权限is_kick
     * 2.通过groupId获取groupUser表中相关管理员信息
     * 3.判断获取的listsize是否达到上限默认五人
     */
    @Override
    public Result getAdminListByGroupId(String groupId) {
        //最终结果封装类
        AdminListRepose adminListRepose = new AdminListRepose();
        //用户详情List
//        List<AdminListUserInfoRepose> list = new ArrayList<>();
        //管理员人数是否已达到上限(0为否，1为是)
        String isAdminLimit = "0";
        // 1
        NewAmwayGroup amwayGroup = amwayGroupService.getById(groupId);
        if(CheckUtils.isEmpty(amwayGroup)){
            log.error("通过groupId：{}，查询的直播间不存在",groupId);
            return ResultUtils.error("0","直播间不存在");
        }
        adminListRepose.setIs_all_mute(amwayGroup.getIsAllMute());
        adminListRepose.setIs_kick(amwayGroup.getIsKick());
        log.debug("直播间：{}，获取的is_all_mute为{}，is_kick为{}",groupId,amwayGroup.getIsAllMute(),amwayGroup.getIsKick());
        // 2
        LambdaQueryWrapper<NewAmwayGroupUser> wrapper = new LambdaQueryWrapper<NewAmwayGroupUser>()
                .eq(NewAmwayGroupUser::getIsManager, "1")
                .eq(NewAmwayGroupUser::getGroupId, groupId);
        List<NewAmwayGroupUser> amwayGroupUserList = amwayGroupUserService.list(wrapper);
        //如果用户畅聊间关联信息不为空，则继续获取用户信息
        if(amwayGroupUserList.size()>0){
            String[] ids=new String[amwayGroupUserList.size()];
            //把amwayGroupUserList对象中的id属性装进ids数组
            int count=0;
            for (NewAmwayGroupUser sp : amwayGroupUserList) {
                ids[count]= sp.getUserId().toString();
                count++;
            }
            adminListRepose.setIds(ids);
            // 3
            if(amwayGroupUserList.size()>=5){
                isAdminLimit="1";
            }

        }

        //组装信息
        adminListRepose.setIs_admin_limit(isAdminLimit);
        return ResultUtils.success(adminListRepose);
    }

    /**
     * 设置直播间管理员权限
     * @param adminSetAuthReq
     * @return
     *
     * 1.通过入参信息，groupId锁定畅聊间对畅聊间进行权限修改
     */
    @Override
    public Result getAdminSetAuth(AdminSetAuthReq adminSetAuthReq) {
        NewAmwayGroup amwayGroup = amwayGroupService.getById(adminSetAuthReq.getGroup_id());
        if(CheckUtils.isEmpty(amwayGroup)){
            log.error("设置直播间管理员权限失败，直播间id为：{}",adminSetAuthReq.getGroup_id());
            return ResultUtils.error("0","畅聊间不存在");
        }
        if(!CheckUtils.isEmpty(adminSetAuthReq.getIs_all_mute()))
            amwayGroup.setIsAllMute(adminSetAuthReq.getIs_all_mute());
        if(!CheckUtils.isEmpty(adminSetAuthReq.getIs_kick()))
            amwayGroup.setIsKick(adminSetAuthReq.getIs_kick());
        if(!amwayGroupService.updateById(amwayGroup))
            return ResultUtils.error("0","修改畅聊间权限失败");
        return ResultUtils.success();
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public Result settingAdminOrCancelByOperationType(String hostUserId, String groupId, List<String> userIdes, AdminOperationTypeEnum operationType) {

        if (CollectionUtils.isEmpty(userIdes)) {
            log.error("调用设置管理员接口，groupId=[{}],userIdes=[{}],operationType=[{}],为空",groupId,userIdes,operationType);
            return ResultUtils.error("0","请至少设置一个用户！");
        }

        if (operationType == null) {
            return ResultUtils.error("0","请至少设置一种操作类型！");
        }
        // 会存在并发问题
        if (operationType.equals(AdminOperationTypeEnum.ADD_ADMIN)) {
            int size = userIdes.size();
            if (size > 5) {
                log.error("调用设置管理员接口，groupId=[{}],userIdes=[{}],operationType=[{}],主播间最多只能设置5个管理员",groupId,userIdes,operationType);
                return ResultUtils.error("0","主播间最多只能设置5个管理员！");
            }
            LambdaQueryWrapper<NewAmwayGroupUser> wrapper = new LambdaQueryWrapper<NewAmwayGroupUser>()
                    .eq(NewAmwayGroupUser::getIsManager, operationType.getOperateType())
                    .eq(NewAmwayGroupUser::getGroupId, groupId);
            List<NewAmwayGroupUser> amwayGroupUsers = amwayGroupUserService.list(wrapper);
            if (CollectionUtils.isNotEmpty(amwayGroupUsers)) {
                Set<Long> userIdSet = amwayGroupUsers.stream().map(NewAmwayGroupUser::getUserId).collect(Collectors.toSet());
                List<Long> uIds = new ArrayList<>();
                userIdes.forEach(s->uIds.add(Long.parseLong(s)));
                userIdSet.addAll(uIds);
                int currAdminNum = userIdSet.size();
                if (currAdminNum > 5) {
                    log.error("调用设置管理员接口，groupId=[{}],userIdes=[{}],operationType=[{}],主播间最多只能设置5个管理员,现主播间已有=[{}]",groupId,userIdes,operationType,amwayGroupUsers.size());
                    return ResultUtils.error("0","主播间最多只能设置5个管理员,现主播间已有" + amwayGroupUsers.size());
                }
            }
        }
        LambdaQueryWrapper<NewAmwayGroupUser> wrapper = new LambdaQueryWrapper<NewAmwayGroupUser>()
                .eq(NewAmwayGroupUser::getGroupId,groupId)
                .in(NewAmwayGroupUser::getUserId,userIdes);
        NewAmwayGroupUser newAmwayGroupUser = new NewAmwayGroupUser();
        newAmwayGroupUser.setIsManager(Integer.parseInt(operationType.getOperateType()));
        boolean result = amwayGroupUserService.update(newAmwayGroupUser, wrapper);
        return result ? ResultUtils.success("设置成功") :ResultUtils.success("设置失败")  ;
    }

    /**
     * 管理员踢人
     * @param adminKickReq
     * @return
     */
    @Override
    public Result adminKick(AdminKickReq adminKickReq) {
        int groupNum = amwayGroupService.count(new LambdaQueryWrapper<NewAmwayGroup>().eq(NewAmwayGroup::getId,adminKickReq.getGroup_id()));
        if(groupNum == 0){
            log.error("管理员踢人adminKick-->当前直播间不存在GroupId{}",adminKickReq.getGroup_id());
            return ResultUtils.error("0", "当前直播间不存在");
        }
        //若根据userid和group_id查询group_user表，判断是否存在，不存在，则用户不在直播间。
        List<AmwayGroupUserLog> amwayGroupUserLogs = amwayGroupUserLogMapper
                .selectList(new LambdaQueryWrapper<AmwayGroupUserLog>()
                        .eq(AmwayGroupUserLog::getGroupId,adminKickReq.getGroup_id())
                        .eq(AmwayGroupUserLog::getUserId,adminKickReq.getUser_id()));
        if(CheckUtils.isEmpty(amwayGroupUserLogs)){
            log.error("管理员踢人adminKick-->未查询到当前直播间下的用户UserId{}，GroupId{}",adminKickReq.getUser_id(), adminKickReq.getGroup_id());
            return ResultUtils.error("0", "当前踢出的用户不存在");
        }

        NewAmwayGroupUser newAmwayGroupUser = amwayGroupUserService
                .getOne(new LambdaQueryWrapper<NewAmwayGroupUser>()
                        .eq(NewAmwayGroupUser::getGroupId,adminKickReq.getGroup_id())
                        .eq(NewAmwayGroupUser::getUserId,adminKickReq.getUser_id()));
        //若为0未退出，则置为1-退出
        if(newAmwayGroupUser != null){
            amwayGroupUserService.removeById(newAmwayGroupUser.getId());
            log.info("管理员踢人adminKick-->踢出成功UserId{}，GroupId{}",adminKickReq.getUser_id(), adminKickReq.getGroup_id());
            return ResultUtils.success("踢出成功");
        } else {
            return ResultUtils.error("0","已踢出该成员，请勿重复操作");
        }
    }
}
