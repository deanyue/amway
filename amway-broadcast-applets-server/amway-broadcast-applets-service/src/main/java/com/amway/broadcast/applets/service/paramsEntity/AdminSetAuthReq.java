package com.amway.broadcast.applets.service.paramsEntity;

import lombok.Data;

import javax.validation.constraints.NotBlank;

/**
 * @author WCX
 * @date 2020/12/10
 * 设置直播间管理员权限接收参数
 */
@Data
public class AdminSetAuthReq {
    /**
     * 直播间id
     */
    @NotBlank(message = "直播间id不能为空")
    private String group_id;

    /**
     * 是否允许管理员设置全体禁言(0为否，1为是)
     */
    private String is_all_mute;

    /**
     * 是否允许管理员踢走成员(0为否，1为是)
     */
    private String is_kick;
}
