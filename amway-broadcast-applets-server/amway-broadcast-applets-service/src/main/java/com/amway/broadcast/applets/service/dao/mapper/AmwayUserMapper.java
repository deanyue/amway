package com.amway.broadcast.applets.service.dao.mapper;

import com.amway.broadcast.applets.service.dao.entity.AmwayUser;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

public interface AmwayUserMapper extends BaseMapper<AmwayUser> {
}
