package com.amway.broadcast.applets.service.paramsEntity;

import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;

/**
 * 手动解散畅聊群
 */
@Data
public class AnchorLiveDisbandReq {

    @NotBlank(message = "畅聊间id不能为空")
    @Pattern(regexp = "^\\d+$", message = "畅聊间id格式不正确")
    private String group_id;
}
