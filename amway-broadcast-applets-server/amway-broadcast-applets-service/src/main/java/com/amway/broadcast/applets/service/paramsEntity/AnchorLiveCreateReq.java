package com.amway.broadcast.applets.service.paramsEntity;

import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;
import java.io.Serializable;

/**
 * 创建畅聊入参req对象
 */
@Data
public class AnchorLiveCreateReq{

    /**
     * 畅聊间名称
     */
    @NotBlank(message = "畅聊间名称不能为空")
    private String name;

    /**
     * 畅聊开始时间
      */
    @NotBlank(message = "畅聊开始时间不能为空")
    @Pattern(regexp = "^((([0-9]{3}[1-9]|[0-9]{2}[1-9][0-9]{1}|[0-9]{1}[1-9][0-9]{2}|[1-9][0-9]{3})-(((0[13578]|1[02])-(0[1-9]|[12][0-9]|3[01]))|((0[469]|11)-(0[1-9]|[12][0-9]|30))|(02-(0[1-9]|[1][0-9]|2[0-8]))))|((([0-9]{2})(0[48]|[2468][048]|[13579][26])|((0[48]|[2468][048]|[3579][26])00))-02-29))\\s+([0-1]?[0-9]|2[0-3]):([0-5][0-9])$", message = "畅聊开始时间格式输入有误(yyyy-MM-dd HH:mm)")
    private  String start_time;

    /**
     * 有效时长，单位：天
      */
    @NotBlank(message = "有效时长不能为空")
    private  String expire;

    /**
     * 收听权限 1：转发可收听 2：转发不可收听
     */
    @NotBlank(message = "收听权限不能为空")
    @Pattern(regexp = "^[12]$", message = "收听权限输入有误,请核实")
    private String limit_type;

    /**
     * im畅聊群id
     */
    @NotBlank(message = "im畅聊群id不能为空")
    private String im_group_id;

    /**
     * 简介
     */
    private String remark;

}
