package com.amway.broadcast.applets.service.service.impl;

import com.amway.broadcast.applets.service.service.AmwayGroupService;
import com.amway.broadcast.applets.service.dao.entity.NewAmwayGroup;
import com.amway.broadcast.applets.service.dao.mapper.AmwayGroupMapper;
import com.amway.broadcast.common.service.impl.BaseServiceImpl;
import org.springframework.stereotype.Service;

/**
 * @auther 薛晨
 * @date 2020/12/14
 * @time 10:14
 * @description
 */
@Service
public class AmwayGroupServiceImpl extends BaseServiceImpl<AmwayGroupMapper, NewAmwayGroup> implements AmwayGroupService {
}
