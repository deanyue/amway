package com.amway.broadcast.applets.service.utils;

import java.io.File;

/**
 * 文件工具类
 */
public class FilesUtils {
    /**
     * 删除文件
     * @param Path
     * @return
     */
    public static  boolean deleteFile(String Path) {
        boolean flag = false;
        File file = new File(Path);
        // 路径为文件且不为空则进行删除
        if (file.isFile() && file.exists()) {
            file.delete();
            flag = true;
        }
        return flag;
    }
}
