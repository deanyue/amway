package com.amway.broadcast.applets.service.paramsEntity;

import com.amway.broadcast.applets.service.dao.entity.AmwayIndex;
import com.amway.broadcast.applets.service.entity.vo.AmwayGroupVO;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.util.List;

/**
 * 首页-全部直播间 返回参数
 */
@Data
public class AnchorIndexRepose {


    @JsonInclude(JsonInclude.Include.NON_NULL)
    // 直播间数量
    private Integer count;

    // 直播间列表
    @JsonProperty("list")
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private List<AmwayIndex> groupList;

    // 直播间信息,此对象仅仅返回直播间首页对象
    @JsonProperty("info")
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private AmwayGroupVO infoList;

    // 直播间列表
    @JsonProperty("list2")
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private List<AmwayIndex> joinGroupList;

}
