package com.amway.broadcast.applets.service.common.aspect;

import com.amway.broadcast.applets.service.utils.CheckUtils;
import com.amway.broadcast.common.constant.HeadTokenConstant;
import com.amway.broadcast.common.exception.user.access.AuthorizationExpiredException;
import com.amway.broadcast.common.exception.user.access.UnauthorizedAccessException;
import com.amway.broadcast.common.util.JwtUtils;
import io.jsonwebtoken.ExpiredJwtException;
import io.jsonwebtoken.SignatureException;
import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.redisson.api.RedissonClient;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;
import org.springframework.web.context.request.RequestAttributes;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

/**
 * @author WCX
 * @date 2020/11/3
 */
@Aspect
@Component
@Slf4j
public class ServiceValidateTokenAspect{

    @Resource
    private RedissonClient redissonClient;

    @Pointcut("@annotation(com.amway.broadcast.applets.service.common.annotation.ServiceTokenRequired)")
    public void validateToken(){}


    /**
     * 验证token
     * @throws Throwable
     * @Author WCX
     */
    @Around("validateToken()")
    public Object around(ProceedingJoinPoint joinPoint) throws Throwable{
        Object result = null;
        RequestAttributes requestAttributes = RequestContextHolder.getRequestAttributes();
        HttpServletRequest request = ((ServletRequestAttributes)requestAttributes).getRequest();
        String token = request.getHeader(HeadTokenConstant.WX_APP_API_TOKEN);
        //拿到redis的key
        String userId = null;
        String redisToken = null;
        //判断是否过期
        try{
            userId = JwtUtils.getRolesFromToken (token);
        }catch(ExpiredJwtException e){
            //过期
            throw new AuthorizationExpiredException("token过期");
        }catch (SignatureException e){
            //JWT签名不匹配
            throw new UnauthorizedAccessException("用户信息错误，请重试");
        }catch (IllegalArgumentException e){
            //JWT 参数不能为null或空
            throw new UnauthorizedAccessException("未找到用户信息，请重试");
        }catch (Exception e){
            throw new UnauthorizedAccessException("用户信息错误，请重试");
        }


        if (CheckUtils.isEmpty (userId)){
            throw new UnauthorizedAccessException("非法用户");
        }
        //校验redis中token是否存在
       /* try {
             redisToken = redissonClient.getBucket(userId).get().toString();
        }catch (Exception e){
            throw new UnauthorizedAccessException("请重新登录");
        }
        //判断是否存在
        if (CheckUtils.isEmpty(redisToken) || !token.equals(redisToken)) {
            throw new UnauthorizedAccessException("非法token");

        }
        //校验redis中token的合法性
        try {
            JwtUtils.getClaimsFromToken(redisToken);
        }catch (Exception e){
            throw new UnauthorizedAccessException("非法token:"+redisToken);
        }*/
            //冗余
     /*       if(JwtUtils.verify(redisToken)){
                Boolean verify = JwtUtils.verify(redisToken);
                log.info("verify:"+verify);
                if(!verify){
                    throw new AuthorizationExpiredException("token过期");
                }
            }*/
            //判断是否刷新token  --废弃  不续时
            /*if(JwtUtils.autoRefreshToken(redisToken)){
                Date date = new Date ();
                redissonClient.getBucket (userId).expire (JwtUtils.generateRedisDate().getTime (), TimeUnit.MILLISECONDS);
            }*/

        result = joinPoint.proceed();
        return result;
    }
}