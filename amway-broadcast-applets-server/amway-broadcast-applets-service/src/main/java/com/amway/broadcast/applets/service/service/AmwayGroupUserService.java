package com.amway.broadcast.applets.service.service;

import com.amway.broadcast.applets.service.dao.entity.NewAmwayGroupUser;
import com.amway.broadcast.common.service.BaseService;

/**
 * @auther 薛晨
 * @date 2020/12/14
 * @time 10:19
 * @description
 */
public interface AmwayGroupUserService extends BaseService<NewAmwayGroupUser> {
}
