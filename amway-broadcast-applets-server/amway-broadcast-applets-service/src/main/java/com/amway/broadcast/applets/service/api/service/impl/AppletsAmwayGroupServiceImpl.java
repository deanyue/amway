//package com.amway.broadcast.applets.service.api.service.impl;
//
//import com.amway.broadcast.applets.service.dao.entity.AmwayGroup;
//import com.amway.broadcast.common.service.AmwayGroupService;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.stereotype.Service;
//import org.springframework.transaction.annotation.Transactional;
//
//import java.util.List;
//
///**
// * @auther 薛晨
// * @date 2020/12/12
// * @time 11:37
// * @description 数据库实现
// */
//@Service
//@Transactional(rollbackFor = Exception.class)
//public class AppletsAmwayGroupServiceImpl implements AmwayGroupService<AmwayGroup> {
//
//    @Autowired
//    private AppletsAmwayGroupDBServiceImpl dbService;
//
//    @Autowired
//    private AppletsAmwayGroupRedisServiceImpl redisService;
//
//    @Override
//    public int insert(AmwayGroup baseAmwayGroup) {
//        int count = dbService.insert(baseAmwayGroup);
//        if (count > 0) {
//            redisService.insert(baseAmwayGroup);
//        }
//        return count;
//    }
//
//    @Override
//    public int delete(String groupId) {
//        return 0;
//    }
//
//    @Override
//    public int update(AmwayGroup baseAmwayGroup) {
//        return 0;
//    }
//
//    @Override
//    public List<AmwayGroup> getListByCondition(AmwayGroup condition) {
//        return null;
//    }
//
//    @Override
//    public AmwayGroup getByGroupId(String groupId) {
//        AmwayGroup group = redisService.getByGroupId(groupId);
//        if (group == null) {
//            group = dbService.getByGroupId(groupId);
//        }
//        return group;
//    }
//
//    @Override
//    public AmwayGroup getByImGroupId(String imGroupId) {
//        AmwayGroup group = redisService.getByImGroupId(imGroupId);
//        if (group == null) {
//            group = dbService.getByImGroupId(imGroupId);
//        }
//        return group;
//    }
//
//    @Override
//    public List<AmwayGroup> getByMemberId(Long memberId) {
//        List<AmwayGroup> list = redisService.getByMemberId(memberId);
//        if (list.isEmpty()) {
//            list = dbService.getByMemberId(memberId);
//        }
//        return list;
//    }
//}
