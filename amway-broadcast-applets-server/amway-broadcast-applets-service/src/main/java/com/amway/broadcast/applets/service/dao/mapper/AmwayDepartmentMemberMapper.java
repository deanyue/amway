package com.amway.broadcast.applets.service.dao.mapper;

import com.amway.broadcast.applets.service.dao.entity.AmwayDepartmentMember;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

public interface AmwayDepartmentMemberMapper extends BaseMapper<AmwayDepartmentMember> {
}
