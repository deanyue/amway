package com.amway.broadcast.applets.service.api.service;


import com.amway.broadcast.applets.service.paramsEntity.LoginReq;
import com.amway.broadcast.applets.service.paramsEntity.LoginWxReq;
import com.amway.broadcast.applets.service.paramsEntity.SaveUserInfoReq;
import com.amway.broadcast.applets.service.paramsEntity.WxGetPhoneNumberReq;
import com.amway.broadcast.common.base.Result;

public interface AmwayUserLoginService {
    //账号密码模拟企业微信登录
    Result login(LoginReq loginReq, String wxAppApiToken);
    //微信登录
    Result getOpenId(String code);
    //用户授权获取手机号
    Result wxGetPhoneNumber(WxGetPhoneNumberReq wxGetPhoneNumberReq, String wxAppApiToken);
    //获取用户登录即时通信IM的UserSig
    Result imGetUserSig(String wxAppApiToken);
    /**
     * 用户授权用户信息 (服务端保存用户信息)
     * @param saveUserInfoReq
     * @return
     */
    Result wxSaveUserInfo(SaveUserInfoReq saveUserInfoReq, String wxAppApiToken);
    /**
     * 企业微信自动登录
     * @param code
     * @return
     */
    Result getQyUserId(String wxappApiToken, String code);
}
