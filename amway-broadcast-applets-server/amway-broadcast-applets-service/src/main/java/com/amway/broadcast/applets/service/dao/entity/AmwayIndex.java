package com.amway.broadcast.applets.service.dao.entity;


import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;

import java.util.Date;


/**
 * 全部直播间实体类,无表对应关系，仅做数据返回封装
 */
@Getter
@Setter
public class AmwayIndex {

    private Long  id;

    @JsonProperty("im_group_id")
    private String imGroupId;

    private String name;

    @JsonProperty("start_time")
//    @JsonFormat(pattern = "HH:mm", timezone="GMT+8")
    private String startTime;

    private Integer status;

    //直播间人数 直播间列表  接口使用
    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty("member_num")
    private Integer memberNum;

    //时间格式 xx-xx（月-日） 直播间列表  接口使用
    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty("start_time_2")
    @JsonFormat(pattern = "MM-dd")
    private Date startTime2;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty("limit_type")
    private Integer limitType;

    private String startDate3;
}
