package com.amway.broadcast.applets.service.paramsEntity;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

/**
 * 管理员人数上限以及伪装发布相关设置 返回repose
 */
@Data
public class pageSettingRepose {
    /**
     * 群管理员上限
     */
    @JsonProperty("max_admin_num")
    private Integer maxAdminNum;

    /**
     * 是否开启敏感词校验 0：否 1：是
     */
    @JsonProperty("open_msg_check")
    private Integer openMsgCheck;

    /**
     * 是否开启视频上传 0：否 1：是
     */
    @JsonProperty("open_video_upload")
    private Integer openVideoUpload;

    @JsonProperty("video_text")
    private String videoText;
}
