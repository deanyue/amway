package com.amway.broadcast.applets.service.paramsEntity;

import lombok.Builder;
import lombok.Data;

/**
 * @author WCX
 * @date 2020/12/11
 */
@Data
@Builder
public class AdminAuthDetailsRepose {

    //是否允许管理员设置全体禁言(0为否，1为是)
    private String is_all_mute;

    //是否允许管理员踢走成员(0为否，1为是)
    private String is_kick;
}
