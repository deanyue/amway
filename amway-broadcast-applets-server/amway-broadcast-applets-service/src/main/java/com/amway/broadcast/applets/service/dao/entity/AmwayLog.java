package com.amway.broadcast.applets.service.dao.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.util.Date;

/**
 * 系统日志表
 */
@Getter
@Setter
@TableName(value = "amway_log")
@Builder
public class AmwayLog implements Serializable {

    private static final long serialVersionUID = -2106824437086379380L;

    @TableId(value="id",type= IdType.AUTO)
    private Long id;

    //ip地址
    @TableField(value = "ip")
    private String ip;

    //日志类型  1：登录日志  2：同步企业微信组织架构日志
    @TableField(value = "type")
    private Integer type;

    //日志内容
    @TableField(value = "content")
    private String content;

    //创建时间
    @TableField(value = "create_time")
    private Date createTime;


}
