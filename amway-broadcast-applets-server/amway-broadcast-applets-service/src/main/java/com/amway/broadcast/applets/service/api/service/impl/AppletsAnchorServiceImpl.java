//package com.amway.broadcast.applets.service.api.service.impl;
//
//import com.alibaba.fastjson.JSONObject;
//import com.amway.broadcast.applets.service.api.service.AppletsAnchorService;
//import com.amway.broadcast.applets.service.api.service.SimpleGenericDataBaseService;
//import com.amway.broadcast.applets.service.api.service.UserCacheService;
//import com.amway.broadcast.applets.service.dao.entity.*;
//import com.amway.broadcast.applets.service.paramsEntity.*;
//import com.amway.broadcast.applets.service.redis.RedisUtils;
//import com.amway.broadcast.applets.service.service.AmwayDepartmentMemberService;
//import com.amway.broadcast.applets.service.service.AmwaySettingService;
//import com.amway.broadcast.applets.service.service.AmwayUserService;
//import com.amway.broadcast.applets.service.utils.*;
//import com.amway.broadcast.common.base.Result;
//import com.amway.broadcast.common.base.ResultUtils;
//import com.amway.broadcast.common.enumeration.ResultCodeEnum;
//import com.amway.broadcast.common.exception.user.parm.ContainsContrabandSensitiveWordsException;
//import com.amway.broadcast.common.util.DateUtils;
//import com.amway.broadcast.common.util.JwtUtils;
//import com.amway.broadcast.mybatis.autoconfigure.util.WrapperUtils;
//import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
//import com.vdurmont.emoji.EmojiParser;
//import jodd.util.StringUtil;
//import lombok.extern.slf4j.Slf4j;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.beans.factory.annotation.Value;
//import org.springframework.http.*;
//import org.springframework.stereotype.Service;
//import org.springframework.web.client.RestTemplate;
//
//import javax.annotation.Resource;
//import java.io.*;
//import java.net.HttpURLConnection;
//import java.net.URL;
//import java.util.*;
//import java.util.stream.Collectors;
//
///**
// * 小程序-->主播端
// */
//@Slf4j
//@Service
//public class AppletsAnchorServiceImpl implements AppletsAnchorService {
//
//    @Autowired
//    private AppletsAmwayGroupServiceImpl amwayGroupService;
//
//    @Autowired
//    private AppletsAmwayGroupUserServiceImpl groupUserService;
//
//    @Resource
//    private RedisUtils redisUtils;
//
//    @Resource
//    private SimpleGenericDataBaseService simpleGenericDataBaseService;
//
//    @Resource
//    private AmwayDepartmentMemberService amwayDepartmentMemberService;
//
//    @Resource
//    private AmwayUserService amwayUserService;
//
//    @Resource
//    private AmwaySettingService amwaySettingService;
//
//    @Resource
//    private UserCacheService userCacheService;
//
//    @Value("${TencentConfig.weChatAppid}")
//    private String weChatAppid;
//
//    @Value("${TencentConfig.weChatSecret}")
//    private String weChatSecret;
//    /**
//     *  畅聊间状态
//     *   1：待开始 2：进行中 3：已解散
//     */
//    private static final int STATUS_DISSOLVED = 3;
//
//    /**
//     * 畅聊间查询时间的选择类型  0：全部 1：当天 2：非当天
//     */
//    private static final String ALL_TOTAY = "0";
//    private static final String SAME_TOTAY = "1";
//    private static final String NOT_SAME_TOTAY = "2";
//
//    /**
//     * 分享时，保存二维码的路径等内容
//     */
//    //背景图文件名
//    private  static final String BACKGROUND_NAME = "background.jpg";
//
//    //服务器上存储合成图片后的路径
//    @Value("${ImageConfig.saveImagePath}")
//    private String SAVE_IMAGE_PATH;
//
//    @Value("${ImageConfig.backgroundUrl}")
//    private String BACKGROUND_URL;
//
//
//
//    /**
//     * 畅聊间首页
//     * @param groupId 	畅聊间id
//     * @return
//     */
//    /*@Override
//    //@Async("asyncServiceExecutor")
//    public Result liveIndex(String groupId) {
//            if(!CheckUtils.isDigits(groupId)){
//                log.error("畅聊间首页live_index-->畅聊间id{}格式不正确", groupId);
//                return ResultUtils.error("0","畅聊间id格式不正确");
//            }
//            AmwayGroup amwayGroup = new AmwayGroup();
//            amwayGroup.setId(Long.parseLong(groupId));
//            amwayGroup = simpleGenericDataBaseService.getOne(WrapperUtils.query(amwayGroup));
//            if(CheckUtils.isEmpty(amwayGroup)){
//                log.error("畅聊间首页live_index--> 未查询到畅聊间信息，当前畅聊间id{}不存在", groupId);
//                return ResultUtils.error("0","当前畅聊间不存在");
//            }
//            if(CheckUtils.isEmpty(amwayGroup.getMemberId())){
//                log.error("畅聊间首页live_index--> 未查询到MemberId信息id{}不存在", groupId);
//                return ResultUtils.success();
//            }
//            //返回的畅聊间首页对象
//            AnchorIndexRepose anchorIndexRepose =  new AnchorIndexRepose();
//            //创建畅聊人头像、创建畅聊人昵称，根据返回的member_id去查询department_member表获得
//            AmwayDepartmentMember amwayDepartmentMember = new  AmwayDepartmentMember();
//            amwayDepartmentMember.setId(amwayGroup.getMemberId());
//            amwayDepartmentMember = simpleGenericDataBaseService.getOne(WrapperUtils.query(amwayDepartmentMember));
//            String avatarUrl = "";
//            String nickname = "";
//            if(!CheckUtils.isEmpty(amwayDepartmentMember)){
//                avatarUrl = amwayDepartmentMember.getAvatar();
//                nickname = EmojiParser.parseToUnicode(amwayDepartmentMember.getName());
//            }
//            amwayGroup.setMemberAvatar(avatarUrl);
//            amwayGroup.setMemberNickname(nickname);
//            //畅聊开始时间-时间戳
//            amwayGroup.setStartTimeTimestamp(amwayGroup.getStartTime().getTime());
//            log.info("畅聊间首页live_index--> 查询创建畅聊人头像、昵称数据完成");
//            //统计畅聊间人数 根据id去查询group_user表有多少条记录
//             AmwayGroupUser amwayGroupUser = new AmwayGroupUser();
//             amwayGroupUser.setGroupId(amwayGroup.getId().toString());
//             //只统计存在的用户
//             amwayGroupUser.setIsQuit (0);
//             List<AmwayGroupUser> amwayGroupUserList = simpleGenericDataBaseService.list(WrapperUtils.query(amwayGroupUser));
//             amwayGroup.setMemberNum(amwayGroupUserList.size());
//
//             //查询畅聊间头像列表 （最多取3个，结构：['', '', '']）
//             List<?> groupAvatarsList= queryGroupAvatars(amwayGroupUserList);
//             amwayGroup.setGroupAvatars(groupAvatarsList);
//            log.info("畅聊间首页live_index--> 查询畅聊间头像列表数据完成count{}", groupAvatarsList.size());
//            //名称emoji解码
//            amwayGroup.setName (EmojiParser.parseToUnicode(amwayGroup.getName ()));
//            //简介remark解码
//            amwayGroup.setRemark(EmojiParser.parseToUnicode(amwayGroup.getRemark()));
//            //统计管理员人数  只统计进入直播间的  是否群管理员(0为否,1为是,默认为1)
//            amwayGroupUser.setIsManager("1");
//            int managerNum = simpleGenericDataBaseService.count(WrapperUtils.query(amwayGroupUser));
//            amwayGroup.setManagerNum(managerNum);
//            anchorIndexRepose.setInfoList(amwayGroup);
//            return ResultUtils.success(anchorIndexRepose);
//
//    }*/
//    @Override
//    //@Async("asyncServiceExecutor")
//    public Result liveIndex(String groupId) {
//        if(!CheckUtils.isDigits(groupId)){
//            log.error("畅聊间首页live_index-->畅聊间id{}格式不正确", groupId);
//            return ResultUtils.error("0","畅聊间id格式不正确");
//        }
//        AmwayGroup amwayGroup = amwayGroupService.getByGroupId(groupId);
//        if(CheckUtils.isEmpty(amwayGroup)){
//            log.error("畅聊间首页live_index--> 未查询到畅聊间信息，当前畅聊间id{}不存在", groupId);
//            return ResultUtils.error("0","当前畅聊间不存在");
//        }
//        if(CheckUtils.isEmpty(amwayGroup.getMemberId())){
//            log.error("畅聊间首页live_index--> 未查询到MemberId信息id{}不存在", groupId);
//            return ResultUtils.success();
//        }
//        //创建畅聊人头像、创建畅聊人昵称，根据返回的member_id去查询department_member表获得
//        AmwayDepartmentMember amwayDepartmentMember = amwayDepartmentMemberService.getById(amwayGroup.getMemberId());
//        String avatarUrl = "";
//        String nickname = "";
//        if(!CheckUtils.isEmpty(amwayDepartmentMember)){
//            avatarUrl = amwayDepartmentMember.getAvatar();
//            nickname = EmojiParser.parseToUnicode(amwayDepartmentMember.getName());
//        }
//        amwayGroup.setMemberAvatar(avatarUrl);
//        amwayGroup.setMemberNickname(nickname);
//        //畅聊开始时间-时间戳
//        amwayGroup.setStartTimeTimestamp(amwayGroup.getStartTime().getTime());
//        log.info("畅聊间首页live_index--> 查询创建畅聊人头像、昵称数据完成");
//        //统计畅聊间人数 根据id去查询group_user表有多少条记录
//        AmwayGroupUser amwayGroupUser = new AmwayGroupUser();
//        amwayGroupUser.setGroupId(amwayGroup.getId().toString());
//        //只统计存在的用户
//        amwayGroupUser.setIsQuit (0);
//        List<AmwayGroupUser> amwayGroupUserList = groupUserService.getByGroupIdAndStatus(groupId,0);
//        amwayGroup.setMemberNum(amwayGroupUserList.size());
//
//        //查询畅聊间头像列表 （最多取3个，结构：['', '', '']）
//        List<?> groupAvatarsList= queryGroupAvatars(amwayGroupUserList);
//        amwayGroup.setGroupAvatars(groupAvatarsList);
//        log.info("畅聊间首页live_index--> 查询畅聊间头像列表数据完成count{}", groupAvatarsList.size());
//        //名称emoji解码
//        amwayGroup.setName (EmojiParser.parseToUnicode(amwayGroup.getName ()));
//        //简介remark解码
//        amwayGroup.setRemark(EmojiParser.parseToUnicode(amwayGroup.getRemark()));
//        //统计管理员人数  只统计进入直播间的  是否群管理员(0为否,1为是,默认为1)
//        amwayGroupUser.setIsManager("1");
//        int managerNum = (int)amwayGroupUserList.stream().filter(amwayGroupUser1 -> "1".equals(amwayGroupUser1.getIsManager())).count();
//        amwayGroup.setManagerNum(managerNum);
//        //返回的畅聊间首页对象
//        AnchorIndexRepose anchorIndexRepose =  new AnchorIndexRepose();
//        anchorIndexRepose.setInfoList(amwayGroup);
//        return ResultUtils.success(anchorIndexRepose);
//
//    }
//    /**
//     * 畅聊间首页  -->查询畅聊间头像列表
//     *
//     * @return
//     */
//    public List<?> queryGroupAvatars(List<AmwayGroupUser> amwayGroupUserList){
//            //返回的头像list
//            List<String>  avatarUrlList = new ArrayList<String>();
//            //获得userId的list集合
//            List<Long>  userIdList = new ArrayList<>();
//            //若查询出的记录大于等于3,则取前三条
//            if(amwayGroupUserList.size()>= 3){
//                List<AmwayGroupUser> newAmwayGroupUserList = amwayGroupUserList.subList(0, 2);
//                for (AmwayGroupUser amwayGroupUsers : newAmwayGroupUserList) {
//                    userIdList.add(amwayGroupUsers.getUserId());
//                }
//            }else {
//                for (AmwayGroupUser amwayGroupUsers : amwayGroupUserList) {
//                    userIdList.add(amwayGroupUsers.getUserId());
//                }
//            }
//            if(userIdList.size()>0) {
//                //根据group_user的user_id去匹配user表的id （最多取3个，结构：['', '', '']）
//                List<AmwayUser> amwayUserList = amwayUserService.listByIds(userIdList);
//                for (AmwayUser amwayUsers : amwayUserList) {
//                    avatarUrlList.add(amwayUsers.getAvatarUrl());
//                }
//            }
//            log.info("畅聊间首页-->畅聊间头像列表查询返回avatarUrlList{}：", avatarUrlList);
//            return avatarUrlList;
//    }
//
//
//
//    /**
//     * 首页-全部畅聊间
//     * @return 封装的全部畅聊间实体类
//     */
//    /*@Override
//    //@Async("asyncServiceExecutor")
//    public Result index(String wxAppApiToken, String status) {
//            //返回前端实体对象
//            AnchorIndexRepose appletsAnchorIndexRepose = new AnchorIndexRepose();
//            //装载畅聊间列表信息list，返回给前端
//            List<AmwayIndex> amwayIndexList = new ArrayList<AmwayIndex>();
//            //通过token查询用户唯一标识openid
//            String openId = JwtUtils.getUserNameFromToken(wxAppApiToken);
//            //通过openid查询用户信息
//            AmwayUser amwayUser = new AmwayUser();
//            amwayUser.setOpenId(openId);
//            //amwayUser = (AmwayUser) redisUtils.checkUser(openId, amwayUser);
//            amwayUser = simpleGenericDataBaseService.getOne(WrapperUtils.query(amwayUser));
//            if (CheckUtils.isEmpty(amwayUser)) {
//                log.error("首页index-->用户信息不存在openid{}", openId);
//                return ResultUtils.error(ResultCodeEnum.A0201.value(), ResultCodeEnum.A0201.getName());
//            }
//            //判断是否授权、未授权则返回
//            if(amwayUser.getIsAuth().longValue() == 0L){
//                log.error("畅聊间列表展示live_list-->用户信息未授权", openId);
//                return ResultUtils.error("999","未授权");
//            }
//            //查询当前自己创建的畅聊间 表amway_group
//            AmwayGroup amwayGroupReq = new AmwayGroup();
//            amwayGroupReq.setStatusAm(Integer.parseInt(status));
//            amwayGroupReq.setMemberId(amwayUser.getMemberId());
//            List<AmwayGroup> amwayGroupList = simpleGenericDataBaseService.list(WrapperUtils.query(amwayGroupReq).orderByAsc("start_time"));
//            for (AmwayGroup amwayGroup : amwayGroupList) {
//                AmwayIndex amwayIndex = new AmwayIndex();
//                amwayIndex.setId(amwayGroup.getId());
//                amwayIndex.setImGroupId(amwayGroup.getImGroupId());
//                amwayIndex.setName(EmojiParser.parseToUnicode(amwayGroup.getName()));
//                amwayIndex.setStartTime(amwayGroup.getStartTime());
//                amwayIndex.setStatus(amwayGroup.getStatusAm());
//                amwayIndexList.add(amwayIndex);
//            }
//            //畅聊间数量
//            AmwayGroup amwayGroupCount = new AmwayGroup();
//            amwayGroupCount.setMemberId(amwayUser.getMemberId());
//            //增加不等于3解散的
//            int count = simpleGenericDataBaseService.count(WrapperUtils.query(amwayGroupCount).notIn("status_am", "3"));
//            appletsAnchorIndexRepose.setCount(count);
//            //畅聊间列表
//            appletsAnchorIndexRepose.setGroupList(amwayIndexList);
//            log.info("首页index-->数据查询成功count[{}],列表listCount[{}]",count, amwayIndexList.size());
//            return ResultUtils.success(appletsAnchorIndexRepose);
//    }*/
//
//    @Override
//    //@Async("asyncServiceExecutor")
//    public Result index(String wxAppApiToken, String status) {
//        //返回前端实体对象
//        AnchorIndexRepose appletsAnchorIndexRepose = new AnchorIndexRepose();
//        //装载畅聊间列表信息list，返回给前端
//        List<AmwayIndex> amwayIndexList = new ArrayList<AmwayIndex>();
//        //通过token查询用户唯一标识openid
//        String openId = JwtUtils.getUserNameFromToken(wxAppApiToken);
//        //通过openid查询用户信息
//        AmwayUser amwayUser = userCacheService.getUserInCacheByOpenId(openId);
//        if (CheckUtils.isEmpty(amwayUser)) {
//            log.error("首页index-->用户信息不存在openid{}", openId);
//            return ResultUtils.error(ResultCodeEnum.A0201.value(), ResultCodeEnum.A0201.getName());
//        }
//        //判断是否授权、未授权则返回
//        if(amwayUser.getIsAuth() == 0L){
//            log.error("畅聊间列表展示live_list-->用户信息未授权", openId);
//            return ResultUtils.error("999","未授权");
//        }
//        //查询当前自己创建的畅聊间 表amway_group
//        AmwayGroup amwayGroupReq = new AmwayGroup();
//        amwayGroupReq.setStatusAm(Integer.parseInt(status));
//        amwayGroupReq.setMemberId(amwayUser.getMemberId());
//        List<AmwayGroup> amwayGroupList = amwayGroupService.getByMemberId(amwayUser.getMemberId());
//        List<AmwayGroup> collect = amwayGroupList.stream().filter(amwayGroup -> amwayGroup.getStatusAm().equals(status)).collect(Collectors.toList());
//        collect.stream().sorted((o1, o2) -> (int)(o2.getStartTime().getTime()-o1.getStartTime().getTime()));
//        for (AmwayGroup amwayGroup : collect) {
//            AmwayIndex amwayIndex = new AmwayIndex();
//            amwayIndex.setId(amwayGroup.getId());
//            amwayIndex.setImGroupId(amwayGroup.getImGroupId());
//            amwayIndex.setName(EmojiParser.parseToUnicode(amwayGroup.getName()));
//            amwayIndex.setStartTime(amwayGroup.getStartTime());
//            amwayIndex.setStatus(amwayGroup.getStatusAm());
//            amwayIndexList.add(amwayIndex);
//        }
//        //畅聊间数量
//        AmwayGroup amwayGroupCount = new AmwayGroup();
//        amwayGroupCount.setMemberId(amwayUser.getMemberId());
//        //增加不等于3解散的
////        int count = simpleGenericDataBaseService.count(WrapperUtils.query(amwayGroupCount).notIn("status_am", "3"));
//        int count = (int)amwayGroupList.stream().filter(amwayGroup -> !amwayGroup.getStatusAm().equals(3)).count();
//        appletsAnchorIndexRepose.setCount(count);
//        //畅聊间列表
//        appletsAnchorIndexRepose.setGroupList(amwayIndexList);
//        log.info("首页index-->数据查询成功count[{}],列表listCount[{}]",count, amwayIndexList.size());
//        return ResultUtils.success(appletsAnchorIndexRepose);
//    }
//
//    /**
//     * 畅聊间列表展示
//     * @param wxAppApiToken 小程序端请求服务器端的凭证
//     * @param status 	状态 1：待开始 2：进行中 3：已过期
//     * @param isToday   0：全部 1：当天 2：非当天
//     * @return
//     */
//   /* @Override
//    //@Async("asyncServiceExecutor")
//    public Result liveList(String wxAppApiToken, String status, String isToday) {
//            //返回前端实体对象
//            AnchorIndexRepose appletsAnchorIndexRepose = new AnchorIndexRepose();
//            //通过token查询用户唯一标识openid
//            String openId = JwtUtils.getUserNameFromToken(wxAppApiToken);
//            //通过openid查询用户信息
//            AmwayUser amwayUser = new AmwayUser();
//            amwayUser.setOpenId(openId);
//            amwayUser = simpleGenericDataBaseService.getOne(WrapperUtils.query(amwayUser));
//            //amwayUser = (AmwayUser)redisUtils.checkUser(openId, amwayUser);
//            if (CheckUtils.isEmpty(amwayUser)) {
//                log.error("畅聊间列表展示live_list-->用户信息不存在openid[{}]", openId);
//                return ResultUtils.error(ResultCodeEnum.A0201.value(), ResultCodeEnum.A0201.getName());
//            }
//    *//*        if(amwayUser.getIsAuth().longValue() == 0L){
//                log.error("畅聊间列表展示live_list-->用户信息未授权", openId);
//                return new AsyncResult(ResultUtils.success());
//            }*//*
//            if(CheckUtils.isEmpty(amwayUser.getMemberId())){
//                log.error("畅聊间列表展示live_list-->用户信息MemberId为空", openId);
//                return ResultUtils.success();
//            }
//            //装载畅聊间列表信息list，返回给前端
//            List<AmwayIndex> amwayIndexList = new ArrayList<AmwayIndex>();
//            //查询list数据
//            List<AmwayGroup> amwayGroupList = new ArrayList<AmwayGroup>();
//            //查询条件对象
//            AmwayGroup amwayGroup = new AmwayGroup();
//            amwayGroup.setStatusAm(Integer.parseInt(status));
//            amwayGroup.setMemberId(amwayUser.getMemberId());
//
//        log.info("查询直播间列表，用户信息userId{},nickName{},memberId{},Status{},opneid{}",amwayUser.getId(),amwayUser.getNickname(),amwayGroup.getMemberId(),Integer.parseInt(status),amwayUser.getOpenId());
//            //根据条件查询时间区间
//          switch (isToday) {
//                //0：全部
//                case ALL_TOTAY:
//                    amwayGroupList = simpleGenericDataBaseService.list(WrapperUtils.query(amwayGroup));
//                    break;
//                 //1:当天
//                case SAME_TOTAY:
//                    amwayGroupList = simpleGenericDataBaseService.list(WrapperUtils.query(amwayGroup).between("start_time",DateUtils.currentMin(new Date()),DateUtils.currentMax(new Date())));
//                    break;
//                 //2、非当天
//                case NOT_SAME_TOTAY:
//                    amwayGroupList = simpleGenericDataBaseService.list(WrapperUtils.query(amwayGroup).notBetween("start_time",DateUtils.currentMin(new Date()),DateUtils.currentMax(new Date())));
//                    break;
//                default:
//                    break;
//            }
//            for (AmwayGroup amwayGrouplist : amwayGroupList) {
//                AmwayIndex amwayIndex = new AmwayIndex();
//                amwayIndex.setId(amwayGrouplist.getId());
//                amwayIndex.setImGroupId(amwayGrouplist.getImGroupId());
//                amwayIndex.setName(EmojiParser.parseToUnicode(amwayGrouplist.getName()));
//                amwayIndex.setStartTime(amwayGrouplist.getStartTime());
//                amwayIndex.setStatus(amwayGrouplist.getStatusAm());
//                amwayIndex.setStartTime2(amwayGrouplist.getStartTime());
//                amwayIndex.setLimitType(amwayGrouplist.getLimitType());
//                //统计畅聊间人数 根据id去查询group_user表有多少条记录
//                AmwayGroupUser amwayGroupUser = new AmwayGroupUser();
//                amwayGroupUser.setGroupId(amwayGrouplist.getId().toString());
//                //只统计存在的用户
//                amwayGroupUser.setIsQuit (0);
//                int memberNum = simpleGenericDataBaseService.count(WrapperUtils.query(amwayGroupUser));
//                amwayIndex.setMemberNum(memberNum);
//                amwayIndexList.add(amwayIndex);
//            }
//            //畅聊间列表
//            appletsAnchorIndexRepose.setGroupList(amwayIndexList);
//            return ResultUtils.success(appletsAnchorIndexRepose);
//    }*/
//
//    @Override
//    //@Async("asyncServiceExecutor")
//    public Result liveList(String wxAppApiToken, String status, String isToday) {
//        //返回前端实体对象
//        AnchorIndexRepose appletsAnchorIndexRepose = new AnchorIndexRepose();
//        //通过token查询用户唯一标识openid
//        String openId = JwtUtils.getUserNameFromToken(wxAppApiToken);
//        //通过openid查询用户信息
//        AmwayUser amwayUser = userCacheService.getUserInCacheByOpenId(openId);
//        if (CheckUtils.isEmpty(amwayUser)) {
//            log.error("畅聊间列表展示live_list-->用户信息不存在openid[{}]", openId);
//            return ResultUtils.error(ResultCodeEnum.A0201.value(), ResultCodeEnum.A0201.getName());
//        }
//    /*        if(amwayUser.getIsAuth().longValue() == 0L){
//                log.error("畅聊间列表展示live_list-->用户信息未授权", openId);
//                return new AsyncResult(ResultUtils.success());
//            }*/
//        if(CheckUtils.isEmpty(amwayUser.getMemberId())){
//            log.error("畅聊间列表展示live_list-->用户信息MemberId为空", openId);
//            return ResultUtils.success();
//        }
//        //装载畅聊间列表信息list，返回给前端
//        List<AmwayIndex> amwayIndexList = new ArrayList<AmwayIndex>();
//        //查询list数据
//        List<AmwayGroup> amwayGroupList = new ArrayList<AmwayGroup>();
//        //查询条件对象
//        AmwayGroup amwayGroup = new AmwayGroup();
//        amwayGroup.setStatusAm(Integer.parseInt(status));
//        amwayGroup.setMemberId(amwayUser.getMemberId());
//
//        log.info("查询直播间列表，用户信息userId{},nickName{},memberId{},Status{},opneid{}",amwayUser.getId(),amwayUser.getNickname(),amwayGroup.getMemberId(),Integer.parseInt(status),amwayUser.getOpenId());
//        //根据条件查询时间区间
//        switch (isToday) {
//            //0：全部
//            case ALL_TOTAY:
//                amwayGroupList = amwayGroupService.getByMemberId(amwayUser.getMemberId());
//                break;
//            //1:当天
//            case SAME_TOTAY:
//                amwayGroupList = amwayGroupService.getByMemberId(amwayUser.getMemberId()).
//                        stream().filter(amwayGroup1 -> amwayGroup1.getStartTime().getTime()>=DateUtils.currentMin(new Date()).getTime()
//                        &&amwayGroup1.getStartTime().getTime()<=DateUtils.currentMax(new Date()).getTime()).collect(Collectors.toList());
//                break;
//            //2、非当天
//            case NOT_SAME_TOTAY:
//                amwayGroupList = amwayGroupService.getByMemberId(amwayUser.getMemberId()).
//                        stream().filter(amwayGroup1 -> amwayGroup1.getStartTime().getTime()<DateUtils.currentMin(new Date()).getTime()
//                        ||amwayGroup1.getStartTime().getTime()>DateUtils.currentMax(new Date()).getTime()).collect(Collectors.toList());
//                break;
//            default:
//                break;
//        }
//        for (AmwayGroup amwayGrouplist : amwayGroupList) {
//            AmwayIndex amwayIndex = new AmwayIndex();
//            amwayIndex.setId(amwayGrouplist.getId());
//            amwayIndex.setImGroupId(amwayGrouplist.getImGroupId());
//            amwayIndex.setName(EmojiParser.parseToUnicode(amwayGrouplist.getName()));
//            amwayIndex.setStartTime(amwayGrouplist.getStartTime());
//            amwayIndex.setStatus(amwayGrouplist.getStatusAm());
//            amwayIndex.setStartTime2(amwayGrouplist.getStartTime());
//            amwayIndex.setLimitType(amwayGrouplist.getLimitType());
//            //统计畅聊间人数 根据id去查询group_user表有多少条记录
//            AmwayGroupUser amwayGroupUser = new AmwayGroupUser();
//            amwayGroupUser.setGroupId(amwayGrouplist.getId().toString());
//            //只统计存在的用户
//            amwayGroupUser.setIsQuit (0);
//            int memberNum = groupUserService.getByGroupIdAndStatus(amwayGrouplist.getId().toString(),0).size();
//            amwayIndex.setMemberNum(memberNum);
//            amwayIndexList.add(amwayIndex);
//        }
//        //畅聊间列表
//        appletsAnchorIndexRepose.setGroupList(amwayIndexList);
//        return ResultUtils.success(appletsAnchorIndexRepose);
//    }
//
//    /**
//     * 畅聊分享
//     * @param groupId 	畅聊间id
//     * @param path 	路径
//     * @return
//     */
//  /*  @Override
//   // @Async("asyncServiceExecutor")
//    public Result liveShare(String groupId, String path) {
//        //二维码文件名
//        long currentTime = System.currentTimeMillis();
//        String codeName = currentTime+".jpg";
//        //合成图片后的文件名
//        String fileName = currentTime+"image.jpg";
//        //校验是否存在指定 按?分割
//        String[] pathPam = CheckUtils.checkSplit(path, "?","\\?");
//        if(CheckUtils.isEmpty(pathPam)){
//            log.error("畅聊分享live_share-->畅聊间分享页路径不正确path{}", path);
//            return ResultUtils.error("0","畅聊间分享页路径格式不正确");
//        }
//        AmwayGroup amwayGroup = new AmwayGroup();
//        amwayGroup.setId(Long.parseLong(groupId));
//        amwayGroup = simpleGenericDataBaseService.getOne(WrapperUtils.query(amwayGroup));
//        if(CheckUtils.isEmpty(amwayGroup)){
//            log.info("畅聊分享live_share--> 未查询到畅聊间信息，当前畅聊间id不存在", groupId);
//            return ResultUtils.error("0","当前畅聊间不存在");
//        }
//        //微信的接口，获取token
//        String accessToken = new AccessTokenUtils().getAccessToken(weChatAppid, weChatSecret, redisUtils);
//        if(accessToken == null){
//            log.error("分享live_share-->未获取到所需的token");
//            return ResultUtils.error("0","当前人数过多，请重新获取分享码");
//        }
//        //获取二维码
//        try {
//            String shareImg = picImage(accessToken, codeName, fileName, pathPam);
//            if (StringUtil.isNotBlank(shareImg)) {
//                AnchorLiveShareRepose anchorLiveShareRepose = new AnchorLiveShareRepose();
//                //分享的路径暂时用传参的，后续调整------
//                anchorLiveShareRepose.setShareUrl(path + "/" + groupId);
//                anchorLiveShareRepose.setShareImg(shareImg);
//                log.info("分享live_share-->分享图片等信息生成成功");
//                return ResultUtils.success(anchorLiveShareRepose);
//              }
//            }catch (Exception e){
//                log.error("生成海报失败，请重新获取{}",e.getMessage());
//                return ResultUtils.error("0","当前人数过多，请重新获取分享码");
//        }
//        log.info("畅聊分享live_share-->分享失败");
//        return ResultUtils.error("0","当前人数过多，请重新获取分享码");
//    }*/
//
//    @Override
//    // @Async("asyncServiceExecutor")
//    public Result liveShare(String groupId, String path) {
//        //二维码文件名
//        long currentTime = System.currentTimeMillis();
//        String codeName = currentTime+".jpg";
//        //合成图片后的文件名
//        String fileName = currentTime+"image.jpg";
//        //校验是否存在指定 按?分割
//        String[] pathPam = CheckUtils.checkSplit(path, "?","\\?");
//        if(CheckUtils.isEmpty(pathPam)){
//            log.error("畅聊分享live_share-->畅聊间分享页路径不正确path{}", path);
//            return ResultUtils.error("0","畅聊间分享页路径格式不正确");
//        }
//        AmwayGroup amwayGroup = new AmwayGroup();
//        amwayGroup.setId(Long.parseLong(groupId));
//        amwayGroup = amwayGroupService.getByGroupId(groupId);
//        if(CheckUtils.isEmpty(amwayGroup)){
//            log.info("畅聊分享live_share--> 未查询到畅聊间信息，当前畅聊间id不存在", groupId);
//            return ResultUtils.error("0","当前畅聊间不存在");
//        }
//        //微信的接口，获取token
//        String accessToken = new AccessTokenUtils().getAccessToken(weChatAppid, weChatSecret, redisUtils);
//        if(accessToken == null){
//            log.error("分享live_share-->未获取到所需的token");
//            return ResultUtils.error("0","当前人数过多，请重新获取分享码");
//        }
//        //获取二维码
//        try {
//            String shareImg = picImage(accessToken, codeName, fileName, pathPam);
//            if (StringUtil.isNotBlank(shareImg)) {
//                AnchorLiveShareRepose anchorLiveShareRepose = new AnchorLiveShareRepose();
//                //分享的路径暂时用传参的，后续调整------
//                anchorLiveShareRepose.setShareUrl(path + "/" + groupId);
//                anchorLiveShareRepose.setShareImg(shareImg);
//                log.info("分享live_share-->分享图片等信息生成成功");
//                return ResultUtils.success(anchorLiveShareRepose);
//            }
//        }catch (Exception e){
//            log.error("生成海报失败，请重新获取{}",e.getMessage());
//            return ResultUtils.error("0","当前人数过多，请重新获取分享码");
//        }
//        log.info("畅聊分享live_share-->分享失败");
//        return ResultUtils.error("0","当前人数过多，请重新获取分享码");
//    }
//    /**
//     * 创建文件路径方法
//     * @return
//     */
//    private Map<String, String>  checkImage(String codeName,String fileName){
//        Map<String, String> pathMap = new HashMap<String, String>();
//        //二维码文件
//        File codeFile = new File(SAVE_IMAGE_PATH );
//        if (!codeFile.exists()) { codeFile.mkdirs(); }
//        String codePath = codeFile.getPath() + File.separator + codeName;
//        codePath = CheckUtils.checkUrl(codePath,"\\","/");
//
//        String imagePath = codeFile.getPath() + File.separator + fileName;
//        imagePath = CheckUtils.checkUrl(imagePath,"\\","/");
//
//        pathMap.put("codePath",codePath);
//        pathMap.put("imagePath",imagePath);
//        return pathMap;
//    }
//
//
//    /**
//     * 获取分享的微信二维码图片
//     * @return
//     */
//    public void getShareImage(String accessToken, String codeName, String fileName,String[] pathPam){
//        Map<String, String> pathMap = checkImage(codeName,fileName);
//        PrintWriter printWriter = null;
//        OutputStream outputStream = null;
//        BufferedInputStream bufferedInputStream = null;
//
//        try{
//            URL url = new URL("https://api.weixin.qq.com/wxa/getwxacodeunlimit?access_token=" + accessToken);
//            HttpURLConnection httpURLConnection = (HttpURLConnection) url.openConnection();
//            httpURLConnection.setRequestMethod("POST");
//            // 发送POST请求必须设置如下两行
//            httpURLConnection.setDoOutput(true);
//            httpURLConnection.setDoInput(true);
//            // 获取URLConnection对象对应的输出流
//             printWriter = new PrintWriter(httpURLConnection.getOutputStream());
//            // 发送请求参数
//            JSONObject paramJson = new JSONObject();
//            //对请求参数进行处理，请求最终格式xx:xx,xx:xx
//            paramJson.put("scene", CheckUtils.getJsonValue(pathPam[1]));
//            paramJson.put("page", CheckUtils.checkStr(pathPam[0],1,"/"));
//            paramJson.put("width", "280px");
//            paramJson.put("is_hyaline", true);
//            printWriter.write(paramJson.toString());
//            // flush输出流的缓冲
//            printWriter.flush();
//            //开始获取数据
//            bufferedInputStream = new BufferedInputStream(httpURLConnection.getInputStream());
//            outputStream = new FileOutputStream(pathMap.get("codePath"));
//            int len = 0;
//            byte[] arr = new byte[1024];
//            while ((len = bufferedInputStream.read(arr)) != -1){
//                outputStream.write(arr, 0, len);
//                outputStream.flush();
//            }
//
//        }catch (Exception e){
//            log.error("获取分享二维码失败:"+e.getMessage());
//        }finally {
//                printWriter.close();
//            try {
//                outputStream.close();
//                bufferedInputStream.close();
//            } catch (IOException e) {
//                e.printStackTrace();
//            }
//        }
//    }
//    /**
//     * 压缩二维码图片，合成二维码图片，并返回base64流
//     * @return
//     */
//    public String picImage(String accessToken, String codeName, String fileName,String[] pathPam){
//        //调用微信小程序api接口获取二维码，保存本地
//        getShareImage(accessToken,codeName,fileName,pathPam);
//
//        Map<String, String> pathMap = checkImage(codeName,fileName);
//        //添加随机数
//        int randomNum  = (int)(1+Math.random()*10);
//        //背景图片保存路径
//        String backgroup = ImageUtils.createImage(BACKGROUND_URL+"?"+randomNum, SAVE_IMAGE_PATH+BACKGROUND_NAME);
//        //二维码压缩
//        ImageUtil.reduceImgCode(pathMap.get("codePath"), pathMap.get("codePath"));
//        ImageUtil.drawString("\\", pathMap.get("imagePath"),  backgroup, pathMap.get("codePath"));
//        //图片压缩
//        //ImageUtil.reduceImg(pathMap.get("imagePath"), pathMap.get("imagePath"));
//        String base64Image = ImageUtil.readImage(pathMap.get("imagePath"));
//        //本地临时文件删除
//        if(FilesUtils.deleteFile(pathMap.get("imagePath"))) log.info("本地生成的临时分享图片删除成功");
//        if(FilesUtils.deleteFile(pathMap.get("codePath"))) log.info("本地生成的临时code分享图片删除成功");
//        return base64Image;
//    }
//
//    private void checkSensitive(AnchorLiveCreateReq anchorLiveCreateReq) {
//        if(CheckUtils.isEmpty(anchorLiveCreateReq)){
//            log.info("校验敏感词-->入参对象为空");
//            throw new ContainsContrabandSensitiveWordsException("包含违禁敏感词");
//        }
//        String accessToken = new AccessTokenUtils().getAccessToken(weChatAppid, weChatSecret, redisUtils);
//        if(accessToken == null){
//            log.error("校验敏感词-->未获取到所需的token");
//            throw  new ContainsContrabandSensitiveWordsException("包含违禁敏感词");
//        }
//        PrintWriter printWriter = null;
//        BufferedInputStream bufferedInputStream = null;
//        try {
//            URL url = new URL("https://api.weixin.qq.com/wxa/msg_sec_check?access_token=" + accessToken);
//            HttpURLConnection httpURLConnection = (HttpURLConnection) url.openConnection();
//            httpURLConnection.setRequestMethod("POST");
//            // 发送POST请求必须设置如下两行
//            httpURLConnection.setDoOutput(true);
//            httpURLConnection.setDoInput(true);
//            // 获取URLConnection对象对应的输出流
//            printWriter = new PrintWriter(httpURLConnection.getOutputStream());
//            // 发送请求参数
//            JSONObject paramJson = new JSONObject();
//            //对请求参数进行处理，请求最终格式xx:xx,xx:xx
//            paramJson.put("content", anchorLiveCreateReq.getName()+anchorLiveCreateReq.getRemark());
//            printWriter.write(paramJson.toString());
//            // flush输出流的缓冲
//            printWriter.flush();
//            //开始获取数据
//            bufferedInputStream = new BufferedInputStream(httpURLConnection.getInputStream());
//            int len = 0;
//            byte[] arr = new byte[1024];
//            String retData = null;
//            while ((len = bufferedInputStream.read(arr)) != -1){
//                retData = new String(arr, 0, len);
//            }
//            printWriter.close();
//            bufferedInputStream.close();
//
//            JSONObject retJson = JSONObject.parseObject(retData);
//            Object errCode = retJson.get("errcode");
//            if(!"0".equals(errCode.toString())){
//                log.info("校验敏感词msgCheck-->{}:", retData);
//                throw  new ContainsContrabandSensitiveWordsException("包含违禁敏感词");
//            }
//        }catch (Exception e){
//            log.error("敏感词获取失败：{}", e.getMessage());
//            throw  new ContainsContrabandSensitiveWordsException("包含违禁敏感词");
//        }finally {
//            if (printWriter != null) {
//                printWriter.close();
//            }
//            try {
//                if (bufferedInputStream != null) {
//                    bufferedInputStream.close();
//                }
//            } catch (IOException e) {
//                e.printStackTrace();
//            }
//        }
//    }
//
//
//    /**
//     * 发起畅聊
//     * @param anchorLiveCreateReq
//     * @return
//     */
//    /*@Override
//    public Result liveCreate(AnchorLiveCreateReq anchorLiveCreateReq, String wxAppApiToken) {
//        checkSensitive(anchorLiveCreateReq);
//            AmwayGroup amwayGroup = new AmwayGroup();
//            amwayGroup.setImGroupId(anchorLiveCreateReq.getIm_group_id());
//            int amwayGroupCount = simpleGenericDataBaseService.count(amwayGroup);
//            if(amwayGroupCount > 0){
//                log.info("发起畅聊live_create-->当前畅聊间已存在，查询条数count{}",amwayGroupCount);
//                return ResultUtils.error("0","当前畅聊间已存在");
//            }
//            //返回前端实体对象
//            AnchorIndexRepose appletsAnchorIndexRepose = new AnchorIndexRepose();
//            amwayGroup.setName(EmojiParser.parseToHtmlDecimal(anchorLiveCreateReq.getName()));
//            amwayGroup.setStartTime(DateUtils.getDateHhMm(anchorLiveCreateReq.getStart_time()));
//            amwayGroup.setExpire(Integer.parseInt(anchorLiveCreateReq.getExpire()));
//            amwayGroup.setLimitType(Integer.parseInt(anchorLiveCreateReq.getLimit_type()));
//            amwayGroup.setRemark(EmojiParser.parseToHtmlDecimal(anchorLiveCreateReq.getRemark()));
//
//            //通过token查询用户唯一标识openid
//            String openId = JwtUtils.getUserNameFromToken(wxAppApiToken);
//            //通过openid查询用户信息
//            AmwayUser amwayUser = new AmwayUser();
//            amwayUser.setOpenId(openId);
//            amwayUser = (AmwayUser)redisUtils.checkUser(openId, amwayUser);
//            if (CheckUtils.isEmpty(amwayUser)) {
//                log.error("发起畅聊live_create-->发起畅聊，用户信息不存在openid{}", openId);
//                return ResultUtils.error(ResultCodeEnum.A0201.value(), ResultCodeEnum.A0201.getName());
//            }
//            //根据openid查询user表获得创建人的畅聊ID
//            amwayGroup.setMemberId(amwayUser.getMemberId());
//            //------取值待定， 一直在变---------
//            //状态  1：待开始  2：进行中  3：已解散
//            if(DateUtils.getUntilSecond(new Date(), DateUtils.getDateHhMm(anchorLiveCreateReq.getStart_time())) <= 0){
//                //立即开播的意思，状态置为2 进行中
//                amwayGroup.setStatusAm(2);
//            }else{ amwayGroup.setStatusAm(1);}
//
//            //目前该字段无实际含义 是否删除 0：否  1：是
//            amwayGroup.setIsDelete(0);
//            amwayGroup.setCreateTime(new Date());
//            boolean amwayFlag = simpleGenericDataBaseService.save(amwayGroup);
//            //新增成功
//            if(amwayFlag) {
//                log.info("发起畅聊live_create-->畅聊发起成功");
//                //新增自己的人数
//                AmwayGroupUser amwayGroupUser = new AmwayGroupUser();
//                amwayGroupUser.setId(0L);
//                amwayGroupUser.setUserId(amwayUser.getId().toString());
//                amwayGroupUser.setCreateTime(new Date());
//                amwayGroupUser.setGroupId(amwayGroup.getId().toString());
//                amwayGroupUser.setShareUserId("0");
//                simpleGenericDataBaseService.save(amwayGroupUser);
//                log.info("发起畅聊live_create-->发起畅聊，进入畅聊间数据更新成功groupId{}", amwayGroupUser.getGroupId());
//                //自己创建，则畅聊间数量初始化为1
//                amwayGroup.setMemberNum(1);
//                appletsAnchorIndexRepose.setInfoList(amwayGroup);
//                return ResultUtils.success(appletsAnchorIndexRepose);
//            }
//            return ResultUtils.error("0","当前人数过多，请重试");
//    }*/
//    //refactor
//    @Override
//    public Result liveCreate(AnchorLiveCreateReq anchorLiveCreateReq, String wxAppApiToken) {
//        AmwayGroup amwayGroup = amwayGroupService.getByImGroupId(anchorLiveCreateReq.getIm_group_id());
//        if(amwayGroup !=null){
//            log.info("发起畅聊live_create-->当前畅聊间已存在，查询条数count{}",amwayGroup);
//            return ResultUtils.error("0","当前畅聊间已存在");
//        }
//        //通过token查询用户唯一标识openid
//        String openId = JwtUtils.getUserNameFromToken(wxAppApiToken);
//        //通过openid查询用户信息
//        AmwayUser amwayUser = userCacheService.getUserInCacheByOpenId(openId);
//        if (CheckUtils.isEmpty(amwayUser)) {
//            log.error("发起畅聊live_create-->发起畅聊，用户信息不存在openid{}", openId);
//            return ResultUtils.error(ResultCodeEnum.A0201.value(), ResultCodeEnum.A0201.getName());
//        }
//        amwayGroup = new AmwayGroup();
//        amwayGroup.setImGroupId(anchorLiveCreateReq.getIm_group_id());
//        amwayGroup.setName(EmojiParser.parseToHtmlDecimal(anchorLiveCreateReq.getName()));
//        amwayGroup.setStartTime(DateUtils.getDateHhMm(anchorLiveCreateReq.getStart_time()));
//        amwayGroup.setExpire(Integer.parseInt(anchorLiveCreateReq.getExpire()));
//        amwayGroup.setLimitType(Integer.parseInt(anchorLiveCreateReq.getLimit_type()));
//        amwayGroup.setRemark(EmojiParser.parseToHtmlDecimal(anchorLiveCreateReq.getRemark()));
//        //根据openid查询user表获得创建人的畅聊ID
//        amwayGroup.setMemberId(amwayUser.getMemberId());
//        //------取值待定， 一直在变---------
//        //状态  1：待开始  2：进行中  3：已解散
//        if(DateUtils.getUntilSecond(new Date(), DateUtils.getDateHhMm(anchorLiveCreateReq.getStart_time())) <= 0){
//            //立即开播的意思，状态置为2 进行中
//            amwayGroup.setStatusAm(2);
//        }else{
//            amwayGroup.setStatusAm(1);
//        }
//        //目前该字段无实际含义 是否删除 0：否  1：是
//        amwayGroup.setIsDelete(0);
//        amwayGroup.setCreateTime(new Date());
////        boolean amwayFlag = simpleGenericDataBaseService.save(amwayGroup);
//        int amwayFlag = amwayGroupService.insert(amwayGroup);
//        //新增成功
//        if(amwayFlag>0) {
//            log.info("发起畅聊live_create-->畅聊发起成功");
//            //新增自己的人数
//            AmwayGroupUser amwayGroupUser = new AmwayGroupUser();
//            amwayGroupUser.setId(0L);
//            amwayGroupUser.setUserId(amwayUser.getId());
//            amwayGroupUser.setCreateTime(new Date());
//            amwayGroupUser.setGroupId(amwayGroup.getId().toString());
//            amwayGroupUser.setShareUserId("0");
//            groupUserService.insert(amwayGroupUser);
//            log.info("发起畅聊live_create-->发起畅聊，进入畅聊间数据更新成功groupId{}", amwayGroupUser.getGroupId());
//            //自己创建，则畅聊间数量初始化为1
//            amwayGroup.setMemberNum(1);
//            //返回前端实体对象
//            AnchorIndexRepose appletsAnchorIndexRepose = new AnchorIndexRepose();
//            appletsAnchorIndexRepose.setInfoList(amwayGroup);
//            return ResultUtils.success(appletsAnchorIndexRepose);
//        }
//        return ResultUtils.error("0","当前人数过多，请重试");
//    }
//    /**
//     * 手动解散畅聊间
//     * @param anchorLiveDisbandReq
//     * @return
//     */
//   /* @Override
//    public Result liveDisband(AnchorLiveDisbandReq anchorLiveDisbandReq) {
//            AmwayGroup amwayGroup = new AmwayGroup();
//            amwayGroup.setId(Long.parseLong(anchorLiveDisbandReq.getGroup_id()));
//            amwayGroup = simpleGenericDataBaseService.getOne(WrapperUtils.query(amwayGroup));
//            if(CheckUtils.isEmpty(amwayGroup)){
//                log.info("手动解散畅聊间live_disband-->当前畅聊群id{}不存在", anchorLiveDisbandReq.getGroup_id());
//                return ResultUtils.error("0","当前畅聊群不存在");
//            }
//            if(STATUS_DISSOLVED == amwayGroup.getStatusAm()){
//                log.info("手动解散畅聊间live_disband-->当前畅聊群id{}状态已解散，请勿重复操作", anchorLiveDisbandReq.getGroup_id());
//                return ResultUtils.error("0","当前畅聊群已解散，请勿重复操作");
//            }
//            //修改畅聊间状态为3、已解散
//            amwayGroup.setStatusAm(3);
//            boolean resultAmwayGroup = simpleGenericDataBaseService.updateById(amwayGroup);
//            if(resultAmwayGroup) {
//                log.info("手动解散畅聊间live_disband-->手动解散畅聊群成功，标识id{}", amwayGroup.getId());
//                return ResultUtils.success("手动解散畅聊群成功");
//            }
//            log.error("手动解散畅聊间live_disband-->手动解散畅聊群id{}失败",anchorLiveDisbandReq.getGroup_id());
//            return ResultUtils.error("0","当前人数过多，请重试");
//    }*/
//
//    @Override
//    public Result liveDisband(AnchorLiveDisbandReq anchorLiveDisbandReq) {
//        AmwayGroup amwayGroup = new AmwayGroup();
//        amwayGroup.setId(Long.parseLong(anchorLiveDisbandReq.getGroup_id()));
//        amwayGroup = amwayGroupService.getByGroupId(anchorLiveDisbandReq.getGroup_id());
//        if(CheckUtils.isEmpty(amwayGroup)){
//            log.info("手动解散畅聊间live_disband-->当前畅聊群id{}不存在", anchorLiveDisbandReq.getGroup_id());
//            return ResultUtils.error("0","当前畅聊群不存在");
//        }
//        if(STATUS_DISSOLVED == amwayGroup.getStatusAm()){
//            log.info("手动解散畅聊间live_disband-->当前畅聊群id{}状态已解散，请勿重复操作", anchorLiveDisbandReq.getGroup_id());
//            return ResultUtils.error("0","当前畅聊群已解散，请勿重复操作");
//        }
//        //修改畅聊间状态为3、已解散
//        amwayGroup.setStatusAm(3);
//        int resultAmwayGroup = amwayGroupService.update(amwayGroup);
//        if(resultAmwayGroup>0) {
//            log.info("手动解散畅聊间live_disband-->手动解散畅聊群成功，标识id{}", amwayGroup.getId());
//            return ResultUtils.success("手动解散畅聊群成功");
//        }
//        log.error("手动解散畅聊间live_disband-->手动解散畅聊群id{}失败",anchorLiveDisbandReq.getGroup_id());
//        return ResultUtils.error("0","当前人数过多，请重试");
//    }
//    /**
//     * 修改直播间转发权限
//     * @param groupId
//     * @param limitType
//     * @return
//     */
//   /* @Override
//    public Result liveSetLimit(String groupId, String limitType) {
//        AmwayGroup amwayGroup = new AmwayGroup();
//        amwayGroup.setId(Long.parseLong(groupId));
//        amwayGroup = simpleGenericDataBaseService.getOne(WrapperUtils.query(amwayGroup));
//        if(CheckUtils.isEmpty(amwayGroup)){
//            log.info("修改畅聊间转发权限live_setLimit-->当前畅聊间id{}不存在", groupId);
//            return ResultUtils.error("0","当前畅聊间id不存在");}
//        if(STATUS_DISSOLVED == amwayGroup.getStatusAm()){
//            log.info("修改畅聊间转发权限live_setLimit-->当前畅聊间id{}已解散，不能修改转发权限", groupId);
//            return ResultUtils.error("0","当前畅聊间已解散，不能修改转发权限");}
//        if(Integer.parseInt(limitType) == amwayGroup.getLimitType()){
//            log.info("修改畅聊间转发权限live_setLimit-->当前畅聊间id{}转发权限未做调整", groupId);
//            return ResultUtils.error("0","当前畅聊间转发权限未做调整");
//        }
//        //根据id更新amway_group表权限limit_type
//        amwayGroup.setLimitType(Integer.parseInt(limitType));
//        boolean resultAmwayGroup = simpleGenericDataBaseService.updateById(amwayGroup);
//        if(resultAmwayGroup) {
//            log.info("修改畅聊间转发权限live_setLimit-->修改畅聊间转发权限成功id{}",amwayGroup.getId());
//            AnchorLiveSetLimitRepose anchorLiveSetLimitRepose = new AnchorLiveSetLimitRepose();
//            anchorLiveSetLimitRepose.setLimit_type(amwayGroup.getLimitType());
//            return ResultUtils.success(anchorLiveSetLimitRepose);
//        }
//        log.info("修改畅聊间转发权限live_setLimit-->修改畅聊间id{}转发权限失败id{}",groupId);
//        return ResultUtils.error("0","当前人数过多，请重试");
//    }*/
//
//    @Override
//    public Result liveSetLimit(String groupId, String limitType) {
//        AmwayGroup amwayGroup = new AmwayGroup();
//        amwayGroup.setId(Long.parseLong(groupId));
//        amwayGroup = simpleGenericDataBaseService.getOne(WrapperUtils.query(amwayGroup));
//        if(CheckUtils.isEmpty(amwayGroup)){
//            log.info("修改畅聊间转发权限live_setLimit-->当前畅聊间id{}不存在", groupId);
//            return ResultUtils.error("0","当前畅聊间id不存在");}
//        if(STATUS_DISSOLVED == amwayGroup.getStatusAm()){
//            log.info("修改畅聊间转发权限live_setLimit-->当前畅聊间id{}已解散，不能修改转发权限", groupId);
//            return ResultUtils.error("0","当前畅聊间已解散，不能修改转发权限");}
//        if(Integer.parseInt(limitType) == amwayGroup.getLimitType()){
//            log.info("修改畅聊间转发权限live_setLimit-->当前畅聊间id{}转发权限未做调整", groupId);
//            return ResultUtils.error("0","当前畅聊间转发权限未做调整");
//        }
//        //根据id更新amway_group表权限limit_type
//        amwayGroup.setLimitType(Integer.parseInt(limitType));
//        boolean resultAmwayGroup = simpleGenericDataBaseService.updateById(amwayGroup);
//        if(resultAmwayGroup) {
//            log.info("修改畅聊间转发权限live_setLimit-->修改畅聊间转发权限成功id{}",amwayGroup.getId());
//            AnchorLiveSetLimitRepose anchorLiveSetLimitRepose = new AnchorLiveSetLimitRepose();
//            anchorLiveSetLimitRepose.setLimit_type(amwayGroup.getLimitType());
//            return ResultUtils.success(anchorLiveSetLimitRepose);
//        }
//        log.info("修改畅聊间转发权限live_setLimit-->修改畅聊间id{}转发权限失败id{}",groupId);
//        return ResultUtils.error("0","当前人数过多，请重试");
//    }
//    /**
//     * 获取分享需要的activity_id
//     * @return
//     */
//    @Override
//    public Result getActivityId() {
//        //微信的接口
//        String accessToken =  new AccessTokenUtils().getAccessToken(weChatAppid, weChatSecret, redisUtils);
//        if(accessToken == null){
//            log.error("获取分享需要的getActivity_id-->未获取到所需的token");
//            return ResultUtils.error("0","获取分享需要的activity_id失败");
//        }
//        String url = "https://api.weixin.qq.com/cgi-bin/message/wxopen/activityid/create?access_token=" + accessToken;
//        RestTemplate restTemplate = new RestTemplate();
//        //进行网络请求
//        ResponseEntity<String> responseEntity = restTemplate.exchange(url, HttpMethod.GET, null, String.class);
//        if (responseEntity != null && responseEntity.getStatusCode() == HttpStatus.OK) {
//            String ActivityData = responseEntity.getBody();
//            log.info("获取分享需要的activity_id-->返回的ActivityData："+ActivityData);
//            JSONObject retJson = JSONObject.parseObject(ActivityData);
//            Object activityId = retJson.get("activity_id");
//            AnchorActivityIdRepose AnchorActivityIdRepose= new AnchorActivityIdRepose();
//            if(activityId != null){
//                AnchorActivityIdRepose.setActivityId(activityId.toString());
//            }
//            return ResultUtils.success(AnchorActivityIdRepose);
//        }
//        log.info("获取分享需要的activity_id-->获取分享需要的activity_id失败");
//        return ResultUtils.error("0","当前人数过多，请重试");
//    }
//
//    /**
//     * 畅聊间数量限制
//     * @param wxAppApiToken
//     * @return
//     */
//    @Override
//    public Result liveCountLimit(String wxAppApiToken) {
//        AmwayGroup amwayGroup = new AmwayGroup();
//        //通过token查询用户唯一标识openid
//        String openId = JwtUtils.getUserNameFromToken(wxAppApiToken);
//        //通过openid查询用户信息
//        AmwayUser amwayUser = userCacheService.getUserInCacheByOpenId(openId);
//        if (CheckUtils.isEmpty(amwayUser)) {
//            log.error("畅聊间数量限制liveCountLimit-->畅聊间数量限制，用户信息不存在openid{}", openId);
//            return ResultUtils.error(ResultCodeEnum.A0201.value(), ResultCodeEnum.A0201.getName());
//        }
//        //获取AmwaySetting数量
//        AmwaySetting amwaySetting = amwaySettingService.getOne();
//        if(CheckUtils.isEmpty(amwaySetting)){
//            log.error("畅聊间数量限制liveCountLimit-->系统畅聊场数未设置，amwaySetting为空");
//            return ResultUtils.error("0", "系统畅聊场数未设置");
//        }
//        //根据openid查询user表获得创建人的畅聊ID
//        amwayGroup.setMemberId(amwayUser.getMemberId());
//        //进行中的
//        amwayGroup.setStatusAm(2);
//        int dayGroupNum = simpleGenericDataBaseService.count(WrapperUtils.query(amwayGroup).between("create_time", DateUtils.currentMin(new Date()), DateUtils.currentMax(new Date())));
//       //若每人每天最多开多少场畅聊数小于 当日主播创建的畅聊
//        if(amwaySetting.getMaxGroupPerDay() < dayGroupNum){
//            log.error("畅聊间数量限制liveCountLimit-->您今日创建畅聊间数量已达上限,创建人的畅聊memberid[{}],进行中的畅聊间数：[{}]", amwayUser.getMemberId(), dayGroupNum);
//            return ResultUtils.error("0", "您今日创建畅聊间数量已达上限，先解散一些进行中的畅聊");
//        }
//        //若每人最多开启多少场畅聊 小于 主播创建的畅聊
//        int sumNum = simpleGenericDataBaseService.count(WrapperUtils.query(amwayGroup));
//        if(amwaySetting.getMaxGroupTotal() < sumNum){
//            log.error("畅聊间数量限制liveCountLimit-->您总创建畅聊间数量已达上限，先解散一些进行中的畅聊,创建人的畅聊memberid[{}],进行中的畅聊间数：[{}]", amwayUser.getMemberId(), sumNum);
//            return ResultUtils.error("0", "您总创建畅聊间数量已达上限，先解散一些进行中的畅聊");
//        }
//        return ResultUtils.success("畅聊间数量限制成功");
//    }
//
//    /**
//     * 畅聊间消息敏感词过滤
//     * @param wxAppApiToken
//     * @return
//     */
//    @Override
//    public Result msgCheck(String wxAppApiToken,String words) {
//        String accessToken = new AccessTokenUtils().getAccessToken(weChatAppid, weChatSecret, redisUtils);
//        PrintWriter printWriter = null;
//        BufferedInputStream bufferedInputStream = null;
//        if(accessToken == null){
//            log.error("校验敏感词msgCheck-->未获取到所需的token");
//            return ResultUtils.error(ResultCodeEnum.A0431.value(), ResultCodeEnum.A0431.getName());
//        }
//        try {
//                URL url = new URL("https://api.weixin.qq.com/wxa/msg_sec_check?access_token=" + accessToken);
//                HttpURLConnection httpURLConnection = (HttpURLConnection) url.openConnection();
//                httpURLConnection.setRequestMethod("POST");
//                // 发送POST请求必须设置如下两行
//                httpURLConnection.setDoOutput(true);
//                httpURLConnection.setDoInput(true);
//                // 获取URLConnection对象对应的输出流
//                printWriter = new PrintWriter(httpURLConnection.getOutputStream());
//                // 发送请求参数
//                JSONObject paramJson = new JSONObject();
//                //对请求参数进行处理，请求最终格式xx:xx,xx:xx
//                paramJson.put("content", words);
//                printWriter.write(paramJson.toString());
//                // flush输出流的缓冲
//                printWriter.flush();
//                //开始获取数据
//                bufferedInputStream = new BufferedInputStream(httpURLConnection.getInputStream());
//                int len = 0;
//                byte[] arr = new byte[1024];
//                String retData = null;
//                while ((len = bufferedInputStream.read(arr)) != -1){
//                    retData = new String(arr, 0, len);
//                }
//
//                JSONObject retJson = JSONObject.parseObject(retData);
//                Object errCode = retJson.get("errcode");
//                if(!"0".equals(errCode.toString())){
//                    log.info("校验敏感词msgCheck-->{}:", retData);
//                    return ResultUtils.error(ResultCodeEnum.A0431.value(), ResultCodeEnum.A0431.getName());
//                }
//        }catch (Exception e){
//            log.error("敏感词获取失败：{}", e.getMessage());
//            return ResultUtils.error(ResultCodeEnum.A0431.value(), ResultCodeEnum.A0431.getName());
//        }finally {
//                printWriter.close();
//            try {
//                bufferedInputStream.close();
//            } catch (IOException e) {
//                e.printStackTrace();
//            }
//        }
//        return ResultUtils.success("畅聊间消息敏感词过滤校验成功");
//    }
//
//    /**
//     * 管理员人数上限以及伪装发布相关设置
//     * @param wxAppApiToken
//     * @return
//     */
//    @Override
//    public Result pageSetting(String wxAppApiToken) {
//        AmwaySetting amwaySetting = amwaySettingService.getOne();
//        if(CheckUtils.isEmpty(amwaySetting)){
//            log.error("管理员人数上限以及伪装发布相关设置pageSetting-->系统管理员未设置，amwaySetting为空");
//            return ResultUtils.error("0", "系统管理员参数未设置");
//        }
//        pageSettingRepose pageSettingRepose = new pageSettingRepose();
//        pageSettingRepose.setMaxAdminNum(amwaySetting.getMaxAdminNun());
//        pageSettingRepose.setOpenMsgCheck(amwaySetting.getOpenMsgCheck());
//        pageSettingRepose.setOpenVideoUpload(amwaySetting.getOpenVideoUpload());
//        pageSettingRepose.setVideoText(amwaySetting.getVideoText());
//        return ResultUtils.success(pageSettingRepose);
//    }
//}