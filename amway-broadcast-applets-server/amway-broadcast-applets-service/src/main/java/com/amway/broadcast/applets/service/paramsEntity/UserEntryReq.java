package com.amway.broadcast.applets.service.paramsEntity;

import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;

/**
 * 用户进入畅聊间
 */
@Data
public class UserEntryReq {

    /**
     * 畅聊间id
     */
    @NotBlank(message = "畅聊间id编号不能为空")
    @Pattern(regexp = "^\\d+$", message = "畅聊间id格式不正确")
    private String group_id;
    /**
     * 分享者用户id
     */
    @NotBlank(message = "分享者用户id不能为空")
    @Pattern(regexp = "^\\d+$", message = "分享者用户id格式不正确")
    private String share_user_id;

    /**
     * 进入类型
     */
    private String entry_type;

}
