package com.amway.broadcast.applets.service.paramsEntity;

import lombok.Data;

import javax.validation.constraints.NotBlank;

/**
 * 获取手机号req对象
 */
@Data
public class WxGetPhoneNumberReq {

    /**
     * 包括敏感数据在内的完整用户信息的加密数据
     */
    @NotBlank(message = "encryptedData不能为空")
    private String encryptedData;
    /**
     * 加密算法的初始向量
     */
    @NotBlank(message = "iv不能为空")
    private String iv;
}
