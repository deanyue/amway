package com.amway.broadcast.applets.service.paramsEntity;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.util.List;

/**
 * 返回直播群搜索用户数据
 */
@Data
public class SearchUserRepose {

    @JsonProperty("user_ids")
    private List<String> userIds;
}
