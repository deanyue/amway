package com.amway.broadcast.applets.service.api;

import com.amway.broadcast.applets.service.api.service.AmwayUserEntryService;
import com.amway.broadcast.applets.service.common.annotation.ServiceTokenRequired;
import com.amway.broadcast.applets.service.paramsEntity.UserEntryReq;
import com.amway.broadcast.applets.service.paramsEntity.UserQuitReq;
import com.amway.broadcast.common.base.Result;
import com.amway.broadcast.common.constant.HeadTokenConstant;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;

/**
 * 用户端
 */
@RestController
@CrossOrigin
@Slf4j
@Api(value = "小程序", tags = {"用户端"})
@Validated
public class ApiAmwayUserEntryController {

    @Resource
    private AmwayUserEntryService amwayUserService;

    /**
     * 用户进入畅聊间
     * @param userEntryReq
     * @return
     */
    @ApiOperation(value = "用户进入畅聊间", response = Result.class)
    @PostMapping("live_join")
    @ServiceTokenRequired
    //@RepeatSubmit(type = RepeatSubmit.Type.NORMAL)
    public Result liveJoin(@Validated UserEntryReq userEntryReq, @RequestHeader(HeadTokenConstant.WX_APP_API_TOKEN)String wxAppApiToken){
        return amwayUserService.liveJoin(userEntryReq, wxAppApiToken);
    }

    /**
     * 用户退出畅聊间
     * @param userQuitReq
     * @return
     */
    @ApiOperation(value = "用户退出畅聊间", response = Result.class)
    @PostMapping("live_quit")
    @ServiceTokenRequired
    //@RepeatSubmit(type = RepeatSubmit.Type.NORMAL)
    public Result liveQuit(@Validated UserQuitReq userQuitReq, @RequestHeader(HeadTokenConstant.WX_APP_API_TOKEN)String wxAppApiToken){
        return amwayUserService.liveQuit(userQuitReq, wxAppApiToken);
    }


    /**
     * 畅聊间列表
     * @param wxAppApiToken
     * @param status
     * @param isToday
     * @return
     */
    @ApiOperation(value = "畅聊间列表", response = Result.class)
    @GetMapping("user_live_list")
    @ServiceTokenRequired
    public Result userLiveList(@RequestHeader(HeadTokenConstant.WX_APP_API_TOKEN) String wxAppApiToken,
                                 @NotBlank(message = "畅聊间状态不能为空")   @Pattern(regexp = "^[12]$", message = "畅聊间状态有误") @RequestParam("status") String status,
                               @NotBlank(message = "时间区域选择不能为空")   @Pattern(regexp = "^[012]$", message = "时间区域选择状态有误") @RequestParam("all_time") String allTime,
                                 @NotBlank(message = "时间选择不能为空")   @Pattern(regexp = "^[012]$", message = "时间选择状态有误") @RequestParam("is_today") String isToday){
        return amwayUserService.userLiveList(wxAppApiToken, status,allTime, isToday);
    }

    /**
     * 直播群搜索用户
     * @param wxAppApiToken
     * @param nickName
     * @return
     */
    @ApiOperation(value = "直播群搜索用户", response = Result.class)
    @GetMapping("search_user")
    @ServiceTokenRequired
    public Result searchUser(@RequestHeader(HeadTokenConstant.WX_APP_API_TOKEN) String wxAppApiToken,
                               @NotBlank(message = "昵称不能为空")  @RequestParam("nickname") String nickName){
        return amwayUserService.searchUser(wxAppApiToken, nickName);
    }
}
