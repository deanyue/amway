package com.amway.broadcast.applets.service.paramsEntity;

import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import java.io.Serializable;
import java.util.List;

/**
 * @author liujch
 * @description 单人修改权限
 * @Package com.amway.broadcast.applets.service.paramsEntity
 * @date 2020年12月11日 09:56
 */

@Data
public class ModifyAdminPrivilegesReq implements Serializable {

    private static final long serialVersionUID = 5598666658236804994L;

    /**
     * 直播间id
     */
    @NotBlank(message = "直播间id不能为空")
    private String group_id;

    /**
     * 用户id
     */
    @NotEmpty(message = "用户id不能为空")
    private List<String> user_id;

    /**
     * 管理员操作：添加1取消0
     */
    private String operation;

}
