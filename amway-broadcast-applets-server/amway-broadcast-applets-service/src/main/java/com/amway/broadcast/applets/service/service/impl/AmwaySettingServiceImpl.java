package com.amway.broadcast.applets.service.service.impl;


import com.amway.broadcast.applets.service.dao.entity.AmwayLog;
import com.amway.broadcast.applets.service.dao.entity.AmwaySetting;
import com.amway.broadcast.applets.service.dao.entity.AmwayUser;
import com.amway.broadcast.applets.service.dao.mapper.AmwayLogMapper;
import com.amway.broadcast.applets.service.dao.mapper.AmwaySettingMapper;
import com.amway.broadcast.applets.service.service.AmwayLogService;
import com.amway.broadcast.applets.service.service.AmwaySettingService;
import com.amway.broadcast.common.service.impl.BaseServiceImpl;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections4.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;


/**
 * @author Dean
 */
@Service
@Slf4j
public class AmwaySettingServiceImpl extends BaseServiceImpl<AmwaySettingMapper, AmwaySetting> implements AmwaySettingService {

    @Autowired
    private AmwaySettingMapper amwaySettingMapper;

    @Override
    public AmwaySetting getOne() {
        List<AmwaySetting> settingList = amwaySettingMapper.selectList(new LambdaQueryWrapper<>());
        return CollectionUtils.isNotEmpty(settingList)? settingList.get(0) : null;
    }
}