package com.amway.broadcast.applets.service.api.service;

import com.amway.broadcast.applets.service.common.constant.AdminOperationTypeEnum;
import com.amway.broadcast.applets.service.paramsEntity.AnchorLiveCreateReq;
import com.amway.broadcast.applets.service.paramsEntity.AnchorLiveDisbandReq;
import com.amway.broadcast.common.base.Result;

import java.util.List;
import java.util.concurrent.Future;

/**
 * 小程序-->主播端
 */
public interface AppletsAnchorService {
    /**
     * 直播间首页
     * @param groupId 	直播间id
     * @return
     */
    Result liveIndex(String groupId);


    /**
     * 首页-全部直播间
     * @param  status 	状态 1未开始 2进行中（废弃）
     * @return 返回值说明：
     *      1。直播列表集合，永远只返回两个直播间数据，分为直播中和待开始两类的数据
     *          直播中的直播（以结束时间正序）优先级高于待开始的直播数据（以开始时间正序），
     *          缺的一类数据用另一类数据补全，集合中直播中的排在前面
     *      2。该人创建的所有直播间
     */
    Result index(String wxAppApiToken, String status);


    /**
     * 直播间列表
     * @param wxAppApiToken 小程序端请求服务器端的凭证
     * @param status 	状态 1：待开始 2：进行中 3：已过期
     * @param  allTime  0：全部 1：一周 2：一个月
     * @param  isToday  0：全部 1：当天 2：非当天
     * @return
     */
    Result liveList(String wxAppApiToken, String status, String allTime, String isToday);


    /**
     * 直播间分享
     * @param groupId 	直播间id
     * @param path 分享页路径
     * @return
     */
    Result liveShare(String groupId, String path);


    /**
     * 发起直播
     * @param anchorLiveCreateReq
     * @return
     */
    Result liveCreate(AnchorLiveCreateReq anchorLiveCreateReq, String wxAppApiToken);

    /**
     * 手动解散直播间
     * @param anchorLiveDisbandReq
     * @return
     */
    Result liveDisband(AnchorLiveDisbandReq anchorLiveDisbandReq);

    /**
     * 修改直播间转发权限
     * @param groupId
     * @param limitType
     * @return
     */
    Result liveSetLimit(String groupId, String limitType);

    /**
     * 获取分享需要的activity_id
     * @return
     */
    Result getActivityId();

    /**
     * 直播间数量限制
     * @return
     */
    Result liveCountLimit(String wxAppApiToken);

    /**
     * 直播间消息敏感词过滤
     * @param wxAppApiToken
     * @param words
     * @return
     */
    Result msgCheck(String wxAppApiToken,String words);

    /**
     * 管理员人数上限以及伪装发布相关设置
     * @param wxAppApiToken
     * @return
     */
    Result pageSetting(String wxAppApiToken);
}