package com.amway.broadcast.applets.service.service.impl;


import com.amway.broadcast.applets.service.dao.entity.AmwayUser;
import com.amway.broadcast.applets.service.dao.mapper.AmwayUserMapper;
import com.amway.broadcast.applets.service.service.AmwayUserService;
import com.amway.broadcast.common.service.impl.BaseServiceImpl;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;


/**
 * @author Dean
 */
@Service
@Slf4j
public class AmwayUserServiceImpl extends BaseServiceImpl<AmwayUserMapper, AmwayUser> implements AmwayUserService {

    @Autowired
    private AmwayUserMapper amwayUserMapper;

    @Override
    public AmwayUser getByOpenId(String openId) {
        LambdaQueryWrapper<AmwayUser> wrapper = new LambdaQueryWrapper<AmwayUser>().eq(AmwayUser::getOpenId, openId);
        return amwayUserMapper.selectOne(wrapper);
    }

    @Override
    public List<AmwayUser> getUserListByMemberId(Long memberId) {
        LambdaQueryWrapper<AmwayUser> wrapper = new LambdaQueryWrapper<AmwayUser>().eq(AmwayUser::getMemberId, memberId);
        return amwayUserMapper.selectList(wrapper);
    }
}