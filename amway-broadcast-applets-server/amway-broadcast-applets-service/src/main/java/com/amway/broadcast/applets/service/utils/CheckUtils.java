package com.amway.broadcast.applets.service.utils;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.alibaba.fastjson.TypeReference;
import lombok.extern.slf4j.Slf4j;

import java.io.UnsupportedEncodingException;
import java.util.*;
import java.util.regex.Pattern;

/**
 * 校验类
 *
 * @author Lee
 */
@Slf4j
public class CheckUtils {

    /**
     * 正则表达式样式
     */
    @SuppressWarnings("rawtypes")
    private static Map regexPatterns = new HashMap();

    /**
     * 获取正则表达式样式
     * 目的：将已使用过的正则表达式样式保存起来，实现反复利用
     *
     * @param regex 正则表达式
     * @return 正则表达式样式
     */
    @SuppressWarnings("unchecked")
    private static Pattern getRegexPattern(String regex) {
        if (!regexPatterns.containsKey(regex)) {
            Pattern p = Pattern.compile(regex);
            regexPatterns.put(regex, p);
        }
        return (Pattern) regexPatterns.get(regex);
    }

    /**
     * 是否能匹配正则表达式
     *
     * @param text  需匹配文本
     * @param regex 正则表达式
     * @return true:能够匹配 false：不能匹配
     */
    public static boolean hasMatched(String text, String regex) {
        Pattern p = getRegexPattern(regex);
        return p.matcher(text).matches();
    }

    /**
     * 判断是否为空
     *
     * @param obj
     * @return true:为空 false：不为空
     */
    @SuppressWarnings("rawtypes")
    public static boolean isEmpty(Object obj) {
        if (obj instanceof String) {
            return obj == null || ((String) obj).length() == 0;
        } else if (obj instanceof CharSequence) {
            return obj == null || ((CharSequence) obj).length() == 0;
        } else if (obj instanceof Object[]) {
            Object[] temp = (Object[]) obj;
            for (int i = 0; i < temp.length; i++) {
                if (!isEmpty(temp[i])) {
                    return false;
                }
            }
            return true;
        } else if (obj instanceof List) {
            return obj == null || ((List) obj).isEmpty();
        }

        return obj == null;
    }

    /**
     * 判断字符串是否为空
     *
     * @param str 要判断的字符串
     * @return true 为空 ，去掉前后空格后为""  false 去掉前后字符串后不为空
     */
    public static boolean isStringEmpty(String str) {
        if (str == null) {
            return true;
        } else if (str.trim().length() == 0) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * 判断是否是邮箱
     *
     * @param text
     * @return
     */
    public static boolean isMail(String text) {
        return text.matches("^\\w+((-\\w+)|(\\.\\w+))*\\@[A-Za-z0-9]+((\\.|-)[A-Za-z0-9]+)*\\.[A-Za-z0-9]+$");
    }

    /**
     * 判断是否是数字（0-9）
     *
     * @param text
     * @return
     */
    public static boolean isDigits(String text) {
        return text.matches("^\\d+$");
    }

    /**
     * 判断是否是数字，最大整数位8位，最大小数位2位
     *
     * @param text
     * @return
     */
    public static boolean isNumber(String text) {
        return text.matches("^(?:\\d{0,8})(?:\\.\\d{0,2})?$");
    }

    /**
     * 判断是否是一个中文汉字
     *
     * @param c 字符
     * @return true表示是中文汉字，false表示是英文字母
     * @throws UnsupportedEncodingException
     * @throws UnsupportedEncodingException 使用了JAVA不支持的编码格式
     */
    public static boolean isChineseChar(char c) {
        // 如果字节数大于1，是汉字   
        // 以这种方式区别英文字母和中文汉字并不是十分严谨，但在这个题目中，这样判断已经足够了   
        return String.valueOf(c).getBytes().length > 1;
    }

    /**
     * 判断第一个字符串是否包含某个路径
     * @param str 字符串
     * @param index 第几位
     * @param regex 符号
     *
     */
    public static String checkStr(String str,int index,String regex){
        if (str.startsWith(regex)) {
            str = str.substring(index);
        }
        return str;
    }

    /**
     * 替换指定的路径(如xx\\xx\\xx 替换成 xx/xx/xx)
     * @param str 字符串
     * @param regex 要替换标识符
     * @param newregex 替换成目的标识符
     * @return
     */
    public static String checkUrl(String str,String regex,String newregex){
        if(str.contains(regex)){
            str = str.replace(regex,newregex);
        }
        return str;
    }

    /**
     * 校验是否包含某个字符，然后按字符分割,返回分割后的字符数组,返回null，则代表不包含
     * @param str
     * @param regex
     * @return
     */
    public static String[] checkSplit(String str,String charset, String regex){
        if(str.indexOf(charset) != -1){
            String[] array = str.split(regex);
            return array;
        }
        return null;
    }

    /**
     *将url路径转成json字符串
     * @return
     */
    public static String checkJson(String str){
        str = str.replaceAll("=", "\":\"").replaceAll("&", "\",\"");;
        return "{\"" + str + "\"}";
    }

    /**
     * 返回json的value
     * @param str
     * @return
     */
    public static String getJsonValue(String str){
        String jsonStr = checkJson(str);
        StringBuffer values = new StringBuffer();
        List list = new ArrayList();
        LinkedHashMap<String, String> jsonMap = JSON.parseObject(jsonStr, new TypeReference<LinkedHashMap<String, String>>(){});
        for (Map.Entry entry : jsonMap.entrySet()) {
            values.append(entry.getValue().toString()+",");
        }
        String keywordStr = values.deleteCharAt(values.length() - 1).toString();
        return keywordStr;
    }
}
