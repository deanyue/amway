package com.amway.broadcast.applets.service.api.service;

import com.amway.broadcast.applets.service.dao.entity.AmwayUser;
import com.amway.broadcast.applets.service.paramsEntity.AdminKickReq;
import com.amway.broadcast.common.base.Result;

import java.util.List;

/**
 * @author Dean
 */
public interface UserCacheService {

    /**
     *  缓存中获取用户信息
     * @param openId
     * @return
     */
    AmwayUser getUserInCacheByOpenId(String openId);

    /**
     *  获取用户信息
     * @param openId
     * @return
     */
    AmwayUser getUserByOpenId(String openId);

    /**
     *  删除用户缓存
     * @param amwayUser
     * @return
     */
    void deleteCacheByAmwayUser(AmwayUser amwayUser);

    /**
     *  存放用户缓存
     * @param amwayUser
     * @return
     */
    void setCacheByAmwayUser(AmwayUser amwayUser);

    /**
     *
     * @param nickName
     * @return
     */
    List<AmwayUser> getUserByNickName(String nickName);
}
