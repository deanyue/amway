package com.amway.broadcast.applets.service.api.service;

import com.amway.broadcast.applets.service.common.constant.AdminOperationTypeEnum;
import com.amway.broadcast.applets.service.paramsEntity.AdminKickReq;
import com.amway.broadcast.applets.service.paramsEntity.AdminSetAuthReq;
import com.amway.broadcast.common.base.Result;

import java.util.List;

/**
 * @author WCX
 * @date 2020/12/10
 * 管理员相关
 *
 * 管理员列表功能
 * 设置直播间管理员权限
 * 添加管理员权限
 * 添加/取消管理员权限（单人）
 */
public interface AmwayManagerService {

    /**
     * 管理员权限详情
     * @param groupId
     * @return
     */
    Result getAdminAuthDetails(String groupId);

    /**
     * 管理员列表功能
     * @param groupId
     * @return
     */
    Result getAdminListByGroupId(String groupId);

    /**
     * 设置直播间管理员权限
     * @param adminSetAuthReq
     * @return
     */
    Result getAdminSetAuth(AdminSetAuthReq adminSetAuthReq);

    /**
     * 通过操作类型设置管理员或取消管理员
     * @param hostUserId 主播的用户id
     * @param groupId 主播间id
     * @param userIdes 用户id集合
     * @param operationType 操作类型（1：管理员｜0：非管理员）
     * @return 是否成功
     */
    Result settingAdminOrCancelByOperationType(String hostUserId, String groupId, List<String> userIdes, AdminOperationTypeEnum operationType);

    /**
     * 管理员踢人
     * @param adminKickReq
     * @return
     */
    Result adminKick(AdminKickReq adminKickReq);
}
