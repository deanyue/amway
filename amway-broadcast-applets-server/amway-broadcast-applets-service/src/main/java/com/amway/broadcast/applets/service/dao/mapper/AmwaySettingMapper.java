package com.amway.broadcast.applets.service.dao.mapper;

import com.amway.broadcast.applets.service.dao.entity.AmwaySetting;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

public interface AmwaySettingMapper extends BaseMapper<AmwaySetting> {
}
