package com.amway.broadcast.applets.service.paramsEntity;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

/**
 * 分享的返回参数 repose
 */
@Data
public class AnchorLiveShareRepose {
    @JsonProperty("share_url")
    private String shareUrl;

    @JsonProperty("share_img")
    private String shareImg;
}
