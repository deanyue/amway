package com.amway.broadcast.applets.service.paramsEntity;

import lombok.Data;

/**
 * @author WCX
 * @date 2020/12/10
 *
 * 管理员详情信息
 */
@Data
public class AdminListUserInfoRepose {
    /**
     * 用户ID
     */
    private Long id;
}
