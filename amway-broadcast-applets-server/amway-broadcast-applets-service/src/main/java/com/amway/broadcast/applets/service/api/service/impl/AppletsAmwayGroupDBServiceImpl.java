//package com.amway.broadcast.applets.service.api.service.impl;
//
//import com.amway.broadcast.applets.service.dao.entity.AmwayGroup;
//import com.amway.broadcast.applets.service.dao.mapper.AmwayGroupMapper;
//import com.amway.broadcast.common.service.AmwayGroupService;
//import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
//import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.stereotype.Service;
//import org.springframework.transaction.annotation.Transactional;
//
//import java.util.List;
//
///**
// * @auther 薛晨
// * @date 2020/12/12
// * @time 11:37
// * @description 数据库实现
// */
//@Service
//@Transactional(rollbackFor = Exception.class)
//public class AppletsAmwayGroupDBServiceImpl implements AmwayGroupService<AmwayGroup> {
//
//    @Autowired
//    private AmwayGroupMapper amwayGroupMapper;
//
//    @Override
//    public int insert(AmwayGroup baseAmwayGroup) {
//        return amwayGroupMapper.insert(baseAmwayGroup);
//    }
//
//    @Override
//    public int delete(String groupId) {
//        return 0;
//    }
//
//    @Override
//    public int update(AmwayGroup baseAmwayGroup) {
//        return amwayGroupMapper.updateById(baseAmwayGroup);
//    }
//
//    @Override
//    public List<AmwayGroup> getListByCondition(AmwayGroup condition) {
//        return amwayGroupMapper.selectList(new QueryWrapper<>(condition));
//    }
//
//    @Override
//    public AmwayGroup getByGroupId(String groupId) {
//        return amwayGroupMapper.selectOne(new LambdaQueryWrapper<AmwayGroup>().eq(AmwayGroup::getId, groupId));
//    }
//
//    @Override
//    public AmwayGroup getByImGroupId(String imGroupId) {
//        return amwayGroupMapper.selectOne(new LambdaQueryWrapper<AmwayGroup>().eq(AmwayGroup::getImGroupId, imGroupId));
//    }
//
//    @Override
//    public List<AmwayGroup> getByMemberId(Long memberId) {
//        return amwayGroupMapper.selectList(new LambdaQueryWrapper<AmwayGroup>().eq(AmwayGroup::getMemberId, memberId));
//    }
//
//}
