package com.amway.broadcast.applets.service.utils;


import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.net.URLConnection;

public class ImageUtils {

    private static File file = null;

    /**
     * 读取图像的二进制流
     *
     * @param infile
     * @return
     */
    public static FileInputStream getByteImage(String infile) {
        FileInputStream inputImage = null;
        file = new File(infile);
        try {
            inputImage = new FileInputStream(file);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        return inputImage;
    }

    /**
     * 输出图片
     *
     * @param inputStream
     * @param path
     */
    public static void readBlob(FileInputStream inputStream, String path) {
        FileOutputStream fileOutputStream = null;
        try {
            fileOutputStream = new FileOutputStream(path);
            byte[] buf = new byte[1024];
            int len = 0;
            while ((len = inputStream.read(buf)) != -1) {
                fileOutputStream.write(buf, 0, len);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }finally {
            try {
                fileOutputStream.close();
                fileOutputStream.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

    }

    public static void readImg(InputStream inputStream, String path) {
        FileOutputStream fileOutputStream = null;
        try {
            fileOutputStream = new FileOutputStream(path);
            byte[] buf = new byte[1024];
            int len = 0;
            while ((len = inputStream.read(buf)) != -1) {
                fileOutputStream.write(buf, 0, len);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }finally {
            try {
                inputStream.close();
                fileOutputStream.close();
            } catch (IOException e) {
                e.printStackTrace();
            }

        }
    }

    public static void downLoadImg(String imgPath,String savePath){
        //测试地址，"http://m.1more.com/image/pic1_ok.jpg";
        BufferedReader reader = null;
        try {
            URL url = new URL(imgPath);
            URLConnection conn = url.openConnection();
            ImageUtils.readImg(conn.getInputStream(), savePath);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (reader != null) {
                try {
                    reader.close();
                } catch (Exception e2) {
                }
            }
        }
    }

    /**
     * 生成图片  返回图片保持路径
     * @param httpPath
     * @param backPath
     */
    public static String createImage(String httpPath, String backPath){
        BufferedImage image = null;
        try {
            File file = new File(backPath);
            if(!file.exists()){
                ImageUtils.downLoadImg (httpPath, backPath);
                //使用系统缓存
                ImageIO.setUseCache(false);
                ImageIO.read(new FileInputStream (backPath));
            }
            return backPath;
        } catch (IOException e) {
            e.printStackTrace ();
        }
        return null;
    }
}
