//package com.amway.broadcast.applets.service.dao.entity;
//
//import com.amway.broadcast.common.base.BaseAmwayGroup;
//import com.amway.broadcast.common.base.BaseAmwayGroupUser;
//import com.baomidou.mybatisplus.annotation.TableField;
//import com.baomidou.mybatisplus.annotation.TableId;
//import com.baomidou.mybatisplus.annotation.TableName;
//import lombok.Getter;
//import lombok.Setter;
//
//import java.io.Serializable;
//import java.util.Date;
//
///**
// * 直播群-用户 关系表（用户加入直播间记录）
// */
//@Getter
//@Setter
//@TableName(value = "amway_group_user")
//public class AmwayGroupUser extends BaseAmwayGroupUser implements Serializable {
//
//    private static final long serialVersionUID = 7883992849173226701L;
//
//    @TableId(value = "id")
//    private Long id;
//
//    //直播群ID号
//    @TableField(value = "group_id")
//    private String groupId;
//
//    //用户id号
//    @TableField(value = "user_id")
//    private Long userId;
//
//    //分享者id号
//    @TableField(value = "share_user_id")
//    private String shareUserId;
//
//    //创建时间
//    @TableField(value = "create_time")
//    private Date createTime;
//
//    //创建时间
//    @TableField(value = "is_quit")
//    private Integer isQuit;
//
//    //是否群管理员
//    @TableField(value = "is_manager")
//    private String isManager;
//}
