package com.amway.broadcast.applets.service.service;


import com.amway.broadcast.applets.service.dao.entity.AmwayLog;
import com.amway.broadcast.applets.service.dao.entity.AmwayUser;
import com.amway.broadcast.common.service.BaseService;


/**
 * @author Dean
 */
public interface AmwayLogService extends BaseService<AmwayLog> {

}