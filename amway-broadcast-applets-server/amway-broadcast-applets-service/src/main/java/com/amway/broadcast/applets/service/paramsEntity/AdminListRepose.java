package com.amway.broadcast.applets.service.paramsEntity;

import lombok.Data;

import java.util.List;

/**
 * @author WCX
 * @date 2020/12/10
 *
 * 管理员列表页返回参数实体封装对象
 */
@Data
public class AdminListRepose {

    /**
     * 管理员详情列表
     */
    private String[] ids;

    /**
     * 是否允许管理员设置全体禁言 (0为否，1为是，默认为1)
     */
    private String is_all_mute;

    /**
     * 是否允许管理员踢走成员(0为否，1为是，默认为1)
     */
    private String is_kick;

    /**
     * 管理员人数是否已达到上限(0为否，1为是)
     */
    private String is_admin_limit;
}
