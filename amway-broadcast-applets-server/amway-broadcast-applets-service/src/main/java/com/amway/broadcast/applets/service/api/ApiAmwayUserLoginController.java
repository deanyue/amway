package com.amway.broadcast.applets.service.api;

import com.amway.broadcast.applets.service.api.service.AmwayUserLoginService;
import com.amway.broadcast.applets.service.common.annotation.ServiceTokenRequired;
import com.amway.broadcast.applets.service.paramsEntity.LoginReq;
import com.amway.broadcast.applets.service.paramsEntity.LoginWxReq;
import com.amway.broadcast.applets.service.paramsEntity.SaveUserInfoReq;
import com.amway.broadcast.common.base.Result;
import com.amway.broadcast.common.constant.HeadTokenConstant;
import com.amway.broadcast.common.util.IpUtils;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.validation.constraints.NotBlank;

@RestController
@CrossOrigin
@Slf4j
@Api(value = "登入API接口", tags = {"amway登入"})
@Validated
public class ApiAmwayUserLoginController {
    @Resource
    private AmwayUserLoginService amwayUserLoginService;

    /***
     * 原wx_login接口
     * 根据code，通过微信API获取openId和sessionKey
     * 通过jwt加密包装后返回
     * @param loginWXReq
     * @return
     */
    @ApiOperation(value = "微信登录", response = Result.class)
    @PostMapping("wx_login")
    public Result getOpenId(@Validated LoginWxReq loginWXReq) {
        return amwayUserLoginService.getOpenId(loginWXReq.getCode());
    }

    @ApiOperation(value = "账号密码模拟登录", response = Result.class)
    @PostMapping("login")
    @ServiceTokenRequired
    //@RepeatSubmit(type = RepeatSubmit.Type.NORMAL)
    public Result login(@Validated LoginReq loginReq, HttpServletRequest request) {
        String wxAppApiToken = request.getHeader(HeadTokenConstant.WX_APP_API_TOKEN);
        return amwayUserLoginService.login(loginReq,  wxAppApiToken);
    }

    /**
     * 暂时弃用
     *
     * @param
     * @return
     */
    @ApiOperation(value = "获取用户登录即时通信IM的UserSig", response = Result.class)
    @GetMapping("im_get_user_sig")
    @ServiceTokenRequired
    //@RepeatSubmit(type = RepeatSubmit.Type.NORMAL)
    public Result imGetUserSig(@RequestHeader(HeadTokenConstant.WX_APP_API_TOKEN) String wxapp_api_token) {
        return amwayUserLoginService.imGetUserSig(wxapp_api_token);
    }

    /**
     * 用户授权用户信息 (服务端保存用户信息)
     * 如果首次登录的话，需要绑定用户信息
     * todo 如果登录者已经授权了的话，是否要考虑刷新授权
     *
     * @param saveUserInfoReq
     * @return
     */
    @ApiOperation(value = "用户授权用户信息", response = Result.class)
    @PostMapping("wx_save_user_info")
    @ServiceTokenRequired
    public Result wxSaveUserInfo(@Validated SaveUserInfoReq saveUserInfoReq, @RequestHeader(HeadTokenConstant.WX_APP_API_TOKEN) String wxAppApiToken) {
        return amwayUserLoginService.wxSaveUserInfo(saveUserInfoReq, wxAppApiToken);
    }

    /**
     * 企业微信自动登录
     *
     * @param code
     * @return
     */
    @ApiOperation(value = "企业微信自动登录", response = Result.class)
    @GetMapping("GetQyUserid")
    @ServiceTokenRequired
    //@RepeatSubmit(type = RepeatSubmit.Type.WX_LOGIN)
    public Result getQyUserId(@RequestHeader(HeadTokenConstant.WX_APP_API_TOKEN) String wxAppApiToken,
                              @NotBlank(message = "企业微信登录获取的code不能为空") @RequestParam("code") String code) {
        return amwayUserLoginService.getQyUserId(wxAppApiToken, code);
    }
}