package com.amway.broadcast.applets.service.service.impl;

import com.amway.broadcast.applets.service.service.AmwayGroupUserService;
import com.amway.broadcast.applets.service.dao.entity.NewAmwayGroupUser;
import com.amway.broadcast.applets.service.dao.mapper.AmwayGroupUserMapper;
import com.amway.broadcast.common.service.impl.BaseServiceImpl;
import org.springframework.stereotype.Service;

/**
 * @auther 薛晨
 * @date 2020/12/14
 * @time 10:20
 * @description
 */
@Service
public class AmwayGroupUserServiceImpl extends BaseServiceImpl<AmwayGroupUserMapper, NewAmwayGroupUser> implements AmwayGroupUserService {
}
