package com.amway.broadcast.applets.service.dao.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * 替换AmwayGroupUser
 */
@Data
@TableName(value = "amway_group_user")
public class NewAmwayGroupUser implements Serializable {

    private static final long serialVersionUID = 7883992849173226701L;

    @TableId(value = "id",type = IdType.AUTO)
    private Long id;

    //直播群ID号
    private Long groupId;

    //用户id号
    private Long userId;

    //分享者id号
    private Long shareUserId;

    //创建时间
    private Date createTime;

    //是否群管理员
    private Integer isManager;


}
