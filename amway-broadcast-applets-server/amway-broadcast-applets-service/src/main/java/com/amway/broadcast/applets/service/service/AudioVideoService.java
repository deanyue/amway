package com.amway.broadcast.applets.service.service;

import com.amway.broadcast.applets.service.entity.vo.AuditResultVO;

/**
 * @auther 薛晨
 * @date 2020/12/16
 * @time 10:37
 * @description
 */
public interface AudioVideoService {
    String recognition(String mp3Url);

    String merge(String media_strs);

    String saveVideo(String video_url);

    String getSignature();

    String audit(String fileId);

    AuditResultVO auditResult(String taskId);

    String publish(String fileId);

    Integer uploadAudio(String media_strs);

    AuditResultVO mergeMedia(Integer taskId);

    String editMedia(String fileIds);
}
