package com.amway.broadcast.applets.service.utils;
import com.amway.broadcast.common.exception.user.request.PleaseWaitForUserActionException;
import lombok.extern.slf4j.Slf4j;
import net.coobird.thumbnailator.Thumbnails;
import org.apache.commons.codec.binary.Base64;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.geom.RoundRectangle2D;
import java.awt.image.BufferedImage;
import java.io.*;
import java.nio.file.Files;

/**
 * @Description TODO 在图片上生成二维码并且在图片上添加文字 保存指定地址文件名
 */
@Slf4j
public class ImageUtil {

    /**
     * @Description TODO 在一张背景图上添加二维码
     * @param   content 二维码内容
     * @param   fielPath 保存文件 例： d:/1.jpg
     * @param   hbPath 海报图片地址 例： d:/1.jpg
     * @param   logoPath 二维码logo
     * @return
     */
    public static void drawString(String content, String fielPath, String hbPath, String logoPath) {
        BufferedImage image = addWater(content, hbPath, logoPath);
        try {
            ImageIO.write(image,"jpg",new File(fielPath));
        } catch (IOException e) {
            log.error("drawString合成图片失败{}", e.getMessage());
            throw new PleaseWaitForUserActionException ("请重新获取分享码");
        }
    }


    /***
     * 在一张背景图上添加二维码
     */
    public static BufferedImage addWater(String content, String hbPath, String logoPath)  {
        //得到文件
        File file = new File(hbPath);
        //文件转化为图片
        Image srcImg = null;
        try {
            srcImg = ImageIO.read(file);
        } catch (IOException e) {
            e.printStackTrace();
        }
        //获取图片的宽
        int srcImgWidth = srcImg.getWidth(null);
        //获取图片的高
        int srcImgHeight = srcImg.getHeight(null);
        // 加水印
        BufferedImage bufImg = new BufferedImage(srcImgWidth, srcImgHeight, BufferedImage.TYPE_INT_RGB);
        Graphics2D g = bufImg.createGraphics();
        g.drawImage(srcImg, 0, 0, srcImgWidth, srcImgHeight, null);
        //使用工具类生成二维码
        Image image = createQrCode(content, 226, 226, logoPath);

        //将小图片绘到大图片上,500,300 .表示你的小图片在大图片上的位置。
        g.drawImage(image, 550, 445, null);
        //设置颜色。
        g.setColor(Color.WHITE);
        g.dispose();
        return bufImg;
    }

    private static BufferedImage createQrCode(String content, int width, int height, String logoPath)  {
        BufferedImage image = null;
        FileInputStream fileInputStream = null;
        try {
            if (logoPath != null) {
                fileInputStream = new FileInputStream(new File(logoPath));
                image = ImageIO.read(fileInputStream);
                image = setClip((BufferedImage) image,200);
            }
        }catch (Exception e){
            log.error("createQrCode 二维码获取失败{}", e.getMessage());
            throw new PleaseWaitForUserActionException("请重新获取分享码");
        }finally {
            try {
                fileInputStream.close ();
            }catch (Exception e){
                log.error ("二维码生成失败",e.getMessage ());
            }
        }
        return image;
    }

    /**
     * 图片切圆角
     * @param srcImage
     * @param radius
     * @return
     */
    public static BufferedImage setClip(BufferedImage srcImage, int radius){
        int width = srcImage.getWidth();
        int height = srcImage.getHeight();
        BufferedImage image = new BufferedImage(width, height, BufferedImage.TYPE_INT_ARGB);
        Graphics2D gs = image.createGraphics();
        gs.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
        gs.setClip(new RoundRectangle2D.Double(0, 0, width, height, radius, radius));
        gs.drawImage(srcImage, 0, 0, null);
        gs.dispose();
        return image;
    }
    /**
     * 指定图片宽度和高度和压缩比例对图片进行压缩
     *
     * @param imgsrc 源图片地址
     * @param imgdist 目标图片地址
     */
    public static void reduceImg(String imgsrc, String imgdist) {
        try {
            Thumbnails.of(imgsrc).scale(0.8f).outputQuality(0.5f).toFile(imgdist);
        } catch (IOException ef) {
            log.error("reduceImg 压缩图片失败{}", ef.getMessage());
            FilesUtils.deleteFile(imgsrc);
            throw new PleaseWaitForUserActionException ("请重新获取分享码");
        }
    }

    /**
     * 压缩code码，并进行白底
     * @param imgsrc
     * @param imgdist
     */
    public static void reduceImgCode(String imgsrc, String imgdist){
        try {
            Thumbnails.of(imgsrc).addFilter(new ThumbnailsImgFilter()).size(150, 150).keepAspectRatio(false).toFile(imgdist);
        } catch (IOException e) {
            FilesUtils.deleteFile(imgsrc);
            log.error("reduceImgCode 压缩code码失败{}", e.getMessage());
            throw new PleaseWaitForUserActionException ("请重新获取分享码");
        }
    }

    /**
     * @Description: 图片转base64编码格式
     * @Auther: pss
     */
    public static String readImage(String path) {
        byte[] fileByte = null;
        try {
            File file = new File(path);
            fileByte = Files.readAllBytes(file.toPath());
        } catch (IOException e) {
            e.printStackTrace();
        }
        return  Base64.encodeBase64String(fileByte);
    }

}