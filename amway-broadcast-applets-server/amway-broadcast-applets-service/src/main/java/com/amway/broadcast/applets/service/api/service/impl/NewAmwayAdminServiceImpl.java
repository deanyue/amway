//package com.amway.broadcast.applets.service.api.service.impl;
//
//import com.amway.broadcast.applets.service.api.service.AmwayAdminService;
//import com.amway.broadcast.applets.service.dao.entity.NewAmwayGroupUser;
//import com.amway.broadcast.applets.service.dao.mapper.AmwayGroupUserMapper;
//import com.amway.broadcast.applets.service.paramsEntity.AdminKickReq;
//import com.amway.broadcast.applets.service.service.AmwayGroupUserService;
//import com.amway.broadcast.common.base.Result;
//import com.amway.broadcast.common.base.ResultUtils;
//import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
//import lombok.extern.slf4j.Slf4j;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.stereotype.Service;
//import org.springframework.transaction.annotation.Transactional;
//
//import java.util.List;
//
///**
// * @auther 薛晨
// * @date 2020/12/14
// * @time 10:36
// * @description
// */
//@Service
//@Slf4j
//public class NewAmwayAdminServiceImpl implements AmwayAdminService {
//    @Autowired
//    private AmwayGroupUserService groupUserService;
//    @Autowired
//    private AmwayGroupUserMapper groupUserMapper;
//
//    @Override
//    @Transactional(rollbackFor = Exception.class)
//    public Result adminKick(AdminKickReq adminKickReq) {
//        String groupId = adminKickReq.getGroup_id();
//        String userId = adminKickReq.getUser_id();
//        LambdaQueryWrapper<NewAmwayGroupUser> condition = new LambdaQueryWrapper<NewAmwayGroupUser>().eq(NewAmwayGroupUser::getGroupId, Long.valueOf(groupId)).eq(NewAmwayGroupUser::getUserId, Long.valueOf(userId));
//        List<NewAmwayGroupUser> list = groupUserMapper.selectList(condition);
//        if (list.isEmpty()) {
//            return ResultUtils.error("0", "当前踢出的用户不存在");
//        }
//        int deleteCount = groupUserMapper.delete(condition);
//        if (deleteCount > 0) {
//            return ResultUtils.success("踢出成功");
//        }
//        return ResultUtils.success("当前用户可能已被其他管理员踢出");
//    }
//}
