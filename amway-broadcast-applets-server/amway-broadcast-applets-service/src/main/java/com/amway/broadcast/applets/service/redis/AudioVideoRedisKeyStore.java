package com.amway.broadcast.applets.service.redis;

import lombok.Data;

/**
 * 音视频相关的redis key 定义
 */
@Data
public class AudioVideoRedisKeyStore {
    public static final String MERGE_MEDIA_PREFIX = "MERGE_MEDIA_PREFIX_";
    public static final String MERGE_MEDIA_TASK_PREFIX = "MERGE_MEDIA_TASK_PREFIX_";
}
