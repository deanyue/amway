package com.amway.broadcast.applets.service.config.thread;

import lombok.extern.slf4j.Slf4j;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

/**
 * @auther 薛晨
 * @date 2020/12/18
 * @time 15:51
 * @description audio_video中间表线程池
 */
@Slf4j
public class AudioVideoThreadPool {
    private static final ExecutorService executorService = new ThreadPoolExecutor(8, 2048,
            10L, TimeUnit.SECONDS,
            new LinkedBlockingQueue<>());

    public static void execute(Runnable runnable) {
        try {
            executorService.execute(runnable);
        } catch (Exception e) {
            log.error("GroupUserThreadPool异常{}", e);
        }
    }
}
