package com.amway.broadcast.applets.service.dao.mapper;

import com.amway.broadcast.applets.service.dao.entity.AmwayDepartment;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

public interface AmwayDepartmentMapper extends BaseMapper<AmwayDepartment> {
}
