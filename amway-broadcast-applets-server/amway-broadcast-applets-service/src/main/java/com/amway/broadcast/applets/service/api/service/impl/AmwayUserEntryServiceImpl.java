//package com.amway.broadcast.applets.service.api.service.impl;
//
//import com.amway.broadcast.applets.service.api.service.AmwayUserEntryService;
//import com.amway.broadcast.applets.service.dao.entity.AmwayGroup;
//import com.amway.broadcast.applets.service.dao.entity.AmwayGroupUser;
//import com.amway.broadcast.applets.service.dao.entity.AmwayIndex;
//import com.amway.broadcast.applets.service.dao.entity.AmwayUser;
//import com.amway.broadcast.applets.service.paramsEntity.AnchorIndexRepose;
//import com.amway.broadcast.applets.service.paramsEntity.UserEntryReq;
//import com.amway.broadcast.applets.service.paramsEntity.UserQuitReq;
//import com.amway.broadcast.applets.service.redis.RedisUtils;
//import com.amway.broadcast.applets.service.utils.CheckUtils;
//import com.amway.broadcast.common.base.Result;
//import com.amway.broadcast.common.base.ResultUtils;
//import com.amway.broadcast.common.enumeration.ResultCodeEnum;
//import com.amway.broadcast.common.util.DateUtils;
//import com.amway.broadcast.common.util.JwtUtils;
//import com.amway.broadcast.mybatis.autoconfigure.util.WrapperUtils;
//import com.vdurmont.emoji.EmojiParser;
//import lombok.extern.slf4j.Slf4j;
//import org.springframework.stereotype.Service;
//
//import javax.annotation.Resource;
//import java.util.ArrayList;
//import java.util.Date;
//import java.util.List;
//
///**
// * 用户端
// */
//@Slf4j
//@Service
//public class AmwayUserEntryServiceImpl implements AmwayUserEntryService {
//    @Resource
//    private RedisUtils redisUtils;
//
//    @Resource
//    private SimpleGenericDataBaseServiceImpl simpleGenericDataBaseService;
//    /**
//     * 是否退出 0否  1是  2待进入
//     */
//    private static final Integer NO_QUIT = 0;
//    private static final Integer YES_QUIT = 1;
//    private static final Integer WAITING_QUIT = 2;
//
//    /**
//     * 用户进入直播间标识位，1代表待进入
//     */
//    private static final String ENTRY_TYPE = "1";
//
//    /**
//     * 畅聊间查询时间的选择类型  0：全部 1：当天 2：非当天
//     */
//    private static final String ALL_TOTAY = "0";
//    private static final String SAME_TOTAY = "1";
//    private static final String NOT_SAME_TOTAY = "2";
//
//
//    /**
//     * 用户进入畅聊间
//     * @param userEntryReq
//     * @return
//     */
//    @Override
//    //@Async("asyncServiceExecutor")
//    public Result liveJoin(UserEntryReq userEntryReq,String wxAppApiToken) {
//        AmwayGroup amwayGroup = new AmwayGroup();
//        amwayGroup.setId(Long.parseLong(userEntryReq.getGroup_id()));
//        amwayGroup = simpleGenericDataBaseService.getOne(WrapperUtils.query(amwayGroup));
//        if(CheckUtils.isEmpty(amwayGroup)){
//            log.info("用户进入畅聊间live_join-->当前畅聊间id{}不存在",userEntryReq.getGroup_id());
//            return ResultUtils.error("0","当前畅聊间id不存在");
//        }
//        //通过token获取openid
//        String openId = JwtUtils.getUserNameFromToken(wxAppApiToken);
//        //通过openid唯一标识查询user表中用户id
//        AmwayUser amwayUser = new AmwayUser();
//        amwayUser.setOpenId(openId);
//        amwayUser = (AmwayUser)redisUtils.checkUser(openId, amwayUser);
//        //未查到用户信息，则代表不存在。
//        if(CheckUtils.isEmpty(amwayUser)){
//            log.error("用户进入畅聊间live_join-->用户信息不存在openid{}", openId);
//            return ResultUtils.error(ResultCodeEnum.A0201.value(),ResultCodeEnum.A0201.getName());
//        }
//
//        String userId = amwayUser.getId().toString();
//        AmwayGroupUser amwayGroupUser = new AmwayGroupUser();
//        amwayGroupUser.setUserId(userId);
//        amwayGroupUser.setGroupId(userEntryReq.getGroup_id());
//        //若根据userid和group_id查询group_user表，判断是否存在，不存在，则新增；若存在，判断是否为老用户重进。
//        AmwayGroupUser newAmwayGroupUser = simpleGenericDataBaseService.getOne(WrapperUtils.query(amwayGroupUser));
//
//        //参数
//        amwayGroupUser.setId(0L);
//        amwayGroupUser.setCreateTime(new Date());
//        amwayGroupUser.setShareUserId(userEntryReq.getShare_user_id());
//        String entryType = userEntryReq.getEntry_type();
//        //1为不进入直播间，其它则进入   且当前直播间无记录
//        if(ENTRY_TYPE.equals(entryType) && CheckUtils.isEmpty(newAmwayGroupUser)){
//            amwayGroupUser.setIsQuit(WAITING_QUIT);
//            simpleGenericDataBaseService.save(amwayGroupUser);
//            log.info("用户进入畅聊间live_join-->不统计直播间人数");
//            return ResultUtils.success("进入畅聊间成功");
//        }
//        //纯新用户
//        if(CheckUtils.isEmpty(newAmwayGroupUser)){
//            log.info("用户进入畅聊间live_join-->新用户进入畅聊间，更新数据成功");
//            simpleGenericDataBaseService.save(amwayGroupUser);
//            return ResultUtils.success("进入畅聊间成功");
//        }
//        //老用户退出，重进
//        if(YES_QUIT.equals(newAmwayGroupUser.getIsQuit()) || ! ENTRY_TYPE.equals(entryType) && WAITING_QUIT.equals(newAmwayGroupUser.getIsQuit())){
//            log.info("用户进入畅聊间live_join-->老用户[{}]进入畅聊间，更新数据状态成功",newAmwayGroupUser.getIsQuit());
//            newAmwayGroupUser.setIsQuit(NO_QUIT);
//            simpleGenericDataBaseService.updateById(newAmwayGroupUser);
//            return ResultUtils.success("进入畅聊间成功");
//        }
//        return ResultUtils.success("进入畅聊间成功");
//    }
//
//    /**
//     * 用户退出畅聊间
//     * @param userQuitReq
//     * @return
//     */
//    @Override
//    public Result  liveQuit(UserQuitReq userQuitReq,String wxAppApiToken) {
//        AmwayGroup amwayGroup = new AmwayGroup();
//        amwayGroup.setId(Long.parseLong(userQuitReq.getGroup_id()));
//        amwayGroup = simpleGenericDataBaseService.getOne(WrapperUtils.query(amwayGroup));
//        if(CheckUtils.isEmpty(amwayGroup)){
//            log.info("用户退出畅聊间live_quit-->当前畅聊间id{}不存在", userQuitReq.getGroup_id());
//            return ResultUtils.error("0","当前畅聊间id不存在");
//        }
//        //通过token获取openid
//        String openId = JwtUtils.getUserNameFromToken(wxAppApiToken);
//        //通过openid唯一标识查询user表中用户id
//        AmwayUser amwayUser = new AmwayUser();
//        amwayUser.setOpenId(openId);
//        amwayUser = (AmwayUser)redisUtils.checkUser(openId, amwayUser);
//        //未查到用户信息，则代表不存在。
//        if(CheckUtils.isEmpty(amwayUser)){
//            log.error("用户退出畅聊间live_quit-->用户信息不存在，无法获取用户的id信息,openId{}", openId);
//            return ResultUtils.error(ResultCodeEnum.A0201.value(),ResultCodeEnum.A0201.getName());
//        }
//        String userId = amwayUser.getId().toString();
//        AmwayGroupUser amwayGroupUser = new AmwayGroupUser();
//        amwayGroupUser.setUserId(userId);
//        amwayGroupUser.setGroupId(userQuitReq.getGroup_id());
//        //若根据userid和group_id查询group_user表，判断是否存在，不存在，则未进入畅聊间；若存在，则判断状态再更新。
//        amwayGroupUser = simpleGenericDataBaseService.getOne(WrapperUtils.query(amwayGroupUser));
//        if(CheckUtils.isEmpty(amwayGroupUser)){
//            log.info("用户退出畅聊间live_quit-->当前用户未进入该畅聊间");
//            return ResultUtils.error("0","当前用户未进入该畅聊间,无法退出");
//        }
//        if(YES_QUIT.equals(amwayGroupUser.getIsQuit())){
//            log.info("用户退出畅聊间live_quit-->当前用户已退出畅聊间，请勿重复操作");
//            return ResultUtils.error("0","当前用户已退出畅聊间，请勿重复操作");
//        }
//        amwayGroupUser.setIsQuit(YES_QUIT);
//        boolean flag = simpleGenericDataBaseService.updateById(amwayGroupUser);
//        if(flag){
//            log.info("用户退出畅聊间live_quit-->退出畅聊间数据更新成功");
//            return ResultUtils.success("退出畅聊间成功");
//        }
//        return ResultUtils.error("0","退出畅聊间失败，请重试");
//    }
//
//    /**
//     * 畅聊间列表
//     * @param wxAppApiToken 小程序端请求服务器端的凭证
//     * @param status 状态 1：待开始 2：进行中 3：已过期
//     * @param isToday 0：全部 1：当天 2：非当天
//     * @return
//     */
//
//    @Override
//    //@Async("asyncServiceExecutor")
//    public Result userLiveList(String wxAppApiToken, String status, String isToday) {
//        //返回前端实体对象
//        AnchorIndexRepose appletsAnchorIndexRepose = new AnchorIndexRepose();
//        //装载畅聊间列表信息list，返回给前端
//        List<AmwayIndex> amwayIndexList = new ArrayList<AmwayIndex>();
//        //userid list集合
//        List<String> groupIdList = new ArrayList<String>();
//        //查询数据库获得的list数据
//        List<AmwayGroup> amwayGroupList = new ArrayList<AmwayGroup>();
//        //通过token查询用户唯一标识openid
//        String openId = JwtUtils.getUserNameFromToken(wxAppApiToken);
//        //通过openid查询用户信息
//        AmwayUser amwayUser = new AmwayUser();
//        amwayUser.setOpenId(openId);
//        amwayUser = (AmwayUser)redisUtils.checkUser(openId, amwayUser);
//        if (CheckUtils.isEmpty(amwayUser)) {
//            log.error("畅聊间列表user_live_list-->用户信息不存在,无法获取id参数，openid{}", openId);
//            return ResultUtils.error(ResultCodeEnum.A0201.value(), ResultCodeEnum.A0201.getName());
//        }
//        //通过userId查询group_user表验证是否预约或者进入畅聊间的所有记录
//        AmwayGroupUser amwayGroupUser = new AmwayGroupUser();
//        amwayGroupUser.setUserId(amwayUser.getId().toString());
//        //未退出畅聊间的
//        //amwayGroupUser.setIsQuit(0);
//        //不为1-退出   则代表0-未退出,2-待进入
//        List<AmwayGroupUser> amwayGroupUserList = simpleGenericDataBaseService.list(WrapperUtils.query(amwayGroupUser).notIn("is_quit","1"));
//        log.info("畅聊间列表user_live_list-->通过userid验证group_user是否存在记录条数："+amwayGroupUserList.size());
//        for(AmwayGroupUser amwayGroupUsers : amwayGroupUserList){
//            groupIdList.add(amwayGroupUsers.getGroupId());
//        }
//        if(groupIdList.size()>0) {
//            AmwayGroup amwayGroup = new AmwayGroup();
//            amwayGroup.setStatusAm(Integer.parseInt(status));
//            //畅聊时间
//            switch (isToday) {
//                //0：全部
//                case ALL_TOTAY:
//                    amwayGroupList = simpleGenericDataBaseService.list(WrapperUtils.query(amwayGroup).in("id", groupIdList));
//                    break;
//                //1:当天
//                case SAME_TOTAY:
//                    amwayGroupList = simpleGenericDataBaseService.list(WrapperUtils.query(amwayGroup).in("id", groupIdList).between("start_time", DateUtils.currentMin(new Date()),DateUtils.currentMax(new Date())));
//                    break;
//                //2、非当天
//                case NOT_SAME_TOTAY:
//                    amwayGroupList = simpleGenericDataBaseService.list(WrapperUtils.query(amwayGroup).in("id", groupIdList).notBetween("start_time",DateUtils.currentMin(new Date()),DateUtils.currentMax(new Date())));
//                    break;
//                default:
//                    break;
//            }
//        }
//        for (AmwayGroup amwayGrouplist : amwayGroupList) {
//            AmwayIndex amwayIndex = new AmwayIndex();
//            amwayIndex.setId(amwayGrouplist.getId());
//            amwayIndex.setImGroupId(amwayGrouplist.getImGroupId());
//            amwayIndex.setName(EmojiParser.parseToUnicode(amwayGrouplist.getName()));
//            amwayIndex.setStartTime(amwayGrouplist.getStartTime());
//            amwayIndex.setStatus(amwayGrouplist.getStatusAm());
//            amwayIndex.setStartTime2(amwayGrouplist.getStartTime());
//            //统计畅聊间人数 根据Id去查询group_user表有多少条记录
//            AmwayGroupUser amwayGroupU = new AmwayGroupUser();
//            amwayGroupU.setGroupId(amwayGrouplist.getId().toString());
//            //只统计存在的用户
//            amwayGroupU.setIsQuit (0);
//            int memberNum = simpleGenericDataBaseService.count(WrapperUtils.query(amwayGroupU));
//            amwayIndex.setMemberNum(memberNum);
//            amwayIndexList.add(amwayIndex);
//        }
//        //畅聊间列表
//        appletsAnchorIndexRepose.setGroupList(amwayIndexList);
//        return ResultUtils.success(appletsAnchorIndexRepose);
//    }
//
//}