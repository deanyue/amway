package com.amway.broadcast.applets.service.service.impl;


import com.amway.broadcast.applets.service.dao.entity.AmwayAccount;
import com.amway.broadcast.applets.service.dao.entity.AmwayUser;
import com.amway.broadcast.applets.service.dao.mapper.AmwayAccountMapper;
import com.amway.broadcast.applets.service.dao.mapper.AmwayUserMapper;
import com.amway.broadcast.applets.service.service.AmwayAccountService;
import com.amway.broadcast.applets.service.service.AmwayUserService;
import com.amway.broadcast.common.service.impl.BaseServiceImpl;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


/**
 * @author Dean
 */
@Service
@Slf4j
public class AmwayAccountServiceImpl extends BaseServiceImpl<AmwayAccountMapper, AmwayAccount> implements AmwayAccountService {

    @Autowired
    private AmwayAccountMapper amwayAccountMapper;

}