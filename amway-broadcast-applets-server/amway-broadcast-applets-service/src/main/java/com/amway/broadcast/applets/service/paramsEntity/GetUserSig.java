package com.amway.broadcast.applets.service.paramsEntity;

import lombok.Data;

/**
 * @author WCX
 * @date 2020/11/3
 */
@Data
public class GetUserSig {
    private String user_id;

    private String user_sig;
}
