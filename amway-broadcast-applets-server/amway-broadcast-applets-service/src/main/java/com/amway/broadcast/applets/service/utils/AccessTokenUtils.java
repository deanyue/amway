package com.amway.broadcast.applets.service.utils;

import com.alibaba.fastjson.JSONObject;
import com.amway.broadcast.applets.service.redis.RedisUtils;
import com.amway.broadcast.common.constant.WXApiConstant;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang.StringUtils;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

/**
 * 获取accessToken方法
 */
@Slf4j
public class AccessTokenUtils {
    /**
     * 微信小程序 redis key
     */
    public static final String ACCESS_TOKEN = "accessToken";
    /**
     * 企业微信小程序 redis key
     */
    public static final String QY_ACCESS_TOKEN = "qyAccessToken";
    /**
     * redis 过期时间 秒
     */
    public static final int EXPIRES_IN = 285;

    /**
     * 获取微信accessToken，失败则返回null
     * @return
     */
    public String getAccessToken(String weChatAppid, String weChatSecret,RedisUtils redisUtils)  {
        String accessToken = null;

       if(redisUtils.getTime(ACCESS_TOKEN) > 0){
           accessToken = redisUtils.get(ACCESS_TOKEN).toString();
           return accessToken;
       }else {
           String url = WXApiConstant.APPID_TOKEN + weChatAppid + "&secret=" + weChatSecret;
           RestTemplate restTemplate = new RestTemplate();
           //进行网络请求
           try {
               ResponseEntity<String> responseEntity = restTemplate.exchange(url, HttpMethod.GET, null, String.class);
               if (responseEntity != null && responseEntity.getStatusCode() == HttpStatus.OK) {
                   String tokenData = responseEntity.getBody();
                   JSONObject retJson = JSONObject.parseObject(tokenData);
                   accessToken = retJson.get("access_token").toString();
                   if (StringUtils.isNotBlank(accessToken)) {
                       redisUtils.set(ACCESS_TOKEN, accessToken, EXPIRES_IN);
                       return accessToken;
                   }
               }
           } catch (Exception e) {
               log.error("获取getAccessToken-->token获取异常{}", e.getMessage());
           }
           return null;
       }
    }

    /**
     * 获取企业微信token，失败则返回null
     * @param corpid
     * @param corpsecret
     * @return
     */
    public String getQyAccessToken(String corpid, String corpsecret, RedisUtils redisUtils)  {
        String qyAccessToken = null;
        if(redisUtils.getTime(QY_ACCESS_TOKEN) > 0){
            qyAccessToken = redisUtils.get(QY_ACCESS_TOKEN).toString();
            return qyAccessToken;
        }else {
            String url = WXApiConstant.GET_TOKEN + corpid + "&corpsecret=" + corpsecret;
            RestTemplate restTemplate = new RestTemplate();
            //进行网络请求
            try {
                ResponseEntity<String> responseEntity = restTemplate.exchange(url, HttpMethod.GET, null, String.class);
                if (responseEntity != null && responseEntity.getStatusCode() == HttpStatus.OK) {
                    String tokenData = responseEntity.getBody();
                    JSONObject retJson = JSONObject.parseObject(tokenData);
                    qyAccessToken = retJson.get("access_token").toString();
                    if (StringUtils.isNotBlank(qyAccessToken)) {
                        redisUtils.set(QY_ACCESS_TOKEN, qyAccessToken, EXPIRES_IN);
                        return qyAccessToken;
                    }
                }
            } catch (Exception e) {
                log.error("获取getQyAccessToken-->企业微信获取access_token异常{}", e.getMessage());
            }
            return null;
        }
    }
}
