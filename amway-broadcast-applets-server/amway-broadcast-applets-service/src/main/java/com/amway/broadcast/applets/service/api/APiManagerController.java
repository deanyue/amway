package com.amway.broadcast.applets.service.api;

import com.amway.broadcast.applets.service.api.service.AmwayManagerService;
import com.amway.broadcast.applets.service.common.annotation.ServiceTokenRequired;
import com.amway.broadcast.applets.service.common.constant.AdminOperationTypeEnum;
import com.amway.broadcast.applets.service.paramsEntity.*;
import com.amway.broadcast.common.base.Result;
import com.amway.broadcast.common.base.ResultUtils;
import com.amway.broadcast.common.constant.HeadTokenConstant;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.validation.constraints.NotBlank;
import java.util.Arrays;
import java.util.Collections;
import java.util.concurrent.Future;

/**
 * @author WCX
 * @date 2020/12/10
 *
 * 管理员功能相关
 *
 * 管理员列表功能
 * 设置直播间管理员权限
 * 添加管理员权限
 * 添加/取消管理员权限（单人）
 */
@RestController
@CrossOrigin
@Slf4j
@Api(value = "小程序2.0", tags = {"主播端-管理员功能"})
@Validated
public class APiManagerController {

    @Resource
    private AmwayManagerService amwayManagerService;
    /**
     * 管理员权限详情
     * @param groupId 	畅聊间id
     * @return
     */
    @ApiOperation(value = "管理员权限详情", response = Result.class)
    @GetMapping("admin_auth_details")
    @ServiceTokenRequired
    public Result adminAuthDetails(@NotBlank(message = "畅聊间id不能为空")  @RequestParam("group_id") String groupId){
        return amwayManagerService.getAdminAuthDetails(groupId);
    }

    /**
     * 管理员列表功能
     * @param groupId 	畅聊间id
     * @return
     */
    @ApiOperation(value = "管理员列表页", response = Result.class)
    @GetMapping("admin_list")
    @ServiceTokenRequired
    public Result getAdminListByGroupId(@NotBlank(message = "畅聊间id不能为空")  @RequestParam("group_id") String groupId){
        return amwayManagerService.getAdminListByGroupId(groupId);
    }

    /**
     * 设置直播间管理员权限
     * @param adminSetAuthReq 	畅聊间id
     * @return
     */
    @ApiOperation(value = "设置直播间管理员权限", response = Result.class)
    @PostMapping("admin_set_auth")
    @ServiceTokenRequired
    public Result getAdminSetAuth(@Validated AdminSetAuthReq adminSetAuthReq){
        return amwayManagerService.getAdminSetAuth(adminSetAuthReq);
    }

    @ApiOperation(value = "废弃-添加管理员权限（多人）", response = Result.class)
    @PostMapping("admin_appoint_multi")
    @ServiceTokenRequired
    @Deprecated
    public Result adminAppointMulti(@RequestHeader(HeadTokenConstant.WX_APP_API_TOKEN)String wxAppApiToken, @Validated ModifyMultiAdminPrivilegesReq modifyMultiAdminPrivilegesReq){
        return amwayManagerService.settingAdminOrCancelByOperationType(wxAppApiToken
                , modifyMultiAdminPrivilegesReq.getGroup_id()
                , Arrays.asList(modifyMultiAdminPrivilegesReq.getUser_ids().split(","))
                , AdminOperationTypeEnum.ADD_ADMIN);
    }

    @ApiOperation(value = "添加/取消管理员权限（单人）", response = Result.class)
    @PostMapping("admin_appoint")
    @ServiceTokenRequired
    public Result adminAppoint(@RequestHeader(HeadTokenConstant.WX_APP_API_TOKEN)String wxAppApiToken, @Validated ModifyAdminPrivilegesReq modifyAdminPrivilegesReq){
        return amwayManagerService.settingAdminOrCancelByOperationType(wxAppApiToken
                , modifyAdminPrivilegesReq.getGroup_id()
                , modifyAdminPrivilegesReq.getUser_id()
                , AdminOperationTypeEnum.getAdminOperationTypeEnum(modifyAdminPrivilegesReq.getOperation()));
    }
    @ApiOperation(value = "管理员踢人（包括主播踢人）", response = Result.class)
    @PostMapping("admin_kick")
    @ServiceTokenRequired
    public Result adminKick(@Validated AdminKickReq adminKickReq, @RequestHeader(HeadTokenConstant.WX_APP_API_TOKEN)String wxAppApiToken){
        return amwayManagerService.adminKick(adminKickReq);
    }
}
