package com.amway.broadcast.mybatis.autoconfigure.cache;

import com.amway.broadcast.mybatis.autoconfigure.properties.MybatisPlusProperties;
import com.amway.broadcast.mybatis.autoconfigure.util.ApplicationContextHolder;
import lombok.extern.slf4j.Slf4j;
import org.apache.ibatis.cache.Cache;
import org.springframework.data.redis.connection.RedisServerCommands;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.util.CollectionUtils;

import java.util.Set;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;

/**
 * Redis二级缓存
 *
 * @author Lee
 */
@Slf4j
public class MybatisRedisCache implements Cache {

    // 读写锁
    private final ReadWriteLock readWriteLock = new ReentrantReadWriteLock(true);

    private RedisTemplate<String, Object> redisTemplate;

    private String id;

    private MybatisPlusProperties mybatisPlusProperties;

    @Override
    public String getId() {
        return this.id;
    }

    @Override
    public ReadWriteLock getReadWriteLock() {
        return this.readWriteLock;
    }

    @Override
    public void putObject(Object key, Object value) {
        try {
            String keyStr  = key.toString().replaceAll(this.id,"{"+this.id+"}");
            if (value != null) {
                redisTemplate.opsForValue().set(keyStr, value, mybatisPlusProperties.getEntryTtl(), TimeUnit.HOURS);
            }
        } catch (Exception e) {
            log.error("缓存出错:{}", e.getStackTrace());
        }
    }

    @Override
    public Object getObject(Object key) {
        try {
            if (key != null) {
                String keyStr  = key.toString().replaceAll(this.id,"{"+this.id+"}");
                Object ret = redisTemplate.opsForValue().get(keyStr);
                return ret;
            }
        } catch (Exception e) {
            log.error("缓存出错:{}", e.getStackTrace());
        }
        return null;
    }

    @Override
    public Object removeObject(Object key) {
        try {
            if (key != null) {
                String keyStr  = key.toString().replaceAll(this.id,"{"+this.id+"}");
                redisTemplate.delete(keyStr);
            }
        } catch (Exception e) {
            log.error("缓存出错:{}", e.getStackTrace());
        }
        return null;
    }

    @Override
    public void clear() {
        try {
            Set<String> keys = redisTemplate.keys("*:{" + this.id + "}*");
            if (!CollectionUtils.isEmpty(keys)) {
                redisTemplate.delete(keys);
            }
        } catch (Exception e) {
            log.error("缓存出错:{}", e.getStackTrace());
        }
    }

    @Override
    public int getSize() {
        Long size = 0L;
        try {
            size = redisTemplate.execute(RedisServerCommands::dbSize);
        } catch (Exception e) {
            log.error("缓存出错:{}", e.getStackTrace());
        }
        return size.intValue();
    }

    @Override
    public int hashCode() {
        if (this.getId() == null) {
            throw new IllegalArgumentException("Cache instances require an ID.");
        }
        return getId().hashCode();
    }

    @Override
    public boolean equals(Object o) {
        if (this.getId() == null) {
            throw new IllegalArgumentException("Cache instances require an ID.");
        } else if (this == o) {
            return true;
        } else if (!(o instanceof Cache)) {
            return false;
        } else {
            Cache otherCache = (Cache) o;
            return this.getId().equals(otherCache.getId());
        }
    }

    public MybatisRedisCache(final String id) {
        if (id == null) {
            throw new IllegalArgumentException("Cache instances require an ID");
        }
        this.id = id;
        this.redisTemplate = ApplicationContextHolder.getBean("redisTemplate");
        this.mybatisPlusProperties = ApplicationContextHolder.getBean("mybatisPlusProperties");
    }

    public MybatisRedisCache() {
    }
}
