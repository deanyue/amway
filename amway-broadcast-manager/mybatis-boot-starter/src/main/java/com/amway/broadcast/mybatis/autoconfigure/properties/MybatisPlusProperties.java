package com.amway.broadcast.mybatis.autoconfigure.properties;

import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@Getter
@Setter
@Component
@ConfigurationProperties(prefix = "mybatis-plus")
public class MybatisPlusProperties {


    /**
     * 缓存有效期
     */
    private Integer entryTtl;




}
