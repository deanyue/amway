package com.amway.broadcast.mybatis.autoconfigure.util;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.core.metadata.TableFieldInfo;
import com.baomidou.mybatisplus.core.metadata.TableInfo;
import com.baomidou.mybatisplus.core.metadata.TableInfoHelper;
import com.baomidou.mybatisplus.core.toolkit.CollectionUtils;
import com.baomidou.mybatisplus.core.toolkit.ReflectionKit;
import com.baomidou.mybatisplus.core.toolkit.StringUtils;
import lombok.extern.slf4j.Slf4j;

import java.io.Serializable;
import java.util.List;
import java.util.Objects;

/**
 * mybatis条件对象工具类
 *
 * @author Lee
 */
@Slf4j
public class WrapperUtils implements Serializable {

    private WrapperUtils() {
    }

    /**
     * 构造空查询条件
     *
     * @param entity
     * @param <T>
     * @return
     */
    public static <T> QueryWrapper<T> query(T entity) {
        return new QueryWrapper(entity);
    }

    /**
     * 构造空更新条件
     *
     * @param <T>
     * @return
     */
    public static <T> UpdateWrapper<T> update() {
        return new UpdateWrapper();
    }

    /**
     * 构造更新条件，用实体对象构造更新条件
     *
     * @param entity
     * @param <T>
     * @return
     */
    public static <T> UpdateWrapper<T> update(T entity) {
        return new UpdateWrapper(entity);
    }


    public static <E> E getObject(List<E> list) {
        if (CollectionUtils.isNotEmpty(list)) {
            int size = list.size();
            if (size > 1) {
                log.warn(String.format("Warn: execute Method There are  %s results.", size));
            }

            return list.get(0);
        } else {
            return null;
        }
    }

    /**
     * 判断条件中的实体对象是否不为空且是否为实体对象
     *
     * @param wrapper 条件对象
     * @param <T>
     * @return
     */
    public static <T> boolean nonEmptyOfEntity(Wrapper wrapper) {
        T entity = (T) wrapper.getEntity();
        if (entity == null) {
            return false;
        } else {
            TableInfo tableInfo = TableInfoHelper.getTableInfo(entity.getClass());
            if (tableInfo == null) {
                return false;
            } else if (tableInfo.getFieldList().stream().anyMatch((e) -> {
                return fieldStrategyMatch(entity, e);
            })) {
                return true;
            } else {
                return StringUtils.isNotBlank(tableInfo.getKeyProperty()) ? true : false;
            }
        }
    }

    /**
     * 判断条件中的实体对象是否为空且是否不为实体对象
     *
     * @param wrapper 条件对象
     * @param <T>
     * @return
     */
    public static <T> boolean isEmptyOfEntity(Wrapper wrapper) {
        return !nonEmptyOfEntity(wrapper);
    }

    private static <T> boolean fieldStrategyMatch(T entity, TableFieldInfo e) {
        switch (e.getWhereStrategy()) {
            case NOT_NULL:
                return Objects.nonNull(ReflectionKit.getFieldValue(entity, e.getProperty()));
            case IGNORED:
                return true;
            case NOT_EMPTY:
                return StringUtils.checkValNotNull(ReflectionKit.getFieldValue(entity, e.getProperty()));
            default:
                return Objects.nonNull(ReflectionKit.getFieldValue(entity, e.getProperty()));
        }
    }


    /********************************************************构造lambda语法条件对象******************************************************************************/
//    public static <T> LambdaQueryWrapper<T> query(T entity) {
//        return new LambdaQueryWrapper(entity);
//    }
//
//    public static <T> LambdaUpdateWrapper<T> update(T entity) {
//        return (new UpdateWrapper(entity)).lambda();
//    }
    /********************************************************构造lambda语法条件对象******************************************************************************/


//    public static <T> QueryWrapper<T> emptyWrapper(T entity) {
//        return new EmptyWrapper();
//    }
}
