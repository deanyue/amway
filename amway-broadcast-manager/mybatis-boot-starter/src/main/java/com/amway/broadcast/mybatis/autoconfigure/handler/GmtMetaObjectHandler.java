package com.amway.broadcast.mybatis.autoconfigure.handler;

import com.baomidou.mybatisplus.core.handlers.MetaObjectHandler;
import org.apache.ibatis.reflection.MetaObject;

import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;

/**
 * 填充器
 *
 * @author Lee
 */
public class GmtMetaObjectHandler implements MetaObjectHandler {

    @Override
    public void insertFill(MetaObject metaObject) {
        metaObject.setValue("version", 0);
        metaObject.setValue("deleted", 0);
        DateTimeFormatter format =  DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
        metaObject.setValue("gmtCreate",format.format(ZonedDateTime.now()));
    }

    @Override
    public void updateFill(MetaObject metaObject) {
        DateTimeFormatter format =  DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
        this.setFieldValByName("gmtModified", format.format(ZonedDateTime.now()), metaObject);
    }
}
