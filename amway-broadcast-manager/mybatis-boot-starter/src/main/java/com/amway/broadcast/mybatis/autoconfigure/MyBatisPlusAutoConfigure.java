package com.amway.broadcast.mybatis.autoconfigure;

import com.amway.broadcast.mybatis.autoconfigure.cache.MybatisRedisCache;
import com.amway.broadcast.mybatis.autoconfigure.handler.GmtMetaObjectHandler;
import com.amway.broadcast.mybatis.autoconfigure.state.MyBatisPlusConfigure;
import com.amway.broadcast.mybatis.autoconfigure.util.ApplicationContextHolder;
import com.baomidou.mybatisplus.autoconfigure.MybatisPlusAutoConfiguration;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringBootConfiguration;
import org.springframework.boot.autoconfigure.AutoConfigureBefore;
import org.springframework.context.annotation.Import;

/**
 * MybatisPlus配置
 *
 * @author Lee
 */
@SpringBootConfiguration
@MapperScan({"com.amway.*.*.*.dao.mapper"})
@Import({GmtMetaObjectHandler.class, MyBatisPlusConfigure.class, MybatisRedisCache.class, ApplicationContextHolder.class})
@AutoConfigureBefore(MybatisPlusAutoConfiguration.class)
public class MyBatisPlusAutoConfigure {

}
