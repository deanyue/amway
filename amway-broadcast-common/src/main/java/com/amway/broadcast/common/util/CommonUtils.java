/**
 *
 */
package com.amway.broadcast.common.util;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.util.StringUtils;
import org.springframework.web.context.WebApplicationContext;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.regex.Pattern;

/**
 * 通用工具类
 *
 * @author Lee
 */
@Slf4j
public class CommonUtils {

    private CommonUtils() {
    }

    /**
     * 系统跟根路径
     */
    public static String bathPath;
    @SuppressWarnings("rawtypes")
    public static Map statisticMap = new HashMap();

    /**
     * spring factory
     */
    public static WebApplicationContext wac;


    private static final String PATTERN_D = "yyyy-MM-dd";
    private static final String PATTERN_S = "yyyy-MM-dd HH:mm:ss";


    /**
     *
     * @return
     * Date
     * @author 李振宇
     * @time 2015-12-30上午10:14:55
     */
    public static Date getDate() {
        return new Date();
    }

    /**
     * 字符串转日期
     * @param dataStr
     * @return
     * @throws ParseException
     * Date
     * @author 李振宇
     * @time 2017-5-10下午5:13:07
     */
    public static Date getDate(String dataStr) throws ParseException {
        SimpleDateFormat sdf = new SimpleDateFormat(PATTERN_S);
        return sdf.parse(dataStr);
    }

    /**
     * 字符串转日期
     * @param dataStr
     * @param pattern
     * @return
     * @throws ParseException
     * Date
     * @author 李振宇
     * @time 2017-5-10下午5:13:18
     */
    public static Date getDate(String dataStr, String pattern) throws ParseException {
        SimpleDateFormat sdf = new SimpleDateFormat(pattern);
        return sdf.parse(dataStr);
    }

    /**
     *
     * @return
     * String
     * @author 李振宇
     * @time 2017-2-15下午8:43:58
     */
    public static String getMilliseconds() {
        Calendar calendar = Calendar.getInstance();
        int YY = calendar.get(Calendar.YEAR);
        int MM = calendar.get(Calendar.MONTH) + 1;
        int DD = calendar.get(Calendar.DATE);
        int HH = calendar.get(Calendar.HOUR_OF_DAY);
        int mm = calendar.get(Calendar.MINUTE);
        int SS = calendar.get(Calendar.SECOND);
        int MI = calendar.get(Calendar.MILLISECOND);
        String curTime = String.valueOf(YY + MM + DD + HH + mm + SS + MI);
        return curTime;
    }

    public static String getTimeStamp() {
        Calendar calendar = Calendar.getInstance();
        int YY = calendar.get(Calendar.YEAR);
        int MM = calendar.get(Calendar.MONTH) + 1;
        int DD = calendar.get(Calendar.DATE);
        int HH = calendar.get(Calendar.HOUR_OF_DAY);
        int mm = calendar.get(Calendar.MINUTE);
        int SS = calendar.get(Calendar.SECOND);
        int MI = calendar.get(Calendar.MILLISECOND);
        String curTime = new String(new StringBuffer().append(YY).append(MM).append(DD).append(HH).append(mm).append(SS).append(MI));
        return curTime;
    }


    /**
     *
     * @return
     * String
     * @author 李振宇
     * @time 2015-12-30上午10:15:03
     */
    public static String getTime() {
        SimpleDateFormat sdf = new SimpleDateFormat(PATTERN_S);
        return sdf.format(getDate());
    }

    /**
     * yyyyMMddHHmmss
     * @param pattern
     * @return
     * String
     * @author 李振宇
     * @time 2015-12-30上午10:15:12
     */
    public static String getTime(String pattern) {
        SimpleDateFormat format = new SimpleDateFormat(pattern);
        return format.format(Calendar.getInstance().getTime());
    }

    /**
     *
     * @param pattern
     * @param date
     * @return
     * String
     * @author 李振宇
     * @time 2015-12-30上午10:15:18
     */
    public static String getTime(String pattern, Date date) {
        if (date == null) {
            return null;
        }
        SimpleDateFormat format = new SimpleDateFormat(pattern);
        return format.format(date);
    }

    /**
     *
     * @param pattern
     * @param time
     * @return
     * Date
     * @author 李振宇
     * @time 2015-12-30上午10:15:23
     */
    public static Date getTime(String pattern, String time) {
        SimpleDateFormat format = new SimpleDateFormat(pattern);
        Date date = null;
        try {
            date = format.parse(time);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return date;
    }

    /**
     *
     * @param pattern
     * @param pattern2
     * @param time
     * @return
     * String
     * @author 李振宇
     * @time 2015-12-30上午10:15:27
     */
    public static String getTime(String pattern, String pattern2, String time) {
        if (time == null || time.equals("")) {
            return "";
        }
        SimpleDateFormat format = null;
        if (time.length() == pattern.length()) {
            format = new SimpleDateFormat(pattern);
        } else if (time.length() == 8) {
            format = new SimpleDateFormat("yyyyMMdd");
        } else if (time.length() == 12) {
            format = new SimpleDateFormat("yyyyMMddHHmm");
        } else if (time.length() == 14) {
            format = new SimpleDateFormat("yyyyMMddHHmmss");
        } else {
            format = new SimpleDateFormat("yyyyMMddHHmmss");
        }

        Date date = null;
        try {
            date = format.parse(time);
        } catch (ParseException e) {
            return time;
        }
        return getTime(pattern2, date);
    }

    /**
     *
     * @param sqlDate
     * @return
     * java.util.Date
     * @author 李振宇
     * @time 2015-12-30上午10:15:32
     */
    public static Date sqlDateToUtilDate(java.sql.Timestamp sqlDate) {
        Date utilDate = null;
        if (sqlDate != null) {
            utilDate = new Date(sqlDate.getTime());
        }
        return utilDate;
    }

    /**
     * 分割字符串
     * @param content
     * @param length
     * @return
     * String[]
     * @author 李振宇
     * @time 2015-12-30上午10:15:40
     */
	/*public static String[] splitString(String content, int length) {
		if (length >= content.length()) {
			String str[] = new String[1];
			str[0] = content;
			return str;
		}
		StringBuffer buffer = new StringBuffer(content);
		StringBuffer res = null;
		int position = 1;
		while (true) {
			try {
				res = buffer.insert((position++) * length - 1, "`");
			} catch (StringIndexOutOfBoundsException e) {
				if (res.indexOf("`") > 0) {
					return res.toString().split("`");
				}
			}
		}
	}*/

    /**
     * 获取一周的第几天
     * @param date
     * @return
     * String[]
     * @author 李振宇
     * @time 2015-12-30上午10:15:54
     */
    public static String[] getWeek(Date date) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        String week[] = new String[7];
        int day_of_week = cal.get(Calendar.DAY_OF_WEEK);
        cal.set(cal.get(Calendar.YEAR), cal.get(Calendar.MONTH),
                cal.get(Calendar.DAY_OF_MONTH) - day_of_week + 1);
        int day = cal.get(Calendar.DAY_OF_MONTH);
        int month = cal.get(Calendar.MONTH);
        int year = cal.get(Calendar.YEAR);
        for (int i = 0; i < 7; i++) {
            cal.set(year, month, day + i);
            week[i] = CommonUtils.getTime("yyyy年MM月dd日 E", cal.getTime());
        }
        return week;
    }

    /**
     *
     * @param str
     * @return
     * String
     * @author 李振宇
     * @time 2015-12-30上午10:16:00
     */
    public static String getDateStr(String str) {
        Calendar cal = Calendar.getInstance();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy年MM月dd日 E");
        StringBuffer strDate = new StringBuffer();
        try {
            Date date = sdf.parse(str);
            cal.setTime(date);
            strDate.append(cal.get(Calendar.YEAR));
            String month = String.valueOf(cal.get(Calendar.MONTH) + 1);
            if (month.length() == 1) {
                month = "0" + month;
            }
            strDate.append(month);
            String day = String.valueOf(cal.get(Calendar.DAY_OF_MONTH));
            if (day.length() == 1) {
                day = "0" + day;
            }
            strDate.append(day);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return strDate.toString();
    }

    /**
     *
     * @param list
     * @return
     * String
     * @author 李振宇
     * @time 2015-12-30上午10:16:05
     */
    @SuppressWarnings({"unchecked", "rawtypes"})
    public static String getMaxElementByList(List list) {
        if (list.size() < 1) {
            return "0";
        }
        Collections.sort(list, new Comparator() {
            public int compare(Object o1, Object o2) {
                return (new Integer(((String) o2))).compareTo(new Integer(
                        ((String) o1)));
            }

            public boolean equals(Object obj) {
                return false;
            }
        });
        return (String) list.get(0);
    }

    /**
     * 8位以上时间格式转换
     * @param time
     * @return
     * String
     * @author 李振宇
     * @time 2015-12-30上午10:16:13
     */
    public static String formateTime(String time) {
        if (time.length() > 8) {
            return time.substring(0, 4) + "-" + time.substring(4, 6) + "-"
                    + time.substring(6, 8) + " " + time.substring(8, 10) + ":"
                    + time.substring(10, 12) + ":" + time.substring(12);
        } else {
            return time.substring(0, 4) + "-" + time.substring(4, 6) + "-"
                    + time.substring(6, 8) + " ";
        }
    }

    /**
     * formate time to chinese pattern with configuration on data length
     * @param time
     * @param length
     * @return
     * String
     * @author 李振宇
     * @time 2015-12-30上午10:16:20
     */
    public static String formateTimeChinese(String time, int length) {
        String tc = "";
        String temps = "";
        try {
            time = time.substring(0, length);
        } catch (Exception e) {
            return formateTimeChinese14(time);
        }
        try {
            temps = time.substring(0, 4);
            tc += temps + "年";
        } catch (Exception e) {
            return tc;
        }
        try {
            temps = time.substring(4, 6);
            tc += (temps.startsWith("0") ? temps.substring(1) : temps) + "月";
        } catch (Exception e) {
            return tc;
        }
        try {
            temps = time.substring(6, 8);
            tc += (temps.startsWith("0") ? temps.substring(1) : temps) + "日";
        } catch (Exception e) {
            return tc;
        }
        try {
            temps = time.substring(8, 10);
            tc += temps + "时";
        } catch (Exception e) {
            return tc;
        }
        try {
            temps = time.substring(10, 12);
            tc += temps + "分";
        } catch (Exception e) {
            return tc;
        }
        try {
            temps = time.substring(12, 14);
            tc += temps + "秒";
        } catch (Exception e) {
            return tc;
        }
        return tc;
    }

    /**
     * 14位时间格式转换
     * @param time
     * @return
     * String
     * @author 李振宇
     * @time 2015-12-30上午10:16:32
     */
    public static String formateTimeChinese14(String time) {
        String tc = "";
        try {
            String year = time.substring(0, 4);
            String month = time.substring(4, 6);
            String day = time.substring(6, 8);
            String hour = time.substring(8, 10);
            String min = time.substring(10, 12);
            String sec = time.substring(12);
            tc = year + "年"
                    + (month.startsWith("0") ? month.substring(1) : month)
                    + "月" + (day.startsWith("0") ? day.substring(1) : day)
                    + "日" + hour + "点" + min + "分" + sec + "秒";
        } catch (Exception e) {
            try {
                String year = time.substring(0, 4);
                String month = time.substring(4, 6);
                String day = time.substring(6, 8);
                tc = year + "年"
                        + (month.startsWith("0") ? month.substring(1) : month)
                        + "月" + (day.startsWith("0") ? day.substring(1) : day)
                        + "日";
            } catch (Exception e1) {
                return time;
            }
        }
        return tc;
    }

    /**
     * 生成随机字符串
     * @param size
     * @return
     * String
     * @author 李振宇
     * @time 2015-12-30上午10:16:41
     */
    public static String getRandomString(int size) {
        char[] c = {'1', '2', '3', '4', '5', '6', '7', '8', '9', '0', 'q',
                'w', 'e', 'r', 't', 'y', 'u', 'i', 'o', 'p', 'a', 's', 'd',
                'f', 'g', 'h', 'j', 'k', 'l', 'z', 'x', 'c', 'v', 'b', 'n', 'm'};
        Random random = new Random(); // 初始化随机数产生器
        StringBuffer sb = new StringBuffer();
        for (int i = 0; i < size; i++) {
            sb.append(c[Math.abs(random.nextInt()) % c.length]);
        }
        return sb.toString();
    }

    /**
     * 过滤request中的参数
     * @param parameterName
     * @param request
     * @return
     * String
     * @author 李振宇
     * @time 2015-12-30上午10:16:49
     */
    public static String getRequestParam(String parameterName,
                                         HttpServletRequest request) {
        String value = request.getParameter(parameterName);
        if (value == null || value.trim().equals("")) {
            return null;
        } else {
            return value;
        }
    }

    /**
     * 对key="value"这类解析出来的字符串进行操作，只取引号里的字符串
     * @param o
     * @return
     * String
     * @author 李振宇
     * @time 2015-12-30上午10:16:58
     */
    public static String analyze(Object o) {
        if (o == null)
            return null;
        String s = o.toString();
        int beginIndex = s.indexOf("\"");
        int endIndex = s.lastIndexOf("\"");
        return s.substring(beginIndex + 1, endIndex);
    }

    /**
     * 产生随机颜色
     * @return
     * String
     * @author 李振宇
     * @time 2015-12-30上午10:17:04
     */
    public static String getColor() {
        char[] zf = {'1', '2', '3', '4', '5', '6', '7', '8', '9', '0', 'A',
                'B', 'C', 'D', 'E', 'F'};
        Random random = new Random();
        StringBuffer sb = new StringBuffer();
        for (int i = 0; i < 6; i++) {
            sb.append(zf[Math.abs(random.nextInt(15))]);
        }
        if (sb.toString().equals("EEEEEE")) {
            getColor();
        }
        return "#" + sb.toString();
    }

    /**
     * 给javabean的所有属性值的两边加上*号
     * 兼容displayTag 让它支持模糊查询
     * @param javabean 满足javabean规范的一个对象,只支持String类型
     * void
     * @author 李振宇
     * @time 2015-12-30上午10:17:13
     */
    @SuppressWarnings({"rawtypes", "unchecked"})
    public static void supportLike(Object javabean) {
        Class cls = javabean.getClass();
        Object result = null;
        // 获取所有的属性
        Field field[] = cls.getDeclaredFields();
        for (int i = 0; i < field.length; i++) {
            try {
                String fieldName = field[i].getName();
                String getter = "get" + UpperFirst(fieldName);
                String setter = "set" + UpperFirst(fieldName);
                Method get = cls.getMethod(getter, new Class[]{});
                result = get.invoke(javabean, new Object[]{});
                if (!(result instanceof String)) {
                    continue;
                }
                Method set = cls.getMethod(setter, new Class[]{String.class});// 只支持字符串类型的set方法
                String identity = "*";
                if (null != get.invoke(javabean, new Object[]{})) {
                    Object getReturn = get.invoke(javabean, new Object[]{})
                            .toString();
                    getReturn = identity
                            + get.invoke(javabean, new Object[]{}).toString()
                            + identity;
                    set.invoke(javabean, new Object[]{getReturn});
                } else {
                    continue;
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * 让首字母变成小写
     * @param name
     * @return
     * String
     * @author 李振宇
     * @time 2015-12-30上午10:17:42
     */
    public static String LowerFirst(String name) {
        char first = name.charAt(0);
        String reg = first + "";
        String names = name.replaceFirst(reg, reg.toLowerCase());
        return names;
    }

    /**
     * 让首字母变成大写
     * @param name
     * @return
     * String
     * @author 李振宇
     * @time 2015-12-30上午10:17:42
     */
    public static String UpperFirst(String name) {
        char first = name.charAt(0);
        String reg = first + "";
        String names = name.replaceFirst(reg, reg.toUpperCase());
        return names;
    }

    /**
     * gbk转utf-8
     * @param str
     * @return
     * String
     * @author 李振宇
     * @time 2015-12-30上午10:17:48
     */
    public static String transToUTF(String str) {
        try {
            if (!"".equals(str) || null != str) {
                return new String(str.getBytes("GBK"), "UTF-8");
            }
            return "";
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
            return "";
        }
    }

    /**
     * 把form中的数据copy到bo中并返回
     * @param source
     * @param cls
     * @return
     * Object
     * @author 李振宇
     * @time 2015-12-30上午10:17:57
     */
    @SuppressWarnings("rawtypes")
    public static Object copySourceToTarget(Object source, Class cls) {
        Object obj = null;
        try {
            obj = cls.newInstance();
            BeanUtils.copyProperties(source, obj);
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
        return obj;
    }

    /**
     * 把form中的数据copy到bo中并返回
     * @param source
     * @param cls
     * @return
     * Object
     * @author 李振宇
     * @time 2015-12-30上午10:18:13
     */
    @SuppressWarnings({"rawtypes", "unchecked"})
    public static Object populate(Map source, Class cls) {
        Object obj = null;
        try {
            obj = cls.newInstance();
            BeanUtils.copyProperties(source, obj);
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
        return obj;
    }

    /**
     * 将srcipt代码发送到浏览器执行 该方法不能连续调用两次
     * @param sb
     * @param response
     * @throws IOException
     * void
     * @author 李振宇
     * @time 2015-12-30上午10:18:21
     */
    public static void printScript(StringBuffer sb, HttpServletResponse response)
            throws IOException {
        response.setCharacterEncoding("UTF-8");
        PrintWriter out = response.getWriter();
        out.print("<script>");
        out.println(sb);
        out.print("</script>");
        out.flush();
    }

    /**
     * 判断非空
     * @param str
     * @return
     * String
     * @author 李振宇
     * @time 2015-12-30上午10:18:27
     */
    public static String validator(Object str) {
        if (null == str) {
            return "";
        }
        return str.toString();
    }
    /**
     * 判断是否为空
     *
     * @param obj
     * @return true:为空 false：不为空
     */
    @SuppressWarnings("rawtypes")
    public static boolean isEmpty(Object obj) {
        if (obj instanceof String) {
            return obj == null || ((String) obj).length() == 0;
        } else if (obj instanceof CharSequence) {
            return obj == null || ((CharSequence) obj).length() == 0;
        } else if (obj instanceof Object[]) {
            Object[] temp = (Object[]) obj;
            for (int i = 0; i < temp.length; i++) {
                if (!isEmpty(temp[i])) {
                    return false;
                }
            }
            return true;
        } else if (obj instanceof List) {
            return obj == null || ((List) obj).isEmpty();
        }

        return obj == null;
    }

    /**
     * 判断字符串是否为空
     *
     * @param str 要判断的字符串
     * @return true 为空 ，去掉前后空格后为""  false 去掉前后字符串后不为空
     */
    public static boolean isStringEmpty(String str) {
        if (str == null) {
            return true;
        } else if (str.trim().length() == 0) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * 分包写入
     * @param os
     * @param b
     * void
     * @author 李振宇
     * @throws IOException
     * @time 2017-2-15下午9:01:13
     */
    public static void write(OutputStream os, byte[] b) throws IOException {
        int length = b.length;
        int size = 1024;
        int totalPage = length % size == 0 ? length / size : (int) (length / size) + 1;
        if (length % size == 0) {
            for (int i = 1; i <= totalPage; i++) {
                os.write(b, (i - 1) * size, size);
            }
        } else {
            for (int i = 1; i < totalPage; i++) {
                os.write(b, (i - 1) * size, size);
            }
            os.write(b, (totalPage - 1) * size, length % 1024);
        }
    }

    /**
     * 将url参数转换成map
     * @param param aa=11&bb=22&cc=33
     * @return
     */
    public static Map<String, String> getUrlParams(String param) {
        Map<String, String> map = new HashMap<>(0);
        if (StringUtils.isEmpty(param)) {
            return map;
        }
        String[] params = param.split("&");
        for (int i = 0; i < params.length; i++) {
            String[] p = params[i].split("=");
            if (p.length == 2) {
                map.put(p[0], p[1]);
            }
        }
        return map;
    }
    /*
     * 判断是否为整数
     * @param str 传入的字符串
     * @return 是整数返回true,否则返回false
     */
    public static boolean isNumeric(String string){
        Pattern pattern = Pattern.compile("[1-9]*");
        return pattern.matcher(string).matches();
    }


}
