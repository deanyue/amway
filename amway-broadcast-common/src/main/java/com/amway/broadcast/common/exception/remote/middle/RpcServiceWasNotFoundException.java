package com.amway.broadcast.common.exception.remote.middle;

import com.amway.broadcast.common.enumeration.ResultCodeEnum;
import com.amway.broadcast.common.exception.BaseException;

/**
 *  RPC服务未找到 C0111
 *
 * @author Lee
 * @version 1.0
 * @date 2020/5/9 9:29
 */
public class RpcServiceWasNotFoundException extends BaseException {

    public RpcServiceWasNotFoundException() {
        super(ResultCodeEnum.C0111.getName());
        this.code = ResultCodeEnum.C0111.value();
    }

    public RpcServiceWasNotFoundException(String message) {
        super(message);
        this.code = ResultCodeEnum.C0111.value();
    }

    public RpcServiceWasNotFoundException(Object data) {
        super(ResultCodeEnum.C0111.getName());
        this.code = ResultCodeEnum.C0111.value();
        this.data = data;
    }

    public RpcServiceWasNotFoundException(String message, Object data) {
        super(message);
        this.code = ResultCodeEnum.C0111.value();
        this.data = data;
    }
}
