package com.amway.broadcast.common.exception.user.version;

import com.amway.broadcast.common.enumeration.ResultCodeEnum;
import com.amway.broadcast.common.exception.BaseException;

/**
 *  用户API请求版本过低 A0807
 *
 * @author Lee
 * @version 1.0
 * @date 2020/5/8 17:39
 */
public class UserApiRequestVersionIsTooLowException extends BaseException {

    public UserApiRequestVersionIsTooLowException() {
        super(ResultCodeEnum.A0807.getName());
        this.code = ResultCodeEnum.A0807.value();
    }

    public UserApiRequestVersionIsTooLowException(String message) {
        super(message);
        this.code = ResultCodeEnum.A0807.value();
    }

    public UserApiRequestVersionIsTooLowException(Object data) {
        super(ResultCodeEnum.A0807.getName());
        this.code = ResultCodeEnum.A0807.value();
        this.data = data;
    }

    public UserApiRequestVersionIsTooLowException(String message, Object data) {
        super(message);
        this.code = ResultCodeEnum.A0807.value();
        this.data = data;
    }
}
