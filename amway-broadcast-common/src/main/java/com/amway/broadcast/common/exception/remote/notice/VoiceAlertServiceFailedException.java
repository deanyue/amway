package com.amway.broadcast.common.exception.remote.notice;

import com.amway.broadcast.common.enumeration.ResultCodeEnum;
import com.amway.broadcast.common.exception.BaseException;

/**
 *  语音提醒服务失败 C0502
 *
 * @author Lee
 * @version 1.0
 * @date 2020/5/9 10:15
 */
public class VoiceAlertServiceFailedException extends BaseException {

    public VoiceAlertServiceFailedException() {
        super(ResultCodeEnum.C0502.getName());
        this.code = ResultCodeEnum.C0502.value();
    }

    public VoiceAlertServiceFailedException(String message) {
        super(message);
        this.code = ResultCodeEnum.C0502.value();
    }

    public VoiceAlertServiceFailedException(Object data) {
        super(ResultCodeEnum.C0502.getName());
        this.code = ResultCodeEnum.C0502.value();
        this.data = data;
    }

    public VoiceAlertServiceFailedException(String message, Object data) {
        super(message);
        this.code = ResultCodeEnum.C0502.value();
        this.data = data;
    }
}
