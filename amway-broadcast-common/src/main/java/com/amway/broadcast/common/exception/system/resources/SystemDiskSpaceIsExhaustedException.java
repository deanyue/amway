package com.amway.broadcast.common.exception.system.resources;

import com.amway.broadcast.common.enumeration.ResultCodeEnum;
import com.amway.broadcast.common.exception.BaseException;

/**
 *  系统磁盘空间耗尽 B0311
 *
 * @author Lee
 * @version 1.0
 * @date 2020/5/9 9:15
 */
public class SystemDiskSpaceIsExhaustedException extends BaseException {

    public SystemDiskSpaceIsExhaustedException() {
        super(ResultCodeEnum.B0311.getName());
        this.code = ResultCodeEnum.B0311.value();
    }

    public SystemDiskSpaceIsExhaustedException(String message) {
        super(message);
        this.code = ResultCodeEnum.B0311.value();
    }

    public SystemDiskSpaceIsExhaustedException(Object data) {
        super(ResultCodeEnum.B0311.getName());
        this.code = ResultCodeEnum.B0311.value();
        this.data = data;
    }

    public SystemDiskSpaceIsExhaustedException(String message, Object data) {
        super(message);
        this.code = ResultCodeEnum.B0311.value();
        this.data = data;
    }
}
