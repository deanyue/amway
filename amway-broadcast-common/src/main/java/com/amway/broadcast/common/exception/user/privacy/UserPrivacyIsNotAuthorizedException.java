package com.amway.broadcast.common.exception.user.privacy;

import com.amway.broadcast.common.enumeration.ResultCodeEnum;
import com.amway.broadcast.common.exception.BaseException;

/**
 *  用户隐私未授权 A0900
 *
 * @author Lee
 * @version 1.0
 * @date 2020/5/8 17:44
 */
public class UserPrivacyIsNotAuthorizedException extends BaseException {

    public UserPrivacyIsNotAuthorizedException() {
        super(ResultCodeEnum.A0900.getName());
        this.code = ResultCodeEnum.A0900.value();
    }

    public UserPrivacyIsNotAuthorizedException(String message) {
        super(message);
        this.code = ResultCodeEnum.A0900.value();
    }

    public UserPrivacyIsNotAuthorizedException(Object data) {
        super(ResultCodeEnum.A0900.getName());
        this.code = ResultCodeEnum.A0900.value();
        this.data = data;
    }

    public UserPrivacyIsNotAuthorizedException(String message, Object data) {
        super(message);
        this.code = ResultCodeEnum.A0900.value();
        this.data = data;
    }
}
