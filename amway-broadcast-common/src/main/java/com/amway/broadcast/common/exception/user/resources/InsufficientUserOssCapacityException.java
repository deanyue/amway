package com.amway.broadcast.common.exception.user.resources;

import com.amway.broadcast.common.enumeration.ResultCodeEnum;
import com.amway.broadcast.common.exception.BaseException;

/**
 *  用户OSS容量不足 A0604
 *
 * @author Lee
 * @version 1.0
 * @date 2020/5/8 17:17
 */
public class InsufficientUserOssCapacityException extends BaseException {

    public InsufficientUserOssCapacityException() {
        super(ResultCodeEnum.A0604.getName());
        this.code = ResultCodeEnum.A0604.value();
    }

    public InsufficientUserOssCapacityException(String message) {
        super(message);
        this.code = ResultCodeEnum.A0604.value();
    }

    public InsufficientUserOssCapacityException(Object data) {
        super(ResultCodeEnum.A0604.getName());
        this.code = ResultCodeEnum.A0604.value();
        this.data = data;
    }

    public InsufficientUserOssCapacityException(String message, Object data) {
        super(message);
        this.code = ResultCodeEnum.A0604.value();
        this.data = data;
    }
}
