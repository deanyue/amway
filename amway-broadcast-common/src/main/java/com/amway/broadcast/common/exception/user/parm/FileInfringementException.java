package com.amway.broadcast.common.exception.user.parm;

import com.amway.broadcast.common.enumeration.ResultCodeEnum;
import com.amway.broadcast.common.exception.BaseException;

/**
 *  文件侵犯版权 A0433
 *
 * @author Lee
 * @version 1.0
 * @date 2020/5/8 16:23
 */
public class FileInfringementException extends BaseException {

    public FileInfringementException() {
        super(ResultCodeEnum.A0433.getName());
        this.code = ResultCodeEnum.A0433.value();
    }

    public FileInfringementException(String message) {
        super(message);
        this.code = ResultCodeEnum.A0433.value();
    }

    public FileInfringementException(Object data) {
        super(ResultCodeEnum.A0433.getName());
        this.code = ResultCodeEnum.A0433.value();
        this.data = data;
    }

    public FileInfringementException(String message, Object data) {
        super(message);
        this.code = ResultCodeEnum.A0433.value();
        this.data = data;
    }
}
