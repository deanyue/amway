package com.amway.broadcast.common.exception.user.access;

import com.amway.broadcast.common.enumeration.ResultCodeEnum;
import com.amway.broadcast.common.exception.BaseException;

/**
 *  无权限使用API A0312
 *
 * @author Lee
 * @version 1.0
 * @date 2020/5/8 15:03
 */
public class NoPermissionToUseTheApiException extends BaseException {

    public NoPermissionToUseTheApiException() {
        super(ResultCodeEnum.A0312.getName());
        this.code = ResultCodeEnum.A0312.value();
    }

    public NoPermissionToUseTheApiException(String message) {
        super(message);
        this.code = ResultCodeEnum.A0312.value();
    }

    public NoPermissionToUseTheApiException(Object data) {
        super(ResultCodeEnum.A0312.getName());
        this.code = ResultCodeEnum.A0312.value();
        this.data = data;
    }

    public NoPermissionToUseTheApiException(String message, Object data) {
        super(message);
        this.code = ResultCodeEnum.A0312.value();
        this.data = data;
    }
}
