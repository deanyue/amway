package com.amway.broadcast.common.exception.user.logon;

import com.amway.broadcast.common.enumeration.ResultCodeEnum;
import com.amway.broadcast.common.exception.BaseException;

/**
 * 手机格式校验失败 A0151
 *
 * @author Lee
 */
public class PhoneVerificationException extends BaseException {

    public PhoneVerificationException() {
        super(ResultCodeEnum.A0151.getName());
        this.code = ResultCodeEnum.A0151.value();
    }

    public PhoneVerificationException(String message) {
        super(message);
        this.code = ResultCodeEnum.A0151.value();
    }

    public PhoneVerificationException(Object data) {
        super(ResultCodeEnum.A0151.getName());
        this.code = ResultCodeEnum.A0151.value();
        this.data = data;
    }

    public PhoneVerificationException(String message, Object data) {
        super(message);
        this.code = ResultCodeEnum.A0151.value();
        this.data = data;
    }
}
