package com.amway.broadcast.common.exception.user.login;

import com.amway.broadcast.common.enumeration.ResultCodeEnum;
import com.amway.broadcast.common.exception.BaseException;

/**
 * 用户账户已作废 A0203
 *
 * @author Lee
 */
public class AccountCanceledException extends BaseException {

    public AccountCanceledException() {
        super(ResultCodeEnum.A0203.getName());
        this.code = ResultCodeEnum.A0203.value();
    }

    public AccountCanceledException(String message) {
        super(message);
        this.code = ResultCodeEnum.A0203.value();
    }

    public AccountCanceledException(Object data) {
        super(ResultCodeEnum.A0203.getName());
        this.code = ResultCodeEnum.A0203.value();
        this.data = data;
    }

    public AccountCanceledException(String message, Object data) {
        super(message);
        this.code = ResultCodeEnum.A0203.value();
        this.data = data;
    }
}
