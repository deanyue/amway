package com.amway.broadcast.common.exception.system.resources;

import com.amway.broadcast.common.enumeration.ResultCodeEnum;
import com.amway.broadcast.common.exception.BaseException;

/**
 *  系统连接池耗尽 B0314
 *
 * @author Lee
 * @version 1.0
 * @date 2020/5/9 9:17
 */
public class SystemConnectionPoolExhaustedException extends BaseException {

    public SystemConnectionPoolExhaustedException() {
        super(ResultCodeEnum.B0314.getName());
        this.code = ResultCodeEnum.B0314.value();
    }

    public SystemConnectionPoolExhaustedException(String message) {
        super(message);
        this.code = ResultCodeEnum.B0314.value();
    }

    public SystemConnectionPoolExhaustedException(Object data) {
        super(ResultCodeEnum.B0314.getName());
        this.code = ResultCodeEnum.B0314.value();
        this.data = data;
    }

    public SystemConnectionPoolExhaustedException(String message, Object data) {
        super(message);
        this.code = ResultCodeEnum.B0314.value();
        this.data = data;
    }
}
