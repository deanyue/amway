package com.amway.broadcast.common.exception.user.logon;

import com.amway.broadcast.common.enumeration.ResultCodeEnum;
import com.amway.broadcast.common.exception.BaseException;

/**
 * 用户未同意隐私条款 A0101
 *
 * @author Lee
 */
public class PrivacyPolicyNotAgreedException extends BaseException {

    public PrivacyPolicyNotAgreedException() {
        super(ResultCodeEnum.A0101.getName());
        this.code = ResultCodeEnum.A0101.value();
    }

    public PrivacyPolicyNotAgreedException(String message) {
        super(message);
        this.code = ResultCodeEnum.A0101.value();
    }

    public PrivacyPolicyNotAgreedException(Object data) {
        super(ResultCodeEnum.A0101.getName());
        this.code = ResultCodeEnum.A0101.value();
        this.data = data;
    }

    public PrivacyPolicyNotAgreedException(String message, Object data) {
        super(message);
        this.code = ResultCodeEnum.A0101.value();
        this.data = data;
    }
}
