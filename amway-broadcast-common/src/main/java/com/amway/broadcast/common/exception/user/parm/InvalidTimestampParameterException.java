package com.amway.broadcast.common.exception.user.parm;

import com.amway.broadcast.common.enumeration.ResultCodeEnum;
import com.amway.broadcast.common.exception.BaseException;

/**
 *  非法的时间戳参数 A0414
 *
 * @author Lee
 * @version 1.0
 * @date 2020/5/8 16:07
 */
public class InvalidTimestampParameterException extends BaseException {

    public InvalidTimestampParameterException() {
        super(ResultCodeEnum.A0414.getName());
        this.code = ResultCodeEnum.A0414.value();
    }

    public InvalidTimestampParameterException(String message) {
        super(message);
        this.code = ResultCodeEnum.A0414.value();
    }

    public InvalidTimestampParameterException(Object data) {
        super(ResultCodeEnum.A0414.getName());
        this.code = ResultCodeEnum.A0414.value();
        this.data = data;
    }

    public InvalidTimestampParameterException(String message, Object data) {
        super(message);
        this.code = ResultCodeEnum.A0414.value();
        this.data = data;
    }
}
