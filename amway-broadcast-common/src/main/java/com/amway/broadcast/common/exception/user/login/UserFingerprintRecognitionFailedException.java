package com.amway.broadcast.common.exception.user.login;

import com.amway.broadcast.common.enumeration.ResultCodeEnum;
import com.amway.broadcast.common.exception.BaseException;

/**
 *  用户指纹识别失败 A0221
 *
 * @author Lee
 * @version 1.0
 * @date 2020/5/8 14:14
 */
public class UserFingerprintRecognitionFailedException extends BaseException {

    public UserFingerprintRecognitionFailedException() {
        super(ResultCodeEnum.A0221.getName());
        this.code = ResultCodeEnum.A0221.value();
    }

    public UserFingerprintRecognitionFailedException(String message) {
        super(message);
        this.code = ResultCodeEnum.A0221.value();
    }

    public UserFingerprintRecognitionFailedException(Object data) {
        super(ResultCodeEnum.A0221.getName());
        this.code = ResultCodeEnum.A0221.value();
        this.data = data;
    }

    public UserFingerprintRecognitionFailedException(String message, Object data) {
        super(message);
        this.code = ResultCodeEnum.A0221.value();
        this.data = data;
    }
}
