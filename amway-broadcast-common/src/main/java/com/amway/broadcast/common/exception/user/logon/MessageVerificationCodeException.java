package com.amway.broadcast.common.exception.user.logon;

import com.amway.broadcast.common.enumeration.ResultCodeEnum;
import com.amway.broadcast.common.exception.BaseException;

/**
 * 短信校验码输入错误 A0131
 *
 * @author Lee
 */
public class MessageVerificationCodeException extends BaseException {

    public MessageVerificationCodeException() {
        super(ResultCodeEnum.A0131.getName());
        this.code = ResultCodeEnum.A0131.value();
    }

    public MessageVerificationCodeException(String message) {
        super(message);
        this.code = ResultCodeEnum.A0131.value();
    }

    public MessageVerificationCodeException(Object data) {
        super(ResultCodeEnum.A0131.getName());
        this.code = ResultCodeEnum.A0131.value();
        this.data = data;
    }

    public MessageVerificationCodeException(String message, Object data) {
        super(message);
        this.code = ResultCodeEnum.A0131.value();
        this.data = data;
    }
}
