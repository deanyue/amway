package com.amway.broadcast.common.annotation;

import java.lang.annotation.*;

/**
 * @author liujch
 * @description 日志
 * @Package com.amway.broadcast.common.annotation
 * @date 2020年12月12日 13:43
 */
@Target({ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface Logging {

    /**
     * 日志操作名称
     */
    String name();

}
