package com.amway.broadcast.common.exception.remote.middle;

import com.amway.broadcast.common.enumeration.ResultCodeEnum;
import com.amway.broadcast.common.exception.BaseException;

/**
 *  消息分组未查到 C0124
 *
 * @author Lee
 * @version 1.0
 * @date 2020/5/9 9:34
 */
public class MessageGroupNotFoundException extends BaseException {

    public MessageGroupNotFoundException() {
        super(ResultCodeEnum.C0124.getName());
        this.code = ResultCodeEnum.C0124.value();
    }

    public MessageGroupNotFoundException(String message) {
        super(message);
        this.code = ResultCodeEnum.C0124.value();
    }

    public MessageGroupNotFoundException(Object data) {
        super(ResultCodeEnum.C0124.getName());
        this.code = ResultCodeEnum.C0124.value();
        this.data = data;
    }

    public MessageGroupNotFoundException(String message, Object data) {
        super(message);
        this.code = ResultCodeEnum.C0124.value();
        this.data = data;
    }
}
