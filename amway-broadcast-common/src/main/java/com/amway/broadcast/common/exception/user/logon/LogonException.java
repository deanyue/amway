package com.amway.broadcast.common.exception.user.logon;

import com.amway.broadcast.common.enumeration.ResultCodeEnum;
import com.amway.broadcast.common.exception.BaseException;

/**
 * 二级宏观异常
 * 用户注册错误 A0100
 *
 * @author Lee
 */
public class LogonException extends BaseException {

    public LogonException() {
        super(ResultCodeEnum.A0100.getName());
        this.code = ResultCodeEnum.A0100.value();
    }

    public LogonException(String message) {
        super(message);
        this.code = ResultCodeEnum.A0100.value();
    }

    public LogonException(Object data) {
        super(ResultCodeEnum.A0100.getName());
        this.code = ResultCodeEnum.A0100.value();
        this.data = data;
    }

    public LogonException(String message, Object data) {
        super(message);
        this.code = ResultCodeEnum.A0100.value();
        this.data = data;
    }
}
