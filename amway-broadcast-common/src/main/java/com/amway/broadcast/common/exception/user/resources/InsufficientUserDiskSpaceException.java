package com.amway.broadcast.common.exception.user.resources;

import com.amway.broadcast.common.enumeration.ResultCodeEnum;
import com.amway.broadcast.common.exception.BaseException;

/**
 *  用户磁盘空间不足 A0602
 *
 * @author Lee
 * @version 1.0
 * @date 2020/5/8 17:15
 */
public class InsufficientUserDiskSpaceException extends BaseException {

    public InsufficientUserDiskSpaceException() {
        super(ResultCodeEnum.A0602.getName());
        this.code = ResultCodeEnum.A0602.value();
    }

    public InsufficientUserDiskSpaceException(String message) {
        super(message);
        this.code = ResultCodeEnum.A0602.value();
    }

    public InsufficientUserDiskSpaceException(Object data) {
        super(ResultCodeEnum.A0602.getName());
        this.code = ResultCodeEnum.A0602.value();
        this.data = data;
    }

    public InsufficientUserDiskSpaceException(String message, Object data) {
        super(message);
        this.code = ResultCodeEnum.A0602.value();
        this.data = data;
    }
}
