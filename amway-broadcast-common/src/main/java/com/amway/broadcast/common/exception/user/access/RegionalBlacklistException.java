package com.amway.broadcast.common.exception.user.access;

import com.amway.broadcast.common.enumeration.ResultCodeEnum;
import com.amway.broadcast.common.exception.BaseException;

/**
 *  地域黑名单 A0325
 *
 * @author Lee
 * @version 1.0
 * @date 2020/5/8 15:11
 */
public class RegionalBlacklistException extends BaseException {

    public RegionalBlacklistException() {
        super(ResultCodeEnum.A0325.getName());
        this.code = ResultCodeEnum.A0325.value();
    }

    public RegionalBlacklistException(String message) {
        super(message);
        this.code = ResultCodeEnum.A0325.value();
    }

    public RegionalBlacklistException(Object data) {
        super(ResultCodeEnum.A0325.getName());
        this.code = ResultCodeEnum.A0325.value();
        this.data = data;
    }

    public RegionalBlacklistException(String message, Object data) {
        super(message);
        this.code = ResultCodeEnum.A0325.value();
        this.data = data;
    }
}
