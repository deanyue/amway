package com.amway.broadcast.common.exception.user.version;

import com.amway.broadcast.common.enumeration.ResultCodeEnum;
import com.amway.broadcast.common.exception.BaseException;

/**
 *  用户安装版本过高 A0803
 *
 * @author Lee
 * @version 1.0
 * @date 2020/5/8 17:34
 */
public class UserInstalledVersionTooHighException extends BaseException {

    public UserInstalledVersionTooHighException() {
        super(ResultCodeEnum.A0803.getName());
        this.code = ResultCodeEnum.A0803.value();
    }

    public UserInstalledVersionTooHighException(String message) {
        super(message);
        this.code = ResultCodeEnum.A0803.value();
    }

    public UserInstalledVersionTooHighException(Object data) {
        super(ResultCodeEnum.A0803.getName());
        this.code = ResultCodeEnum.A0803.value();
        this.data = data;
    }

    public UserInstalledVersionTooHighException(String message, Object data) {
        super(message);
        this.code = ResultCodeEnum.A0803.value();
        this.data = data;
    }
}
