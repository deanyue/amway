package com.amway.broadcast.common.exception.user.parm;

import com.amway.broadcast.common.enumeration.ResultCodeEnum;
import com.amway.broadcast.common.exception.BaseException;

/**
 *  请求批量处理总个数超出限制 A0426
 *
 * @author Lee
 * @version 1.0
 * @date 2020/5/8 16:20
 */
public class TotalNumberExceedsTheLimitException extends BaseException {

    public TotalNumberExceedsTheLimitException() {
        super(ResultCodeEnum.A0426.getName());
        this.code = ResultCodeEnum.A0426.value();
    }

    public TotalNumberExceedsTheLimitException(String message) {
        super(message);
        this.code = ResultCodeEnum.A0426.value();
    }

    public TotalNumberExceedsTheLimitException(Object data) {
        super(ResultCodeEnum.A0426.getName());
        this.code = ResultCodeEnum.A0426.value();
        this.data = data;
    }

    public TotalNumberExceedsTheLimitException(String message, Object data) {
        super(message);
        this.code = ResultCodeEnum.A0426.value();
        this.data = data;
    }
}
