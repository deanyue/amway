package com.amway.broadcast.common.exception.user.login;

import com.amway.broadcast.common.enumeration.ResultCodeEnum;
import com.amway.broadcast.common.exception.BaseException;

/**
 * 二级宏观异常
 * 用户登录异常  A0200
 *
 * @author Lee
 */
public class LoginException extends BaseException {

    public LoginException() {
        super(ResultCodeEnum.A0200.getName());
        this.code = ResultCodeEnum.A0200.value();
    }

    public LoginException(String message) {
        super(message);
        this.code = ResultCodeEnum.A0200.value();
    }

    public LoginException(Object data) {
        super(ResultCodeEnum.A0200.getName());
        this.code = ResultCodeEnum.A0200.value();
        this.data = data;
    }

    public LoginException(String message, Object data) {
        super(message);
        this.code = ResultCodeEnum.A0200.value();
        this.data = data;
    }
}
