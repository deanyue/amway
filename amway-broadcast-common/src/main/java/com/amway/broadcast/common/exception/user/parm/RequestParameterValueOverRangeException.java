package com.amway.broadcast.common.exception.user.parm;

import com.amway.broadcast.common.enumeration.ResultCodeEnum;
import com.amway.broadcast.common.exception.BaseException;

/**
 *  请求参数值超出允许的范围 A0420
 *
 * @author a good man
 * @version 1.0
 * @date 2020/5/8 16:09
 */
public class RequestParameterValueOverRangeException extends BaseException {

    public RequestParameterValueOverRangeException() {
        super(ResultCodeEnum.A0420.getName());
        this.code = ResultCodeEnum.A0420.value();
    }

    public RequestParameterValueOverRangeException(String message) {
        super(message);
        this.code = ResultCodeEnum.A0420.value();
    }

    public RequestParameterValueOverRangeException(Object data) {
        super(ResultCodeEnum.A0420.getName());
        this.code = ResultCodeEnum.A0420.value();
        this.data = data;
    }

    public RequestParameterValueOverRangeException(String message, Object data) {
        super(message);
        this.code = ResultCodeEnum.A0420.value();
        this.data = data;
    }
}
