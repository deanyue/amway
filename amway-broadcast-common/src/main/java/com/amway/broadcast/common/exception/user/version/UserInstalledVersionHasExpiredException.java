package com.amway.broadcast.common.exception.user.version;

import com.amway.broadcast.common.enumeration.ResultCodeEnum;
import com.amway.broadcast.common.exception.BaseException;

/**
 *  用户安装版本已过期 A0804
 *
 * @author Lee
 * @version 1.0
 * @date 2020/5/8 17:35
 */
public class UserInstalledVersionHasExpiredException extends BaseException {

    public UserInstalledVersionHasExpiredException() {
        super(ResultCodeEnum.A0804.getName());
        this.code = ResultCodeEnum.A0804.value();
    }

    public UserInstalledVersionHasExpiredException(String message) {
        super(message);
        this.code = ResultCodeEnum.A0804.value();
    }

    public UserInstalledVersionHasExpiredException(Object data) {
        super(ResultCodeEnum.A0804.getName());
        this.code = ResultCodeEnum.A0804.value();
        this.data = data;
    }

    public UserInstalledVersionHasExpiredException(String message, Object data) {
        super(message);
        this.code = ResultCodeEnum.A0804.value();
        this.data = data;
    }
}
