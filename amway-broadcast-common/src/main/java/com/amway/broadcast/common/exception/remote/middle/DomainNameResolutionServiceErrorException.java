package com.amway.broadcast.common.exception.remote.middle;

import com.amway.broadcast.common.enumeration.ResultCodeEnum;
import com.amway.broadcast.common.exception.BaseException;

/**
 *  域名解析服务出错 C0153
 *
 * @author Lee
 * @version 1.0
 * @date 2020/5/9 9:43
 */
public class DomainNameResolutionServiceErrorException extends BaseException {

    public DomainNameResolutionServiceErrorException() {
        super(ResultCodeEnum.C0153.getName());
        this.code = ResultCodeEnum.C0153.value();
    }

    public DomainNameResolutionServiceErrorException(String message) {
        super(message);
        this.code = ResultCodeEnum.C0153.value();
    }

    public DomainNameResolutionServiceErrorException(Object data) {
        super(ResultCodeEnum.C0153.getName());
        this.code = ResultCodeEnum.C0153.value();
        this.data = data;
    }

    public DomainNameResolutionServiceErrorException(String message, Object data) {
        super(message);
        this.code = ResultCodeEnum.C0153.value();
        this.data = data;
    }
}
