package com.amway.broadcast.common.exception.remote.middle;

import com.amway.broadcast.common.enumeration.ResultCodeEnum;
import com.amway.broadcast.common.exception.BaseException;

/**
 *  配置服务出错 C0140
 *
 * @author Lee
 * @version 1.0
 * @date 2020/5/9 9:38
 */
public class ConfigurationServiceErrorException extends BaseException {

    public ConfigurationServiceErrorException() {
        super(ResultCodeEnum.C0140.getName());
        this.code = ResultCodeEnum.C0140.value();
    }

    public ConfigurationServiceErrorException(String message) {
        super(message);
        this.code = ResultCodeEnum.C0140.value();
    }

    public ConfigurationServiceErrorException(Object data) {
        super(ResultCodeEnum.C0140.getName());
        this.code = ResultCodeEnum.C0140.value();
        this.data = data;
    }

    public ConfigurationServiceErrorException(String message, Object data) {
        super(message);
        this.code = ResultCodeEnum.C0140.value();
        this.data = data;
    }
}
