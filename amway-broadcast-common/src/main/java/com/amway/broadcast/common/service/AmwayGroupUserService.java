package com.amway.broadcast.common.service;

import com.amway.broadcast.common.base.BaseAmwayGroupUser;

import java.util.List;

/**
 * @auther 薛晨
 * @date 2020/12/12
 * @time 11:12
 * @description
 */
public interface AmwayGroupUserService<T extends BaseAmwayGroupUser> {

    int insert(T baseAmwayGroupUser);

    int update(T baseAmwayGroupUser);

    List<T> getListByCondition(T condition);

    T getOne(String groupId,String userId);

    List<T> getByGroupIdAndStatus(String groupId,Integer quitStatus);
}
