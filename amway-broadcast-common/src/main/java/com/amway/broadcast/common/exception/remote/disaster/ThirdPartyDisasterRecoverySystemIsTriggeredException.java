package com.amway.broadcast.common.exception.remote.disaster;

import com.amway.broadcast.common.enumeration.ResultCodeEnum;
import com.amway.broadcast.common.exception.BaseException;

/**
 *  第三方容灾系统被触发 C0400
 *
 * @author Lee
 * @version 1.0
 * @date 2020/5/9 10:10
 */
public class ThirdPartyDisasterRecoverySystemIsTriggeredException extends BaseException {

    public ThirdPartyDisasterRecoverySystemIsTriggeredException() {
        super(ResultCodeEnum.C0400.getName());
        this.code = ResultCodeEnum.C0400.value();
    }

    public ThirdPartyDisasterRecoverySystemIsTriggeredException(String message) {
        super(message);
        this.code = ResultCodeEnum.C0400.value();
    }

    public ThirdPartyDisasterRecoverySystemIsTriggeredException(Object data) {
        super(ResultCodeEnum.C0400.getName());
        this.code = ResultCodeEnum.C0400.value();
        this.data = data;
    }

    public ThirdPartyDisasterRecoverySystemIsTriggeredException(String message, Object data) {
        super(message);
        this.code = ResultCodeEnum.C0400.value();
        this.data = data;
    }
}
