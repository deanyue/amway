package com.amway.broadcast.common.exception.remote.middle;

import com.amway.broadcast.common.enumeration.ResultCodeEnum;
import com.amway.broadcast.common.exception.BaseException;

/**
 *  网络资源服务出错 C0150
 *
 * @author Lee
 * @version 1.0
 * @date 2020/5/9 9:39
 */
public class NetworkResourceServiceErrorException extends BaseException {

    public NetworkResourceServiceErrorException() {
        super(ResultCodeEnum.C0150.getName());
        this.code = ResultCodeEnum.C0150.value();
    }

    public NetworkResourceServiceErrorException(String message) {
        super(message);
        this.code = ResultCodeEnum.C0150.value();
    }

    public NetworkResourceServiceErrorException(Object data) {
        super(ResultCodeEnum.C0150.getName());
        this.code = ResultCodeEnum.C0150.value();
        this.data = data;
    }

    public NetworkResourceServiceErrorException(String message, Object data) {
        super(message);
        this.code = ResultCodeEnum.C0150.value();
        this.data = data;
    }
}
