package com.amway.broadcast.common.exception.user.login;

import com.amway.broadcast.common.enumeration.ResultCodeEnum;
import com.amway.broadcast.common.exception.BaseException;

/**
 * 用户验证码尝试次数超限 A0241
 *
 * @author Lee
 */
public class VerificationCodeRetryLimitedException extends BaseException {

    public VerificationCodeRetryLimitedException() {
        super(ResultCodeEnum.A0241.getName());
        this.code = ResultCodeEnum.A0241.value();
    }

    public VerificationCodeRetryLimitedException(String message) {
        super(message);
        this.code = ResultCodeEnum.A0241.value();
    }

    public VerificationCodeRetryLimitedException(Object data) {
        super(ResultCodeEnum.A0241.getName());
        this.code = ResultCodeEnum.A0241.value();
        this.data = data;
    }

    public VerificationCodeRetryLimitedException(String message, Object data) {
        super(message);
        this.code = ResultCodeEnum.A0241.value();
        this.data = data;
    }
}
