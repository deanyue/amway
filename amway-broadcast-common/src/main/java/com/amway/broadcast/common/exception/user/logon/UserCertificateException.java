package com.amway.broadcast.common.exception.user.logon;

import com.amway.broadcast.common.enumeration.ResultCodeEnum;
import com.amway.broadcast.common.exception.BaseException;

/**
 * 用户证件异常 A0140
 *
 * @author Lee
 */
public class UserCertificateException extends BaseException {

    public UserCertificateException() {
        super(ResultCodeEnum.A0140.getName());
        this.code = ResultCodeEnum.A0140.value();
    }

    public UserCertificateException(String message) {
        super(message);
        this.code = ResultCodeEnum.A0140.value();
    }

    public UserCertificateException(Object data) {
        super(ResultCodeEnum.A0140.getName());
        this.code = ResultCodeEnum.A0140.value();
        this.data = data;
    }

    public UserCertificateException(String message, Object data) {
        super(message);
        this.code = ResultCodeEnum.A0140.value();
        this.data = data;
    }
}
