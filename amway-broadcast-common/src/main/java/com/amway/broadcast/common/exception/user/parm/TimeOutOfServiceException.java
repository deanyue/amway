package com.amway.broadcast.common.exception.user.parm;

import com.amway.broadcast.common.enumeration.ResultCodeEnum;
import com.amway.broadcast.common.exception.BaseException;

/**
 *  时间不在服务范围 A0423
 *
 * @author Lee
 * @version 1.0
 * @date 2020/5/8 16:13
 */
public class TimeOutOfServiceException extends BaseException {

    public TimeOutOfServiceException() {
        super(ResultCodeEnum.A0423.getName());
        this.code = ResultCodeEnum.A0423.value();
    }

    public TimeOutOfServiceException(String message) {
        super(message);
        this.code = ResultCodeEnum.A0423.value();
    }

    public TimeOutOfServiceException(Object data) {
        super(ResultCodeEnum.A0423.getName());
        this.code = ResultCodeEnum.A0423.value();
        this.data = data;
    }

    public TimeOutOfServiceException(String message, Object data) {
        super(message);
        this.code = ResultCodeEnum.A0423.value();
        this.data = data;
    }
}
