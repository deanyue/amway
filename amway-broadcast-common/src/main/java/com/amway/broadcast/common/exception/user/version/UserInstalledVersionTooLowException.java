package com.amway.broadcast.common.exception.user.version;

import com.amway.broadcast.common.enumeration.ResultCodeEnum;
import com.amway.broadcast.common.exception.BaseException;

/**
 *  用户安装版本过低 A0802
 *
 * @author Lee
 * @version 1.0
 * @date 2020/5/8 17:33
 */
public class UserInstalledVersionTooLowException extends BaseException {

    public UserInstalledVersionTooLowException() {
        super(ResultCodeEnum.A0802.getName());
        this.code = ResultCodeEnum.A0802.value();
    }

    public UserInstalledVersionTooLowException(String message) {
        super(message);
        this.code = ResultCodeEnum.A0802.value();
    }

    public UserInstalledVersionTooLowException(Object data) {
        super(ResultCodeEnum.A0802.getName());
        this.code = ResultCodeEnum.A0802.value();
        this.data = data;
    }

    public UserInstalledVersionTooLowException(String message, Object data) {
        super(message);
        this.code = ResultCodeEnum.A0802.value();
        this.data = data;
    }
}
