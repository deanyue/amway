package com.amway.broadcast.common.exception.user.login;

import com.amway.broadcast.common.enumeration.ResultCodeEnum;
import com.amway.broadcast.common.exception.BaseException;

/**
 * 用户密码错误 A0210
 *
 * @author Lee
 */
public class PasswordErrorException extends BaseException {

    public PasswordErrorException() {
        super(ResultCodeEnum.A0210.getName());
        this.code = ResultCodeEnum.A0210.value();
    }

    public PasswordErrorException(String message) {
        super(message);
        this.code = ResultCodeEnum.A0210.value();
    }

    public PasswordErrorException(Object data) {
        super(ResultCodeEnum.A0210.getName());
        this.code = ResultCodeEnum.A0210.value();
        this.data = data;
    }

    public PasswordErrorException(String message, Object data) {
        super(message);
        this.code = ResultCodeEnum.A0210.value();
        this.data = data;
    }
}
