package com.amway.broadcast.common.constant;

/**
 * 状态检查
 *
 * @author Lee
 */
public class CheckState {

    private CheckState(){}

    /**
     * 否
     */
    public static final String STATE_FALSE = "0";

    /**
     * 是
     */
    public static final String STATE_TRUE = "1";

}
