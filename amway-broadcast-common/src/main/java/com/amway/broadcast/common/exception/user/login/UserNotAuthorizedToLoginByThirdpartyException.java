package com.amway.broadcast.common.exception.user.login;

import com.amway.broadcast.common.enumeration.ResultCodeEnum;
import com.amway.broadcast.common.exception.BaseException;

/**
 *  用户未获得第三方登陆授权 A0223
 *
 * @author Lee
 * @version 1.0
 * @date 2020/5/8 14:44
 */
public class UserNotAuthorizedToLoginByThirdpartyException extends BaseException {

    public UserNotAuthorizedToLoginByThirdpartyException() {
        super(ResultCodeEnum.A0223.getName());
        this.code = ResultCodeEnum.A0223.value();
    }

    public UserNotAuthorizedToLoginByThirdpartyException(String message) {
        super(message);
        this.code = ResultCodeEnum.A0223.value();
    }

    public UserNotAuthorizedToLoginByThirdpartyException(Object data) {
        super(ResultCodeEnum.A0223.getName());
        this.code = ResultCodeEnum.A0223.value();
        this.data = data;
    }

    public UserNotAuthorizedToLoginByThirdpartyException(String message, Object data) {
        super(message);
        this.code = ResultCodeEnum.A0223.value();
        this.data = data;
    }
}
