package com.amway.broadcast.common.exception.remote.notice;

import com.amway.broadcast.common.enumeration.ResultCodeEnum;
import com.amway.broadcast.common.exception.BaseException;

/**
 *  通知服务出错 C0500
 *
 * @author Lee
 * @version 1.0
 * @date 2020/5/9 10:13
 */
public class NotificationServiceErrorException extends BaseException {

    public NotificationServiceErrorException() {
        super(ResultCodeEnum.C0500.getName());
        this.code = ResultCodeEnum.C0500.value();
    }

    public NotificationServiceErrorException(String message) {
        super(message);
        this.code = ResultCodeEnum.C0500.value();
    }

    public NotificationServiceErrorException(Object data) {
        super(ResultCodeEnum.C0500.getName());
        this.code = ResultCodeEnum.C0500.value();
        this.data = data;
    }

    public NotificationServiceErrorException(String message, Object data) {
        super(message);
        this.code = ResultCodeEnum.C0500.value();
        this.data = data;
    }
}
