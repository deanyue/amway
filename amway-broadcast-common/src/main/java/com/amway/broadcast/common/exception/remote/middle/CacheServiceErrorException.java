package com.amway.broadcast.common.exception.remote.middle;


import com.amway.broadcast.common.enumeration.ResultCodeEnum;
import com.amway.broadcast.common.exception.BaseException;

/**
 *  缓存服务出错 C0130
 *
 * @author Lee
 * @version 1.0
 * @date 2020/5/9 9:35
 */
public class CacheServiceErrorException extends BaseException {

    public CacheServiceErrorException() {
        super(ResultCodeEnum.C0130.getName());
        this.code = ResultCodeEnum.C0130.value();
    }

    public CacheServiceErrorException(String message) {
        super(message);
        this.code = ResultCodeEnum.C0130.value();
    }

    public CacheServiceErrorException(Object data) {
        super(ResultCodeEnum.C0130.getName());
        this.code = ResultCodeEnum.C0130.value();
        this.data = data;
    }

    public CacheServiceErrorException(String message, Object data) {
        super(message);
        this.code = ResultCodeEnum.C0130.value();
        this.data = data;
    }
}
