package com.amway.broadcast.common.constant;

/**
 * 角色
 *
 * @author Lee
 */
public class Role {

    /**
     * 访客
     */
    public static final String GUEST = "0";

    /**
     * 普通用户
     */
    public static final String NORMAL = "1";


}
