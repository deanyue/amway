package com.amway.broadcast.common.exception.user.logon;

import com.amway.broadcast.common.enumeration.ResultCodeEnum;
import com.amway.broadcast.common.exception.BaseException;

/**
 * 密码长度不够 A0121
 *
 * @author Lee
 */
public class PasswordNotLongEnoughException extends BaseException {

    public PasswordNotLongEnoughException() {
        super(ResultCodeEnum.A0121.getName());
        this.code = ResultCodeEnum.A0121.value();
    }

    public PasswordNotLongEnoughException(String message) {
        super(message);
        this.code = ResultCodeEnum.A0121.value();
    }

    public PasswordNotLongEnoughException(Object data) {
        super(ResultCodeEnum.A0121.getName());
        this.code = ResultCodeEnum.A0121.value();
        this.data = data;
    }

    public PasswordNotLongEnoughException(String message, Object data) {
        super(message);
        this.code = ResultCodeEnum.A0121.value();
        this.data = data;
    }
}
