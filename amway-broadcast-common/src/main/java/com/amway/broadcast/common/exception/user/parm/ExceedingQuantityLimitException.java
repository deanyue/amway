package com.amway.broadcast.common.exception.user.parm;

import com.amway.broadcast.common.enumeration.ResultCodeEnum;
import com.amway.broadcast.common.exception.BaseException;

/**
 *  数量超出限制 A0425
 *
 * @author Lee
 * @version 1.0
 * @date 2020/5/8 16:17
 */
public class ExceedingQuantityLimitException extends BaseException {

    public ExceedingQuantityLimitException() {
        super(ResultCodeEnum.A0425.getName());
        this.code = ResultCodeEnum.A0425.value();
    }

    public ExceedingQuantityLimitException(String message) {
        super(message);
        this.code = ResultCodeEnum.A0425.value();
    }

    public ExceedingQuantityLimitException(Object data) {
        super(ResultCodeEnum.A0425.getName());
        this.code = ResultCodeEnum.A0425.value();
        this.data = data;
    }

    public ExceedingQuantityLimitException(String message, Object data) {
        super(message);
        this.code = ResultCodeEnum.A0425.value();
        this.data = data;
    }
}
