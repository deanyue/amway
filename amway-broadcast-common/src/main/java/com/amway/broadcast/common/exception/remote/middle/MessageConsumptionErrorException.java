package com.amway.broadcast.common.exception.remote.middle;

import com.amway.broadcast.common.enumeration.ResultCodeEnum;
import com.amway.broadcast.common.exception.BaseException;

/**
 *  消息消费出错 C0122
 *
 * @author Lee
 * @version 1.0
 * @date 2020/5/9 9:33
 */
public class MessageConsumptionErrorException extends BaseException {

    public MessageConsumptionErrorException() {
        super(ResultCodeEnum.C0122.getName());
        this.code = ResultCodeEnum.C0122.value();
    }

    public MessageConsumptionErrorException(String message) {
        super(message);
        this.code = ResultCodeEnum.C0122.value();
    }

    public MessageConsumptionErrorException(Object data) {
        super(ResultCodeEnum.C0122.getName());
        this.code = ResultCodeEnum.C0122.value();
        this.data = data;
    }

    public MessageConsumptionErrorException(String message, Object data) {
        super(message);
        this.code = ResultCodeEnum.C0122.value();
        this.data = data;
    }
}
