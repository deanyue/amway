package com.amway.broadcast.common.exception.user.login;

import com.amway.broadcast.common.enumeration.ResultCodeEnum;
import com.amway.broadcast.common.exception.BaseException;

/**
 *  用户面容识别失败 A0222
 *
 * @author Lee
 * @version 1.0
 * @date 2020/5/8 14:24
 */
public class UserFaceRecognitionFailedException extends BaseException {

    public UserFaceRecognitionFailedException() {
        super(ResultCodeEnum.A0222.getName());
        this.code = ResultCodeEnum.A0222.value();
    }

    public UserFaceRecognitionFailedException(String message) {
        super(message);
        this.code = ResultCodeEnum.A0222.value();
    }

    public UserFaceRecognitionFailedException(Object data) {
        super(ResultCodeEnum.A0222.getName());
        this.code = ResultCodeEnum.A0222.value();
        this.data = data;
    }

    public UserFaceRecognitionFailedException(String message, Object data) {
        super(message);
        this.code = ResultCodeEnum.A0222.value();
        this.data = data;
    }
}
