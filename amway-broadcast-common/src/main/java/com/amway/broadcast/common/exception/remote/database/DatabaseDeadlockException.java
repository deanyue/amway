package com.amway.broadcast.common.exception.remote.database;

import com.amway.broadcast.common.enumeration.ResultCodeEnum;
import com.amway.broadcast.common.exception.BaseException;

/**
 *  数据库死锁 C0331
 *
 * @author Lee
 * @version 1.0
 * @date 2020/5/9 10:05
 */
public class DatabaseDeadlockException extends BaseException {

    public DatabaseDeadlockException() {
        super(ResultCodeEnum.C0331.getName());
        this.code = ResultCodeEnum.C0331.value();
    }

    public DatabaseDeadlockException(String message) {
        super(message);
        this.code = ResultCodeEnum.C0331.value();
    }

    public DatabaseDeadlockException(Object data) {
        super(ResultCodeEnum.C0331.getName());
        this.code = ResultCodeEnum.C0331.value();
        this.data = data;
    }

    public DatabaseDeadlockException(String message, Object data) {
        super(message);
        this.code = ResultCodeEnum.C0331.value();
        this.data = data;
    }
}
