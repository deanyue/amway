package com.amway.broadcast.common.constant;

/**
 * 状态检查
 *
 * @author Lee
 */
public class HttpResponseCode {

    private HttpResponseCode() {
    }

    /**
     * 成功
     */
    public static final String SUCCESS_CODE = "200";


}
