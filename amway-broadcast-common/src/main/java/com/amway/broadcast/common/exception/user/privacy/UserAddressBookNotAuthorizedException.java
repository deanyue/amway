package com.amway.broadcast.common.exception.user.privacy;

import com.amway.broadcast.common.enumeration.ResultCodeEnum;
import com.amway.broadcast.common.exception.BaseException;

/**
 *  用户通讯录未授权 A0907
 *
 * @author Lee
 * @version 1.0
 * @date 2020/5/8 17:54
 */
public class UserAddressBookNotAuthorizedException extends BaseException {

    public UserAddressBookNotAuthorizedException() {
        super(ResultCodeEnum.A0907.getName());
        this.code = ResultCodeEnum.A0907.value();
    }

    public UserAddressBookNotAuthorizedException(String message) {
        super(message);
        this.code = ResultCodeEnum.A0907.value();
    }

    public UserAddressBookNotAuthorizedException(Object data) {
        super(ResultCodeEnum.A0907.getName());
        this.code = ResultCodeEnum.A0907.value();
        this.data = data;
    }

    public UserAddressBookNotAuthorizedException(String message, Object data) {
        super(message);
        this.code = ResultCodeEnum.A0907.value();
        this.data = data;
    }
}
