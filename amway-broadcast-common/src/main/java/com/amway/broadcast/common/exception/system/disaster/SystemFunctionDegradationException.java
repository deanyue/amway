package com.amway.broadcast.common.exception.system.disaster;

import com.amway.broadcast.common.enumeration.ResultCodeEnum;
import com.amway.broadcast.common.exception.BaseException;

/**
 *  系统功能降级 B0220
 *
 * @author Lee
 * @version 1.0
 * @date 2020/5/8 18:16
 */
public class SystemFunctionDegradationException extends BaseException {

    public SystemFunctionDegradationException() {
        super(ResultCodeEnum.B0220.getName());
        this.code = ResultCodeEnum.B0220.value();
    }

    public SystemFunctionDegradationException(String message) {
        super(message);
        this.code = ResultCodeEnum.B0220.value();
    }

    public SystemFunctionDegradationException(Object data) {
        super(ResultCodeEnum.B0220.getName());
        this.code = ResultCodeEnum.B0220.value();
        this.data = data;
    }

    public SystemFunctionDegradationException(String message, Object data) {
        super(message);
        this.code = ResultCodeEnum.B0220.value();
        this.data = data;
    }
}
