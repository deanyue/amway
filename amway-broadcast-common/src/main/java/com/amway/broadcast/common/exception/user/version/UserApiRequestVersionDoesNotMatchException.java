package com.amway.broadcast.common.exception.user.version;

import com.amway.broadcast.common.enumeration.ResultCodeEnum;
import com.amway.broadcast.common.exception.BaseException;

/**
 *  用户API请求版本不匹配 A0805
 *
 * @author Lee
 * @version 1.0
 * @date 2020/5/8 17:36
 */
public class UserApiRequestVersionDoesNotMatchException extends BaseException {

    public UserApiRequestVersionDoesNotMatchException() {
        super(ResultCodeEnum.A0805.getName());
        this.code = ResultCodeEnum.A0805.value();
    }

    public UserApiRequestVersionDoesNotMatchException(String message) {
        super(message);
        this.code = ResultCodeEnum.A0805.value();
    }

    public UserApiRequestVersionDoesNotMatchException(Object data) {
        super(ResultCodeEnum.A0805.getName());
        this.code = ResultCodeEnum.A0805.value();
        this.data = data;
    }

    public UserApiRequestVersionDoesNotMatchException(String message, Object data) {
        super(message);
        this.code = ResultCodeEnum.A0805.value();
        this.data = data;
    }
}
