package com.amway.broadcast.common.base;

import lombok.Data;

import java.util.Date;

/**
 * @auther 薛晨
 * @date 2020/12/12
 * @time 11:14
 * @description
 */
@Data
public class BaseAmwayGroup {

    //主键
    private Long id;

    //创建直播人ID号
    private Long memberId;

    //直播群ID号
    private String imGroupId;

    //直播间名称
    private String name;

    //直播开始时间
    private Date startTime;

    //有效时长，单位：天
    private Integer expire;

    //权限  1：转发可收听  2：转发不可收听
    private Integer limitType;

    //管理员人数
    private String groupManagerNum;

    //简介
    private String remark;

    //状态   1：待开始  2：进行中  3：已过期
    private Integer statusAm;

    //是否解散 0：否  1：是
    private Integer isDelete;

    //创建时间
    private Date createTime;

    //是否允许管理员设置全体禁言(0为否,1为是,默认为1)
    private String isAllMute;

    //是否允许管理员踢走成员(0为否,1为是,默认为1)
    private String isKick;

}
