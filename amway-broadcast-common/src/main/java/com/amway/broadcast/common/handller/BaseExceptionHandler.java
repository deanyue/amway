package com.amway.broadcast.common.handller;


import com.amway.broadcast.common.base.Result;
import com.amway.broadcast.common.base.ResultUtils;
import com.amway.broadcast.common.enumeration.ResultCodeEnum;
import com.amway.broadcast.common.exception.BaseException;
import com.amway.broadcast.common.exception.remote.RemoteErrorException;
import com.amway.broadcast.common.exception.remote.database.*;
import com.amway.broadcast.common.exception.remote.disaster.ThirdPartyDisasterRecoverySystemIsTriggeredException;
import com.amway.broadcast.common.exception.remote.disaster.ThirdPartyFunctionDegradationException;
import com.amway.broadcast.common.exception.remote.disaster.ThirdPartySystemsLimitTrafficException;
import com.amway.broadcast.common.exception.remote.middle.*;
import com.amway.broadcast.common.exception.remote.notice.MailReminderServiceFailedException;
import com.amway.broadcast.common.exception.remote.notice.NotificationServiceErrorException;
import com.amway.broadcast.common.exception.remote.notice.SmsAlertServiceFailedException;
import com.amway.broadcast.common.exception.remote.notice.VoiceAlertServiceFailedException;
import com.amway.broadcast.common.exception.remote.timeout.*;
import com.amway.broadcast.common.exception.system.SystemErrorException;
import com.amway.broadcast.common.exception.system.disaster.SystemCurrentLimitingException;
import com.amway.broadcast.common.exception.system.disaster.SystemDisasterRecoveryFunctionIsTriggeredException;
import com.amway.broadcast.common.exception.system.disaster.SystemFunctionDegradationException;
import com.amway.broadcast.common.exception.system.resources.*;
import com.amway.broadcast.common.exception.system.timeout.SystemExecutionTimeoutException;
import com.amway.broadcast.common.exception.system.timeout.SystemOrderProcessingTimeoutException;
import com.amway.broadcast.common.exception.user.UserException;
import com.amway.broadcast.common.exception.user.access.*;
import com.amway.broadcast.common.exception.user.equipment.*;
import com.amway.broadcast.common.exception.user.login.*;
import com.amway.broadcast.common.exception.user.logon.*;
import com.amway.broadcast.common.exception.user.overall.OverallException;
import com.amway.broadcast.common.exception.user.parm.*;
import com.amway.broadcast.common.exception.user.privacy.*;
import com.amway.broadcast.common.exception.user.request.*;
import com.amway.broadcast.common.exception.user.resources.*;
import com.amway.broadcast.common.exception.user.upload.*;
import com.amway.broadcast.common.exception.user.version.*;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.ExceptionHandler;

/**
 * 统一异常处理  需要在子模块中继承该类以使用
 *
 * @author Lee
 */
@Slf4j
public class BaseExceptionHandler {
    @ExceptionHandler(OverallException.class)
    public Result error(OverallException e){
        String eName = e.getClass().getSimpleName();
        return ResultUtils.error(e.getCode(), e.getMessage(), e.getData());
    }

    @ExceptionHandler(ColumnDoesNotExistException.class)
    public Result error(ColumnDoesNotExistException e) {
        String eName = e.getClass().getSimpleName();
        return ResultUtils.error(e.getCode(), e.getMessage(), e.getData());
    }

    @ExceptionHandler(DatabaseDeadlockException.class)
    public Result error(DatabaseDeadlockException e) {
        String eName = e.getClass().getSimpleName();
        return ResultUtils.error(e.getCode(), e.getMessage(), e.getData());
    }

    @ExceptionHandler(DatabaseServiceErrorException.class)
    public Result error(DatabaseServiceErrorException e) {
        String eName = e.getClass().getSimpleName();
        return ResultUtils.error(e.getCode(), e.getMessage(), e.getData());
    }

    @ExceptionHandler(MultipleTableAssociationsHaveTheSameColumnsException.class)
    public Result error(MultipleTableAssociationsHaveTheSameColumnsException e) {
        String eName = e.getClass().getSimpleName();
        return ResultUtils.error(e.getCode(), e.getMessage(), e.getData());
    }

    @ExceptionHandler(PrimaryKeyConflictException.class)
    public Result error(PrimaryKeyConflictException e) {
        String eName = e.getClass().getSimpleName();
        return ResultUtils.error(e.getCode(), e.getMessage(), e.getData());
    }

    @ExceptionHandler(TableDoesNotExistException.class)
    public Result error(TableDoesNotExistException e) {
        String eName = e.getClass().getSimpleName();
        return ResultUtils.error(e.getCode(), e.getMessage(), e.getData());
    }

    @ExceptionHandler(ThirdPartyDisasterRecoverySystemIsTriggeredException.class)
    public Result error(ThirdPartyDisasterRecoverySystemIsTriggeredException e) {
        String eName = e.getClass().getSimpleName();
        return ResultUtils.error(e.getCode(), e.getMessage(), e.getData());
    }

    @ExceptionHandler(ThirdPartyFunctionDegradationException.class)
    public Result error(ThirdPartyFunctionDegradationException e) {
        String eName = e.getClass().getSimpleName();
        return ResultUtils.error(e.getCode(), e.getMessage(), e.getData());
    }

    @ExceptionHandler(ThirdPartySystemsLimitTrafficException.class)
    public Result error(ThirdPartySystemsLimitTrafficException e) {
        String eName = e.getClass().getSimpleName();
        return ResultUtils.error(e.getCode(), e.getMessage(), e.getData());
    }

    @ExceptionHandler(CacheServiceErrorException.class)
    public Result error(CacheServiceErrorException e) {
        String eName = e.getClass().getSimpleName();
        return ResultUtils.error(e.getCode(), e.getMessage(), e.getData());
    }

    @ExceptionHandler(CdnServiceErrorException.class)
    public Result error(CdnServiceErrorException e) {
        String eName = e.getClass().getSimpleName();
        return ResultUtils.error(e.getCode(), e.getMessage(), e.getData());
    }

    @ExceptionHandler(ConfigurationServiceErrorException.class)
    public Result error(ConfigurationServiceErrorException e) {
        String eName = e.getClass().getSimpleName();
        return ResultUtils.error(e.getCode(), e.getMessage(), e.getData());
    }

    @ExceptionHandler(DomainNameResolutionServiceErrorException.class)
    public Result error(DomainNameResolutionServiceErrorException e) {
        String eName = e.getClass().getSimpleName();
        return ResultUtils.error(e.getCode(), e.getMessage(), e.getData());
    }

    @ExceptionHandler(GatewayServiceErrorException.class)
    public Result error(GatewayServiceErrorException e) {
        String eName = e.getClass().getSimpleName();
        return ResultUtils.error(e.getCode(), e.getMessage(), e.getData());
    }

    @ExceptionHandler(InterfaceDoesNotExistException.class)
    public Result error(InterfaceDoesNotExistException e) {
        String eName = e.getClass().getSimpleName();
        return ResultUtils.error(e.getCode(), e.getMessage(), e.getData());
    }

    @ExceptionHandler(KeyLengthExceedsTheLimitException.class)
    public Result error(KeyLengthExceedsTheLimitException e) {
        String eName = e.getClass().getSimpleName();
        return ResultUtils.error(e.getCode(), e.getMessage(), e.getData());
    }

    @ExceptionHandler(MessageConsumptionErrorException.class)
    public Result error(MessageConsumptionErrorException e) {
        String eName = e.getClass().getSimpleName();
        return ResultUtils.error(e.getCode(), e.getMessage(), e.getData());
    }

    @ExceptionHandler(MessageDeliveryErrorException.class)
    public Result error(MessageDeliveryErrorException e) {
        String eName = e.getClass().getSimpleName();
        return ResultUtils.error(e.getCode(), e.getMessage(), e.getData());
    }

    @ExceptionHandler(MessageGroupNotFoundException.class)
    public Result error(MessageGroupNotFoundException e) {
        String eName = e.getClass().getSimpleName();
        return ResultUtils.error(e.getCode(), e.getMessage(), e.getData());
    }

    @ExceptionHandler(MessageServiceErrorException.class)
    public Result error(MessageServiceErrorException e) {
        String eName = e.getClass().getSimpleName();
        return ResultUtils.error(e.getCode(), e.getMessage(), e.getData());
    }

    @ExceptionHandler(MessageSubscriptionErrorException.class)
    public Result error(MessageSubscriptionErrorException e) {
        String eName = e.getClass().getSimpleName();
        return ResultUtils.error(e.getCode(), e.getMessage(), e.getData());
    }

    @ExceptionHandler(MiddlewareServiceErrorException.class)
    public Result error(MiddlewareServiceErrorException e) {
        String eName = e.getClass().getSimpleName();
        return ResultUtils.error(e.getCode(), e.getMessage(), e.getData());
    }

    @ExceptionHandler(NetworkResourceServiceErrorException.class)
    public Result error(NetworkResourceServiceErrorException e) {
        String eName = e.getClass().getSimpleName();
        return ResultUtils.error(e.getCode(), e.getMessage(), e.getData());
    }

    @ExceptionHandler(RpcServiceError.class)
    public Result error(RpcServiceError e) {
        String eName = e.getClass().getSimpleName();
        return ResultUtils.error(e.getCode(), e.getMessage(), e.getData());
    }

    @ExceptionHandler(RpcServiceIsNotRegisteredException.class)
    public Result error(RpcServiceIsNotRegisteredException e) {
        String eName = e.getClass().getSimpleName();
        return ResultUtils.error(e.getCode(), e.getMessage(), e.getData());
    }

    @ExceptionHandler(RpcServiceWasNotFoundException.class)
    public Result error(RpcServiceWasNotFoundException e) {
        String eName = e.getClass().getSimpleName();
        return ResultUtils.error(e.getCode(), e.getMessage(), e.getData());
    }

    @ExceptionHandler(StorageCapacityIsFullException.class)
    public Result error(StorageCapacityIsFullException e) {
        String eName = e.getClass().getSimpleName();
        return ResultUtils.error(e.getCode(), e.getMessage(), e.getData());
    }

    @ExceptionHandler(UnsupportedDataFormatException.class)
    public Result error(UnsupportedDataFormatException e) {
        String eName = e.getClass().getSimpleName();
        return ResultUtils.error(e.getCode(), e.getMessage(), e.getData());
    }

    @ExceptionHandler(ValueLengthExceedsTheLimitException.class)
    public Result error(ValueLengthExceedsTheLimitException e) {
        String eName = e.getClass().getSimpleName();
        return ResultUtils.error(e.getCode(), e.getMessage(), e.getData());
    }

    @ExceptionHandler(VpnServiceErrorException.class)
    public Result error(VpnServiceErrorException e) {
        String eName = e.getClass().getSimpleName();
        return ResultUtils.error(e.getCode(), e.getMessage(), e.getData());
    }

    @ExceptionHandler(MailReminderServiceFailedException.class)
    public Result error(MailReminderServiceFailedException e) {
        String eName = e.getClass().getSimpleName();
        return ResultUtils.error(e.getCode(), e.getMessage(), e.getData());
    }

    @ExceptionHandler(NotificationServiceErrorException.class)
    public Result error(NotificationServiceErrorException e) {
        String eName = e.getClass().getSimpleName();
        return ResultUtils.error(e.getCode(), e.getMessage(), e.getData());
    }

    @ExceptionHandler(SmsAlertServiceFailedException.class)
    public Result error(SmsAlertServiceFailedException e) {
        String eName = e.getClass().getSimpleName();
        return ResultUtils.error(e.getCode(), e.getMessage(), e.getData());
    }

    @ExceptionHandler(VoiceAlertServiceFailedException.class)
    public Result error(VoiceAlertServiceFailedException e) {
        String eName = e.getClass().getSimpleName();
        return ResultUtils.error(e.getCode(), e.getMessage(), e.getData());
    }

    @ExceptionHandler(CacheServiceTimeoutException.class)
    public Result error(CacheServiceTimeoutException e) {
        String eName = e.getClass().getSimpleName();
        return ResultUtils.error(e.getCode(), e.getMessage(), e.getData());
    }

    @ExceptionHandler(ConfigureServiceTimeoutException.class)
    public Result error(ConfigureServiceTimeoutException e) {
        String eName = e.getClass().getSimpleName();
        return ResultUtils.error(e.getCode(), e.getMessage(), e.getData());
    }

    @ExceptionHandler(DatabaseServiceTimeoutException.class)
    public Result error(DatabaseServiceTimeoutException e) {
        String eName = e.getClass().getSimpleName();
        return ResultUtils.error(e.getCode(), e.getMessage(), e.getData());
    }

    @ExceptionHandler(MessageDeliveryTimeoutException.class)
    public Result error(MessageDeliveryTimeoutException e) {
        String eName = e.getClass().getSimpleName();
        return ResultUtils.error(e.getCode(), e.getMessage(), e.getData());
    }

    @ExceptionHandler(RpcExecutionTimeoutException.class)
    public Result error(RpcExecutionTimeoutException e) {
        String eName = e.getClass().getSimpleName();
        return ResultUtils.error(e.getCode(), e.getMessage(), e.getData());
    }

    @ExceptionHandler(ThirdPartySystemsExecuteTimeoutException.class)
    public Result error(ThirdPartySystemsExecuteTimeoutException e) {
        String eName = e.getClass().getSimpleName();
        return ResultUtils.error(e.getCode(), e.getMessage(), e.getData());
    }

    @ExceptionHandler(RemoteErrorException.class)
    public Result error(RemoteErrorException e) {
        String eName = e.getClass().getSimpleName();
        return ResultUtils.error(e.getCode(), e.getMessage(), e.getData());
    }

    @ExceptionHandler(SystemCurrentLimitingException.class)
    public Result error(SystemCurrentLimitingException e) {
        String eName = e.getClass().getSimpleName();
        return ResultUtils.error(e.getCode(), e.getMessage(), e.getData());
    }

    @ExceptionHandler(SystemDisasterRecoveryFunctionIsTriggeredException.class)
    public Result error(SystemDisasterRecoveryFunctionIsTriggeredException e) {
        String eName = e.getClass().getSimpleName();
        return ResultUtils.error(e.getCode(), e.getMessage(), e.getData());
    }

    @ExceptionHandler(SystemFunctionDegradationException.class)
    public Result error(SystemFunctionDegradationException e) {
        String eName = e.getClass().getSimpleName();
        return ResultUtils.error(e.getCode(), e.getMessage(), e.getData());
    }

    @ExceptionHandler(FileHandleIsExhaustedException.class)
    public Result error(FileHandleIsExhaustedException e) {
        String eName = e.getClass().getSimpleName();
        return ResultUtils.error(e.getCode(), e.getMessage(), e.getData());
    }

    @ExceptionHandler(SystemConnectionPoolExhaustedException.class)
    public Result error(SystemConnectionPoolExhaustedException e) {
        String eName = e.getClass().getSimpleName();
        return ResultUtils.error(e.getCode(), e.getMessage(), e.getData());
    }

    @ExceptionHandler(SystemDiskSpaceIsExhaustedException.class)
    public Result error(SystemDiskSpaceIsExhaustedException e) {
        String eName = e.getClass().getSimpleName();
        return ResultUtils.error(e.getCode(), e.getMessage(), e.getData());
    }

    @ExceptionHandler(SystemFailedToReadTheDiskFileException.class)
    public Result error(SystemFailedToReadTheDiskFileException e) {
        String eName = e.getClass().getSimpleName();
        return ResultUtils.error(e.getCode(), e.getMessage(), e.getData());
    }

    @ExceptionHandler(SystemMemoryDepletionException.class)
    public Result error(SystemMemoryDepletionException e) {
        String eName = e.getClass().getSimpleName();
        return ResultUtils.error(e.getCode(), e.getMessage(), e.getData());
    }

    @ExceptionHandler(SystemResourceAccessException.class)
    public Result error(SystemResourceAccessException e) {
        String eName = e.getClass().getSimpleName();
        return ResultUtils.error(e.getCode(), e.getMessage(), e.getData());
    }

    @ExceptionHandler(SystemResourceAnomalyException.class)
    public Result error(SystemResourceAnomalyException e) {
        String eName = e.getClass().getSimpleName();
        return ResultUtils.error(e.getCode(), e.getMessage(), e.getData());
    }

    @ExceptionHandler(SystemResourceDepletionException.class)
    public Result error(SystemResourceDepletionException e) {
        String eName = e.getClass().getSimpleName();
        return ResultUtils.error(e.getCode(), e.getMessage(), e.getData());
    }

    @ExceptionHandler(SystemThreadPoolIsExhaustedException.class)
    public Result error(SystemThreadPoolIsExhaustedException e) {
        String eName = e.getClass().getSimpleName();
        return ResultUtils.error(e.getCode(), e.getMessage(), e.getData());
    }

    @ExceptionHandler(SystemExecutionTimeoutException.class)
    public Result error(SystemExecutionTimeoutException e) {
        String eName = e.getClass().getSimpleName();
        return ResultUtils.error(e.getCode(), e.getMessage(), e.getData());
    }

    @ExceptionHandler(SystemOrderProcessingTimeoutException.class)
    public Result error(SystemOrderProcessingTimeoutException e) {
        String eName = e.getClass().getSimpleName();
        return ResultUtils.error(e.getCode(), e.getMessage(), e.getData());
    }

    @ExceptionHandler(SystemErrorException.class)
    public Result error(SystemErrorException e) {
        String eName = e.getClass().getSimpleName();
        return ResultUtils.error(e.getCode(), e.getMessage(), e.getData());
    }

    @ExceptionHandler(AccessedObjectPrivacySettingsBlockedException.class)
    public Result error(AccessedObjectPrivacySettingsBlockedException e) {
        String eName = e.getClass().getSimpleName();
        return ResultUtils.error(e.getCode(), e.getMessage(), e.getData());
    }

    @ExceptionHandler(AccessException.class)
    public Result error(AccessException e) {
        String eName = e.getClass().getSimpleName();
        return ResultUtils.error(e.getCode(), e.getMessage(), e.getData());
    }

    @ExceptionHandler(AccountHasBeenFrozenException.class)
    public Result error(AccountHasBeenFrozenException e) {
        String eName = e.getClass().getSimpleName();
        return ResultUtils.error(e.getCode(), e.getMessage(), e.getData());
    }

    @ExceptionHandler(AuthorizationExpiredException.class)
    public Result error(AuthorizationExpiredException e) {
        String eName = e.getClass().getSimpleName();
        return ResultUtils.error(e.getCode(), e.getMessage(), e.getData());
    }

    @ExceptionHandler(BlacklistUserException.class)
    public Result error(BlacklistUserException e) {
        String eName = e.getClass().getSimpleName();
        return ResultUtils.error(e.getCode(), e.getMessage(), e.getData());
    }

    @ExceptionHandler(GatewayAccessRestrictedException.class)
    public Result error(GatewayAccessRestrictedException e) {
        String eName = e.getClass().getSimpleName();
        return ResultUtils.error(e.getCode(), e.getMessage(), e.getData());
    }

    @ExceptionHandler(InvalidIpAddressException.class)
    public Result error(InvalidIpAddressException e) {
        String eName = e.getClass().getSimpleName();
        return ResultUtils.error(e.getCode(), e.getMessage(), e.getData());
    }

    @ExceptionHandler(NoPermissionToUseTheApiException.class)
    public Result error(NoPermissionToUseTheApiException e) {
        String eName = e.getClass().getSimpleName();
        return ResultUtils.error(e.getCode(), e.getMessage(), e.getData());
    }

    @ExceptionHandler(RegionalBlacklistException.class)
    public Result error(RegionalBlacklistException e) {
        String eName = e.getClass().getSimpleName();
        return ResultUtils.error(e.getCode(), e.getMessage(), e.getData());
    }

    @ExceptionHandler(RsaSignatureErrorException.class)
    public Result error(RsaSignatureErrorException e) {
        String eName = e.getClass().getSimpleName();
        return ResultUtils.error(e.getCode(), e.getMessage(), e.getData());
    }

    @ExceptionHandler(ServiceIsOverdueException.class)
    public Result error(ServiceIsOverdueException e) {
        String eName = e.getClass().getSimpleName();
        return ResultUtils.error(e.getCode(), e.getMessage(), e.getData());
    }

    @ExceptionHandler(UnauthorizedAccessException.class)
    public Result error(UnauthorizedAccessException e) {
        String eName = e.getClass().getSimpleName();
        return ResultUtils.error(e.getCode(), e.getMessage(), e.getData());
    }

    @ExceptionHandler(UnderAuthorizationException.class)
    public Result error(UnderAuthorizationException e) {
        String eName = e.getClass().getSimpleName();
        return ResultUtils.error(e.getCode(), e.getMessage(), e.getData());
    }

    @ExceptionHandler(UserAccessIsBlockedException.class)
    public Result error(UserAccessIsBlockedException e) {
        String eName = e.getClass().getSimpleName();
        return ResultUtils.error(e.getCode(), e.getMessage(), e.getData());
    }

    @ExceptionHandler(UserAuthorizationWasRejectedException.class)
    public Result error(UserAuthorizationWasRejectedException e) {
        String eName = e.getClass().getSimpleName();
        return ResultUtils.error(e.getCode(), e.getMessage(), e.getData());
    }

    @ExceptionHandler(UserSignatureException.class)
    public Result error(UserSignatureException e) {
        String eName = e.getClass().getSimpleName();
        return ResultUtils.error(e.getCode(), e.getMessage(), e.getData());
    }

    @ExceptionHandler(AbnormalUserEquipmentException.class)
    public Result error(AbnormalUserEquipmentException e) {
        String eName = e.getClass().getSimpleName();
        return ResultUtils.error(e.getCode(), e.getMessage(), e.getData());
    }

    @ExceptionHandler(UserCameraAnomalyException.class)
    public Result error(UserCameraAnomalyException e) {
        String eName = e.getClass().getSimpleName();
        return ResultUtils.error(e.getCode(), e.getMessage(), e.getData());
    }

    @ExceptionHandler(UserGpsPositionException.class)
    public Result error(UserGpsPositionException e) {
        String eName = e.getClass().getSimpleName();
        return ResultUtils.error(e.getCode(), e.getMessage(), e.getData());
    }

    @ExceptionHandler(UserMicrophoneAbnormalException.class)
    public Result error(UserMicrophoneAbnormalException e) {
        String eName = e.getClass().getSimpleName();
        return ResultUtils.error(e.getCode(), e.getMessage(), e.getData());
    }

    @ExceptionHandler(UserReceiverIsAbnormalException.class)
    public Result error(UserReceiverIsAbnormalException e) {
        String eName = e.getClass().getSimpleName();
        return ResultUtils.error(e.getCode(), e.getMessage(), e.getData());
    }

    @ExceptionHandler(UserSpeakerException.class)
    public Result error(UserSpeakerException e) {
        String eName = e.getClass().getSimpleName();
        return ResultUtils.error(e.getCode(), e.getMessage(), e.getData());
    }

    @ExceptionHandler(AccountCanceledException.class)
    public Result error(AccountCanceledException e) {
        String eName = e.getClass().getSimpleName();
        return ResultUtils.error(e.getCode(), e.getMessage(), e.getData());
    }

    @ExceptionHandler(AccountLockedException.class)
    public Result error(AccountLockedException e) {
        String eName = e.getClass().getSimpleName();
        return ResultUtils.error(e.getCode(), e.getMessage(), e.getData());
    }

    @ExceptionHandler(AccountNotExistException.class)
    public Result error(AccountNotExistException e) {
        String eName = e.getClass().getSimpleName();
        return ResultUtils.error(e.getCode(), e.getMessage(), e.getData());
    }

    @ExceptionHandler(LoginException.class)
    public Result error(LoginException e) {
        String eName = e.getClass().getSimpleName();
        return ResultUtils.error(e.getCode(), e.getMessage(), e.getData());
    }

    @ExceptionHandler(LoginExpiredException.class)
    public Result error(LoginExpiredException e) {
        String eName = e.getClass().getSimpleName();
        return ResultUtils.error(e.getCode(), e.getMessage(), e.getData());
    }

    @ExceptionHandler(PasswordErrorException.class)
    public Result error(PasswordErrorException e) {
        String eName = e.getClass().getSimpleName();
        return ResultUtils.error(e.getCode(), e.getMessage(), e.getData());
    }

    @ExceptionHandler(PasswordRetryLimitedException.class)
    public Result error(PasswordRetryLimitedException e) {
        String eName = e.getClass().getSimpleName();
        return ResultUtils.error(e.getCode(), e.getMessage(), e.getData());
    }

    @ExceptionHandler(UserFaceRecognitionFailedException.class)
    public Result error(UserFaceRecognitionFailedException e) {
        String eName = e.getClass().getSimpleName();
        return ResultUtils.error(e.getCode(), e.getMessage(), e.getData());
    }

    @ExceptionHandler(UserFingerprintRecognitionFailedException.class)
    public Result error(UserFingerprintRecognitionFailedException e) {
        String eName = e.getClass().getSimpleName();
        return ResultUtils.error(e.getCode(), e.getMessage(), e.getData());
    }

    @ExceptionHandler(UserIdentityVerificationFailedException.class)
    public Result error(UserIdentityVerificationFailedException e) {
        String eName = e.getClass().getSimpleName();
        return ResultUtils.error(e.getCode(), e.getMessage(), e.getData());
    }

    @ExceptionHandler(UserNotAuthorizedToLoginByThirdpartyException.class)
    public Result error(UserNotAuthorizedToLoginByThirdpartyException e) {
        String eName = e.getClass().getSimpleName();
        return ResultUtils.error(e.getCode(), e.getMessage(), e.getData());
    }

    @ExceptionHandler(VerificationLoginCodeException.class)
    public Result error(VerificationLoginCodeException e) {
        String eName = e.getClass().getSimpleName();
        return ResultUtils.error(e.getCode(), e.getMessage(), e.getData());
    }

    @ExceptionHandler(AddressVerificationException.class)
    public Result error(AddressVerificationException e) {
        String eName = e.getClass().getSimpleName();
        return ResultUtils.error(e.getCode(), e.getMessage(), e.getData());
    }

    @ExceptionHandler(EmailVerificationCodeException.class)
    public Result error(EmailVerificationCodeException e) {
        String eName = e.getClass().getSimpleName();
        return ResultUtils.error(e.getCode(), e.getMessage(), e.getData());
    }

    @ExceptionHandler(EmailVerificationException.class)
    public Result error(EmailVerificationException e) {
        String eName = e.getClass().getSimpleName();
        return ResultUtils.error(e.getCode(), e.getMessage(), e.getData());
    }

    @ExceptionHandler(IdCardVerificationException.class)
    public Result error(IdCardVerificationException e) {
        String eName = e.getClass().getSimpleName();
        return ResultUtils.error(e.getCode(), e.getMessage(), e.getData());
    }

    @ExceptionHandler(LogonAreaLimitedException.class)
    public Result error(LogonAreaLimitedException e) {
        String eName = e.getClass().getSimpleName();
        return ResultUtils.error(e.getCode(), e.getMessage(), e.getData());
    }

    @ExceptionHandler(LogonException.class)
    public Result error(LogonException e) {
        String eName = e.getClass().getSimpleName();
        return ResultUtils.error(e.getCode(), e.getMessage(), e.getData());
    }

    @ExceptionHandler(MessageVerificationCodeException.class)
    public Result error(MessageVerificationCodeException e) {
        String eName = e.getClass().getSimpleName();
        return ResultUtils.error(e.getCode(), e.getMessage(), e.getData());
    }

    @ExceptionHandler(OfficialCardVerificationException.class)
    public Result error(OfficialCardVerificationException e) {
        String eName = e.getClass().getSimpleName();
        return ResultUtils.error(e.getCode(), e.getMessage(), e.getData());
    }

    @ExceptionHandler(PassportVerificationException.class)
    public Result error(PassportVerificationException e) {
        String eName = e.getClass().getSimpleName();
        return ResultUtils.error(e.getCode(), e.getMessage(), e.getData());
    }

    @ExceptionHandler(PasswordNotLongEnoughException.class)
    public Result error(PasswordNotLongEnoughException e) {
        String eName = e.getClass().getSimpleName();
        return ResultUtils.error(e.getCode(), e.getMessage(), e.getData());
    }

    @ExceptionHandler(PasswordNotStrongEnoughException.class)
    public Result error(PasswordNotStrongEnoughException e) {
        String eName = e.getClass().getSimpleName();
        return ResultUtils.error(e.getCode(), e.getMessage(), e.getData());
    }

    @ExceptionHandler(PasswordVerificationException.class)
    public Result error(PasswordVerificationException e) {
        String eName = e.getClass().getSimpleName();
        return ResultUtils.error(e.getCode(), e.getMessage(), e.getData());
    }

    @ExceptionHandler(PhoneVerificationException.class)
    public Result error(PhoneVerificationException e) {
        String eName = e.getClass().getSimpleName();
        return ResultUtils.error(e.getCode(), e.getMessage(), e.getData());
    }

    @ExceptionHandler(PrivacyPolicyNotAgreedException.class)
    public Result error(PrivacyPolicyNotAgreedException e) {
        String eName = e.getClass().getSimpleName();
        return ResultUtils.error(e.getCode(), e.getMessage(), e.getData());
    }

    @ExceptionHandler(UserCertificateException.class)
    public Result error(UserCertificateException e) {
        String eName = e.getClass().getSimpleName();
        return ResultUtils.error(e.getCode(), e.getMessage(), e.getData());
    }

    @ExceptionHandler(UserCertificateTypeNotChoseException.class)
    public Result error(UserCertificateTypeNotChoseException e) {
        String eName = e.getClass().getSimpleName();
        return ResultUtils.error(e.getCode(), e.getMessage(), e.getData());
    }

    @ExceptionHandler(UserInfoVerificationException.class)
    public Result error(UserInfoVerificationException e) {
        String eName = e.getClass().getSimpleName();
        return ResultUtils.error(e.getCode(), e.getMessage(), e.getData());
    }

    @ExceptionHandler(UsernameExistException.class)
    public Result error(UsernameExistException e) {
        String eName = e.getClass().getSimpleName();
        return ResultUtils.error(e.getCode(), e.getMessage(), e.getData());
    }

    @ExceptionHandler(UsernameNotAllowedException.class)
    public Result error(UsernameNotAllowedException e) {
        String eName = e.getClass().getSimpleName();
        return ResultUtils.error(e.getCode(), e.getMessage(), e.getData());
    }

    @ExceptionHandler(UsernameNotCompliantException.class)
    public Result error(UsernameNotCompliantException e) {
        String eName = e.getClass().getSimpleName();
        return ResultUtils.error(e.getCode(), e.getMessage(), e.getData());
    }

    @ExceptionHandler(UsernameVerificationException.class)
    public Result error(UsernameVerificationException e) {
        String eName = e.getClass().getSimpleName();
        return ResultUtils.error(e.getCode(), e.getMessage(), e.getData());
    }

    @ExceptionHandler(VerificationLogonCodeException.class)
    public Result error(VerificationLogonCodeException e) {
        String eName = e.getClass().getSimpleName();
        return ResultUtils.error(e.getCode(), e.getMessage(), e.getData());
    }

    @ExceptionHandler(VoiceVerificationCodeException.class)
    public Result error(VoiceVerificationCodeException e) {
        String eName = e.getClass().getSimpleName();
        return ResultUtils.error(e.getCode(), e.getMessage(), e.getData());
    }

    @ExceptionHandler(AddressIsNotInServiceException.class)
    public Result error(AddressIsNotInServiceException e) {
        String eName = e.getClass().getSimpleName();
        return ResultUtils.error(e.getCode(), e.getMessage(), e.getData());
    }

    @ExceptionHandler(AmountExceedingLimitException.class)
    public Result error(AmountExceedingLimitException e) {
        String eName = e.getClass().getSimpleName();
        return ResultUtils.error(e.getCode(), e.getMessage(), e.getData());
    }

    @ExceptionHandler(ConfirmOrderTimeoutException.class)
    public Result error(ConfirmOrderTimeoutException e) {
        String eName = e.getClass().getSimpleName();
        return ResultUtils.error(e.getCode(), e.getMessage(), e.getData());
    }

    @ExceptionHandler(ContainsContrabandSensitiveWordsException.class)
    public Result error(ContainsContrabandSensitiveWordsException e) {
        String eName = e.getClass().getSimpleName();
        return ResultUtils.error(e.getCode(), e.getMessage(), e.getData());
    }

    @ExceptionHandler(ContainsIllegalLinksException.class)
    public Result error(ContainsIllegalLinksException e) {
        String eName = e.getClass().getSimpleName();
        return ResultUtils.error(e.getCode(), e.getMessage(), e.getData());
    }

    @ExceptionHandler(ExceedingQuantityLimitException.class)
    public Result error(ExceedingQuantityLimitException e) {
        String eName = e.getClass().getSimpleName();
        return ResultUtils.error(e.getCode(), e.getMessage(), e.getData());
    }

    @ExceptionHandler(FileInfringementException.class)
    public Result error(FileInfringementException e) {
        String eName = e.getClass().getSimpleName();
        return ResultUtils.error(e.getCode(), e.getMessage(), e.getData());
    }

    @ExceptionHandler(IllegalUserInputException.class)
    public Result error(IllegalUserInputException e) {
        String eName = e.getClass().getSimpleName();
        return ResultUtils.error(e.getCode(), e.getMessage(), e.getData());
    }

    @ExceptionHandler(InvalidTimestampParameterException.class)
    public Result error(InvalidTimestampParameterException e) {
        String eName = e.getClass().getSimpleName();
        return ResultUtils.error(e.getCode(), e.getMessage(), e.getData());
    }

    @ExceptionHandler(InvalidUserInputException.class)
    public Result error(InvalidUserInputException e) {
        String eName = e.getClass().getSimpleName();
        return ResultUtils.error(e.getCode(), e.getMessage(), e.getData());
    }

    @ExceptionHandler(MissingTimestampParameterException.class)
    public Result error(MissingTimestampParameterException e) {
        String eName = e.getClass().getSimpleName();
        return ResultUtils.error(e.getCode(), e.getMessage(), e.getData());
    }

    @ExceptionHandler(OrderClosedException.class)
    public Result error(OrderClosedException e) {
        String eName = e.getClass().getSimpleName();
        return ResultUtils.error(e.getCode(), e.getMessage(), e.getData());
    }

    @ExceptionHandler(OrderQuantityIsEmptyException.class)
    public Result error(OrderQuantityIsEmptyException e) {
        String eName = e.getClass().getSimpleName();
        return ResultUtils.error(e.getCode(), e.getMessage(), e.getData());
    }

    @ExceptionHandler(ParameterFormatDoesNotMatchException.class)
    public Result error(ParameterFormatDoesNotMatchException e) {
        String eName = e.getClass().getSimpleName();
        return ResultUtils.error(e.getCode(), e.getMessage(), e.getData());
    }

    @ExceptionHandler(PicturesContainProhibitedInformationException.class)
    public Result error(PicturesContainProhibitedInformationException e) {
        String eName = e.getClass().getSimpleName();
        return ResultUtils.error(e.getCode(), e.getMessage(), e.getData());
    }

    @ExceptionHandler(RequestForJsonParsingFailedException.class)
    public Result error(RequestForJsonParsingFailedException e) {
        String eName = e.getClass().getSimpleName();
        return ResultUtils.error(e.getCode(), e.getMessage(), e.getData());
    }

    @ExceptionHandler(RequestParameterValueOverRangeException.class)
    public Result error(RequestParameterValueOverRangeException e) {
        String eName = e.getClass().getSimpleName();
        return ResultUtils.error(e.getCode(), e.getMessage(), e.getData());
    }

    @ExceptionHandler(RequiredParameterForTheRequestIsEmptyException.class)
    public Result error(RequiredParameterForTheRequestIsEmptyException e) {
        String eName = e.getClass().getSimpleName();
        return ResultUtils.error(e.getCode(), e.getMessage(), e.getData());
    }

    @ExceptionHandler(TimeOutOfServiceException.class)
    public Result error(TimeOutOfServiceException e) {
        String eName = e.getClass().getSimpleName();
        return ResultUtils.error(e.getCode(), e.getMessage(), e.getData());
    }

    @ExceptionHandler(TotalNumberExceedsTheLimitException.class)
    public Result error(TotalNumberExceedsTheLimitException e) {
        String eName = e.getClass().getSimpleName();
        return ResultUtils.error(e.getCode(), e.getMessage(), e.getData());
    }

    @ExceptionHandler(UserOperationException.class)
    public Result error(UserOperationException e) {
        String eName = e.getClass().getSimpleName();
        return ResultUtils.error(e.getCode(), e.getMessage(), e.getData());
    }

    @ExceptionHandler(UserOrderNumberIsEmptyException.class)
    public Result error(UserOrderNumberIsEmptyException e) {
        String eName = e.getClass().getSimpleName();
        return ResultUtils.error(e.getCode(), e.getMessage(), e.getData());
    }

    @ExceptionHandler(UserPaymentTimeoutException.class)
    public Result error(UserPaymentTimeoutException e) {
        String eName = e.getClass().getSimpleName();
        return ResultUtils.error(e.getCode(), e.getMessage(), e.getData());
    }

    @ExceptionHandler(UserRequestParameterErrorException.class)
    public Result error(UserRequestParameterErrorException e) {
        String eName = e.getClass().getSimpleName();
        return ResultUtils.error(e.getCode(), e.getMessage(), e.getData());
    }

    @ExceptionHandler(UserAddressBookNotAuthorizedException.class)
    public Result error(UserAddressBookNotAuthorizedException e) {
        String eName = e.getClass().getSimpleName();
        return ResultUtils.error(e.getCode(), e.getMessage(), e.getData());
    }

    @ExceptionHandler(UserCameraIsNotAuthorizedException.class)
    public Result error(UserCameraIsNotAuthorizedException e) {
        String eName = e.getClass().getSimpleName();
        return ResultUtils.error(e.getCode(), e.getMessage(), e.getData());
    }

    @ExceptionHandler(UserCameraPhotoIsNotAuthorizedException.class)
    public Result error(UserCameraPhotoIsNotAuthorizedException e) {
        String eName = e.getClass().getSimpleName();
        return ResultUtils.error(e.getCode(), e.getMessage(), e.getData());
    }

    @ExceptionHandler(UserFileIsNotAuthorizedException.class)
    public Result error(UserFileIsNotAuthorizedException e) {
        String eName = e.getClass().getSimpleName();
        return ResultUtils.error(e.getCode(), e.getMessage(), e.getData());
    }

    @ExceptionHandler(UserImageLibraryIsNotAuthorizedException.class)
    public Result error(UserImageLibraryIsNotAuthorizedException e) {
        String eName = e.getClass().getSimpleName();
        return ResultUtils.error(e.getCode(), e.getMessage(), e.getData());
    }

    @ExceptionHandler(UserLocationInformationIsNotAuthorizedException.class)
    public Result error(UserLocationInformationIsNotAuthorizedException e) {
        String eName = e.getClass().getSimpleName();
        return ResultUtils.error(e.getCode(), e.getMessage(), e.getData());
    }

    @ExceptionHandler(UserPrivacyIsNotAuthorizedException.class)
    public Result error(UserPrivacyIsNotAuthorizedException e) {
        String eName = e.getClass().getSimpleName();
        return ResultUtils.error(e.getCode(), e.getMessage(), e.getData());
    }

    @ExceptionHandler(UserPrivacyNotSignedException.class)
    public Result error(UserPrivacyNotSignedException e) {
        String eName = e.getClass().getSimpleName();
        return ResultUtils.error(e.getCode(), e.getMessage(), e.getData());
    }

    @ExceptionHandler(ConcurrentRequestsExceededTheLimitException.class)
    public Result error(ConcurrentRequestsExceededTheLimitException e) {
        String eName = e.getClass().getSimpleName();
        return ResultUtils.error(e.getCode(), e.getMessage(), e.getData());
    }

    @ExceptionHandler(PleaseWaitForUserActionException.class)
    public Result error(PleaseWaitForUserActionException e) {
        String eName = e.getClass().getSimpleName();
        return ResultUtils.error(e.getCode(), e.getMessage(), e.getData());
    }

    @ExceptionHandler(RequestsExceededTheLimitException.class)
    public Result error(RequestsExceededTheLimitException e) {
        String eName = e.getClass().getSimpleName();
        return ResultUtils.error(e.getCode(), e.getMessage(), e.getData());
    }

    @ExceptionHandler(UserRepeatRequestException.class)
    public Result error(UserRepeatRequestException e) {
        String eName = e.getClass().getSimpleName();
        return ResultUtils.error(e.getCode(), e.getMessage(), e.getData());
    }

    @ExceptionHandler(UserRequestServiceException.class)
    public Result error(UserRequestServiceException e) {
        String eName = e.getClass().getSimpleName();
        return ResultUtils.error(e.getCode(), e.getMessage(), e.getData());
    }

    @ExceptionHandler(WebSocketConnectionException.class)
    public Result error(WebSocketConnectionException e) {
        String eName = e.getClass().getSimpleName();
        return ResultUtils.error(e.getCode(), e.getMessage(), e.getData());
    }

    @ExceptionHandler(WebSocketConnectionIsBrokenException.class)
    public Result error(WebSocketConnectionIsBrokenException e) {
        String eName = e.getClass().getSimpleName();
        return ResultUtils.error(e.getCode(), e.getMessage(), e.getData());
    }

    @ExceptionHandler(InsufficientAccountBalanceException.class)
    public Result error(InsufficientAccountBalanceException e) {
        String eName = e.getClass().getSimpleName();
        return ResultUtils.error(e.getCode(), e.getMessage(), e.getData());
    }

    @ExceptionHandler(InsufficientUserDiskSpaceException.class)
    public Result error(InsufficientUserDiskSpaceException e) {
        String eName = e.getClass().getSimpleName();
        return ResultUtils.error(e.getCode(), e.getMessage(), e.getData());
    }

    @ExceptionHandler(InsufficientUserOssCapacityException.class)
    public Result error(InsufficientUserOssCapacityException e) {
        String eName = e.getClass().getSimpleName();
        return ResultUtils.error(e.getCode(), e.getMessage(), e.getData());
    }

    @ExceptionHandler(NotEnoughUserMemoryException.class)
    public Result error(NotEnoughUserMemoryException e) {
        String eName = e.getClass().getSimpleName();
        return ResultUtils.error(e.getCode(), e.getMessage(), e.getData());
    }

    @ExceptionHandler(UserQuotaHasBeenUsedUpException.class)
    public Result error(UserQuotaHasBeenUsedUpException e) {
        String eName = e.getClass().getSimpleName();
        return ResultUtils.error(e.getCode(), e.getMessage(), e.getData());
    }

    @ExceptionHandler(UserResourceException.class)
    public Result error(UserResourceException e) {
        String eName = e.getClass().getSimpleName();
        return ResultUtils.error(e.getCode(), e.getMessage(), e.getData());
    }

    @ExceptionHandler(UsersUploadPicturesThatAreTooBigException.class)
    public Result error(UsersUploadPicturesThatAreTooBigException e) {
        String eName = e.getClass().getSimpleName();
        return ResultUtils.error(e.getCode(), e.getMessage(), e.getData());
    }

    @ExceptionHandler(UserUploadedTheCompressedFileIsTooLargeException.class)
    public Result error(UserUploadedTheCompressedFileIsTooLargeException e) {
        String eName = e.getClass().getSimpleName();
        return ResultUtils.error(e.getCode(), e.getMessage(), e.getData());
    }

    @ExceptionHandler(UserUploadFileException.class)
    public Result error(UserUploadFileException e) {
        String eName = e.getClass().getSimpleName();
        return ResultUtils.error(e.getCode(), e.getMessage(), e.getData());
    }

    @ExceptionHandler(UserUploadFileTypeDoesNotMatchException.class)
    public Result error(UserUploadFileTypeDoesNotMatchException e) {
        String eName = e.getClass().getSimpleName();
        return ResultUtils.error(e.getCode(), e.getMessage(), e.getData());
    }

    @ExceptionHandler(UserUploadsFileTooLargeException.class)
    public Result error(UserUploadsFileTooLargeException e) {
        String eName = e.getClass().getSimpleName();
        return ResultUtils.error(e.getCode(), e.getMessage(), e.getData());
    }

    @ExceptionHandler(UserUploadsTheVideoTooBigException.class)
    public Result error(UserUploadsTheVideoTooBigException e) {
        String eName = e.getClass().getSimpleName();
        return ResultUtils.error(e.getCode(), e.getMessage(), e.getData());
    }

    @ExceptionHandler(UserApiRequestVersionDoesNotMatchException.class)
    public Result error(UserApiRequestVersionDoesNotMatchException e) {
        String eName = e.getClass().getSimpleName();
        return ResultUtils.error(e.getCode(), e.getMessage(), e.getData());
    }

    @ExceptionHandler(UserApiRequestVersionIsTooLowException.class)
    public Result error(UserApiRequestVersionIsTooLowException e) {
        String eName = e.getClass().getSimpleName();
        return ResultUtils.error(e.getCode(), e.getMessage(), e.getData());
    }

    @ExceptionHandler(UserApiRequestVersionTooHighException.class)
    public Result error(UserApiRequestVersionTooHighException e) {
        String eName = e.getClass().getSimpleName();
        return ResultUtils.error(e.getCode(), e.getMessage(), e.getData());
    }

    @ExceptionHandler(UserCurrentVersionException.class)
    public Result error(UserCurrentVersionException e) {
        String eName = e.getClass().getSimpleName();
        return ResultUtils.error(e.getCode(), e.getMessage(), e.getData());
    }

    @ExceptionHandler(UserInstalledVersionDoesNotMatchTheSystemException.class)
    public Result error(UserInstalledVersionDoesNotMatchTheSystemException e) {
        String eName = e.getClass().getSimpleName();
        return ResultUtils.error(e.getCode(), e.getMessage(), e.getData());
    }

    @ExceptionHandler(UserInstalledVersionHasExpiredException.class)
    public Result error(UserInstalledVersionHasExpiredException e) {
        String eName = e.getClass().getSimpleName();
        return ResultUtils.error(e.getCode(), e.getMessage(), e.getData());
    }

    @ExceptionHandler(UserInstalledVersionTooHighException.class)
    public Result error(UserInstalledVersionTooHighException e) {
        String eName = e.getClass().getSimpleName();
        return ResultUtils.error(e.getCode(), e.getMessage(), e.getData());
    }

    @ExceptionHandler(UserInstalledVersionTooLowException.class)
    public Result error(UserInstalledVersionTooLowException e) {
        String eName = e.getClass().getSimpleName();
        return ResultUtils.error(e.getCode(), e.getMessage(), e.getData());
    }

    @ExceptionHandler(UserException.class)
    public Result error(UserException e) {
        String eName = e.getClass().getSimpleName();
        return ResultUtils.error(e.getCode(), e.getMessage(), e.getData());
    }






    @ExceptionHandler(BaseException.class)
    public Result error(BaseException e) {
        log.error("系统异常:{}",e.getStackTrace());
        e.printStackTrace();
        return ResultUtils.error(ResultCodeEnum.B0001.value(), ResultCodeEnum.B0001.getName());
    }

    @ExceptionHandler(RuntimeException.class)
    public Result error(RuntimeException e) {
        log.error("系统异常:{}",e.getStackTrace());
        e.printStackTrace();
        return ResultUtils.error(ResultCodeEnum.B0001.value(), ResultCodeEnum.B0001.getName());
    }

    @ExceptionHandler(Exception.class)
    public Result error(Exception e) {
        log.error("系统异常:{}",e.getStackTrace());
        e.printStackTrace();
        return ResultUtils.error(ResultCodeEnum.B0001.value(), ResultCodeEnum.B0001.getName());
    }

    @ExceptionHandler(Throwable.class)
    public Result error(Throwable e) {
        log.error("系统异常:{}",e.getStackTrace());
        e.printStackTrace();
        return ResultUtils.error(ResultCodeEnum.B0001.value(), ResultCodeEnum.B0001.getName());
    }


}
