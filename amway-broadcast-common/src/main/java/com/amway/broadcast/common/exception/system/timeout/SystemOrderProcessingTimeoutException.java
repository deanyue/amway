package com.amway.broadcast.common.exception.system.timeout;

import com.amway.broadcast.common.enumeration.ResultCodeEnum;
import com.amway.broadcast.common.exception.BaseException;

/**
 *  系统订单处理超时 B0101
 *
 * @author Lee
 * @version 1.0
 * @date 2020/5/8 18:09
 */
public class SystemOrderProcessingTimeoutException extends BaseException {

    public SystemOrderProcessingTimeoutException() {
        super(ResultCodeEnum.B0101.getName());
        this.code = ResultCodeEnum.B0101.value();
    }

    public SystemOrderProcessingTimeoutException(String message) {
        super(message);
        this.code = ResultCodeEnum.B0101.value();
    }

    public SystemOrderProcessingTimeoutException(Object data) {
        super(ResultCodeEnum.B0101.getName());
        this.code = ResultCodeEnum.B0101.value();
        this.data = data;
    }

    public SystemOrderProcessingTimeoutException(String message, Object data) {
        super(message);
        this.code = ResultCodeEnum.B0101.value();
        this.data = data;
    }
}
