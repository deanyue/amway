package com.amway.broadcast.common.util;

import lombok.extern.slf4j.Slf4j;
import org.springframework.data.redis.core.StringRedisTemplate;

import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;

/**
 * redis 工具类
 *
 * @author
 */
@Slf4j
public class RedisUtil {

    /**
     * 设置缓存
     *
     * @param key
     * @param value
     */
    public static void set(String key, String value) {
        StringRedisTemplate stringRedisTemplate = ApplicationContextUtils.getBean(StringRedisTemplate.class);
        stringRedisTemplate.opsForValue().set(key, value);
    }

    /**
     * 设置缓存，并设置过期时间
     *
     * @param key
     * @param value
     * @param timeout
     */
    public static void set(String key, String value, long timeout) {
        StringRedisTemplate stringRedisTemplate = ApplicationContextUtils.getBean(StringRedisTemplate.class);
        stringRedisTemplate.opsForValue().set(key, value, timeout, TimeUnit.SECONDS);
    }

    /**
     * 判断key是否存在
     *
     * @param key
     * @return
     */
    public static boolean hasKey(String key) {
        StringRedisTemplate stringRedisTemplate = ApplicationContextUtils.getBean(StringRedisTemplate.class);
        return stringRedisTemplate.hasKey(key);
    }

    /**
     * 获取缓存值
     *
     * @param key
     * @return
     */
    public static String get(String key) {
        StringRedisTemplate stringRedisTemplate = ApplicationContextUtils.getBean(StringRedisTemplate.class);
        return stringRedisTemplate.opsForValue().get(key);
    }

    /**
     * 删除键
     *
     * @param key
     */
    public static void del(String key) {
        StringRedisTemplate stringRedisTemplate = ApplicationContextUtils.getBean(StringRedisTemplate.class);
        stringRedisTemplate.delete(key);
    }

    /**
     * 删除所有的键值对
     */
    public static void delAll() {
        StringRedisTemplate stringRedisTemplate = ApplicationContextUtils.getBean(StringRedisTemplate.class);
        Set<String> keys = stringRedisTemplate.keys("*");
        stringRedisTemplate.delete(keys);
    }

    /**
     * 模糊删除
     *
     * @param key
     */
    public static void matchDelete(String key) {
        StringRedisTemplate stringRedisTemplate = ApplicationContextUtils.getBean(StringRedisTemplate.class);
        stringRedisTemplate.delete(stringRedisTemplate.keys(key + "*"));
    }

    /**
     * 设置锁和超时时间
     *
     * @param lockKey
     * @param expireTime
     * @return
     */
    public static Boolean setNxAndExpireTime(String lockKey, long expireTime) {
        StringRedisTemplate stringRedisTemplate = ApplicationContextUtils.getBean(StringRedisTemplate.class);
        return stringRedisTemplate.opsForValue().setIfAbsent(lockKey, lockKey, expireTime, TimeUnit.SECONDS);
    }

    public static Long setList(String key, List<String> records) {
        StringRedisTemplate stringRedisTemplate = ApplicationContextUtils.getBean(StringRedisTemplate.class);
        return stringRedisTemplate.opsForList().rightPushAll(key, records);
    }

    public static List<String> getList(String key) {
        StringRedisTemplate stringRedisTemplate = ApplicationContextUtils.getBean(StringRedisTemplate.class);
        return stringRedisTemplate.opsForList().range(key, 0, -1);
    }

    public static Set<String> keys(String keyPattern) {
        StringRedisTemplate stringRedisTemplate = ApplicationContextUtils.getBean(StringRedisTemplate.class);
        Set<String> keys = stringRedisTemplate.keys(keyPattern);
        return keys;
    }
}
