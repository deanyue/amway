package com.amway.broadcast.common.exception.remote.database;

import com.amway.broadcast.common.enumeration.ResultCodeEnum;
import com.amway.broadcast.common.exception.BaseException;

/**
 *  主键冲突 C0341
 *
 * @author a good man
 * @version 1.0
 * @date 2020/5/9 10:06
 */
public class PrimaryKeyConflictException extends BaseException {

    public PrimaryKeyConflictException() {
        super(ResultCodeEnum.C0341.getName());
        this.code = ResultCodeEnum.C0341.value();
    }

    public PrimaryKeyConflictException(String message) {
        super(message);
        this.code = ResultCodeEnum.C0341.value();
    }

    public PrimaryKeyConflictException(Object data) {
        super(ResultCodeEnum.C0341.getName());
        this.code = ResultCodeEnum.C0341.value();
        this.data = data;
    }

    public PrimaryKeyConflictException(String message, Object data) {
        super(message);
        this.code = ResultCodeEnum.C0341.value();
        this.data = data;
    }
}
