package com.amway.broadcast.common.base;

import com.alibaba.fastjson.JSONObject;
import com.amway.broadcast.common.enumeration.ResultCodeEnum;
import lombok.extern.slf4j.Slf4j;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * RestController返回结果处理
 *
 * @author Lee
 */
@Slf4j
public class ResultUtils {

    private ResultUtils() {
    }

    /**
     * 成功结果返回
     *
     * @return
     */
    public static Result success() {
        return success(null);
    }

    /**
     * 成功结果返回
     *
     * @param obj
     * @return
     */
    public static Result success(Object obj) {
        return Result.builder().code(ResultCodeEnum.OK.value()).msg(ResultCodeEnum.OK.getName()).data(obj).build();
    }

    /**
     * 异常结果返回
     *
     * @param code
     * @param msg
     * @return
     */
    public static Result error(String code, String msg) {
        return error(code, msg, null);
    }

    /**
     * 异常结果返回
     *
     * @param code
     * @param msg
     * @param obj
     * @return
     */
    public static Result error(String code, String msg, Object obj) {
        return Result.builder().code(code).msg(msg).data(obj).build();
    }

    /**
     * 获取验证结果
     *
     * @param bindingResult
     * @return
     */
    public static Object getJsonResult(BindingResult bindingResult) {
        List<FieldError> errors = bindingResult.getFieldErrors();
        Map<String, String> map = new HashMap<String, String>();
        for (FieldError error : errors) {
            map.put(error.getField(), error.getDefaultMessage());
        }
        return JSONObject.toJSON(map);
    }

    /**
     * 判断请求是否成功
     *
     * @param result
     * @return
     */
    public static boolean isSuccess(Result result) {
        return ResultCodeEnum.OK.value().equals(result.getCode());
    }
}

