package com.amway.broadcast.common.exception.user.upload;

import com.amway.broadcast.common.enumeration.ResultCodeEnum;
import com.amway.broadcast.common.exception.BaseException;

/**
 *  用户上传文件太大 A0702
 *
 * @author Lee
 * @version 1.0
 * @date 2020/5/8 17:23
 */
public class UserUploadsFileTooLargeException extends BaseException {

    public UserUploadsFileTooLargeException() {
        super(ResultCodeEnum.A0702.getName());
        this.code = ResultCodeEnum.A0702.value();
    }

    public UserUploadsFileTooLargeException(String message) {
        super(message);
        this.code = ResultCodeEnum.A0702.value();
    }

    public UserUploadsFileTooLargeException(Object data) {
        super(ResultCodeEnum.A0702.getName());
        this.code = ResultCodeEnum.A0702.value();
        this.data = data;
    }

    public UserUploadsFileTooLargeException(String message, Object data) {
        super(message);
        this.code = ResultCodeEnum.A0702.value();
        this.data = data;
    }
}
