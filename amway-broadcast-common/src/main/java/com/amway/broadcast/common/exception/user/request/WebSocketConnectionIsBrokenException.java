package com.amway.broadcast.common.exception.user.request;

import com.amway.broadcast.common.enumeration.ResultCodeEnum;
import com.amway.broadcast.common.exception.BaseException;

/**
 *  WebSocket连接断开 A0505
 *
 * @author Lee
 * @version 1.0
 * @date 2020/5/8 17:05
 */
public class WebSocketConnectionIsBrokenException extends BaseException {

    public WebSocketConnectionIsBrokenException() {
        super(ResultCodeEnum.A0505.getName());
        this.code = ResultCodeEnum.A0505.value();
    }

    public WebSocketConnectionIsBrokenException(String message) {
        super(message);
        this.code = ResultCodeEnum.A0505.value();
    }

    public WebSocketConnectionIsBrokenException(Object data) {
        super(ResultCodeEnum.A0505.getName());
        this.code = ResultCodeEnum.A0505.value();
        this.data = data;
    }

    public WebSocketConnectionIsBrokenException(String message, Object data) {
        super(message);
        this.code = ResultCodeEnum.A0505.value();
        this.data = data;
    }
}
