package com.amway.broadcast.common.exception.user.equipment;

import com.amway.broadcast.common.enumeration.ResultCodeEnum;
import com.amway.broadcast.common.exception.BaseException;

/**
 *  用户设备异常 A1000
 *
 * @author Lee
 * @version 1.0
 * @date 2020/5/8 17:58
 */
public class AbnormalUserEquipmentException extends BaseException {

    public AbnormalUserEquipmentException() {
        super(ResultCodeEnum.A1000.getName());
        this.code = ResultCodeEnum.A1000.value();
    }

    public AbnormalUserEquipmentException(String message) {
        super(message);
        this.code = ResultCodeEnum.A1000.value();
    }

    public AbnormalUserEquipmentException(Object data) {
        super(ResultCodeEnum.A1000.getName());
        this.code = ResultCodeEnum.A1000.value();
        this.data = data;
    }

    public AbnormalUserEquipmentException(String message, Object data) {
        super(message);
        this.code = ResultCodeEnum.A1000.value();
        this.data = data;
    }
}
