package com.amway.broadcast.common.exception.user.parm;

import com.amway.broadcast.common.enumeration.ResultCodeEnum;
import com.amway.broadcast.common.exception.BaseException;

/**
 *  确认订单超时 A0442
 *
 * @author Lee
 * @version 1.0
 * @date 2020/5/8 16:25
 */
public class ConfirmOrderTimeoutException extends BaseException {

    public ConfirmOrderTimeoutException() {
        super(ResultCodeEnum.A0442.getName());
        this.code = ResultCodeEnum.A0442.value();
    }

    public ConfirmOrderTimeoutException(String message) {
        super(message);
        this.code = ResultCodeEnum.A0442.value();
    }

    public ConfirmOrderTimeoutException(Object data) {
        super(ResultCodeEnum.A0442.getName());
        this.code = ResultCodeEnum.A0442.value();
        this.data = data;
    }

    public ConfirmOrderTimeoutException(String message, Object data) {
        super(message);
        this.code = ResultCodeEnum.A0442.value();
        this.data = data;
    }
}
