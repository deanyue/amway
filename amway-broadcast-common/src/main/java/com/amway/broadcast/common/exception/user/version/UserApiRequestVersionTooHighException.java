package com.amway.broadcast.common.exception.user.version;

import com.amway.broadcast.common.enumeration.ResultCodeEnum;
import com.amway.broadcast.common.exception.BaseException;

/**
 *  用户API请求版本过高 A0806
 *
 * @author Lee
 * @version 1.0
 * @date 2020/5/8 17:38
 */
public class UserApiRequestVersionTooHighException extends BaseException {

    public UserApiRequestVersionTooHighException() {
        super(ResultCodeEnum.A0806.getName());
        this.code = ResultCodeEnum.A0806.value();
    }

    public UserApiRequestVersionTooHighException(String message) {
        super(message);
        this.code = ResultCodeEnum.A0806.value();
    }

    public UserApiRequestVersionTooHighException(Object data) {
        super(ResultCodeEnum.A0806.getName());
        this.code = ResultCodeEnum.A0806.value();
        this.data = data;
    }

    public UserApiRequestVersionTooHighException(String message, Object data) {
        super(message);
        this.code = ResultCodeEnum.A0806.value();
        this.data = data;
    }
}
