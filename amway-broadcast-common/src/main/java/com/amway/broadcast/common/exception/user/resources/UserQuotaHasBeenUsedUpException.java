package com.amway.broadcast.common.exception.user.resources;

import com.amway.broadcast.common.enumeration.ResultCodeEnum;
import com.amway.broadcast.common.exception.BaseException;

/**
 *  用户配额已用光 A0605
 *
 * @author Lee
 * @version 1.0
 * @date 2020/5/8 17:17
 */
public class UserQuotaHasBeenUsedUpException extends BaseException {

    public UserQuotaHasBeenUsedUpException() {
        super(ResultCodeEnum.A0605.getName());
        this.code = ResultCodeEnum.A0605.value();
    }

    public UserQuotaHasBeenUsedUpException(String message) {
        super(message);
        this.code = ResultCodeEnum.A0605.value();
    }

    public UserQuotaHasBeenUsedUpException(Object data) {
        super(ResultCodeEnum.A0605.getName());
        this.code = ResultCodeEnum.A0605.value();
        this.data = data;
    }

    public UserQuotaHasBeenUsedUpException(String message, Object data) {
        super(message);
        this.code = ResultCodeEnum.A0605.value();
        this.data = data;
    }
}
