package com.amway.broadcast.common.exception.user.upload;

import com.amway.broadcast.common.enumeration.ResultCodeEnum;
import com.amway.broadcast.common.exception.BaseException;

/**
 *  用户上传文件异常 A0700
 *
 * @author Lee
 * @version 1.0
 * @date 2020/5/8 17:21
 */
public class UserUploadFileException extends BaseException {

    public UserUploadFileException() {
        super(ResultCodeEnum.A0700.getName());
        this.code = ResultCodeEnum.A0700.value();
    }

    public UserUploadFileException(String message) {
        super(message);
        this.code = ResultCodeEnum.A0700.value();
    }

    public UserUploadFileException(Object data) {
        super(ResultCodeEnum.A0700.getName());
        this.code = ResultCodeEnum.A0700.value();
        this.data = data;
    }

    public UserUploadFileException(String message, Object data) {
        super(message);
        this.code = ResultCodeEnum.A0700.value();
        this.data = data;
    }
}
