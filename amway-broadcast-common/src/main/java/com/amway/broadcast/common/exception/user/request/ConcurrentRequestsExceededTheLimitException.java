package com.amway.broadcast.common.exception.user.request;

import com.amway.broadcast.common.enumeration.ResultCodeEnum;
import com.amway.broadcast.common.exception.BaseException;

/**
 *  请求并发数超出限制 A0502
 *
 * @author Lee
 * @version 1.0
 * @date 2020/5/8 17:03
 */
public class ConcurrentRequestsExceededTheLimitException extends BaseException {

    public ConcurrentRequestsExceededTheLimitException() {
        super(ResultCodeEnum.A0502.getName());
        this.code = ResultCodeEnum.A0502.value();
    }

    public ConcurrentRequestsExceededTheLimitException(String message) {
        super(message);
        this.code = ResultCodeEnum.A0502.value();
    }

    public ConcurrentRequestsExceededTheLimitException(Object data) {
        super(ResultCodeEnum.A0502.getName());
        this.code = ResultCodeEnum.A0502.value();
        this.data = data;
    }

    public ConcurrentRequestsExceededTheLimitException(String message, Object data) {
        super(message);
        this.code = ResultCodeEnum.A0502.value();
        this.data = data;
    }
}
