package com.amway.broadcast.common.exception.remote.timeout;

import com.amway.broadcast.common.enumeration.ResultCodeEnum;
import com.amway.broadcast.common.exception.BaseException;

/**
 *  RPC执行超时 C0210
 *
 * @author Lee
 * @version 1.0
 * @date 2020/5/9 9:56
 */
public class RpcExecutionTimeoutException extends BaseException {

    public RpcExecutionTimeoutException() {
        super(ResultCodeEnum.C0210.getName());
        this.code = ResultCodeEnum.C0210.value();
    }

    public RpcExecutionTimeoutException(String message) {
        super(message);
        this.code = ResultCodeEnum.C0210.value();
    }

    public RpcExecutionTimeoutException(Object data) {
        super(ResultCodeEnum.C0210.getName());
        this.code = ResultCodeEnum.C0210.value();
        this.data = data;
    }

    public RpcExecutionTimeoutException(String message, Object data) {
        super(message);
        this.code = ResultCodeEnum.C0210.value();
        this.data = data;
    }
}
