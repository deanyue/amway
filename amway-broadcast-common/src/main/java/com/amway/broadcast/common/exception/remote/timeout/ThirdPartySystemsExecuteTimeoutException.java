package com.amway.broadcast.common.exception.remote.timeout;

import com.amway.broadcast.common.enumeration.ResultCodeEnum;
import com.amway.broadcast.common.exception.BaseException;

/**
 *  第三方系统执行超时 C0200
 *
 * @author Lee
 * @version 1.0
 * @date 2020/5/9 9:55
 */
public class ThirdPartySystemsExecuteTimeoutException extends BaseException {

    public ThirdPartySystemsExecuteTimeoutException() {
        super(ResultCodeEnum.C0200.getName());
        this.code = ResultCodeEnum.C0200.value();
    }

    public ThirdPartySystemsExecuteTimeoutException(String message) {
        super(message);
        this.code = ResultCodeEnum.C0200.value();
    }

    public ThirdPartySystemsExecuteTimeoutException(Object data) {
        super(ResultCodeEnum.C0200.getName());
        this.code = ResultCodeEnum.C0200.value();
        this.data = data;
    }

    public ThirdPartySystemsExecuteTimeoutException(String message, Object data) {
        super(message);
        this.code = ResultCodeEnum.C0200.value();
        this.data = data;
    }
}
