package com.amway.broadcast.common.exception.remote.middle;

import com.amway.broadcast.common.enumeration.ResultCodeEnum;
import com.amway.broadcast.common.exception.BaseException;

/**
 *  RPC服务未注册 C0112
 *
 * @author Lee
 * @version 1.0
 * @date 2020/5/9 9:30
 */
public class RpcServiceIsNotRegisteredException extends BaseException {

    public RpcServiceIsNotRegisteredException() {
        super(ResultCodeEnum.C0112.getName());
        this.code = ResultCodeEnum.C0112.value();
    }

    public RpcServiceIsNotRegisteredException(String message) {
        super(message);
        this.code = ResultCodeEnum.C0112.value();
    }

    public RpcServiceIsNotRegisteredException(Object data) {
        super(ResultCodeEnum.C0112.getName());
        this.code = ResultCodeEnum.C0112.value();
        this.data = data;
    }

    public RpcServiceIsNotRegisteredException(String message, Object data) {
        super(message);
        this.code = ResultCodeEnum.C0112.value();
        this.data = data;
    }
}
