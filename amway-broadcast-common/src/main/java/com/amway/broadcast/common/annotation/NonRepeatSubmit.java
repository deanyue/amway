package com.amway.broadcast.common.annotation;

import java.lang.annotation.*;

/**
 * @author liujch
 * @description 日志
 * @Package com.amway.broadcast.common.annotation
 * @date 2020年12月12日 11:43
 */
@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface NonRepeatSubmit {

    int keepSeconds() default 3;

}
