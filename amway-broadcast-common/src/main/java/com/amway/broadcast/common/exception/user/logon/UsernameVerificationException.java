package com.amway.broadcast.common.exception.user.logon;

import com.amway.broadcast.common.enumeration.ResultCodeEnum;
import com.amway.broadcast.common.exception.BaseException;

/**
 * 用户名校验失败 A0110
 *
 * @author Lee
 */
public class UsernameVerificationException extends BaseException {

    public UsernameVerificationException() {
        super(ResultCodeEnum.A0110.getName());
        this.code = ResultCodeEnum.A0110.value();
    }

    public UsernameVerificationException(String message) {
        super(message);
        this.code = ResultCodeEnum.A0110.value();
    }

    public UsernameVerificationException(Object data) {
        super(ResultCodeEnum.A0110.getName());
        this.code = ResultCodeEnum.A0110.value();
        this.data = data;
    }

    public UsernameVerificationException(String message, Object data) {
        super(message);
        this.code = ResultCodeEnum.A0110.value();
        this.data = data;
    }
}
