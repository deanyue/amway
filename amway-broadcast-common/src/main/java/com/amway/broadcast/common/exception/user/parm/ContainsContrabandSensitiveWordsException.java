package com.amway.broadcast.common.exception.user.parm;

import com.amway.broadcast.common.enumeration.ResultCodeEnum;
import com.amway.broadcast.common.exception.BaseException;

/**
 *  包含违禁敏感词 A0431
 *
 * @author Lee
 * @version 1.0
 * @date 2020/5/8 16:22
 */
public class ContainsContrabandSensitiveWordsException extends BaseException {

    public ContainsContrabandSensitiveWordsException() {
        super(ResultCodeEnum.A0431.getName());
        this.code = ResultCodeEnum.A0431.value();
    }

    public ContainsContrabandSensitiveWordsException(String message) {
        super(message);
        this.code = ResultCodeEnum.A0431.value();
    }

    public ContainsContrabandSensitiveWordsException(Object data) {
        super(ResultCodeEnum.A0431.getName());
        this.code = ResultCodeEnum.A0431.value();
        this.data = data;
    }

    public ContainsContrabandSensitiveWordsException(String message, Object data) {
        super(message);
        this.code = ResultCodeEnum.A0431.value();
        this.data = data;
    }
}
