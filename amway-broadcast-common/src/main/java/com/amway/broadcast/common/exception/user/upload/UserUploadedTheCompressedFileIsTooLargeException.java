package com.amway.broadcast.common.exception.user.upload;

import com.amway.broadcast.common.enumeration.ResultCodeEnum;
import com.amway.broadcast.common.exception.BaseException;

/**
 *  用户上传压缩文件太大 A0705
 *
 * @author Lee
 * @version 1.0
 * @date 2020/5/8 17:27
 */
public class UserUploadedTheCompressedFileIsTooLargeException extends BaseException {

    public UserUploadedTheCompressedFileIsTooLargeException() {
        super(ResultCodeEnum.A0705.getName());
        this.code = ResultCodeEnum.A0705.value();
    }

    public UserUploadedTheCompressedFileIsTooLargeException(String message) {
        super(message);
        this.code = ResultCodeEnum.A0705.value();
    }

    public UserUploadedTheCompressedFileIsTooLargeException(Object data) {
        super(ResultCodeEnum.A0705.getName());
        this.code = ResultCodeEnum.A0705.value();
        this.data = data;
    }

    public UserUploadedTheCompressedFileIsTooLargeException(String message, Object data) {
        super(message);
        this.code = ResultCodeEnum.A0705.value();
        this.data = data;
    }
}
