package com.amway.broadcast.common.exception.remote.middle;

import com.amway.broadcast.common.enumeration.ResultCodeEnum;
import com.amway.broadcast.common.exception.BaseException;

/**
 *  中间件服务出错 C0100
 *
 * @author a good man
 * @version 1.0
 * @date 2020/5/9 9:28
 */
public class MiddlewareServiceErrorException extends BaseException {

    public MiddlewareServiceErrorException() {
        super(ResultCodeEnum.C0100.getName());
        this.code = ResultCodeEnum.C0100.value();
    }

    public MiddlewareServiceErrorException(String message) {
        super(message);
        this.code = ResultCodeEnum.C0100.value();
    }

    public MiddlewareServiceErrorException(Object data) {
        super(ResultCodeEnum.C0100.getName());
        this.code = ResultCodeEnum.C0100.value();
        this.data = data;
    }

    public MiddlewareServiceErrorException(String message, Object data) {
        super(message);
        this.code = ResultCodeEnum.C0100.value();
        this.data = data;
    }
}
