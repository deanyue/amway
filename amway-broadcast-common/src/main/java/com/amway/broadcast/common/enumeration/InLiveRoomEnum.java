package com.amway.broadcast.common.enumeration;


import lombok.extern.slf4j.Slf4j;

/**
 *
 *
 * @author dean
 */
@Slf4j
public enum InLiveRoomEnum {
    /**
     *
     */
    NO_IN_LIVEROOM("未进入直播间", "0"),
    /**
     *
     */
    IN_LIVEROOM("已进入直播间", "1"),

    ;

    private String desc;

    private String code;

    InLiveRoomEnum(String desc, String code) {
        this.desc = desc;
        this.code = code;
    }

    public String getDesc() {
        return desc;
    }

    public String getCode() {
        return code;
    }

}
