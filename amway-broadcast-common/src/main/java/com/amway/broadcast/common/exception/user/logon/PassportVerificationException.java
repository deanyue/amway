package com.amway.broadcast.common.exception.user.logon;

import com.amway.broadcast.common.enumeration.ResultCodeEnum;
import com.amway.broadcast.common.exception.BaseException;

/**
 * 护照编号校验非法 A0143
 *
 * @author Lee
 */
public class PassportVerificationException extends BaseException {

    public PassportVerificationException() {
        super(ResultCodeEnum.A0143.getName());
        this.code = ResultCodeEnum.A0143.value();
    }

    public PassportVerificationException(String message) {
        super(message);
        this.code = ResultCodeEnum.A0143.value();
    }

    public PassportVerificationException(Object data) {
        super(ResultCodeEnum.A0143.getName());
        this.code = ResultCodeEnum.A0143.value();
        this.data = data;
    }

    public PassportVerificationException(String message, Object data) {
        super(message);
        this.code = ResultCodeEnum.A0143.value();
        this.data = data;
    }
}
