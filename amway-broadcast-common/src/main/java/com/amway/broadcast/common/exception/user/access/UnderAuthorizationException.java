package com.amway.broadcast.common.exception.user.access;

import com.amway.broadcast.common.enumeration.ResultCodeEnum;
import com.amway.broadcast.common.exception.BaseException;

/**
 *  正在授权中 A0302
 *
 * @author Lee
 * @version 1.0
 * @date 2020/5/8 14:56
 */
public class UnderAuthorizationException extends BaseException {

    public UnderAuthorizationException() {
        super(ResultCodeEnum.A0302.getName());
        this.code = ResultCodeEnum.A0302.value();
    }

    public UnderAuthorizationException(String message) {
        super(message);
        this.code = ResultCodeEnum.A0302.value();
    }

    public UnderAuthorizationException(Object data) {
        super(ResultCodeEnum.A0302.getName());
        this.code = ResultCodeEnum.A0302.value();
        this.data = data;
    }

    public UnderAuthorizationException(String message, Object data) {
        super(message);
        this.code = ResultCodeEnum.A0302.value();
        this.data = data;
    }
}
