package com.amway.broadcast.common.exception.user.privacy;

import com.amway.broadcast.common.enumeration.ResultCodeEnum;
import com.amway.broadcast.common.exception.BaseException;

/**
 *  用户相机未授权 A0903
 *
 * @author Lee
 * @version 1.0
 * @date 2020/5/8 17:51
 */
public class UserCameraPhotoIsNotAuthorizedException extends BaseException {

    public UserCameraPhotoIsNotAuthorizedException() {
        super(ResultCodeEnum.A0903.getName());
        this.code = ResultCodeEnum.A0903.value();
    }

    public UserCameraPhotoIsNotAuthorizedException(String message) {
        super(message);
        this.code = ResultCodeEnum.A0903.value();
    }

    public UserCameraPhotoIsNotAuthorizedException(Object data) {
        super(ResultCodeEnum.A0903.getName());
        this.code = ResultCodeEnum.A0903.value();
        this.data = data;
    }

    public UserCameraPhotoIsNotAuthorizedException(String message, Object data) {
        super(message);
        this.code = ResultCodeEnum.A0903.value();
        this.data = data;
    }
}
