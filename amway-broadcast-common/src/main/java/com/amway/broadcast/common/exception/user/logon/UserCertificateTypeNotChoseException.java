package com.amway.broadcast.common.exception.user.logon;

import com.amway.broadcast.common.enumeration.ResultCodeEnum;
import com.amway.broadcast.common.exception.BaseException;

/**
 * 用户证件类型未选择 A0141
 *
 * @author Lee
 */
public class UserCertificateTypeNotChoseException extends BaseException {

    public UserCertificateTypeNotChoseException() {
        super(ResultCodeEnum.A0141.getName());
        this.code = ResultCodeEnum.A0141.value();
    }

    public UserCertificateTypeNotChoseException(String message) {
        super(message);
        this.code = ResultCodeEnum.A0141.value();
    }

    public UserCertificateTypeNotChoseException(Object data) {
        super(ResultCodeEnum.A0141.getName());
        this.code = ResultCodeEnum.A0141.value();
        this.data = data;
    }

    public UserCertificateTypeNotChoseException(String message, Object data) {
        super(message);
        this.code = ResultCodeEnum.A0141.value();
        this.data = data;
    }
}
