package com.amway.broadcast.common.exception.user.access;

import com.amway.broadcast.common.enumeration.ResultCodeEnum;
import com.amway.broadcast.common.exception.BaseException;

/**
 *  服务已欠费 A0330
 *
 * @author Lee
 * @version 1.0
 * @date 2020/5/8 15:12
 */
public class ServiceIsOverdueException extends BaseException {

    public ServiceIsOverdueException() {
        super(ResultCodeEnum.A0330.getName());
        this.code = ResultCodeEnum.A0330.value();
    }

    public ServiceIsOverdueException(String message) {
        super(message);
        this.code = ResultCodeEnum.A0330.value();
    }

    public ServiceIsOverdueException(Object data) {
        super(ResultCodeEnum.A0330.getName());
        this.code = ResultCodeEnum.A0330.value();
        this.data = data;
    }

    public ServiceIsOverdueException(String message, Object data) {
        super(message);
        this.code = ResultCodeEnum.A0330.value();
        this.data = data;
    }
}
