package com.amway.broadcast.common.exception.system.disaster;

import com.amway.broadcast.common.enumeration.ResultCodeEnum;
import com.amway.broadcast.common.exception.BaseException;

/**
 *  系统限流 B0210
 *
 * @author Lee
 * @version 1.0
 * @date 2020/5/8 18:15
 */
public class SystemCurrentLimitingException extends BaseException {

    public SystemCurrentLimitingException() {
        super(ResultCodeEnum.B0210.getName());
        this.code = ResultCodeEnum.B0210.value();
    }

    public SystemCurrentLimitingException(String message) {
        super(message);
        this.code = ResultCodeEnum.B0210.value();
    }

    public SystemCurrentLimitingException(Object data) {
        super(ResultCodeEnum.B0210.getName());
        this.code = ResultCodeEnum.B0210.value();
        this.data = data;
    }

    public SystemCurrentLimitingException(String message, Object data) {
        super(message);
        this.code = ResultCodeEnum.B0210.value();
        this.data = data;
    }
}
