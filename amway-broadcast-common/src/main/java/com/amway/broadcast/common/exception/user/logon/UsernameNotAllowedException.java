package com.amway.broadcast.common.exception.user.logon;

import com.amway.broadcast.common.enumeration.ResultCodeEnum;
import com.amway.broadcast.common.exception.BaseException;

/**
 * 用户名包含敏感词 A0112
 *
 * @author Lee
 */
public class UsernameNotAllowedException extends BaseException {

    public UsernameNotAllowedException() {
        super(ResultCodeEnum.A0112.getName());
        this.code = ResultCodeEnum.A0112.value();
    }

    public UsernameNotAllowedException(String message) {
        super(message);
        this.code = ResultCodeEnum.A0112.value();
    }

    public UsernameNotAllowedException(Object data) {
        super(ResultCodeEnum.A0112.getName());
        this.code = ResultCodeEnum.A0112.value();
        this.data = data;
    }

    public UsernameNotAllowedException(String message, Object data) {
        super(message);
        this.code = ResultCodeEnum.A0112.value();
        this.data = data;
    }
}
