package com.amway.broadcast.common.exception.user.resources;

import com.amway.broadcast.common.enumeration.ResultCodeEnum;
import com.amway.broadcast.common.exception.BaseException;

/**
 *  账户余额不足 A0601
 *
 * @author Lee
 * @version 1.0
 * @date 2020/5/8 17:15
 */
public class InsufficientAccountBalanceException extends BaseException {

    public InsufficientAccountBalanceException() {
        super(ResultCodeEnum.A0601.getName());
        this.code = ResultCodeEnum.A0601.value();
    }

    public InsufficientAccountBalanceException(String message) {
        super(message);
        this.code = ResultCodeEnum.A0601.value();
    }

    public InsufficientAccountBalanceException(Object data) {
        super(ResultCodeEnum.A0601.getName());
        this.code = ResultCodeEnum.A0601.value();
        this.data = data;
    }

    public InsufficientAccountBalanceException(String message, Object data) {
        super(message);
        this.code = ResultCodeEnum.A0601.value();
        this.data = data;
    }
}
