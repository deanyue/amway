package com.amway.broadcast.common.exception.user.equipment;

import com.amway.broadcast.common.enumeration.ResultCodeEnum;
import com.amway.broadcast.common.exception.BaseException;

/**
 *  用户相机异常 A1001
 *
 * @author Lee
 * @version 1.0
 * @date 2020/5/8 17:59
 */
public class UserCameraAnomalyException extends BaseException {

    public UserCameraAnomalyException() {
        super(ResultCodeEnum.A1001.getName());
        this.code = ResultCodeEnum.A1001.value();
    }

    public UserCameraAnomalyException(String message) {
        super(message);
        this.code = ResultCodeEnum.A1001.value();
    }

    public UserCameraAnomalyException(Object data) {
        super(ResultCodeEnum.A1001.getName());
        this.code = ResultCodeEnum.A1001.value();
        this.data = data;
    }

    public UserCameraAnomalyException(String message, Object data) {
        super(message);
        this.code = ResultCodeEnum.A1001.value();
        this.data = data;
    }
}
