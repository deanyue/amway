package com.amway.broadcast.common.exception.system.resources;

import com.amway.broadcast.common.enumeration.ResultCodeEnum;
import com.amway.broadcast.common.exception.BaseException;

/**
 *  系统读取磁盘文件失败 B0321
 *
 * @author Lee
 * @version 1.0
 * @date 2020/5/9 9:20
 */
public class SystemFailedToReadTheDiskFileException extends BaseException {

    public SystemFailedToReadTheDiskFileException() {
        super(ResultCodeEnum.B0321.getName());
        this.code = ResultCodeEnum.B0321.value();
    }

    public SystemFailedToReadTheDiskFileException(String message) {
        super(message);
        this.code = ResultCodeEnum.B0321.value();
    }

    public SystemFailedToReadTheDiskFileException(Object data) {
        super(ResultCodeEnum.B0321.getName());
        this.code = ResultCodeEnum.B0321.value();
        this.data = data;
    }

    public SystemFailedToReadTheDiskFileException(String message, Object data) {
        super(message);
        this.code = ResultCodeEnum.B0321.value();
        this.data = data;
    }
}
