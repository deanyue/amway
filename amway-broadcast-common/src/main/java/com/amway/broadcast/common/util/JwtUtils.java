package com.amway.broadcast.common.util;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;

import java.util.Date;

/**
 * Jwt工具类
 *
 * @author Lee
 */
public class JwtUtils {

    private JwtUtils() {
    }

    public static final String HEADER_AUTH = "token";
    public static final String SESSION_KEY = "sessionKey";
    public static final String USER_ID_KEY = "userId";
    public static final String USER_NAME_KEY = "userName";
    public static final String ROLES_KEY = "roles";
    public static final String CORP_ID_KEY = "corpId";
    public static final String USER_TYPE = "userType";
    public static final Long expiration = 8 * 3600L;

    private static final String secret = "secret.wshoto";

    public static Claims getClaimsFromToken(String token) {
        Claims claims = Jwts.parser()
                .setSigningKey(secret)
                .parseClaimsJws(token)
                .getBody();
        return claims;
    }


    /**
     * 生成token
     *
     * @param id
     * @param userId
     * @param openId
     * @param sessionKey
     * @return
     */
    public static String generateToken(String id, Long userId, String openId, String roles, String sessionKey) {
        Date current = new Date();
        Claims claims = Jwts.claims()
                .setId(id)
                .setSubject(openId)
                .setIssuedAt(current)
                .setExpiration(generateExpirationDate(current));
        claims.put(ROLES_KEY, roles);
        claims.put(USER_ID_KEY, userId);
        claims.put(SESSION_KEY, sessionKey);
        return generateToken(claims);
    }


    /**
     * 运营平台生成token
     * @param
     * @return
     */
    public static String generateOperationToken(String userId, String userName,String roles) {
        Date current = new Date();
        Claims claims = Jwts.claims()
                .setIssuedAt(current)
                .setExpiration(generateExpirationDate(current));
        claims.put(USER_ID_KEY, userId);
        claims.put(USER_NAME_KEY, userName);
        claims.put(ROLES_KEY, roles);
        return generateToken(claims);
    }


    private static String generateToken(Claims claims) {
        return Jwts.builder()
                .setClaims(claims)
                .signWith(SignatureAlgorithm.HS512, secret)
                .compact();
    }

    /**
     * 生成token时间 = 当前时间 + expiration（properties中配置的失效时间）
     *
     * @return
     */
    private static Date generateExpirationDate(Date date) {
        return new Date(date.getTime() + expiration * 1000);
    }

    /**
     * 根据token获取用户名
     *
     * @param token
     * @return
     */
    public static String getOpenIdFromToken(String token) {
        String openId = null;
        final Claims claims = getClaimsFromToken(token);
        openId = claims.getSubject();
        return openId;
    }

    public static Long getUserIdFromToken(String token) {
        Long userId = null;
        final Claims claims = getClaimsFromToken(token);
        userId = claims.get(USER_ID_KEY, Long.class);
        return userId;
    }

    public static String getSessionKey(String token) {
        String sessionKey = null;
        final Claims claims = getClaimsFromToken(token);
        sessionKey = claims.get(SESSION_KEY, String.class);
        return sessionKey;
    }

    public static String getRolesFromToken(String token) {
        String role = null;
        final Claims claims = getClaimsFromToken(token);
        role = claims.get(ROLES_KEY, String.class);
        return role;
    }

    public static String getCorpIdFromToken(String token) {
        String corpId = null;
        final Claims claims = getClaimsFromToken(token);
        corpId = claims.get(CORP_ID_KEY, String.class);
        return corpId;
    }

    public static String getMerchantUserIdFromToken(String token) {
        String userId = null;
        final Claims claims = getClaimsFromToken(token);
        userId = claims.get(USER_ID_KEY, String.class);
        return userId;
    }

    public static String getUserNameFromToken(String token) {
        String userName = null;
        final Claims claims = getClaimsFromToken(token);
        userName = claims.get(USER_NAME_KEY, String.class);
        return userName;
    }

    public static String getUserTypeFromToken(String token) {
        String userType = null;
        final Claims claims = getClaimsFromToken(token);
        userType = claims.get(USER_TYPE, String.class);
        return userType;
    }

    /**
     * 判断token失效时间是否到了
     *
     * @param token
     * @return
     */
    private static Boolean isTokenExpired(String token) {
        final Date expiration = getExpirationDateFromToken(token);
        return expiration.before(new Date());
    }

    /**
     * 获取设置的token失效时间
     *
     * @param token
     * @return
     */
    public static Date getExpirationDateFromToken(String token) {
        Date expiration = null;
        final Claims claims = getClaimsFromToken(token);
        expiration = claims.getExpiration();
        return expiration;
    }

    /**
     * Token失效校验
     *
     * @param token token字符串
     * @return
     */
    public static Boolean verify(String token) {
        return !isTokenExpired(token);
    }

    /**
     * 刷新Token失效期
     *
     * @param token
     * @return
     */
    public static String refreshToken(String token) {
        final Claims claims = getClaimsFromToken(token);
        claims.setExpiration(generateExpirationDate(new Date()));
        return generateToken(claims);
    }
    /**
     * 此方法仅generateOperationToken方法生产的token有效
     *
     * 自动根据时间判断决定是否刷新token
     * 并返回true刷新成功，false刷新失败
     * true后要在token切面中刷新redis
     * 以保持数据一致性
     *
     * 目前token有效期为八小时，设定在四个小时到八小时之间如果访问接口自动续时
     */
    public static Boolean autoRefreshToken(String token){
        Boolean refresh = false;
        //初始判断时间 四小时后
        Date refreshDate = new Date (getExpirationDateFromToken (token).getTime ()-expiration*500);
        //如果token未超过失效期并且在四小时后,刷新token时长
        if(verify (token) && refreshDate.before (new Date ())){
            refresh = true;
            refreshToken(token);
        }
        return refresh;
    }

    /**
     * 生成token时间 = 当前时间 + expiration（properties中配置的失效时间）
     *
     * @return
     */
    public static Date generateRedisDate() {
        Date date = new Date ();
        return new Date(date.getTime() + expiration * 1000);
    }

//    public static void main(String[] args) {
//        String token = JwtUtils.generateMerchantToken("1", "2", "3","ww8e09372aff8d9190");
//        System.out.println(token);
//        System.out.println(JwtUtils.getMerchantUserIdFromToken(token));
//    }
}
