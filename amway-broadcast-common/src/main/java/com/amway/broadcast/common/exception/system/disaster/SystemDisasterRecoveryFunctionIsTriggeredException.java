package com.amway.broadcast.common.exception.system.disaster;

import com.amway.broadcast.common.enumeration.ResultCodeEnum;
import com.amway.broadcast.common.exception.BaseException;

/**
 *  系统容灾功能被触发 B0200
 *
 * @author Lee
 * @version 1.0
 * @date 2020/5/8 18:14
 */
public class SystemDisasterRecoveryFunctionIsTriggeredException extends BaseException {

    public SystemDisasterRecoveryFunctionIsTriggeredException() {
        super(ResultCodeEnum.B0200.getName());
        this.code = ResultCodeEnum.B0200.value();
    }

    public SystemDisasterRecoveryFunctionIsTriggeredException(String message) {
        super(message);
        this.code = ResultCodeEnum.B0200.value();
    }

    public SystemDisasterRecoveryFunctionIsTriggeredException(Object data) {
        super(ResultCodeEnum.B0200.getName());
        this.code = ResultCodeEnum.B0200.value();
        this.data = data;
    }

    public SystemDisasterRecoveryFunctionIsTriggeredException(String message, Object data) {
        super(message);
        this.code = ResultCodeEnum.B0200.value();
        this.data = data;
    }
}
