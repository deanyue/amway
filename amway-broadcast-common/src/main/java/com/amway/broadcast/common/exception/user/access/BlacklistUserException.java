package com.amway.broadcast.common.exception.user.access;

import com.amway.broadcast.common.enumeration.ResultCodeEnum;
import com.amway.broadcast.common.exception.BaseException;

/**
 *  黑名单用户 A0321
 *
 * @author Lee
 * @version 1.0
 * @date 2020/5/8 15:06
 */
public class BlacklistUserException extends BaseException {

    public BlacklistUserException() {
        super(ResultCodeEnum.A0321.getName());
        this.code = ResultCodeEnum.A0321.value();
    }

    public BlacklistUserException(String message) {
        super(message);
        this.code = ResultCodeEnum.A0321.value();
    }

    public BlacklistUserException(Object data) {
        super(ResultCodeEnum.A0321.getName());
        this.code = ResultCodeEnum.A0321.value();
        this.data = data;
    }

    public BlacklistUserException(String message, Object data) {
        super(message);
        this.code = ResultCodeEnum.A0321.value();
        this.data = data;
    }
}
