package com.amway.broadcast.common.exception.remote.timeout;

import com.amway.broadcast.common.enumeration.ResultCodeEnum;
import com.amway.broadcast.common.exception.BaseException;

/**
 *  消息投递超时 C0220
 *
 * @author Lee
 * @version 1.0
 * @date 2020/5/9 9:57
 */
public class MessageDeliveryTimeoutException extends BaseException {

    public MessageDeliveryTimeoutException() {
        super(ResultCodeEnum.C0220.getName());
        this.code = ResultCodeEnum.C0220.value();
    }

    public MessageDeliveryTimeoutException(String message) {
        super(message);
        this.code = ResultCodeEnum.C0220.value();
    }

    public MessageDeliveryTimeoutException(Object data) {
        super(ResultCodeEnum.C0220.getName());
        this.code = ResultCodeEnum.C0220.value();
        this.data = data;
    }

    public MessageDeliveryTimeoutException(String message, Object data) {
        super(message);
        this.code = ResultCodeEnum.C0220.value();
        this.data = data;
    }
}
