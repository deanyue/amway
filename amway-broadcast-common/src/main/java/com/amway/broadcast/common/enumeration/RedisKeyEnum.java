package com.amway.broadcast.common.enumeration;


import lombok.extern.slf4j.Slf4j;

/**
 * redis Key的前缀和超时时间
 *
 * @author dean
 */
@Slf4j
public enum RedisKeyEnum {
    /**
     * 部门用户同步锁
     */
    SYNC_USER_DEPARTMENT_LOCK("sync_user_department_lock", 60 * 60 * 2),
    /**
     * 用户缓存key
     */
    CACHE_USER("cache_user_", 60 * 60 * 2),
    ;

    private String prefix;

    private int expiredTime;

    RedisKeyEnum(String prefix, int expiredTime) {
        this.prefix = prefix;
        this.expiredTime = expiredTime;
    }

    public String getPrefix() {
        return prefix;
    }

    public int getExpiredTime() {
        return expiredTime;
    }

}
