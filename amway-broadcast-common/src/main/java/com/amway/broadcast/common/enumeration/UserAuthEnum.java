package com.amway.broadcast.common.enumeration;


import lombok.extern.slf4j.Slf4j;

/**
 *
 *
 * @author dean
 */
@Slf4j
public enum UserAuthEnum {
    /**
     *
     */
    IS_AUTH("已授权", 1L),
    /**
     *
     */
    NO_AUTH("未授权", 0L),
    ;

    private String desc;

    private Long code;

    UserAuthEnum(String desc, Long code) {
        this.desc = desc;
        this.code = code;
    }

    public String getDesc() {
        return desc;
    }

    public Long getCode() {
        return code;
    }

}
