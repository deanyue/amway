package com.amway.broadcast.common.exception.user.version;

import com.amway.broadcast.common.enumeration.ResultCodeEnum;
import com.amway.broadcast.common.exception.BaseException;

/**
 *  用户当前版本异常 A0800
 *
 * @author Lee
 * @version 1.0
 * @date 2020/5/8 17:31
 */
public class UserCurrentVersionException extends BaseException {

    public UserCurrentVersionException() {
        super(ResultCodeEnum.A0800.getName());
        this.code = ResultCodeEnum.A0800.value();
    }

    public UserCurrentVersionException(String message) {
        super(message);
        this.code = ResultCodeEnum.A0800.value();
    }

    public UserCurrentVersionException(Object data) {
        super(ResultCodeEnum.A0800.getName());
        this.code = ResultCodeEnum.A0800.value();
        this.data = data;
    }

    public UserCurrentVersionException(String message, Object data) {
        super(message);
        this.code = ResultCodeEnum.A0800.value();
        this.data = data;
    }
}
