package com.amway.broadcast.common.base;

import com.alibaba.fastjson.JSONObject;
import com.alibaba.fastjson.serializer.SerializerFeature;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

/**
 * 返回结果领域模型
 * @author Lee
 * @param <T>
 */
@Getter
@Setter
public class Result<T> implements Serializable {

    private String code;
    private String msg;
    private T data;

    public Result() {
    }

    @Builder
    public Result(String code, String msg, T data) {
        this.code = code;
        this.msg = msg;
        this.data = data;
    }
    public Result(String code, String msg) {
        this.code = code;
        this.msg = msg;
    }
    public Result(String code,T data) {
        this.code = code;
        this.data = data;
    }

    @Override
    public String toString() {
        return JSONObject.toJSONString(this, SerializerFeature.WriteMapNullValue, SerializerFeature.WriteNullListAsEmpty, SerializerFeature.WriteNonStringKeyAsString, SerializerFeature.WriteNonStringValueAsString);
    }
}
