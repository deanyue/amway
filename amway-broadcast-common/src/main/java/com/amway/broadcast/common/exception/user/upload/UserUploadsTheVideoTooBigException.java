package com.amway.broadcast.common.exception.user.upload;

import com.amway.broadcast.common.enumeration.ResultCodeEnum;
import com.amway.broadcast.common.exception.BaseException;

/**
 *  用户上传视频太大 A0704
 *
 * @author Lee
 * @version 1.0
 * @date 2020/5/8 17:26
 */
public class UserUploadsTheVideoTooBigException extends BaseException {

    public UserUploadsTheVideoTooBigException() {
        super(ResultCodeEnum.A0704.getName());
        this.code = ResultCodeEnum.A0704.value();
    }

    public UserUploadsTheVideoTooBigException(String message) {
        super(message);
        this.code = ResultCodeEnum.A0704.value();
    }

    public UserUploadsTheVideoTooBigException(Object data) {
        super(ResultCodeEnum.A0704.getName());
        this.code = ResultCodeEnum.A0704.value();
        this.data = data;
    }

    public UserUploadsTheVideoTooBigException(String message, Object data) {
        super(message);
        this.code = ResultCodeEnum.A0704.value();
        this.data = data;
    }
}
