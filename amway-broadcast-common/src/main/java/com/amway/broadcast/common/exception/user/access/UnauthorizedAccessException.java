package com.amway.broadcast.common.exception.user.access;

import com.amway.broadcast.common.enumeration.ResultCodeEnum;
import com.amway.broadcast.common.exception.BaseException;

/**
 *  访问未授权 A0301
 *
 * @author Lee
 * @version 1.0
 * @date 2020/5/8 14:55
 */
public class UnauthorizedAccessException extends BaseException {

    public UnauthorizedAccessException() {
        super(ResultCodeEnum.A0301.getName());
        this.code = ResultCodeEnum.A0301.value();
    }

    public UnauthorizedAccessException(String message) {
        super(message);
        this.code = ResultCodeEnum.A0301.value();
    }

    public UnauthorizedAccessException(Object data) {
        super(ResultCodeEnum.A0301.getName());
        this.code = ResultCodeEnum.A0301.value();
        this.data = data;
    }

    public UnauthorizedAccessException(String message, Object data) {
        super(message);
        this.code = ResultCodeEnum.A0301.value();
        this.data = data;
    }
}
