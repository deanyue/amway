package com.amway.broadcast.common.exception.user.access;

import com.amway.broadcast.common.enumeration.ResultCodeEnum;
import com.amway.broadcast.common.exception.BaseException;

/**
 *  RSA签名错误 A0341
 *
 * @author Lee
 * @version 1.0
 * @date 2020/5/8 15:13
 */
public class RsaSignatureErrorException extends BaseException {

    public RsaSignatureErrorException() {
        super(ResultCodeEnum.A0341.getName());
        this.code = ResultCodeEnum.A0341.value();
    }

    public RsaSignatureErrorException(String message) {
        super(message);
        this.code = ResultCodeEnum.A0341.value();
    }

    public RsaSignatureErrorException(Object data) {
        super(ResultCodeEnum.A0341.getName());
        this.code = ResultCodeEnum.A0341.value();
        this.data = data;
    }

    public RsaSignatureErrorException(String message, Object data) {
        super(message);
        this.code = ResultCodeEnum.A0341.value();
        this.data = data;
    }
}
