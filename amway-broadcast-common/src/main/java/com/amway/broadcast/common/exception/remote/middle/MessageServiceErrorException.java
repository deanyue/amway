package com.amway.broadcast.common.exception.remote.middle;

import com.amway.broadcast.common.enumeration.ResultCodeEnum;
import com.amway.broadcast.common.exception.BaseException;

/**
 *  消息服务出错 C0120
 *
 * @author Lee
 * @version 1.0
 * @date 2020/5/9 9:31
 */
public class MessageServiceErrorException extends BaseException {

    public MessageServiceErrorException() {
        super(ResultCodeEnum.C0120.getName());
        this.code = ResultCodeEnum.C0120.value();
    }

    public MessageServiceErrorException(String message) {
        super(message);
        this.code = ResultCodeEnum.C0120.value();
    }

    public MessageServiceErrorException(Object data) {
        super(ResultCodeEnum.C0120.getName());
        this.code = ResultCodeEnum.C0120.value();
        this.data = data;
    }

    public MessageServiceErrorException(String message, Object data) {
        super(message);
        this.code = ResultCodeEnum.C0120.value();
        this.data = data;
    }
}
