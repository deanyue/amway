package com.amway.broadcast.common.constant;

/**
 * 微信接口
 *
 * @author dean
 */
public class WXApiConstant {

    /**
     * 微信登录接口wxLogin
     */
    public static final String SNS_JSCODE2SESSION = "https://api.weixin.qq.com/sns/jscode2session?appid=";

    /**
     * 微信自动登录接口jscode2session
     */
    public static final String MINIPROGRAM_JSCODE2SESSION = "https://qyapi.weixin.qq.com/cgi-bin/miniprogram/jscode2session?access_token=";
    /**
     * 微信获取token
     */
    public static final String GET_TOKEN = "https://qyapi.weixin.qq.com/cgi-bin/gettoken?corpid=";
    /**
     * 获取部门数据
     */
    public static final String LIST_DEPARTMENT = "https://qyapi.weixin.qq.com/cgi-bin/department/list?access_token=";
    /**
     * 获取用户数据
     */
    public static final String LIST_USER = "https://qyapi.weixin.qq.com/cgi-bin/user/list?access_token=";
    /**
     * 获取appid token
     */
    public static final String APPID_TOKEN = "https://api.weixin.qq.com/cgi-bin/token?grant_type=client_credential&appid=";
    /**
     *
     */
    public static final String CREATE_ACTIVITY = "https://api.weixin.qq.com/cgi-bin/message/wxopen/activityid/create?access_token=";

    /**
     *
     */
    public static final String MSG_SEC_CHECK = "https://api.weixin.qq.com/wxa/msg_sec_check?access_token=";
    /**
     *
     */
    public static final String CODE_UN_LIMIT = "https://api.weixin.qq.com/wxa/getwxacodeunlimit?access_token=";
}
