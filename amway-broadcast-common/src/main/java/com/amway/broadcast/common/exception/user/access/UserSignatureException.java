package com.amway.broadcast.common.exception.user.access;

import com.amway.broadcast.common.enumeration.ResultCodeEnum;
import com.amway.broadcast.common.exception.BaseException;

/**
 *  用户签名异常 A0340
 *
 * @author Lee
 * @version 1.0
 * @date 2020/5/8 15:13
 */
public class UserSignatureException extends BaseException {

    public UserSignatureException() {
        super(ResultCodeEnum.A0340.getName());
        this.code = ResultCodeEnum.A0340.value();
    }

    public UserSignatureException(String message) {
        super(message);
        this.code = ResultCodeEnum.A0340.value();
    }

    public UserSignatureException(Object data) {
        super(ResultCodeEnum.A0340.getName());
        this.code = ResultCodeEnum.A0340.value();
        this.data = data;
    }

    public UserSignatureException(String message, Object data) {
        super(message);
        this.code = ResultCodeEnum.A0340.value();
        this.data = data;
    }
}
