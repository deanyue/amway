package com.amway.broadcast.common.exception;

import lombok.Getter;
import lombok.Setter;

/**
 * 异常处理类
 * 
 * @author Lee
 */
@Getter
@Setter
public class BaseException extends RuntimeException {

	private static final long serialVersionUID = 1L;

	/** 异常码 */
	protected String code;

	/**
	 * 异常内容
	 */
	protected Object data;

	public BaseException() {
		super();
	}

	public BaseException(String message) {
		super(message);
	}

}
