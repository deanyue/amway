package com.amway.broadcast.common.constant;

/**
 *
 *
 * @author dean
 */
public class HeadTokenConstant {

    /**
     * 微信登录接口token名称
     */
    public static final String WX_APP_API_TOKEN = "wxappapitoken";


}
