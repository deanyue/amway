package com.amway.broadcast.common.exception.user;

import com.amway.broadcast.common.enumeration.ResultCodeEnum;
import com.amway.broadcast.common.exception.BaseException;

/**
 * 一级宏观异常
 * 用户端错误 A0001
 *
 * @author Lee
 */
public class UserException extends BaseException {

    public UserException() {
        super(ResultCodeEnum.A0001.getName());
        this.code = ResultCodeEnum.A0001.value();
    }

    public UserException(String message) {
        super(message);
        this.code = ResultCodeEnum.A0001.value();
    }

    public UserException(Object data) {
        super(ResultCodeEnum.A0001.getName());
        this.code = ResultCodeEnum.A0001.value();
        this.data = data;
    }

    public UserException(String message, Object data) {
        super(message);
        this.code = ResultCodeEnum.A0001.value();
        this.data = data;
    }
}
