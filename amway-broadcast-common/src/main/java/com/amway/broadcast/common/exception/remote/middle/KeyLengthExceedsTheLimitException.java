package com.amway.broadcast.common.exception.remote.middle;

import com.amway.broadcast.common.enumeration.ResultCodeEnum;
import com.amway.broadcast.common.exception.BaseException;

/**
 *  key长度超过限制 C0131
 *
 * @author Lee
 * @version 1.0
 * @date 2020/5/9 9:35
 */
public class KeyLengthExceedsTheLimitException extends BaseException {

    public KeyLengthExceedsTheLimitException() {
        super(ResultCodeEnum.C0131.getName());
        this.code = ResultCodeEnum.C0131.value();
    }

    public KeyLengthExceedsTheLimitException(String message) {
        super(message);
        this.code = ResultCodeEnum.C0131.value();
    }

    public KeyLengthExceedsTheLimitException(Object data) {
        super(ResultCodeEnum.C0131.getName());
        this.code = ResultCodeEnum.C0131.value();
        this.data = data;
    }

    public KeyLengthExceedsTheLimitException(String message, Object data) {
        super(message);
        this.code = ResultCodeEnum.C0131.value();
        this.data = data;
    }
}
