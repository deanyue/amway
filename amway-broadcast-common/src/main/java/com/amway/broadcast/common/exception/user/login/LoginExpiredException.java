package com.amway.broadcast.common.exception.user.login;

import com.amway.broadcast.common.enumeration.ResultCodeEnum;
import com.amway.broadcast.common.exception.BaseException;

/**
 * 用户登陆已过期 A0230
 *
 * @author Lee
 */
public class LoginExpiredException extends BaseException {

    public LoginExpiredException() {
        super(ResultCodeEnum.A0230.getName());
        this.code = ResultCodeEnum.A0230.value();
    }

    public LoginExpiredException(String message) {
        super(message);
        this.code = ResultCodeEnum.A0230.value();
    }

    public LoginExpiredException(Object data) {
        super(ResultCodeEnum.A0230.getName());
        this.code = ResultCodeEnum.A0230.value();
        this.data = data;
    }

    public LoginExpiredException(String message, Object data) {
        super(message);
        this.code = ResultCodeEnum.A0230.value();
        this.data = data;
    }
}
