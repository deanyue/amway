package com.amway.broadcast.common.exception.remote.timeout;

import com.amway.broadcast.common.enumeration.ResultCodeEnum;
import com.amway.broadcast.common.exception.BaseException;

/**
 *  缓存服务超时 C0230
 *
 * @author Lee
 * @version 1.0
 * @date 2020/5/9 9:57
 */
public class CacheServiceTimeoutException extends BaseException {

    public CacheServiceTimeoutException() {
        super(ResultCodeEnum.C0230.getName());
        this.code = ResultCodeEnum.C0230.value();
    }

    public CacheServiceTimeoutException(String message) {
        super(message);
        this.code = ResultCodeEnum.C0230.value();
    }

    public CacheServiceTimeoutException(Object data) {
        super(ResultCodeEnum.C0230.getName());
        this.code = ResultCodeEnum.C0230.value();
        this.data = data;
    }

    public CacheServiceTimeoutException(String message, Object data) {
        super(message);
        this.code = ResultCodeEnum.C0230.value();
        this.data = data;
    }
}
