package com.amway.broadcast.common.exception.user.equipment;

import com.amway.broadcast.common.enumeration.ResultCodeEnum;
import com.amway.broadcast.common.exception.BaseException;

/**
 *  用户扬声器异常 A1004
 *
 * @author Lee
 * @version 1.0
 * @date 2020/5/8 18:01
 */
public class UserSpeakerException extends BaseException {

    public UserSpeakerException() {
        super(ResultCodeEnum.A1004.getName());
        this.code = ResultCodeEnum.A1004.value();
    }

    public UserSpeakerException(String message) {
        super(message);
        this.code = ResultCodeEnum.A1004.value();
    }

    public UserSpeakerException(Object data) {
        super(ResultCodeEnum.A1004.getName());
        this.code = ResultCodeEnum.A1004.value();
        this.data = data;
    }

    public UserSpeakerException(String message, Object data) {
        super(message);
        this.code = ResultCodeEnum.A1004.value();
        this.data = data;
    }
}
