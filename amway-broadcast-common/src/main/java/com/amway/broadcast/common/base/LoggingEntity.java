package com.amway.broadcast.common.base;

import lombok.Data;

import java.util.Date;

/**
 * @author liujch
 * @description 日志实体
 * @Package com.amway.broadcast.common.base
 * @date 2020年12月11日 14:10
 */
@Data
public class LoggingEntity {

    private String applicationName;

    /**
     * 客户端ip
     */
    private String clientIpAdd;

    /**
     * 日志操作名称
     */
    private String operationName;

    /**
     * 开始时间
     */
    private Date startTime;

    /**
     * 执行时间
     */
    private long executionTime;

    /**
     * 方法名
     */
    private String executionClassName;

    /**
     * 方法名
     */
    private String executionMethodName;

    /**
     * 方法入参
     */
    private String methodParams;
}
