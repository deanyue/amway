package com.amway.broadcast.common.exception.user.logon;

import com.amway.broadcast.common.enumeration.ResultCodeEnum;
import com.amway.broadcast.common.exception.BaseException;

/**
 * 校验码输入错误 A0130
 *
 * @author Lee
 */
public class VerificationLogonCodeException extends BaseException {

    public VerificationLogonCodeException() {
        super(ResultCodeEnum.A0130.getName());
        this.code = ResultCodeEnum.A0130.value();
    }

    public VerificationLogonCodeException(String message) {
        super(message);
        this.code = ResultCodeEnum.A0130.value();
    }

    public VerificationLogonCodeException(Object data) {
        super(ResultCodeEnum.A0130.getName());
        this.code = ResultCodeEnum.A0130.value();
        this.data = data;
    }

    public VerificationLogonCodeException(String message, Object data) {
        super(message);
        this.code = ResultCodeEnum.A0130.value();
        this.data = data;
    }
}
