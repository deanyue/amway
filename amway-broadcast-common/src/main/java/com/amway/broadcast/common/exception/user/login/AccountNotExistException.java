package com.amway.broadcast.common.exception.user.login;

import com.amway.broadcast.common.enumeration.ResultCodeEnum;
import com.amway.broadcast.common.exception.BaseException;

/**
 * 用户账户不存在 A0201
 *
 * @author Lee
 */
public class AccountNotExistException extends BaseException {

    public AccountNotExistException() {
        super(ResultCodeEnum.A0201.getName());
        this.code = ResultCodeEnum.A0201.value();
    }

    public AccountNotExistException(String message) {
        super(message);
        this.code = ResultCodeEnum.A0201.value();
    }

    public AccountNotExistException(Object data) {
        super(ResultCodeEnum.A0201.getName());
        this.code = ResultCodeEnum.A0201.value();
        this.data = data;
    }

    public AccountNotExistException(String message, Object data) {
        super(message);
        this.code = ResultCodeEnum.A0201.value();
        this.data = data;
    }
}
