package com.amway.broadcast.common.exception.user.parm;

import com.amway.broadcast.common.enumeration.ResultCodeEnum;
import com.amway.broadcast.common.exception.BaseException;

/**
 *  缺少时间戳参数 A0413
 *
 * @author Lee
 * @version 1.0
 * @date 2020/5/8 16:06
 */
public class MissingTimestampParameterException extends BaseException {

    public MissingTimestampParameterException() {
        super(ResultCodeEnum.A0413.getName());
        this.code = ResultCodeEnum.A0413.value();
    }

    public MissingTimestampParameterException(String message) {
        super(message);
        this.code = ResultCodeEnum.A0413.value();
    }

    public MissingTimestampParameterException(Object data) {
        super(ResultCodeEnum.A0413.getName());
        this.code = ResultCodeEnum.A0413.value();
        this.data = data;
    }

    public MissingTimestampParameterException(String message, Object data) {
        super(message);
        this.code = ResultCodeEnum.A0413.value();
        this.data = data;
    }
}
