package com.amway.broadcast.common.exception.user.parm;

import com.amway.broadcast.common.enumeration.ResultCodeEnum;
import com.amway.broadcast.common.exception.BaseException;

/**
 *  订购数量为空 A0412
 *
 * @author Lee
 * @version 1.0
 * @date 2020/5/8 16:06
 */
public class OrderQuantityIsEmptyException extends BaseException {

    public OrderQuantityIsEmptyException() {
        super(ResultCodeEnum.A0412.getName());
        this.code = ResultCodeEnum.A0412.value();
    }

    public OrderQuantityIsEmptyException(String message) {
        super(message);
        this.code = ResultCodeEnum.A0412.value();
    }

    public OrderQuantityIsEmptyException(Object data) {
        super(ResultCodeEnum.A0412.getName());
        this.code = ResultCodeEnum.A0412.value();
        this.data = data;
    }

    public OrderQuantityIsEmptyException(String message, Object data) {
        super(message);
        this.code = ResultCodeEnum.A0412.value();
        this.data = data;
    }
}
