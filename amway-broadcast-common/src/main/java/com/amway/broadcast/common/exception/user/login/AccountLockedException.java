package com.amway.broadcast.common.exception.user.login;

import com.amway.broadcast.common.enumeration.ResultCodeEnum;
import com.amway.broadcast.common.exception.BaseException;

/**
 * 用户账户被冻结 A0202
 *
 * @author Lee
 */
public class AccountLockedException extends BaseException {

    public AccountLockedException() {
        super(ResultCodeEnum.A0202.getName());
        this.code = ResultCodeEnum.A0202.value();
    }

    public AccountLockedException(String message) {
        super(message);
        this.code = ResultCodeEnum.A0202.value();
    }

    public AccountLockedException(Object data) {
        super(ResultCodeEnum.A0202.getName());
        this.code = ResultCodeEnum.A0202.value();
        this.data = data;
    }

    public AccountLockedException(String message, Object data) {
        super(message);
        this.code = ResultCodeEnum.A0202.value();
        this.data = data;
    }
}
