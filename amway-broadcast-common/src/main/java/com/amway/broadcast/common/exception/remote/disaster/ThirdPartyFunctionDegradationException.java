package com.amway.broadcast.common.exception.remote.disaster;

import com.amway.broadcast.common.enumeration.ResultCodeEnum;
import com.amway.broadcast.common.exception.BaseException;

/**
 *  第三方功能降级 C0402
 *
 * @author Lee
 * @version 1.0
 * @date 2020/5/9 10:11
 */
public class ThirdPartyFunctionDegradationException extends BaseException {

    public ThirdPartyFunctionDegradationException() {
        super(ResultCodeEnum.C0402.getName());
        this.code = ResultCodeEnum.C0402.value();
    }

    public ThirdPartyFunctionDegradationException(String message) {
        super(message);
        this.code = ResultCodeEnum.C0402.value();
    }

    public ThirdPartyFunctionDegradationException(Object data) {
        super(ResultCodeEnum.C0402.getName());
        this.code = ResultCodeEnum.C0402.value();
        this.data = data;
    }

    public ThirdPartyFunctionDegradationException(String message, Object data) {
        super(message);
        this.code = ResultCodeEnum.C0402.value();
        this.data = data;
    }
}
