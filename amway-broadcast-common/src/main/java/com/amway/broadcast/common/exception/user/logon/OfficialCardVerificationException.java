package com.amway.broadcast.common.exception.user.logon;

import com.amway.broadcast.common.enumeration.ResultCodeEnum;
import com.amway.broadcast.common.exception.BaseException;

/**
 * 军官证编号校验非法 A0144
 *
 * @author Lee
 */
public class OfficialCardVerificationException extends BaseException {

    public OfficialCardVerificationException() {
        super(ResultCodeEnum.A0144.getName());
        this.code = ResultCodeEnum.A0144.value();
    }

    public OfficialCardVerificationException(String message) {
        super(message);
        this.code = ResultCodeEnum.A0144.value();
    }

    public OfficialCardVerificationException(Object data) {
        super(ResultCodeEnum.A0144.getName());
        this.code = ResultCodeEnum.A0144.value();
        this.data = data;
    }

    public OfficialCardVerificationException(String message, Object data) {
        super(message);
        this.code = ResultCodeEnum.A0144.value();
        this.data = data;
    }
}
