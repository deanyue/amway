package com.amway.broadcast.common.exception.remote.database;

import com.amway.broadcast.common.enumeration.ResultCodeEnum;
import com.amway.broadcast.common.exception.BaseException;

/**
 *  表不存在 C0311
 *
 * @author Lee
 * @version 1.0
 * @date 2020/5/9 10:03
 */
public class TableDoesNotExistException extends BaseException {

    public TableDoesNotExistException() {
        super(ResultCodeEnum.C0311.getName());
        this.code = ResultCodeEnum.C0311.value();
    }

    public TableDoesNotExistException(String message) {
        super(message);
        this.code = ResultCodeEnum.C0311.value();
    }

    public TableDoesNotExistException(Object data) {
        super(ResultCodeEnum.C0311.getName());
        this.code = ResultCodeEnum.C0311.value();
        this.data = data;
    }

    public TableDoesNotExistException(String message, Object data) {
        super(message);
        this.code = ResultCodeEnum.C0311.value();
        this.data = data;
    }
}
