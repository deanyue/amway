package com.amway.broadcast.common.constant;

/**
 * 缓存名称
 *
 * @author Lee
 */
public class CacheName {

    /**
     * 默认缓存
     */
    public static final String DEFALUT_CACHE = "defalutCache";

    public static final String AUTHENTICATION_CACHE = "authenticationCache";

    public static final String AUTHORIZATION_CACHE = "authorizationCache";

}
