package com.amway.broadcast.common.exception.remote.notice;

import com.amway.broadcast.common.enumeration.ResultCodeEnum;
import com.amway.broadcast.common.exception.BaseException;

/**
 *  短信提醒服务失败 C0501
 *
 * @author Lee
 * @version 1.0
 * @date 2020/5/9 10:14
 */
public class SmsAlertServiceFailedException extends BaseException {

    public SmsAlertServiceFailedException() {
        super(ResultCodeEnum.C0501.getName());
        this.code = ResultCodeEnum.C0501.value();
    }

    public SmsAlertServiceFailedException(String message) {
        super(message);
        this.code = ResultCodeEnum.C0501.value();
    }

    public SmsAlertServiceFailedException(Object data) {
        super(ResultCodeEnum.C0501.getName());
        this.code = ResultCodeEnum.C0501.value();
        this.data = data;
    }

    public SmsAlertServiceFailedException(String message, Object data) {
        super(message);
        this.code = ResultCodeEnum.C0501.value();
        this.data = data;
    }
}
