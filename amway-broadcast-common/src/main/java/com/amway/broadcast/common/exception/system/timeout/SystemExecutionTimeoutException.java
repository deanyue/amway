package com.amway.broadcast.common.exception.system.timeout;

import com.amway.broadcast.common.enumeration.ResultCodeEnum;
import com.amway.broadcast.common.exception.BaseException;

/**
 *  系统执行超时 B0100
 *
 * @author Lee
 * @version 1.0
 * @date 2020/5/8 18:09
 */
public class SystemExecutionTimeoutException extends BaseException {

    public SystemExecutionTimeoutException() {
        super(ResultCodeEnum.B0100.getName());
        this.code = ResultCodeEnum.B0100.value();
    }

    public SystemExecutionTimeoutException(String message) {
        super(message);
        this.code = ResultCodeEnum.B0100.value();
    }

    public SystemExecutionTimeoutException(Object data) {
        super(ResultCodeEnum.B0100.getName());
        this.code = ResultCodeEnum.B0100.value();
        this.data = data;
    }

    public SystemExecutionTimeoutException(String message, Object data) {
        super(message);
        this.code = ResultCodeEnum.B0100.value();
        this.data = data;
    }
}
