package com.amway.broadcast.common.exception.user.request;

import com.amway.broadcast.common.enumeration.ResultCodeEnum;
import com.amway.broadcast.common.exception.BaseException;

/**
 *  用户操作请等待 A0503
 *
 * @author Lee
 * @version 1.0
 * @date 2020/5/8 17:03
 */
public class PleaseWaitForUserActionException extends BaseException {

    public PleaseWaitForUserActionException() {
        super(ResultCodeEnum.A0503.getName());
        this.code = ResultCodeEnum.A0503.value();
    }

    public PleaseWaitForUserActionException(String message) {
        super(message);
        this.code = ResultCodeEnum.A0503.value();
    }

    public PleaseWaitForUserActionException(Object data) {
        super(ResultCodeEnum.A0503.getName());
        this.code = ResultCodeEnum.A0503.value();
        this.data = data;
    }

    public PleaseWaitForUserActionException(String message, Object data) {
        super(message);
        this.code = ResultCodeEnum.A0503.value();
        this.data = data;
    }
}
