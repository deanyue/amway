package com.amway.broadcast.common.exception.remote;

import com.amway.broadcast.common.enumeration.ResultCodeEnum;
import com.amway.broadcast.common.exception.BaseException;

/**
 *  调用第三方服务出错 C0001
 *
 * @author Lee
 * @version 1.0
 * @date 2020/5/9 9:26
 */
public class RemoteErrorException extends BaseException {

    public RemoteErrorException() {
        super(ResultCodeEnum.C0001.getName());
        this.code = ResultCodeEnum.C0001.value();
    }

    public RemoteErrorException(String message) {
        super(message);
        this.code = ResultCodeEnum.C0001.value();
    }

    public RemoteErrorException(Object data) {
        super(ResultCodeEnum.C0001.getName());
        this.code = ResultCodeEnum.C0001.value();
        this.data = data;
    }

    public RemoteErrorException(String message, Object data) {
        super(message);
        this.code = ResultCodeEnum.C0001.value();
        this.data = data;
    }
}
