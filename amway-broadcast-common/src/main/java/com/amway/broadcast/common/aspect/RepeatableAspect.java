package com.amway.broadcast.common.aspect;

import com.alibaba.fastjson.JSON;
import com.amway.broadcast.common.annotation.NonRepeatSubmit;
import com.amway.broadcast.common.exception.user.request.UserRepeatRequestException;
import com.amway.broadcast.common.util.MD5Utils;
import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.aspectj.lang.reflect.MethodSignature;
import org.redisson.api.RLock;
import org.redisson.api.RedissonClient;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.lang.reflect.Method;
import java.util.concurrent.TimeUnit;


@Slf4j
@Aspect
@Component
public class RepeatableAspect {

    @Resource
    private RedissonClient redissonClient;

    @Pointcut("@annotation(com.amway.broadcast.common.annotation.NonRepeatSubmit)")
    private void pointCut() {

    }

    @Around("pointCut()")
    public Object aroundCheck(ProceedingJoinPoint pjp) throws Throwable {
        String className = pjp.getTarget().getClass().getName();
        MethodSignature methodSignature = (MethodSignature)pjp.getSignature();
        String methodName = methodSignature.getName();
        Method method = methodSignature.getMethod();
        NonRepeatSubmit annotation = method.getAnnotation(NonRepeatSubmit.class);
        Object[] args = pjp.getArgs();
        String paramsJsonStr = JSON.toJSONString(args);
        String srcStr = className + "_" + methodName + "_" + paramsJsonStr;
        String signStr = MD5Utils.string2MD5(srcStr);
        log.info("防止重复提交的请求 Redis Key：" + signStr);
        //同一个用户当前方法未执行完之前不允许再次请求
        RLock lock = redissonClient.getLock(signStr);
        if (lock.isLocked()) {
            throw new UserRepeatRequestException("当前正在被其他人员编辑,请确认后尝试");
        }
        try {
            boolean locked = lock.tryLock(annotation.keepSeconds(), 10, TimeUnit.SECONDS);
            if (!locked) {
                throw new UserRepeatRequestException("当前正在被其他人员编辑,请确认后尝试");
            }
            return pjp.proceed();
        } finally {
            if (lock.isHeldByCurrentThread()) {
                lock.unlock();
            }
        }
    }
}
