package com.amway.broadcast.common.exception.remote.middle;

import com.amway.broadcast.common.enumeration.ResultCodeEnum;
import com.amway.broadcast.common.exception.BaseException;

/**
 *  value长度超过限制 C0132
 *
 * @author Lee
 * @version 1.0
 * @date 2020/5/9 9:36
 */
public class ValueLengthExceedsTheLimitException extends BaseException {

    public ValueLengthExceedsTheLimitException() {
        super(ResultCodeEnum.C0132.getName());
        this.code = ResultCodeEnum.C0132.value();
    }

    public ValueLengthExceedsTheLimitException(String message) {
        super(message);
        this.code = ResultCodeEnum.C0132.value();
    }

    public ValueLengthExceedsTheLimitException(Object data) {
        super(ResultCodeEnum.C0132.getName());
        this.code = ResultCodeEnum.C0132.value();
        this.data = data;
    }

    public ValueLengthExceedsTheLimitException(String message, Object data) {
        super(message);
        this.code = ResultCodeEnum.C0132.value();
        this.data = data;
    }
}
