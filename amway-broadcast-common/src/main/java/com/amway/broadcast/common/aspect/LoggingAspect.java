package com.amway.broadcast.common.aspect;

import com.alibaba.fastjson.JSONObject;
import com.amway.broadcast.common.annotation.Logging;
import com.amway.broadcast.common.base.LoggingEntity;
import com.amway.broadcast.common.util.IpUtils;
import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.aspectj.lang.reflect.MethodSignature;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.web.context.request.RequestAttributes;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletRequest;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.Map;

/**
 * @author liujch
 * @description
 * @Package com.amway.broadcast.common.aspect
 * @date 2020年12月12日 13:52
 */
@Aspect
@Component
@Slf4j
public class LoggingAspect {

    /**
     * 模块名称
     */
    @Value("${spring.application.name}")
    private String applicationName;

    @Pointcut(value = "@annotation(com.amway.broadcast.common.annotation.Logging)")
    public void loggingCut() {

    }

    @Around("loggingCut()")
    public Object dealWithLogging(ProceedingJoinPoint pjp) throws Throwable {
        String remoteIp = "";
        LoggingEntity loggingEntity = new LoggingEntity();
        Date startTime = new Date();
        try {
            RequestAttributes requestAttributes = RequestContextHolder.getRequestAttributes();
            if (requestAttributes != null) {
                HttpServletRequest request = ((ServletRequestAttributes)requestAttributes).getRequest();
                IpUtils.getIpAddress(request);
                log.info("获取的ip地址={}",remoteIp);
            }
            Logging logging = pjp.getTarget().getClass().getDeclaredAnnotation(Logging.class);
            loggingEntity.setClientIpAdd(remoteIp);
            loggingEntity.setApplicationName(applicationName);
            loggingEntity.setOperationName(logging.name());

            MethodSignature methodSignature = (MethodSignature)pjp.getSignature();
            loggingEntity.setExecutionClassName(methodSignature.getDeclaringTypeName());
            loggingEntity.setExecutionMethodName(methodSignature.getName());
            loggingEntity.setMethodParams(JSONObject.toJSONString(getMethodParams(methodSignature, pjp.getArgs())));
            loggingEntity.setStartTime(startTime);
        } catch (Exception e) {
            log.error("日志实体填充属性异常",e);
        }

        Object result = pjp.proceed();

        loggingEntity.setExecutionTime(System.currentTimeMillis() - startTime.getTime());
        // todo 后期修改为异步存入es
        log.info("日志切面输出{}",loggingEntity);
        return result;
    }

    private Map<String, Object> getMethodParams(MethodSignature methodSignature, Object[] args) {
        Map<String, Object> params = new LinkedHashMap<>();
        String[] paramNames = methodSignature.getParameterNames();
        for (int i = 0; i < paramNames.length; i++) {
            params.put(paramNames[i], args[i]);
        }
        return params;
    }
}
