package com.amway.broadcast.common.exception.user.access;

import com.amway.broadcast.common.enumeration.ResultCodeEnum;
import com.amway.broadcast.common.exception.BaseException;

/**
 *  访问权限异常 A0300
 *
 * @author Lee
 * @version 1.0
 * @date 2020/5/8 14:54
 */
public class AccessException extends BaseException {

    public AccessException() {
        super(ResultCodeEnum.A0300.getName());
        this.code = ResultCodeEnum.A0300.value();
    }

    public AccessException(String message) {
        super(message);
        this.code = ResultCodeEnum.A0300.value();
    }

    public AccessException(Object data) {
        super(ResultCodeEnum.A0300.getName());
        this.code = ResultCodeEnum.A0300.value();
        this.data = data;
    }

    public AccessException(String message, Object data) {
        super(message);
        this.code = ResultCodeEnum.A0300.value();
        this.data = data;
    }
}
