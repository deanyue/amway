package com.amway.broadcast.common.exception.user.parm;

import com.amway.broadcast.common.enumeration.ResultCodeEnum;
import com.amway.broadcast.common.exception.BaseException;

/**
 *  无效的用户输入 A0402
 *
 * @author Lee
 * @version 1.0
 * @date 2020/5/8 16:03
 */
public class InvalidUserInputException extends BaseException {

    public InvalidUserInputException() {
        super(ResultCodeEnum.A0402.getName());
        this.code = ResultCodeEnum.A0402.value();
    }

    public InvalidUserInputException(String message) {
        super(message);
        this.code = ResultCodeEnum.A0402.value();
    }

    public InvalidUserInputException(Object data) {
        super(ResultCodeEnum.A0402.getName());
        this.code = ResultCodeEnum.A0402.value();
        this.data = data;
    }

    public InvalidUserInputException(String message, Object data) {
        super(message);
        this.code = ResultCodeEnum.A0402.value();
        this.data = data;
    }
}
