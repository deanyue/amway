package com.amway.broadcast.common.exception.user.access;

import com.amway.broadcast.common.enumeration.ResultCodeEnum;
import com.amway.broadcast.common.exception.BaseException;

/**
 *  授权已过期 A0311
 *
 * @author Lee
 * @version 1.0
 * @date 2020/5/8 15:02
 */
public class AuthorizationExpiredException extends BaseException {

    public AuthorizationExpiredException() {
        super(ResultCodeEnum.A0311.getName());
        this.code = ResultCodeEnum.A0311.value();
    }

    public AuthorizationExpiredException(String message) {
        super(message);
        this.code = ResultCodeEnum.A0311.value();
    }

    public AuthorizationExpiredException(Object data) {
        super(ResultCodeEnum.A0311.getName());
        this.code = ResultCodeEnum.A0311.value();
        this.data = data;
    }

    public AuthorizationExpiredException(String message, Object data) {
        super(message);
        this.code = ResultCodeEnum.A0311.value();
        this.data = data;
    }
}
