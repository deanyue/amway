package com.amway.broadcast.common.exception.remote.middle;

import com.amway.broadcast.common.enumeration.ResultCodeEnum;
import com.amway.broadcast.common.exception.BaseException;

/**
 *  CDN服务出错 C0152
 *
 * @author Lee
 * @version 1.0
 * @date 2020/5/9 9:42
 */
public class CdnServiceErrorException extends BaseException {

    public CdnServiceErrorException() {
        super(ResultCodeEnum.C0152.getName());
        this.code = ResultCodeEnum.C0152.value();
    }

    public CdnServiceErrorException(String message) {
        super(message);
        this.code = ResultCodeEnum.C0152.value();
    }

    public CdnServiceErrorException(Object data) {
        super(ResultCodeEnum.C0152.getName());
        this.code = ResultCodeEnum.C0152.value();
        this.data = data;
    }

    public CdnServiceErrorException(String message, Object data) {
        super(message);
        this.code = ResultCodeEnum.C0152.value();
        this.data = data;
    }
}
