package com.amway.broadcast.common.exception.user.resources;

import com.amway.broadcast.common.enumeration.ResultCodeEnum;
import com.amway.broadcast.common.exception.BaseException;

/**
 *  用户资源异常 A0600
 *
 * @author Lee
 * @version 1.0
 * @date 2020/5/8 17:14
 */
public class UserResourceException extends BaseException {

    public UserResourceException() {
        super(ResultCodeEnum.A0600.getName());
        this.code = ResultCodeEnum.A0600.value();
    }

    public UserResourceException(String message) {
        super(message);
        this.code = ResultCodeEnum.A0600.value();
    }

    public UserResourceException(Object data) {
        super(ResultCodeEnum.A0600.getName());
        this.code = ResultCodeEnum.A0600.value();
        this.data = data;
    }

    public UserResourceException(String message, Object data) {
        super(message);
        this.code = ResultCodeEnum.A0600.value();
        this.data = data;
    }
}
