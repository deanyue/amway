package com.amway.broadcast.common.exception.system.resources;

import com.amway.broadcast.common.enumeration.ResultCodeEnum;
import com.amway.broadcast.common.exception.BaseException;

/**
 *  系统资源访问异常 B0320
 *
 * @author Lee
 * @version 1.0
 * @date 2020/5/9 9:19
 */
public class SystemResourceAccessException extends BaseException {

    public SystemResourceAccessException() {
        super(ResultCodeEnum.B0320.getName());
        this.code = ResultCodeEnum.B0320.value();
    }

    public SystemResourceAccessException(String message) {
        super(message);
        this.code = ResultCodeEnum.B0320.value();
    }

    public SystemResourceAccessException(Object data) {
        super(ResultCodeEnum.B0320.getName());
        this.code = ResultCodeEnum.B0320.value();
        this.data = data;
    }

    public SystemResourceAccessException(String message, Object data) {
        super(message);
        this.code = ResultCodeEnum.B0320.value();
        this.data = data;
    }
}
