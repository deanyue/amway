package com.amway.broadcast.common.exception.user.request;

import com.amway.broadcast.common.enumeration.ResultCodeEnum;
import com.amway.broadcast.common.exception.BaseException;

/**
 *  请求次数超出限制 A0501
 *
 * @author Lee
 * @version 1.0
 * @date 2020/5/8 17:01
 */
public class RequestsExceededTheLimitException extends BaseException {

    public RequestsExceededTheLimitException() {
        super(ResultCodeEnum.A0501.getName());
        this.code = ResultCodeEnum.A0501.value();
    }

    public RequestsExceededTheLimitException(String message) {
        super(message);
        this.code = ResultCodeEnum.A0501.value();
    }

    public RequestsExceededTheLimitException(Object data) {
        super(ResultCodeEnum.A0501.getName());
        this.code = ResultCodeEnum.A0501.value();
        this.data = data;
    }

    public RequestsExceededTheLimitException(String message, Object data) {
        super(message);
        this.code = ResultCodeEnum.A0501.value();
        this.data = data;
    }
}
