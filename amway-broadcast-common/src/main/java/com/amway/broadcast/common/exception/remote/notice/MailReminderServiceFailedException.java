package com.amway.broadcast.common.exception.remote.notice;

import com.amway.broadcast.common.enumeration.ResultCodeEnum;
import com.amway.broadcast.common.exception.BaseException;

/**
 *  邮件提醒服务失败 C0503
 *
 * @author Lee
 * @version 1.0
 * @date 2020/5/9 10:16
 */
public class MailReminderServiceFailedException extends BaseException {

    public MailReminderServiceFailedException() {
        super(ResultCodeEnum.C0503.getName());
        this.code = ResultCodeEnum.C0503.value();
    }

    public MailReminderServiceFailedException(String message) {
        super(message);
        this.code = ResultCodeEnum.C0503.value();
    }

    public MailReminderServiceFailedException(Object data) {
        super(ResultCodeEnum.C0503.getName());
        this.code = ResultCodeEnum.C0503.value();
        this.data = data;
    }

    public MailReminderServiceFailedException(String message, Object data) {
        super(message);
        this.code = ResultCodeEnum.C0503.value();
        this.data = data;
    }
}
