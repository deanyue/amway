package com.amway.broadcast.common.exception.user.parm;

import com.amway.broadcast.common.enumeration.ResultCodeEnum;
import com.amway.broadcast.common.exception.BaseException;

/**
 *  图片包含违禁信息 A0432
 *
 * @author Lee
 * @version 1.0
 * @date 2020/5/8 16:23
 */
public class PicturesContainProhibitedInformationException extends BaseException {

    public PicturesContainProhibitedInformationException() {
        super(ResultCodeEnum.A0432.getName());
        this.code = ResultCodeEnum.A0432.value();
    }

    public PicturesContainProhibitedInformationException(String message) {
        super(message);
        this.code = ResultCodeEnum.A0432.value();
    }

    public PicturesContainProhibitedInformationException(Object data) {
        super(ResultCodeEnum.A0432.getName());
        this.code = ResultCodeEnum.A0432.value();
        this.data = data;
    }

    public PicturesContainProhibitedInformationException(String message, Object data) {
        super(message);
        this.code = ResultCodeEnum.A0432.value();
        this.data = data;
    }
}
