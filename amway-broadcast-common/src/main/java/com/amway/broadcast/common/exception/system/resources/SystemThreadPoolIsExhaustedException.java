package com.amway.broadcast.common.exception.system.resources;

import com.amway.broadcast.common.enumeration.ResultCodeEnum;
import com.amway.broadcast.common.exception.BaseException;

/**
 *  系统线程池耗尽 B0315
 *
 * @author Lee
 * @version 1.0
 * @date 2020/5/9 9:19
 */
public class SystemThreadPoolIsExhaustedException extends BaseException {

    public SystemThreadPoolIsExhaustedException() {
        super(ResultCodeEnum.B0315.getName());
        this.code = ResultCodeEnum.B0315.value();
    }

    public SystemThreadPoolIsExhaustedException(String message) {
        super(message);
        this.code = ResultCodeEnum.B0315.value();
    }

    public SystemThreadPoolIsExhaustedException(Object data) {
        super(ResultCodeEnum.B0315.getName());
        this.code = ResultCodeEnum.B0315.value();
        this.data = data;
    }

    public SystemThreadPoolIsExhaustedException(String message, Object data) {
        super(message);
        this.code = ResultCodeEnum.B0315.value();
        this.data = data;
    }
}
