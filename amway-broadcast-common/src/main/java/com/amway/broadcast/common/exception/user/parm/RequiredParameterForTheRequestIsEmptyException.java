package com.amway.broadcast.common.exception.user.parm;

import com.amway.broadcast.common.enumeration.ResultCodeEnum;
import com.amway.broadcast.common.exception.BaseException;

/**
 *  请求必填参数为空 A0410
 *
 * @author Lee
 * @version 1.0
 * @date 2020/5/8 16:04
 */
public class RequiredParameterForTheRequestIsEmptyException extends BaseException {

    public RequiredParameterForTheRequestIsEmptyException() {
        super(ResultCodeEnum.A0410.getName());
        this.code = ResultCodeEnum.A0410.value();
    }

    public RequiredParameterForTheRequestIsEmptyException(String message) {
        super(message);
        this.code = ResultCodeEnum.A0410.value();
    }

    public RequiredParameterForTheRequestIsEmptyException(Object data) {
        super(ResultCodeEnum.A0410.getName());
        this.code = ResultCodeEnum.A0410.value();
        this.data = data;
    }

    public RequiredParameterForTheRequestIsEmptyException(String message, Object data) {
        super(message);
        this.code = ResultCodeEnum.A0410.value();
        this.data = data;
    }
}
