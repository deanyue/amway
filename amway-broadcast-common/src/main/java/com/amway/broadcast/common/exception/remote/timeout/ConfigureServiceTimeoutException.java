package com.amway.broadcast.common.exception.remote.timeout;

import com.amway.broadcast.common.enumeration.ResultCodeEnum;
import com.amway.broadcast.common.exception.BaseException;

/**
 *  配置服务超时 C0240
 *
 * @author Lee
 * @version 1.0
 * @date 2020/5/9 9:58
 */
public class ConfigureServiceTimeoutException extends BaseException {

    public ConfigureServiceTimeoutException() {
        super(ResultCodeEnum.C0240.getName());
        this.code = ResultCodeEnum.C0240.value();
    }

    public ConfigureServiceTimeoutException(String message) {
        super(message);
        this.code = ResultCodeEnum.C0240.value();
    }

    public ConfigureServiceTimeoutException(Object data) {
        super(ResultCodeEnum.C0240.getName());
        this.code = ResultCodeEnum.C0240.value();
        this.data = data;
    }

    public ConfigureServiceTimeoutException(String message, Object data) {
        super(message);
        this.code = ResultCodeEnum.C0240.value();
        this.data = data;
    }
}
