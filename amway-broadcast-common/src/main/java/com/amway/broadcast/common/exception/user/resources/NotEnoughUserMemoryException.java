package com.amway.broadcast.common.exception.user.resources;

import com.amway.broadcast.common.enumeration.ResultCodeEnum;
import com.amway.broadcast.common.exception.BaseException;

/**
 *  用户内存空间不足 A0603
 *
 * @author Lee
 * @version 1.0
 * @date 2020/5/8 17:16
 */
public class NotEnoughUserMemoryException extends BaseException {

    public NotEnoughUserMemoryException() {
        super(ResultCodeEnum.A0603.getName());
        this.code = ResultCodeEnum.A0603.value();
    }

    public NotEnoughUserMemoryException(String message) {
        super(message);
        this.code = ResultCodeEnum.A0603.value();
    }

    public NotEnoughUserMemoryException(Object data) {
        super(ResultCodeEnum.A0603.getName());
        this.code = ResultCodeEnum.A0603.value();
        this.data = data;
    }

    public NotEnoughUserMemoryException(String message, Object data) {
        super(message);
        this.code = ResultCodeEnum.A0603.value();
        this.data = data;
    }
}
