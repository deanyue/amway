package com.amway.broadcast.common.exception.user.parm;

import com.amway.broadcast.common.enumeration.ResultCodeEnum;
import com.amway.broadcast.common.exception.BaseException;

/**
 *  包含非法恶意跳转链接 A0401
 *
 * @author Lee
 * @version 1.0
 * @date 2020/5/8 16:02
 */
public class ContainsIllegalLinksException extends BaseException {

    public ContainsIllegalLinksException() {
        super(ResultCodeEnum.A0401.getName());
        this.code = ResultCodeEnum.A0401.value();
    }

    public ContainsIllegalLinksException(String message) {
        super(message);
        this.code = ResultCodeEnum.A0401.value();
    }

    public ContainsIllegalLinksException(Object data) {
        super(ResultCodeEnum.A0401.getName());
        this.code = ResultCodeEnum.A0401.value();
        this.data = data;
    }

    public ContainsIllegalLinksException(String message, Object data) {
        super(message);
        this.code = ResultCodeEnum.A0401.value();
        this.data = data;
    }
}
