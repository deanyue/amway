package com.amway.broadcast.common.exception.user.access;

import com.amway.broadcast.common.enumeration.ResultCodeEnum;
import com.amway.broadcast.common.exception.BaseException;

/**
 *  账号被冻结 A0322
 *
 * @author Lee
 * @version 1.0
 * @date 2020/5/8 15:07
 */
public class AccountHasBeenFrozenException extends BaseException {

    public AccountHasBeenFrozenException() {
        super(ResultCodeEnum.A0322.getName());
        this.code = ResultCodeEnum.A0322.value();
    }

    public AccountHasBeenFrozenException(String message) {
        super(message);
        this.code = ResultCodeEnum.A0322.value();
    }

    public AccountHasBeenFrozenException(Object data) {
        super(ResultCodeEnum.A0322.getName());
        this.code = ResultCodeEnum.A0322.value();
        this.data = data;
    }

    public AccountHasBeenFrozenException(String message, Object data) {
        super(message);
        this.code = ResultCodeEnum.A0322.value();
        this.data = data;
    }
}
