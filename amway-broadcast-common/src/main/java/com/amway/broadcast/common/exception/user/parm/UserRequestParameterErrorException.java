package com.amway.broadcast.common.exception.user.parm;

import com.amway.broadcast.common.enumeration.ResultCodeEnum;
import com.amway.broadcast.common.exception.BaseException;

/**
 *  用户请求参数错误 A0400
 *
 * @author Lee
 * @version 1.0
 * @date 2020/5/8 16:00
 */
public class UserRequestParameterErrorException extends BaseException {

    public UserRequestParameterErrorException() {
        super(ResultCodeEnum.A0400.getName());
        this.code = ResultCodeEnum.A0400.value();
    }

    public UserRequestParameterErrorException(String message) {
        super(message);
        this.code = ResultCodeEnum.A0400.value();
    }

    public UserRequestParameterErrorException(Object data) {
        super(ResultCodeEnum.A0400.getName());
        this.code = ResultCodeEnum.A0400.value();
        this.data = data;
    }

    public UserRequestParameterErrorException(String message, Object data) {
        super(message);
        this.code = ResultCodeEnum.A0400.value();
        this.data = data;
    }
}
