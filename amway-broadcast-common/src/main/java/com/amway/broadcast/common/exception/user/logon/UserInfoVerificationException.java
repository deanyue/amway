package com.amway.broadcast.common.exception.user.logon;

import com.amway.broadcast.common.enumeration.ResultCodeEnum;
import com.amway.broadcast.common.exception.BaseException;

/**
 * 用户基本信息校验失败 A0150
 *
 * @author Lee
 */
public class UserInfoVerificationException extends BaseException {

    public UserInfoVerificationException() {
        super(ResultCodeEnum.A0150.getName());
        this.code = ResultCodeEnum.A0150.value();
    }

    public UserInfoVerificationException(String message) {
        super(message);
        this.code = ResultCodeEnum.A0150.value();
    }

    public UserInfoVerificationException(Object data) {
        super(ResultCodeEnum.A0150.getName());
        this.code = ResultCodeEnum.A0150.value();
        this.data = data;
    }

    public UserInfoVerificationException(String message, Object data) {
        super(message);
        this.code = ResultCodeEnum.A0150.value();
        this.data = data;
    }
}
