package com.amway.broadcast.common.exception.remote.database;

import com.amway.broadcast.common.enumeration.ResultCodeEnum;
import com.amway.broadcast.common.exception.BaseException;

/**
 *  数据库服务出错 C0300
 *
 * @author Lee
 * @version 1.0
 * @date 2020/5/9 10:02
 */
public class DatabaseServiceErrorException extends BaseException {


    public DatabaseServiceErrorException() {
        super(ResultCodeEnum.C0300.getName());
        this.code = ResultCodeEnum.C0300.value();
    }

    public DatabaseServiceErrorException(String message) {
        super(message);
        this.code = ResultCodeEnum.C0300.value();
    }

    public DatabaseServiceErrorException(Object data) {
        super(ResultCodeEnum.C0300.getName());
        this.code = ResultCodeEnum.C0300.value();
        this.data = data;
    }

    public DatabaseServiceErrorException(String message, Object data) {
        super(message);
        this.code = ResultCodeEnum.C0300.value();
        this.data = data;
    }
}
