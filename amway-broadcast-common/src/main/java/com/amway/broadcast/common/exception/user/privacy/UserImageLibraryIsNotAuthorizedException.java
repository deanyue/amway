package com.amway.broadcast.common.exception.user.privacy;

import com.amway.broadcast.common.enumeration.ResultCodeEnum;
import com.amway.broadcast.common.exception.BaseException;

/**
 *  用户图片库未授权 A0904
 *
 * @author Lee
 * @version 1.0
 * @date 2020/5/8 17:52
 */
public class UserImageLibraryIsNotAuthorizedException extends BaseException {

    public UserImageLibraryIsNotAuthorizedException() {
        super(ResultCodeEnum.A0904.getName());
        this.code = ResultCodeEnum.A0904.value();
    }

    public UserImageLibraryIsNotAuthorizedException(String message) {
        super(message);
        this.code = ResultCodeEnum.A0904.value();
    }

    public UserImageLibraryIsNotAuthorizedException(Object data) {
        super(ResultCodeEnum.A0904.getName());
        this.code = ResultCodeEnum.A0904.value();
        this.data = data;
    }

    public UserImageLibraryIsNotAuthorizedException(String message, Object data) {
        super(message);
        this.code = ResultCodeEnum.A0904.value();
        this.data = data;
    }
}
