package com.amway.broadcast.common.constant;

/**
 * @author Lee
 */
public class RedisKeyPrefixAndSuffix {

    private RedisKeyPrefixAndSuffix() {
    }

    /**
     * 消息过滤公平锁前缀
     */
    public static final String RECORD_FILTER_LOCK_PREFIX = "record-filter-";

    /**
     * 消息记录集合Key后缀
     */
    public static final String RECORD_SET_KEY_SUFFIX = "-record-key";

    /**
     * 活动锁前缀
     */
    public static final String ACTIVITY_LOCK_KEY_PREFIX = "activity-";

    /**
     * 活动钱包KEY前缀
     */
    public static final String ACTIVITY_WALLET_KEY_PREFIX = "activity-wallet-";

    /**
     * 当前活动用户数量KEY前缀，用于匹配活动人数上限
     */
    public static final String ACTIVITY_LIMIT_KEY_PREFIX = "activity-limit-";

    /**
     * corp AccessToken
     */
    public static final String CORP_ACCESS_TOKEN_KEY_PREFIX = "corp:corp-access-token:";

    /**
     * corp agentJSApiTIcket
     */
    public static final String CORP_AGENT_JSAPI_TICKET_KEY_PREFIX = "corp:agent-jsapi-ticket:";

    /**
     * 同一公司下media_id通用
     */
    public static final String CORP_MEDIA_ID_KEY_PREFIX = "corp:media-id:{0}:{1}";
    //直播群 key前缀
    public static final String AMWAY_GROUP_PREFIX = "amway-group-";
}
