package com.amway.broadcast.common.exception.user.logon;

import com.amway.broadcast.common.enumeration.ResultCodeEnum;
import com.amway.broadcast.common.exception.BaseException;

/**
 * 语音校验码输入错误 A0133
 *
 * @author Lee
 */
public class VoiceVerificationCodeException extends BaseException {

    public VoiceVerificationCodeException() {
        super(ResultCodeEnum.A0133.getName());
        this.code = ResultCodeEnum.A0133.value();
    }

    public VoiceVerificationCodeException(String message) {
        super(message);
        this.code = ResultCodeEnum.A0133.value();
    }

    public VoiceVerificationCodeException(Object data) {
        super(ResultCodeEnum.A0133.getName());
        this.code = ResultCodeEnum.A0133.value();
        this.data = data;
    }

    public VoiceVerificationCodeException(String message, Object data) {
        super(message);
        this.code = ResultCodeEnum.A0133.value();
        this.data = data;
    }
}
