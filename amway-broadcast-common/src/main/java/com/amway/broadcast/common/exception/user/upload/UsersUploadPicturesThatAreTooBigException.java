package com.amway.broadcast.common.exception.user.upload;

import com.amway.broadcast.common.enumeration.ResultCodeEnum;
import com.amway.broadcast.common.exception.BaseException;

/**
 *  用户上传图片太大 A0703
 *
 * @author Lee
 * @version 1.0
 * @date 2020/5/8 17:25
 */
public class UsersUploadPicturesThatAreTooBigException extends BaseException {

    public UsersUploadPicturesThatAreTooBigException() {
        super(ResultCodeEnum.A0703.getName());
        this.code = ResultCodeEnum.A0703.value();
    }

    public UsersUploadPicturesThatAreTooBigException(String message) {
        super(message);
        this.code = ResultCodeEnum.A0703.value();
    }

    public UsersUploadPicturesThatAreTooBigException(Object data) {
        super(ResultCodeEnum.A0703.getName());
        this.code = ResultCodeEnum.A0703.value();
        this.data = data;
    }

    public UsersUploadPicturesThatAreTooBigException(String message, Object data) {
        super(message);
        this.code = ResultCodeEnum.A0703.value();
        this.data = data;
    }
}
