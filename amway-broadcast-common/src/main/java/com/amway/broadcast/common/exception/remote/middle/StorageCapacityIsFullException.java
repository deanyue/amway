package com.amway.broadcast.common.exception.remote.middle;

import com.amway.broadcast.common.enumeration.ResultCodeEnum;
import com.amway.broadcast.common.exception.BaseException;

/**
 *  存储容量已满 C0133
 *
 * @author Lee
 * @version 1.0
 * @date 2020/5/9 9:37
 */
public class StorageCapacityIsFullException extends BaseException {

    public StorageCapacityIsFullException() {
        super(ResultCodeEnum.C0133.getName());
        this.code = ResultCodeEnum.C0133.value();
    }

    public StorageCapacityIsFullException(String message) {
        super(message);
        this.code = ResultCodeEnum.C0133.value();
    }

    public StorageCapacityIsFullException(Object data) {
        super(ResultCodeEnum.C0133.getName());
        this.code = ResultCodeEnum.C0133.value();
        this.data = data;
    }

    public StorageCapacityIsFullException(String message, Object data) {
        super(message);
        this.code = ResultCodeEnum.C0133.value();
        this.data = data;
    }
}
