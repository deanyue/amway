package com.amway.broadcast.common.exception.remote.middle;

import com.amway.broadcast.common.enumeration.ResultCodeEnum;
import com.amway.broadcast.common.exception.BaseException;

/**
 *  网关服务出错 C0154
 *
 * @author Lee
 * @version 1.0
 * @date 2020/5/9 9:44
 */
public class GatewayServiceErrorException extends BaseException {

    public GatewayServiceErrorException() {
        super(ResultCodeEnum.C0154.getName());
        this.code = ResultCodeEnum.C0154.value();
    }

    public GatewayServiceErrorException(String message) {
        super(message);
        this.code = ResultCodeEnum.C0154.value();
    }

    public GatewayServiceErrorException(Object data) {
        super(ResultCodeEnum.C0154.getName());
        this.code = ResultCodeEnum.C0154.value();
        this.data = data;
    }

    public GatewayServiceErrorException(String message, Object data) {
        super(message);
        this.code = ResultCodeEnum.C0154.value();
        this.data = data;
    }
}
