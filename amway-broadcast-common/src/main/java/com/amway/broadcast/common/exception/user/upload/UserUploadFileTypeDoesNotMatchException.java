package com.amway.broadcast.common.exception.user.upload;

import com.amway.broadcast.common.enumeration.ResultCodeEnum;
import com.amway.broadcast.common.exception.BaseException;

/**
 *  用户上传文件类型不匹配 A0701
 *
 * @author Lee
 * @version 1.0
 * @date 2020/5/8 17:22
 */
public class UserUploadFileTypeDoesNotMatchException extends BaseException {

    public UserUploadFileTypeDoesNotMatchException() {
        super(ResultCodeEnum.A0701.getName());
        this.code = ResultCodeEnum.A0701.value();
    }

    public UserUploadFileTypeDoesNotMatchException(String message) {
        super(message);
        this.code = ResultCodeEnum.A0701.value();
    }

    public UserUploadFileTypeDoesNotMatchException(Object data) {
        super(ResultCodeEnum.A0701.getName());
        this.code = ResultCodeEnum.A0701.value();
        this.data = data;
    }

    public UserUploadFileTypeDoesNotMatchException(String message, Object data) {
        super(message);
        this.code = ResultCodeEnum.A0701.value();
        this.data = data;
    }
}
