package com.amway.broadcast.common.exception.system.resources;

import com.amway.broadcast.common.enumeration.ResultCodeEnum;
import com.amway.broadcast.common.exception.BaseException;

/**
 *  系统资源异常 B0300
 *
 * @author Lee
 * @version 1.0
 * @date 2020/5/8 18:19
 */
public class SystemResourceAnomalyException extends BaseException {

    public SystemResourceAnomalyException() {
        super(ResultCodeEnum.B0300.getName());
        this.code = ResultCodeEnum.B0300.value();
    }

    public SystemResourceAnomalyException(String message) {
        super(message);
        this.code = ResultCodeEnum.B0300.value();
    }

    public SystemResourceAnomalyException(Object data) {
        super(ResultCodeEnum.B0300.getName());
        this.code = ResultCodeEnum.B0300.value();
        this.data = data;
    }

    public SystemResourceAnomalyException(String message, Object data) {
        super(message);
        this.code = ResultCodeEnum.B0300.value();
        this.data = data;
    }
}
