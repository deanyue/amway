package com.amway.broadcast.common.exception.user.parm;

import com.amway.broadcast.common.enumeration.ResultCodeEnum;
import com.amway.broadcast.common.exception.BaseException;

/**
 *  用户支付超时 A0441
 *
 * @author Lee
 * @version 1.0
 * @date 2020/5/8 16:24
 */
public class UserPaymentTimeoutException extends BaseException {

    public UserPaymentTimeoutException() {
        super(ResultCodeEnum.A0441.getName());
        this.code = ResultCodeEnum.A0441.value();
    }

    public UserPaymentTimeoutException(String message) {
        super(message);
        this.code = ResultCodeEnum.A0441.value();
    }

    public UserPaymentTimeoutException(Object data) {
        super(ResultCodeEnum.A0441.getName());
        this.code = ResultCodeEnum.A0441.value();
        this.data = data;
    }

    public UserPaymentTimeoutException(String message, Object data) {
        super(message);
        this.code = ResultCodeEnum.A0441.value();
        this.data = data;
    }
}
