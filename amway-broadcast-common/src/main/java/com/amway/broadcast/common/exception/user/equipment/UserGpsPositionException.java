package com.amway.broadcast.common.exception.user.equipment;

import com.amway.broadcast.common.enumeration.ResultCodeEnum;
import com.amway.broadcast.common.exception.BaseException;

/**
 *  用户GPS定位异常 A1005
 *
 * @author Lee
 * @version 1.0
 * @date 2020/5/8 18:02
 */
public class UserGpsPositionException extends BaseException {

    public UserGpsPositionException() {
        super(ResultCodeEnum.A1005.getName());
        this.code = ResultCodeEnum.A1005.value();
    }

    public UserGpsPositionException(String message) {
        super(message);
        this.code = ResultCodeEnum.A1005.value();
    }

    public UserGpsPositionException(Object data) {
        super(ResultCodeEnum.A1005.getName());
        this.code = ResultCodeEnum.A1005.value();
        this.data = data;
    }

    public UserGpsPositionException(String message, Object data) {
        super(message);
        this.code = ResultCodeEnum.A1005.value();
        this.data = data;
    }
}
