package com.amway.broadcast.common.exception.user.access;

import com.amway.broadcast.common.enumeration.ResultCodeEnum;
import com.amway.broadcast.common.exception.BaseException;

/**
 *  用户授权申请被拒绝 A0303
 *
 * @author Lee
 * @version 1.0
 * @date 2020/5/8 15:00
 */
public class UserAuthorizationWasRejectedException extends BaseException {

    public UserAuthorizationWasRejectedException() {
        super(ResultCodeEnum.A0303.getName());
        this.code = ResultCodeEnum.A0303.value();
    }

    public UserAuthorizationWasRejectedException(String message) {
        super(message);
        this.code = ResultCodeEnum.A0303.value();
    }

    public UserAuthorizationWasRejectedException(Object data) {
        super(ResultCodeEnum.A0303.getName());
        this.code = ResultCodeEnum.A0303.value();
        this.data = data;
    }

    public UserAuthorizationWasRejectedException(String message, Object data) {
        super(message);
        this.code = ResultCodeEnum.A0303.value();
        this.data = data;
    }
}
