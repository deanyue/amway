package com.amway.broadcast.common.exception.remote.middle;

import com.amway.broadcast.common.enumeration.ResultCodeEnum;
import com.amway.broadcast.common.exception.BaseException;

/**
 *  接口不存在 C0113
 *
 * @author Lee
 * @version 1.0
 * @date 2020/5/9 9:31
 */
public class InterfaceDoesNotExistException extends BaseException {

    public InterfaceDoesNotExistException() {
        super(ResultCodeEnum.C0113.getName());
        this.code = ResultCodeEnum.C0113.value();
    }

    public InterfaceDoesNotExistException(String message) {
        super(message);
        this.code = ResultCodeEnum.C0113.value();
    }

    public InterfaceDoesNotExistException(Object data) {
        super(ResultCodeEnum.C0113.getName());
        this.code = ResultCodeEnum.C0113.value();
        this.data = data;
    }

    public InterfaceDoesNotExistException(String message, Object data) {
        super(message);
        this.code = ResultCodeEnum.C0113.value();
        this.data = data;
    }
}
