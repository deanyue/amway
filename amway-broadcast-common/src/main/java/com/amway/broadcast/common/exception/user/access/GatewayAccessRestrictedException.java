package com.amway.broadcast.common.exception.user.access;

import com.amway.broadcast.common.enumeration.ResultCodeEnum;
import com.amway.broadcast.common.exception.BaseException;

/**
 *  网关访问受限 A0324
 *
 * @author Lee
 * @version 1.0
 * @date 2020/5/8 15:10
 */
public class GatewayAccessRestrictedException extends BaseException {

    public GatewayAccessRestrictedException() {
        super(ResultCodeEnum.A0324.getName());
        this.code = ResultCodeEnum.A0324.value();
    }

    public GatewayAccessRestrictedException(String message) {
        super(message);
        this.code = ResultCodeEnum.A0324.value();
    }

    public GatewayAccessRestrictedException(Object data) {
        super(ResultCodeEnum.A0324.getName());
        this.code = ResultCodeEnum.A0324.value();
        this.data = data;
    }

    public GatewayAccessRestrictedException(String message, Object data) {
        super(message);
        this.code = ResultCodeEnum.A0324.value();
        this.data = data;
    }
}
