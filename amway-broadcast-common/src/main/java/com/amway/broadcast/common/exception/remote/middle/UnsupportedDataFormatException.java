package com.amway.broadcast.common.exception.remote.middle;

import com.amway.broadcast.common.enumeration.ResultCodeEnum;
import com.amway.broadcast.common.exception.BaseException;

/**
 *  不支持的数据格式 C0134
 *
 * @author Lee
 * @version 1.0
 * @date 2020/5/9 9:37
 */
public class UnsupportedDataFormatException extends BaseException {

    public UnsupportedDataFormatException() {
        super(ResultCodeEnum.C0134.getName());
        this.code = ResultCodeEnum.C0134.value();
    }

    public UnsupportedDataFormatException(String message) {
        super(message);
        this.code = ResultCodeEnum.C0134.value();
    }

    public UnsupportedDataFormatException(Object data) {
        super(ResultCodeEnum.C0134.getName());
        this.code = ResultCodeEnum.C0134.value();
        this.data = data;
    }

    public UnsupportedDataFormatException(String message, Object data) {
        super(message);
        this.code = ResultCodeEnum.C0134.value();
        this.data = data;
    }
}
