package com.amway.broadcast.common.exception.user.parm;

import com.amway.broadcast.common.enumeration.ResultCodeEnum;
import com.amway.broadcast.common.exception.BaseException;

/**
 *  用户订单号为空 A0411
 *
 * @author Lee
 * @version 1.0
 * @date 2020/5/8 16:05
 */
public class UserOrderNumberIsEmptyException extends BaseException {

    public UserOrderNumberIsEmptyException() {
        super(ResultCodeEnum.A0411.getName());
        this.code = ResultCodeEnum.A0411.value();
    }

    public UserOrderNumberIsEmptyException(String message) {
        super(message);
        this.code = ResultCodeEnum.A0411.value();
    }

    public UserOrderNumberIsEmptyException(Object data) {
        super(ResultCodeEnum.A0411.getName());
        this.code = ResultCodeEnum.A0411.value();
        this.data = data;
    }

    public UserOrderNumberIsEmptyException(String message, Object data) {
        super(message);
        this.code = ResultCodeEnum.A0411.value();
        this.data = data;
    }
}
