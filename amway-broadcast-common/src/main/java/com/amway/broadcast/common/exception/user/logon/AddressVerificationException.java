package com.amway.broadcast.common.exception.user.logon;

import com.amway.broadcast.common.enumeration.ResultCodeEnum;
import com.amway.broadcast.common.exception.BaseException;

/**
 * 地址格式校验失败 A0152
 *
 * @author Lee
 */
public class AddressVerificationException extends BaseException {

    public AddressVerificationException() {
        super(ResultCodeEnum.A0152.getName());
        this.code = ResultCodeEnum.A0152.value();
    }

    public AddressVerificationException(String message) {
        super(message);
        this.code = ResultCodeEnum.A0152.value();
    }

    public AddressVerificationException(Object data) {
        super(ResultCodeEnum.A0152.getName());
        this.code = ResultCodeEnum.A0152.value();
        this.data = data;
    }

    public AddressVerificationException(String message, Object data) {
        super(message);
        this.code = ResultCodeEnum.A0152.value();
        this.data = data;
    }
}
