package com.amway.broadcast.common.exception.user.logon;

import com.amway.broadcast.common.enumeration.ResultCodeEnum;
import com.amway.broadcast.common.exception.BaseException;

/**
 * 密码强度不够 A0122
 *
 * @author Lee
 */
public class PasswordNotStrongEnoughException extends BaseException {

    public PasswordNotStrongEnoughException() {
        super(ResultCodeEnum.A0122.getName());
        this.code = ResultCodeEnum.A0122.value();
    }

    public PasswordNotStrongEnoughException(String message) {
        super(message);
        this.code = ResultCodeEnum.A0122.value();
    }

    public PasswordNotStrongEnoughException(Object data) {
        super(ResultCodeEnum.A0122.getName());
        this.code = ResultCodeEnum.A0122.value();
        this.data = data;
    }

    public PasswordNotStrongEnoughException(String message, Object data) {
        super(message);
        this.code = ResultCodeEnum.A0122.value();
        this.data = data;
    }
}
