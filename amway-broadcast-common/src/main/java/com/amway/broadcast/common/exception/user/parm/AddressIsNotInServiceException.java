package com.amway.broadcast.common.exception.user.parm;

import com.amway.broadcast.common.enumeration.ResultCodeEnum;
import com.amway.broadcast.common.exception.BaseException;

/**
 *  地址不在服务范围 A0422
 *
 * @author Lee
 * @version 1.0
 * @date 2020/5/8 16:11
 */
public class AddressIsNotInServiceException extends BaseException {

    public AddressIsNotInServiceException() {
        super(ResultCodeEnum.A0422.getName());
        this.code = ResultCodeEnum.A0422.value();
    }

    public AddressIsNotInServiceException(String message) {
        super(message);
        this.code = ResultCodeEnum.A0422.value();
    }

    public AddressIsNotInServiceException(Object data) {
        super(ResultCodeEnum.A0422.getName());
        this.code = ResultCodeEnum.A0422.value();
        this.data = data;
    }

    public AddressIsNotInServiceException(String message, Object data) {
        super(message);
        this.code = ResultCodeEnum.A0422.value();
        this.data = data;
    }
}
