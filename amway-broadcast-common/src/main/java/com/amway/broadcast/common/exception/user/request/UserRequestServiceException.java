package com.amway.broadcast.common.exception.user.request;

import com.amway.broadcast.common.enumeration.ResultCodeEnum;
import com.amway.broadcast.common.exception.BaseException;

/**
 *  用户请求服务异常 A0500
 *
 * @author Lee
 * @version 1.0
 * @date 2020/5/8 17:00
 */
public class UserRequestServiceException extends BaseException {

    public UserRequestServiceException() {
        super(ResultCodeEnum.A0500.getName());
        this.code = ResultCodeEnum.A0500.value();
    }

    public UserRequestServiceException(String message) {
        super(message);
        this.code = ResultCodeEnum.A0500.value();
    }

    public UserRequestServiceException(Object data) {
        super(ResultCodeEnum.A0500.getName());
        this.code = ResultCodeEnum.A0500.value();
        this.data = data;
    }

    public UserRequestServiceException(String message, Object data) {
        super(message);
        this.code = ResultCodeEnum.A0500.value();
        this.data = data;
    }
}
