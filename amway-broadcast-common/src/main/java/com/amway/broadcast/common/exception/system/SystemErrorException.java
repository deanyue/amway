package com.amway.broadcast.common.exception.system;

import com.amway.broadcast.common.enumeration.ResultCodeEnum;
import com.amway.broadcast.common.exception.BaseException;

/**
 *  系统执行出错 B0001
 *
 * @author Lee
 * @version 1.0
 * @date 2020/5/8 18:07
 */
public class SystemErrorException extends BaseException {

    public SystemErrorException() {
        super(ResultCodeEnum.B0001.getName());
        this.code = ResultCodeEnum.B0001.value();
    }

    public SystemErrorException(String message) {
        super(message);
        this.code = ResultCodeEnum.B0001.value();
    }

    public SystemErrorException(Object data) {
        super(ResultCodeEnum.B0001.getName());
        this.code = ResultCodeEnum.B0001.value();
        this.data = data;
    }

    public SystemErrorException(String message, Object data) {
        super(message);
        this.code = ResultCodeEnum.B0001.value();
        this.data = data;
    }
}
