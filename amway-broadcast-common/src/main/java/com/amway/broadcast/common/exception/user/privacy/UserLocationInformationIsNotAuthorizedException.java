package com.amway.broadcast.common.exception.user.privacy;

import com.amway.broadcast.common.enumeration.ResultCodeEnum;
import com.amway.broadcast.common.exception.BaseException;

/**
 *  用户位置信息未授权 A0906
 *
 * @author Lee
 * @version 1.0
 * @date 2020/5/8 17:53
 */
public class UserLocationInformationIsNotAuthorizedException extends BaseException {

    public UserLocationInformationIsNotAuthorizedException() {
        super(ResultCodeEnum.A0906.getName());
        this.code = ResultCodeEnum.A0906.value();
    }

    public UserLocationInformationIsNotAuthorizedException(String message) {
        super(message);
        this.code = ResultCodeEnum.A0906.value();
    }

    public UserLocationInformationIsNotAuthorizedException(Object data) {
        super(ResultCodeEnum.A0906.getName());
        this.code = ResultCodeEnum.A0906.value();
        this.data = data;
    }

    public UserLocationInformationIsNotAuthorizedException(String message, Object data) {
        super(message);
        this.code = ResultCodeEnum.A0906.value();
        this.data = data;
    }
}
