package com.amway.broadcast.common.exception.user.login;

import com.amway.broadcast.common.enumeration.ResultCodeEnum;
import com.amway.broadcast.common.exception.BaseException;

/**
 *  用户身份校验失败 A0220
 *
 * @author Lee
 * @version 1.0
 * @date 2020/5/8 14:07
 */
public class UserIdentityVerificationFailedException extends BaseException {

    public UserIdentityVerificationFailedException() {
        super(ResultCodeEnum.A0220.getName());
        this.code = ResultCodeEnum.A0220.value();
    }

    public UserIdentityVerificationFailedException(String message) {
        super(message);
        this.code = ResultCodeEnum.A0220.value();
    }

    public UserIdentityVerificationFailedException(Object data) {
        super(ResultCodeEnum.A0220.getName());
        this.code = ResultCodeEnum.A0220.value();
        this.data = data;
    }

    public UserIdentityVerificationFailedException(String message, Object data) {
        super(message);
        this.code = ResultCodeEnum.A0220.value();
        this.data = data;
    }
}
