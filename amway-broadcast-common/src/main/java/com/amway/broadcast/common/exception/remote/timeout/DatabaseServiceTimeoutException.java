package com.amway.broadcast.common.exception.remote.timeout;

import com.amway.broadcast.common.enumeration.ResultCodeEnum;
import com.amway.broadcast.common.exception.BaseException;

/**
 *  数据库服务超时 C0250
 *
 * @author Lee
 * @version 1.0
 * @date 2020/5/9 9:58
 */
public class DatabaseServiceTimeoutException extends BaseException {

    public DatabaseServiceTimeoutException() {
        super(ResultCodeEnum.C0250.getName());
        this.code = ResultCodeEnum.C0250.value();
    }

    public DatabaseServiceTimeoutException(String message) {
        super(message);
        this.code = ResultCodeEnum.C0250.value();
    }

    public DatabaseServiceTimeoutException(Object data) {
        super(ResultCodeEnum.C0250.getName());
        this.code = ResultCodeEnum.C0250.value();
        this.data = data;
    }

    public DatabaseServiceTimeoutException(String message, Object data) {
        super(message);
        this.code = ResultCodeEnum.C0250.value();
        this.data = data;
    }
}
