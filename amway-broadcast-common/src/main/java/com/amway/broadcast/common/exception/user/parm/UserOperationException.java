package com.amway.broadcast.common.exception.user.parm;

import com.amway.broadcast.common.enumeration.ResultCodeEnum;
import com.amway.broadcast.common.exception.BaseException;

/**
 *  用户操作异常 A0440
 *
 * @author Lee
 * @version 1.0
 * @date 2020/5/8 16:24
 */
public class UserOperationException extends BaseException {

    public UserOperationException() {
        super(ResultCodeEnum.A0440.getName());
        this.code = ResultCodeEnum.A0440.value();
    }

    public UserOperationException(String message) {
        super(message);
        this.code = ResultCodeEnum.A0440.value();
    }

    public UserOperationException(Object data) {
        super(ResultCodeEnum.A0440.getName());
        this.code = ResultCodeEnum.A0440.value();
        this.data = data;
    }

    public UserOperationException(String message, Object data) {
        super(message);
        this.code = ResultCodeEnum.A0440.value();
        this.data = data;
    }
}
