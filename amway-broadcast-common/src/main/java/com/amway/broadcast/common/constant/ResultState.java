package com.amway.broadcast.common.constant;

/**
 * 结果状态
 *
 * @author Lee
 */
public class ResultState {

    private ResultState(){}

    /**
     * 失败
     */
    public static final String FAILED = "0";

    /**
     * 成功
     */
    public static final String SUCCESS = "1";

}
