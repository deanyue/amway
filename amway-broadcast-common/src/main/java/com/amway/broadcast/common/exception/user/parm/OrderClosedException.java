package com.amway.broadcast.common.exception.user.parm;

import com.amway.broadcast.common.enumeration.ResultCodeEnum;
import com.amway.broadcast.common.exception.BaseException;

/**
 *  订单已关闭 A0443
 *
 * @author Lee
 * @version 1.0
 * @date 2020/5/8 16:25
 */
public class OrderClosedException extends BaseException {

    public OrderClosedException() {
        super(ResultCodeEnum.A0443.getName());
        this.code = ResultCodeEnum.A0443.value();
    }

    public OrderClosedException(String message) {
        super(message);
        this.code = ResultCodeEnum.A0443.value();
    }

    public OrderClosedException(Object data) {
        super(ResultCodeEnum.A0443.getName());
        this.code = ResultCodeEnum.A0443.value();
        this.data = data;
    }

    public OrderClosedException(String message, Object data) {
        super(message);
        this.code = ResultCodeEnum.A0443.value();
        this.data = data;
    }
}
