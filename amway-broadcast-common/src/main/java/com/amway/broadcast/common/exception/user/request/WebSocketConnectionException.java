package com.amway.broadcast.common.exception.user.request;

import com.amway.broadcast.common.enumeration.ResultCodeEnum;
import com.amway.broadcast.common.exception.BaseException;

/**
 *  WebSocket连接异常 A0504
 *
 * @author Lee
 * @version 1.0
 * @date 2020/5/8 17:04
 */
public class WebSocketConnectionException extends BaseException {

    public WebSocketConnectionException() {
        super(ResultCodeEnum.A0504.getName());
        this.code = ResultCodeEnum.A0504.value();
    }

    public WebSocketConnectionException(String message) {
        super(message);
        this.code = ResultCodeEnum.A0504.value();
    }

    public WebSocketConnectionException(Object data) {
        super(ResultCodeEnum.A0504.getName());
        this.code = ResultCodeEnum.A0504.value();
        this.data = data;
    }

    public WebSocketConnectionException(String message, Object data) {
        super(message);
        this.code = ResultCodeEnum.A0504.value();
        this.data = data;
    }
}
