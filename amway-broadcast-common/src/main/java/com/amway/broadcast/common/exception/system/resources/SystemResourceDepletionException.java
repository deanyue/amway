package com.amway.broadcast.common.exception.system.resources;

import com.amway.broadcast.common.enumeration.ResultCodeEnum;
import com.amway.broadcast.common.exception.BaseException;

/**
 *  系统资源耗尽 B0310
 *
 * @author Lee
 * @version 1.0
 * @date 2020/5/8 18:20
 */
public class SystemResourceDepletionException extends BaseException {

    public SystemResourceDepletionException() {
        super(ResultCodeEnum.B0310.getName());
        this.code = ResultCodeEnum.B0310.value();
    }

    public SystemResourceDepletionException(String message) {
        super(message);
        this.code = ResultCodeEnum.B0310.value();
    }

    public SystemResourceDepletionException(Object data) {
        super(ResultCodeEnum.B0310.getName());
        this.code = ResultCodeEnum.B0310.value();
        this.data = data;
    }

    public SystemResourceDepletionException(String message, Object data) {
        super(message);
        this.code = ResultCodeEnum.B0310.value();
        this.data = data;
    }
}
