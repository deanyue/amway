package com.amway.broadcast.common.exception.user.logon;

import com.amway.broadcast.common.enumeration.ResultCodeEnum;
import com.amway.broadcast.common.exception.BaseException;

/**
 * 大陆身份证编号校验非法 A0142
 *
 * @author Lee
 */
public class IdCardVerificationException extends BaseException {

    public IdCardVerificationException() {
        super(ResultCodeEnum.A0142.getName());
        this.code = ResultCodeEnum.A0142.value();
    }

    public IdCardVerificationException(String message) {
        super(message);
        this.code = ResultCodeEnum.A0142.value();
    }

    public IdCardVerificationException(Object data) {
        super(ResultCodeEnum.A0142.getName());
        this.code = ResultCodeEnum.A0142.value();
        this.data = data;
    }

    public IdCardVerificationException(String message, Object data) {
        super(message);
        this.code = ResultCodeEnum.A0142.value();
        this.data = data;
    }
}
