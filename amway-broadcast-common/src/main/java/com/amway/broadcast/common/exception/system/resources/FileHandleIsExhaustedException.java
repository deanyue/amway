package com.amway.broadcast.common.exception.system.resources;

import com.amway.broadcast.common.enumeration.ResultCodeEnum;
import com.amway.broadcast.common.exception.BaseException;

/**
 *  文件句柄耗尽 B0313
 *
 * @author Lee
 * @version 1.0
 * @date 2020/5/9 9:17
 */
public class FileHandleIsExhaustedException extends BaseException {

    public FileHandleIsExhaustedException() {
        super(ResultCodeEnum.B0313.getName());
        this.code = ResultCodeEnum.B0313.value();
    }

    public FileHandleIsExhaustedException(String message) {
        super(message);
        this.code = ResultCodeEnum.B0313.value();
    }

    public FileHandleIsExhaustedException(Object data) {
        super(ResultCodeEnum.B0313.getName());
        this.code = ResultCodeEnum.B0313.value();
        this.data = data;
    }

    public FileHandleIsExhaustedException(String message, Object data) {
        super(message);
        this.code = ResultCodeEnum.B0313.value();
        this.data = data;
    }
}
