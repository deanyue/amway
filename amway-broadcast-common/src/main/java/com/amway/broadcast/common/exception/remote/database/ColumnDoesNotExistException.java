package com.amway.broadcast.common.exception.remote.database;

import com.amway.broadcast.common.enumeration.ResultCodeEnum;
import com.amway.broadcast.common.exception.BaseException;
/**
 *  列不存在 C0312
 *
 * @author Lee
 * @version 1.0
 * @date 2020/5/9 10:03
 */
public class ColumnDoesNotExistException extends BaseException {

    public ColumnDoesNotExistException() {
        super(ResultCodeEnum.C0312.getName());
        this.code = ResultCodeEnum.C0312.value();
    }

    public ColumnDoesNotExistException(String message) {
        super(message);
        this.code = ResultCodeEnum.C0312.value();
    }

    public ColumnDoesNotExistException(Object data) {
        super(ResultCodeEnum.C0312.getName());
        this.code = ResultCodeEnum.C0312.value();
        this.data = data;
    }

    public ColumnDoesNotExistException(String message, Object data) {
        super(message);
        this.code = ResultCodeEnum.C0312.value();
        this.data = data;
    }

}
