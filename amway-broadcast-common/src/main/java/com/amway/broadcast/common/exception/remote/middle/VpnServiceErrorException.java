package com.amway.broadcast.common.exception.remote.middle;

import com.amway.broadcast.common.enumeration.ResultCodeEnum;
import com.amway.broadcast.common.exception.BaseException;

/**
 *  VPN服务出错 C0151
 *
 * @author Lee
 * @version 1.0
 * @date 2020/5/9 9:39
 */
public class VpnServiceErrorException extends BaseException {

    public VpnServiceErrorException() {
        super(ResultCodeEnum.C0151.getName());
        this.code = ResultCodeEnum.C0151.value();
    }

    public VpnServiceErrorException(String message) {
        super(message);
        this.code = ResultCodeEnum.C0151.value();
    }

    public VpnServiceErrorException(Object data) {
        super(ResultCodeEnum.C0151.getName());
        this.code = ResultCodeEnum.C0151.value();
        this.data = data;
    }

    public VpnServiceErrorException(String message, Object data) {
        super(message);
        this.code = ResultCodeEnum.C0151.value();
        this.data = data;
    }
}
