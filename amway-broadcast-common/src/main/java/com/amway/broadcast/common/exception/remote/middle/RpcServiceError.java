package com.amway.broadcast.common.exception.remote.middle;

import com.amway.broadcast.common.enumeration.ResultCodeEnum;
import com.amway.broadcast.common.exception.BaseException;

/**
 *  RPC服务出错 C0110
 *
 * @author Lee
 * @version 1.0
 * @date 2020/5/9 9:28
 */
public class RpcServiceError extends BaseException {

    public RpcServiceError() {
        super(ResultCodeEnum.C0110.getName());
        this.code = ResultCodeEnum.C0110.value();
    }

    public RpcServiceError(String message) {
        super(message);
        this.code = ResultCodeEnum.C0110.value();
    }

    public RpcServiceError(Object data) {
        super(ResultCodeEnum.C0110.getName());
        this.code = ResultCodeEnum.C0110.value();
        this.data = data;
    }

    public RpcServiceError(String message, Object data) {
        super(message);
        this.code = ResultCodeEnum.C0110.value();
        this.data = data;
    }
}
