package com.amway.broadcast.common.exception.user.parm;

import com.amway.broadcast.common.enumeration.ResultCodeEnum;
import com.amway.broadcast.common.exception.BaseException;

/**
 *  金额超出限制 A0424
 *
 * @author Lee
 * @version 1.0
 * @date 2020/5/8 16:16
 */
public class AmountExceedingLimitException extends BaseException {

    public AmountExceedingLimitException() {
        super(ResultCodeEnum.A0424.getName());
        this.code = ResultCodeEnum.A0424.value();
    }

    public AmountExceedingLimitException(String message) {
        super(message);
        this.code = ResultCodeEnum.A0424.value();
    }

    public AmountExceedingLimitException(Object data) {
        super(ResultCodeEnum.A0424.getName());
        this.code = ResultCodeEnum.A0424.value();
        this.data = data;
    }

    public AmountExceedingLimitException(String message, Object data) {
        super(message);
        this.code = ResultCodeEnum.A0424.value();
        this.data = data;
    }
}
