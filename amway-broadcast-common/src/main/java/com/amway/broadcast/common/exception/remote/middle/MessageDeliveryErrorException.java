package com.amway.broadcast.common.exception.remote.middle;

import com.amway.broadcast.common.enumeration.ResultCodeEnum;
import com.amway.broadcast.common.exception.BaseException;

/**
 *  消息投递出错 C0121
 *
 * @author Lee
 * @version 1.0
 * @date 2020/5/9 9:32
 */
public class MessageDeliveryErrorException extends BaseException {

    public MessageDeliveryErrorException() {
        super(ResultCodeEnum.C0121.getName());
        this.code = ResultCodeEnum.C0121.value();
    }

    public MessageDeliveryErrorException(String message) {
        super(message);
        this.code = ResultCodeEnum.C0121.value();
    }

    public MessageDeliveryErrorException(Object data) {
        super(ResultCodeEnum.C0121.getName());
        this.code = ResultCodeEnum.C0121.value();
        this.data = data;
    }

    public MessageDeliveryErrorException(String message, Object data) {
        super(message);
        this.code = ResultCodeEnum.C0121.value();
        this.data = data;
    }
}
