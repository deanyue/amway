package com.amway.broadcast.common.exception.user.privacy;

import com.amway.broadcast.common.enumeration.ResultCodeEnum;
import com.amway.broadcast.common.exception.BaseException;

/**
 *  用户摄像头未授权 A0902
 *
 * @author Lee
 * @version 1.0
 * @date 2020/5/8 17:46
 */
public class UserCameraIsNotAuthorizedException extends BaseException {

    public UserCameraIsNotAuthorizedException() {
        super(ResultCodeEnum.A0902.getName());
        this.code = ResultCodeEnum.A0902.value();
    }

    public UserCameraIsNotAuthorizedException(String message) {
        super(message);
        this.code = ResultCodeEnum.A0902.value();
    }

    public UserCameraIsNotAuthorizedException(Object data) {
        super(ResultCodeEnum.A0902.getName());
        this.code = ResultCodeEnum.A0902.value();
        this.data = data;
    }

    public UserCameraIsNotAuthorizedException(String message, Object data) {
        super(message);
        this.code = ResultCodeEnum.A0902.value();
        this.data = data;
    }
}
