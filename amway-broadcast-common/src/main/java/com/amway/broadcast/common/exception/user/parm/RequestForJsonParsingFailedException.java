package com.amway.broadcast.common.exception.user.parm;

import com.amway.broadcast.common.enumeration.ResultCodeEnum;
import com.amway.broadcast.common.exception.BaseException;

/**
 *  请求JSON解析失败 A0427
 *
 * @author Lee
 * @version 1.0
 * @date 2020/5/8 16:21
 */
public class RequestForJsonParsingFailedException extends BaseException {

    public RequestForJsonParsingFailedException() {
        super(ResultCodeEnum.A0427.getName());
        this.code = ResultCodeEnum.A0427.value();
    }

    public RequestForJsonParsingFailedException(String message) {
        super(message);
        this.code = ResultCodeEnum.A0427.value();
    }

    public RequestForJsonParsingFailedException(Object data) {
        super(ResultCodeEnum.A0427.getName());
        this.code = ResultCodeEnum.A0427.value();
        this.data = data;
    }

    public RequestForJsonParsingFailedException(String message, Object data) {
        super(message);
        this.code = ResultCodeEnum.A0427.value();
        this.data = data;
    }
}
