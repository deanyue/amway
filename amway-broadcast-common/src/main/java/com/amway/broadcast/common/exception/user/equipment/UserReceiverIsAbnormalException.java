package com.amway.broadcast.common.exception.user.equipment;

import com.amway.broadcast.common.enumeration.ResultCodeEnum;
import com.amway.broadcast.common.exception.BaseException;

/**
 *  用户听筒异常 A1003
 *
 * @author Lee
 * @version 1.0
 * @date 2020/5/8 18:00
 */
public class UserReceiverIsAbnormalException extends BaseException {

    public UserReceiverIsAbnormalException() {
        super(ResultCodeEnum.A1003.getName());
        this.code = ResultCodeEnum.A1003.value();
    }

    public UserReceiverIsAbnormalException(String message) {
        super(message);
        this.code = ResultCodeEnum.A1003.value();
    }

    public UserReceiverIsAbnormalException(Object data) {
        super(ResultCodeEnum.A1003.getName());
        this.code = ResultCodeEnum.A1003.value();
        this.data = data;
    }

    public UserReceiverIsAbnormalException(String message, Object data) {
        super(message);
        this.code = ResultCodeEnum.A1003.value();
        this.data = data;
    }
}
