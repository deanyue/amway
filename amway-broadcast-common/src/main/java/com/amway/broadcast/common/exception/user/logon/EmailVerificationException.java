package com.amway.broadcast.common.exception.user.logon;

import com.amway.broadcast.common.enumeration.ResultCodeEnum;
import com.amway.broadcast.common.exception.BaseException;

/**
 * 邮箱格式校验失败 A0153
 *
 * @author Lee
 */
public class EmailVerificationException extends BaseException {

    public EmailVerificationException() {
        super(ResultCodeEnum.A0153.getName());
        this.code = ResultCodeEnum.A0153.value();
    }

    public EmailVerificationException(String message) {
        super(message);
        this.code = ResultCodeEnum.A0153.value();
    }

    public EmailVerificationException(Object data) {
        super(ResultCodeEnum.A0153.getName());
        this.code = ResultCodeEnum.A0153.value();
        this.data = data;
    }

    public EmailVerificationException(String message, Object data) {
        super(message);
        this.code = ResultCodeEnum.A0153.value();
        this.data = data;
    }
}
