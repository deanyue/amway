package com.amway.broadcast.common.exception.user.logon;

import com.amway.broadcast.common.enumeration.ResultCodeEnum;
import com.amway.broadcast.common.exception.BaseException;

/**
 * 注册国家或地区受限 A0102
 *
 * @author Lee
 */
public class LogonAreaLimitedException extends BaseException {

    public LogonAreaLimitedException() {
        super(ResultCodeEnum.A0102.getName());
        this.code = ResultCodeEnum.A0102.value();
    }

    public LogonAreaLimitedException(String message) {
        super(message);
        this.code = ResultCodeEnum.A0102.value();
    }

    public LogonAreaLimitedException(Object data) {
        super(ResultCodeEnum.A0102.getName());
        this.code = ResultCodeEnum.A0102.value();
        this.data = data;
    }

    public LogonAreaLimitedException(String message, Object data) {
        super(message);
        this.code = ResultCodeEnum.A0102.value();
        this.data = data;
    }
}
