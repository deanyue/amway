package com.amway.broadcast.common.exception.user.overall;

import com.amway.broadcast.common.enumeration.ResultCodeEnum;
import com.amway.broadcast.common.exception.BaseException;

public class OverallException extends BaseException {
    public OverallException(String message) {
        super(message);
        this.code = ResultCodeEnum.A0.value();
    }
}
