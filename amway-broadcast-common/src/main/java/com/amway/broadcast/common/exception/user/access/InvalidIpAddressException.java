package com.amway.broadcast.common.exception.user.access;

import com.amway.broadcast.common.enumeration.ResultCodeEnum;
import com.amway.broadcast.common.exception.BaseException;

/**
 *  非法IP地址 A0323
 *
 * @author Lee
 * @version 1.0
 * @date 2020/5/8 15:09
 */
public class InvalidIpAddressException extends BaseException {

    public InvalidIpAddressException() {
        super(ResultCodeEnum.A0323.getName());
        this.code = ResultCodeEnum.A0323.value();
    }

    public InvalidIpAddressException(String message) {
        super(message);
        this.code = ResultCodeEnum.A0323.value();
    }

    public InvalidIpAddressException(Object data) {
        super(ResultCodeEnum.A0323.getName());
        this.code = ResultCodeEnum.A0323.value();
        this.data = data;
    }

    public InvalidIpAddressException(String message, Object data) {
        super(message);
        this.code = ResultCodeEnum.A0323.value();
        this.data = data;
    }
}
