package com.amway.broadcast.common.exception.remote.database;

import com.amway.broadcast.common.enumeration.ResultCodeEnum;
import com.amway.broadcast.common.exception.BaseException;

/**
 *  多表关联中存在多个相同名称的列 C0321
 *
 * @author Lee
 * @version 1.0
 * @date 2020/5/9 10:05
 */
public class MultipleTableAssociationsHaveTheSameColumnsException extends BaseException {

    public MultipleTableAssociationsHaveTheSameColumnsException() {
        super(ResultCodeEnum.C0321.getName());
        this.code = ResultCodeEnum.C0321.value();
    }

    public MultipleTableAssociationsHaveTheSameColumnsException(String message) {
        super(message);
        this.code = ResultCodeEnum.C0321.value();
    }

    public MultipleTableAssociationsHaveTheSameColumnsException(Object data) {
        super(ResultCodeEnum.C0321.getName());
        this.code = ResultCodeEnum.C0321.value();
        this.data = data;
    }

    public MultipleTableAssociationsHaveTheSameColumnsException(String message, Object data) {
        super(message);
        this.code = ResultCodeEnum.C0321.value();
        this.data = data;
    }
}
