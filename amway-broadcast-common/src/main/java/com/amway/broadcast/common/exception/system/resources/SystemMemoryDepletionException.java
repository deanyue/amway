package com.amway.broadcast.common.exception.system.resources;

import com.amway.broadcast.common.enumeration.ResultCodeEnum;
import com.amway.broadcast.common.exception.BaseException;

/**
 *  系统内存耗尽 B0312
 *
 * @author Lee
 * @version 1.0
 * @date 2020/5/9 9:16
 */
public class SystemMemoryDepletionException extends BaseException {

    public SystemMemoryDepletionException() {
        super(ResultCodeEnum.B0312.getName());
        this.code = ResultCodeEnum.B0312.value();
    }

    public SystemMemoryDepletionException(String message) {
        super(message);
        this.code = ResultCodeEnum.B0312.value();
    }

    public SystemMemoryDepletionException(Object data) {
        super(ResultCodeEnum.B0312.getName());
        this.code = ResultCodeEnum.B0312.value();
        this.data = data;
    }

    public SystemMemoryDepletionException(String message, Object data) {
        super(message);
        this.code = ResultCodeEnum.B0312.value();
        this.data = data;
    }
}
