package com.amway.broadcast.common.exception.user.privacy;

import com.amway.broadcast.common.enumeration.ResultCodeEnum;
import com.amway.broadcast.common.exception.BaseException;

/**
 *  用户隐私未签署 A0901
 *
 * @author a good man
 * @version 1.0
 * @date 2020/5/8 17:45
 */
public class UserPrivacyNotSignedException extends BaseException {

    public UserPrivacyNotSignedException() {
        super(ResultCodeEnum.A0901.getName());
        this.code = ResultCodeEnum.A0901.value();
    }

    public UserPrivacyNotSignedException(String message) {
        super(message);
        this.code = ResultCodeEnum.A0901.value();
    }

    public UserPrivacyNotSignedException(Object data) {
        super(ResultCodeEnum.A0901.getName());
        this.code = ResultCodeEnum.A0901.value();
        this.data = data;
    }

    public UserPrivacyNotSignedException(String message, Object data) {
        super(message);
        this.code = ResultCodeEnum.A0901.value();
        this.data = data;
    }
}
