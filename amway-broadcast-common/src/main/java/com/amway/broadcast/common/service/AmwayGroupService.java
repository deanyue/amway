package com.amway.broadcast.common.service;

import com.amway.broadcast.common.base.BaseAmwayGroup;

import java.util.List;

/**
 * @auther 薛晨
 * @date 2020/12/12
 * @time 11:12
 * @description 直播间1000
 */
public interface AmwayGroupService<T extends BaseAmwayGroup> {
    int insert(T baseAmwayGroup);

    int delete(String groupId);

    int update(T baseAmwayGroup);

    List<T> getListByCondition(T condition);

    T getByGroupId(String groupId);

    T getByImGroupId(String imGroupId);

    List<T> getByMemberId(Long memberId);
}
