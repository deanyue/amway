package com.amway.broadcast.common.exception.user.version;

import com.amway.broadcast.common.enumeration.ResultCodeEnum;
import com.amway.broadcast.common.exception.BaseException;

/**
 *  用户安装版本与系统不匹配 A0801
 *
 * @author Lee
 * @version 1.0
 * @date 2020/5/8 17:32
 */
public class UserInstalledVersionDoesNotMatchTheSystemException extends BaseException {

    public UserInstalledVersionDoesNotMatchTheSystemException() {
        super(ResultCodeEnum.A0801.getName());
        this.code = ResultCodeEnum.A0801.value();
    }

    public UserInstalledVersionDoesNotMatchTheSystemException(String message) {
        super(message);
        this.code = ResultCodeEnum.A0801.value();
    }

    public UserInstalledVersionDoesNotMatchTheSystemException(Object data) {
        super(ResultCodeEnum.A0801.getName());
        this.code = ResultCodeEnum.A0801.value();
        this.data = data;
    }

    public UserInstalledVersionDoesNotMatchTheSystemException(String message, Object data) {
        super(message);
        this.code = ResultCodeEnum.A0801.value();
        this.data = data;
    }
}
