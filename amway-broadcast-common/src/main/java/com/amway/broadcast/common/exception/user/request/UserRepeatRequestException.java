package com.amway.broadcast.common.exception.user.request;

import com.amway.broadcast.common.enumeration.ResultCodeEnum;
import com.amway.broadcast.common.exception.BaseException;

/**
 *  用户重复请求 A0506
 *
 * @author Lee
 * @version 1.0
 * @date 2020/5/8 17:07
 */
public class UserRepeatRequestException extends BaseException {

    public UserRepeatRequestException() {
        super(ResultCodeEnum.A0506.getName());
        this.code = ResultCodeEnum.A0506.value();
    }

    public UserRepeatRequestException(String message) {
        super(message);
        this.code = ResultCodeEnum.A0506.value();
    }

    public UserRepeatRequestException(Object data) {
        super(ResultCodeEnum.A0506.getName());
        this.code = ResultCodeEnum.A0506.value();
        this.data = data;
    }

    public UserRepeatRequestException(String message, Object data) {
        super(message);
        this.code = ResultCodeEnum.A0506.value();
        this.data = data;
    }
}
