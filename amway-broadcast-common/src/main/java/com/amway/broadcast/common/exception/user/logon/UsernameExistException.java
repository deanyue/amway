package com.amway.broadcast.common.exception.user.logon;

import com.amway.broadcast.common.enumeration.ResultCodeEnum;
import com.amway.broadcast.common.exception.BaseException;

/**
 * 用户名已存在 A0111
 *
 * @author Lee
 */
public class UsernameExistException extends BaseException {

    public UsernameExistException() {
        super(ResultCodeEnum.A0111.getName());
        this.code = ResultCodeEnum.A0111.value();
    }

    public UsernameExistException(String message) {
        super(message);
        this.code = ResultCodeEnum.A0111.value();
    }

    public UsernameExistException(Object data) {
        super(ResultCodeEnum.A0111.getName());
        this.code = ResultCodeEnum.A0111.value();
        this.data = data;
    }

    public UsernameExistException(String message, Object data) {
        super(message);
        this.code = ResultCodeEnum.A0111.value();
        this.data = data;
    }
}
