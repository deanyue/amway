package com.amway.broadcast.common.exception.user.logon;

import com.amway.broadcast.common.enumeration.ResultCodeEnum;
import com.amway.broadcast.common.exception.BaseException;

/**
 * 用户名包含特殊字符 A0113
 *
 * @author Lee
 */
public class UsernameNotCompliantException extends BaseException {

    public UsernameNotCompliantException() {
        super(ResultCodeEnum.A0113.getName());
        this.code = ResultCodeEnum.A0113.value();
    }

    public UsernameNotCompliantException(String message) {
        super(message);
        this.code = ResultCodeEnum.A0113.value();
    }

    public UsernameNotCompliantException(Object data) {
        super(ResultCodeEnum.A0113.getName());
        this.code = ResultCodeEnum.A0113.value();
        this.data = data;
    }

    public UsernameNotCompliantException(String message, Object data) {
        super(message);
        this.code = ResultCodeEnum.A0113.value();
        this.data = data;
    }
}
