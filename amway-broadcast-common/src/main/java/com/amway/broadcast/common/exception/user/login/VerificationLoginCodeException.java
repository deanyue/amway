package com.amway.broadcast.common.exception.user.login;

import com.amway.broadcast.common.enumeration.ResultCodeEnum;
import com.amway.broadcast.common.exception.BaseException;

/**
 * 校验码输入错误 A0240
 *
 * @author Lee
 */
public class VerificationLoginCodeException extends BaseException {

    public VerificationLoginCodeException() {
        super(ResultCodeEnum.A0240.getName());
        this.code = ResultCodeEnum.A0240.value();
    }

    public VerificationLoginCodeException(String message) {
        super(message);
        this.code = ResultCodeEnum.A0240.value();
    }

    public VerificationLoginCodeException(Object data) {
        super(ResultCodeEnum.A0240.getName());
        this.code = ResultCodeEnum.A0240.value();
        this.data = data;
    }

    public VerificationLoginCodeException(String message, Object data) {
        super(message);
        this.code = ResultCodeEnum.A0240.value();
        this.data = data;
    }
}
