package com.amway.broadcast.common.enumeration;


import lombok.extern.slf4j.Slf4j;

/**
 *
 *
 * @author dean
 */
@Slf4j
public enum UserAnchorEnum {
    /**
     *
     */
    IANCHOR("主播", "1"),
    /**
     *
     */
    USER("用户", "0"),

    ;

    private String desc;

    private String code;

    UserAnchorEnum(String desc, String code) {
        this.desc = desc;
        this.code = code;
    }

    public String getDesc() {
        return desc;
    }

    public String getCode() {
        return code;
    }

}
