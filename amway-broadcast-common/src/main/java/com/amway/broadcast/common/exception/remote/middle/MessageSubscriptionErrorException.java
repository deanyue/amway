package com.amway.broadcast.common.exception.remote.middle;

import com.amway.broadcast.common.enumeration.ResultCodeEnum;
import com.amway.broadcast.common.exception.BaseException;
/**
 *  消息订阅出错 C0123
 *
 * @author Lee
 * @version 1.0
 * @date 2020/5/9 9:33
 */
public class MessageSubscriptionErrorException extends BaseException {

    public MessageSubscriptionErrorException() {
        super(ResultCodeEnum.C0123.getName());
        this.code = ResultCodeEnum.C0123.value();
    }

    public MessageSubscriptionErrorException(String message) {
        super(message);
        this.code = ResultCodeEnum.C0123.value();
    }

    public MessageSubscriptionErrorException(Object data) {
        super(ResultCodeEnum.C0123.getName());
        this.code = ResultCodeEnum.C0123.value();
        this.data = data;
    }

    public MessageSubscriptionErrorException(String message, Object data) {
        super(message);
        this.code = ResultCodeEnum.C0123.value();
        this.data = data;
    }
}
