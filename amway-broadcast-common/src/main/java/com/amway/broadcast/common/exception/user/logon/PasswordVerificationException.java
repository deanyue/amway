package com.amway.broadcast.common.exception.user.logon;

import com.amway.broadcast.common.enumeration.ResultCodeEnum;
import com.amway.broadcast.common.exception.BaseException;

/**
 * 密码校验失败 A0120
 *
 * @author Lee
 */
public class PasswordVerificationException extends BaseException {

    public PasswordVerificationException() {
        super(ResultCodeEnum.A0120.getName());
        this.code = ResultCodeEnum.A0120.value();
    }

    public PasswordVerificationException(String message) {
        super(message);
        this.code = ResultCodeEnum.A0120.value();
    }

    public PasswordVerificationException(Object data) {
        super(ResultCodeEnum.A0120.getName());
        this.code = ResultCodeEnum.A0120.value();
        this.data = data;
    }

    public PasswordVerificationException(String message, Object data) {
        super(message);
        this.code = ResultCodeEnum.A0120.value();
        this.data = data;
    }
}
