package com.amway.broadcast.common.exception.user.logon;

import com.amway.broadcast.common.enumeration.ResultCodeEnum;
import com.amway.broadcast.common.exception.BaseException;

/**
 * 邮件校验码输入错误 A0132
 *
 * @author Lee
 */
public class EmailVerificationCodeException extends BaseException {

    public EmailVerificationCodeException() {
        super(ResultCodeEnum.A0132.getName());
        this.code = ResultCodeEnum.A0132.value();
    }

    public EmailVerificationCodeException(String message) {
        super(message);
        this.code = ResultCodeEnum.A0132.value();
    }

    public EmailVerificationCodeException(Object data) {
        super(ResultCodeEnum.A0132.getName());
        this.code = ResultCodeEnum.A0132.value();
        this.data = data;
    }

    public EmailVerificationCodeException(String message, Object data) {
        super(message);
        this.code = ResultCodeEnum.A0132.value();
        this.data = data;
    }
}
