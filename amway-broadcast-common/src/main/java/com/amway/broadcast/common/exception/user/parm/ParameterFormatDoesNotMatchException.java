package com.amway.broadcast.common.exception.user.parm;

import com.amway.broadcast.common.enumeration.ResultCodeEnum;
import com.amway.broadcast.common.exception.BaseException;

/**
 *  参数格式不匹配 A0421
 *
 * @author Lee
 * @version 1.0
 * @date 2020/5/8 16:10
 */
public class ParameterFormatDoesNotMatchException extends BaseException {

    public ParameterFormatDoesNotMatchException() {
        super(ResultCodeEnum.A0421.getName());
        this.code = ResultCodeEnum.A0421.value();
    }

    public ParameterFormatDoesNotMatchException(String message) {
        super(message);
        this.code = ResultCodeEnum.A0421.value();
    }

    public ParameterFormatDoesNotMatchException(Object data) {
        super(ResultCodeEnum.A0421.getName());
        this.code = ResultCodeEnum.A0421.value();
        this.data = data;
    }

    public ParameterFormatDoesNotMatchException(String message, Object data) {
        super(message);
        this.code = ResultCodeEnum.A0421.value();
        this.data = data;
    }
}
