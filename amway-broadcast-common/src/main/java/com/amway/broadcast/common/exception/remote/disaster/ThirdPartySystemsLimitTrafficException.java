package com.amway.broadcast.common.exception.remote.disaster;

import com.amway.broadcast.common.enumeration.ResultCodeEnum;
import com.amway.broadcast.common.exception.BaseException;

/**
 *  第三方系统限流 C0401
 *
 * @author Lee
 * @version 1.0
 * @date 2020/5/9 10:11
 */
public class ThirdPartySystemsLimitTrafficException extends BaseException {

    public ThirdPartySystemsLimitTrafficException() {
        super(ResultCodeEnum.C0401.getName());
        this.code = ResultCodeEnum.C0401.value();
    }

    public ThirdPartySystemsLimitTrafficException(String message) {
        super(message);
        this.code = ResultCodeEnum.C0401.value();
    }

    public ThirdPartySystemsLimitTrafficException(Object data) {
        super(ResultCodeEnum.C0401.getName());
        this.code = ResultCodeEnum.C0401.value();
        this.data = data;
    }

    public ThirdPartySystemsLimitTrafficException(String message, Object data) {
        super(message);
        this.code = ResultCodeEnum.C0401.value();
        this.data = data;
    }
}
