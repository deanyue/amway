package com.amway.broadcast.common.exception.user.parm;

import com.amway.broadcast.common.enumeration.ResultCodeEnum;
import com.amway.broadcast.common.exception.BaseException;

/**
 *  用户输入内容非法 A0430
 *
 * @author Lee
 * @version 1.0
 * @date 2020/5/8 16:21
 */
public class IllegalUserInputException extends BaseException {

    public IllegalUserInputException() {
        super(ResultCodeEnum.A0430.getName());
        this.code = ResultCodeEnum.A0430.value();
    }

    public IllegalUserInputException(String message) {
        super(message);
        this.code = ResultCodeEnum.A0430.value();
    }

    public IllegalUserInputException(Object data) {
        super(ResultCodeEnum.A0430.getName());
        this.code = ResultCodeEnum.A0430.value();
        this.data = data;
    }

    public IllegalUserInputException(String message, Object data) {
        super(message);
        this.code = ResultCodeEnum.A0430.value();
        this.data = data;
    }
}
