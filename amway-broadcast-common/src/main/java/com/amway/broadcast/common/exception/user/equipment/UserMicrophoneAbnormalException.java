package com.amway.broadcast.common.exception.user.equipment;

import com.amway.broadcast.common.enumeration.ResultCodeEnum;
import com.amway.broadcast.common.exception.BaseException;

/**
 *  用户麦克风异常 A1002
 *
 * @author Lee
 * @version 1.0
 * @date 2020/5/8 17:59
 */
public class UserMicrophoneAbnormalException extends BaseException {

    public UserMicrophoneAbnormalException() {
        super(ResultCodeEnum.A1002.getName());
        this.code = ResultCodeEnum.A1002.value();
    }

    public UserMicrophoneAbnormalException(String message) {
        super(message);
        this.code = ResultCodeEnum.A1002.value();
    }

    public UserMicrophoneAbnormalException(Object data) {
        super(ResultCodeEnum.A1002.getName());
        this.code = ResultCodeEnum.A1002.value();
        this.data = data;
    }

    public UserMicrophoneAbnormalException(String message, Object data) {
        super(message);
        this.code = ResultCodeEnum.A1002.value();
        this.data = data;
    }
}
