package com.amway.broadcast.common.exception.user.privacy;

import com.amway.broadcast.common.enumeration.ResultCodeEnum;
import com.amway.broadcast.common.exception.BaseException;

/**
 *  用户文件未授权 A0905
 *
 * @author Lee
 * @version 1.0
 * @date 2020/5/8 17:52
 */
public class UserFileIsNotAuthorizedException extends BaseException {

    public UserFileIsNotAuthorizedException() {
        super(ResultCodeEnum.A0905.getName());
        this.code = ResultCodeEnum.A0905.value();
    }

    public UserFileIsNotAuthorizedException(String message) {
        super(message);
        this.code = ResultCodeEnum.A0905.value();
    }

    public UserFileIsNotAuthorizedException(Object data) {
        super(ResultCodeEnum.A0905.getName());
        this.code = ResultCodeEnum.A0905.value();
        this.data = data;
    }

    public UserFileIsNotAuthorizedException(String message, Object data) {
        super(message);
        this.code = ResultCodeEnum.A0905.value();
        this.data = data;
    }
}
