package com.amway.broadcast.common.exception.user.login;

import com.amway.broadcast.common.enumeration.ResultCodeEnum;
import com.amway.broadcast.common.exception.BaseException;

/**
 * 用户输入密码次数超限 A0211
 *
 * @author Lee
 */
public class PasswordRetryLimitedException extends BaseException {

    public PasswordRetryLimitedException() {
        super(ResultCodeEnum.A0211.getName());
        this.code = ResultCodeEnum.A0211.value();
    }

    public PasswordRetryLimitedException(String message) {
        super(message);
        this.code = ResultCodeEnum.A0211.value();
    }

    public PasswordRetryLimitedException(Object data) {
        super(ResultCodeEnum.A0211.getName());
        this.code = ResultCodeEnum.A0211.value();
        this.data = data;
    }

    public PasswordRetryLimitedException(String message, Object data) {
        super(message);
        this.code = ResultCodeEnum.A0211.value();
        this.data = data;
    }
}
