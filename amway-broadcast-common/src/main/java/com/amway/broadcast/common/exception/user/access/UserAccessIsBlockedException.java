package com.amway.broadcast.common.exception.user.access;

import com.amway.broadcast.common.enumeration.ResultCodeEnum;
import com.amway.broadcast.common.exception.BaseException;

/**
 *  用户访问被拦截 A0320
 *
 * @author Lee
 * @version 1.0
 * @date 2020/5/8 15:05
 */
public class UserAccessIsBlockedException extends BaseException {

    public UserAccessIsBlockedException() {
        super(ResultCodeEnum.A0320.getName());
        this.code = ResultCodeEnum.A0320.value();
    }

    public UserAccessIsBlockedException(String message) {
        super(message);
        this.code = ResultCodeEnum.A0320.value();
    }

    public UserAccessIsBlockedException(Object data) {
        super(ResultCodeEnum.A0320.getName());
        this.code = ResultCodeEnum.A0320.value();
        this.data = data;
    }

    public UserAccessIsBlockedException(String message, Object data) {
        super(message);
        this.code = ResultCodeEnum.A0320.value();
        this.data = data;
    }
}
