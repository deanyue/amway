package com.amway.broadcast.common.exception.user.access;

import com.amway.broadcast.common.enumeration.ResultCodeEnum;
import com.amway.broadcast.common.exception.BaseException;

/**
 *  因访问对象隐私设置被拦截 A0310
 *
 * @author Lee
 * @version 1.0
 * @date 2020/5/8 15:01
 */
public class AccessedObjectPrivacySettingsBlockedException extends BaseException {

    public AccessedObjectPrivacySettingsBlockedException() {
        super(ResultCodeEnum.A0310.getName());
        this.code = ResultCodeEnum.A0310.value();
    }

    public AccessedObjectPrivacySettingsBlockedException(String message) {
        super(message);
        this.code = ResultCodeEnum.A0310.value();
    }

    public AccessedObjectPrivacySettingsBlockedException(Object data) {
        super(ResultCodeEnum.A0310.getName());
        this.code = ResultCodeEnum.A0310.value();
        this.data = data;
    }

    public AccessedObjectPrivacySettingsBlockedException(String message, Object data) {
        super(message);
        this.code = ResultCodeEnum.A0310.value();
        this.data = data;
    }
}
