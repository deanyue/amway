package com.amway.broadcast.backstage.bootstrap.properties;

import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * cache配置属性
 */
@Getter
@Setter
@ConfigurationProperties(prefix = "spring.cache-manager")
public class CacheManagerProperties {

    /**
     * 缓存有效期
     */
    private Integer entryTtl = 1;

}
