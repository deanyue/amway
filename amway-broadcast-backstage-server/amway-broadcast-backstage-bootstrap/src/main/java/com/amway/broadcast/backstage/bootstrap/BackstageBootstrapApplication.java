package com.amway.broadcast.backstage.bootstrap;

import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.boot.web.servlet.ServletComponentScan;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import org.springframework.tsf.annotation.EnableTsf;

@Slf4j
@EnableTransactionManagement
@ServletComponentScan
@EnableScheduling
@SpringBootApplication(scanBasePackages = {"com.amway"})
@EnableTsf
public class BackstageBootstrapApplication {
    public static void main(String[] args) {
        SpringApplication.run(BackstageBootstrapApplication.class,args);
    }
}
