package com.amway.broadcast.backstage.service.common.request;

import lombok.Data;

@Data
public class BasicReq {

    private String api_token;
}
