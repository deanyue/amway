package com.amway.broadcast.backstage.service.common.bo;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class GlobalSettingsPreview {
    private Integer max_group_per_day;
    private Integer max_group_total;
    private Integer id;
    private Integer open_msg_check;
    private Integer open_video_upload;
    private Integer max_admin_num;

}
