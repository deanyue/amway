package com.amway.broadcast.backstage.service.api;

import com.amway.broadcast.backstage.service.api.service.AmwaySettingService;
import com.amway.broadcast.backstage.service.common.annotation.BackstageCheck;
import com.amway.broadcast.common.annotation.NonRepeatSubmit;
import com.amway.broadcast.backstage.service.common.request.AccountEdiTaccountReq;
import com.amway.broadcast.backstage.service.common.request.AddBroadcastNumReq;
import com.amway.broadcast.common.base.Result;
import com.amway.broadcast.common.util.JwtUtils;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import javax.annotation.Resource;

@RestController
@CrossOrigin
@RequestMapping("/vue")
@Slf4j
@Api(value = "系统设置", tags = {"系统设置"})
public class ApiAmwaySettingController {
    @Resource
    private AmwaySettingService amwaySettingService;

    @ApiOperation(value = "编辑直播的次数限制", response = Result.class)
    @PostMapping("setting-edit")
    @BackstageCheck
    @NonRepeatSubmit
    public Result settingEedit(@Validated AddBroadcastNumReq addBroadcastNumReq){
        log.info(JwtUtils.getUserNameFromToken(addBroadcastNumReq.getApi_token())+"访问编辑直播的次数接口，请求参数：{"+addBroadcastNumReq.toString()+"}");
        return amwaySettingService.settingEedit(addBroadcastNumReq);
    }

    @ApiOperation(value = "预览直播的次数限制", response = Result.class)
    @GetMapping("setting-view")
    @BackstageCheck
    public Result settingView(@RequestParam(required=false) String api_token){
        log.info(JwtUtils.getUserNameFromToken(api_token)+"访问预览直播的次数限制接口");
        return amwaySettingService.settingVedit(api_token);
    }


    @ApiOperation(value = "系统日志列表", response = Result.class)
    @GetMapping("loglist")
    @BackstageCheck
    public Result logList(@RequestParam(required = false) String api_token,
                          @RequestParam(value = "current_page",defaultValue = "1") Integer current_page,
                          @RequestParam(value = "page_size",defaultValue = "20") Integer page_size,
                          @RequestParam Integer type){
        log.info(JwtUtils.getUserNameFromToken(api_token)+"访问系统日志列表，"+"请求参数：{"+"页码："+current_page+",页面大小："+page_size+",日志类型："+type+"}");
        return amwaySettingService.logList(api_token,current_page,page_size,type);
    }

    @ApiOperation(value = "账户列表", response = Result.class)
    @GetMapping("accountlist")
    @BackstageCheck
    public Result accountList(@RequestParam(required = false) String api_token,
                              @RequestParam(value = "current_page",defaultValue = "1") Integer current_page,
                              @RequestParam(value = "page_size",defaultValue = "20") Integer page_size){
        log.info(JwtUtils.getUserNameFromToken(api_token)+"访问账户列表接口，"+"请求参数：{"+"页码："+current_page+",页面大小："+page_size+"}");
        return amwaySettingService.accountList(api_token,current_page,page_size);
    }

    @ApiOperation(value = "账户详情", response = Result.class)
    @GetMapping("accountinfo")
    @BackstageCheck
    public Result accountInfo(@RequestParam(required = false) String api_token,@RequestParam Integer id){
        log.info(JwtUtils.getUserNameFromToken(api_token)+"访问账户详情接口，"+"请求参数：{"+"账户id："+id+"}");
        return amwaySettingService.accountInfo(api_token,id);
    }
    /**
     * 新增和编辑后的提交
     */
    @ApiOperation(value = "账户的编辑添加提交按钮", response = Result.class)
    @PostMapping("editaccount")
    @BackstageCheck
    @NonRepeatSubmit
    public Result editAccount(@Validated AccountEdiTaccountReq accountEdiTaccountReq){
        log.info(JwtUtils.getUserNameFromToken(accountEdiTaccountReq.getApi_token())+"访问账户的编辑添接口，"+"请求参数：{"+accountEdiTaccountReq.toString()+"}");
        return amwaySettingService.editAccount(accountEdiTaccountReq);
    }
}
