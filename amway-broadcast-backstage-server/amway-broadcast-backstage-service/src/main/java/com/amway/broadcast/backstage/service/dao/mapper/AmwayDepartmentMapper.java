package com.amway.broadcast.backstage.service.dao.mapper;

import com.amway.broadcast.backstage.service.dao.entity.AmwayDepartment;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Select;

import java.util.List;

public interface AmwayDepartmentMapper extends BaseMapper<AmwayDepartment> {
    /**
     *获取所有的部门
     * @return
     */
    @Select("SELECT  id,department_id,name,parent_id,t.order_am  FROM amway_department t")
    List<AmwayDepartment> getDepartmentList();

}
