package com.amway.broadcast.backstage.service.common.util;

import com.amway.broadcast.backstage.service.common.bo.DepartMentNode;

import java.util.ArrayList;
import java.util.List;

public class TreeUtils {

    /**
     * 部门数据
     * @param list
     * @return
     */
    public static List<DepartMentNode> listToTree(List<DepartMentNode> list) {
        //用递归找子。
        List<DepartMentNode> treeList = new ArrayList<DepartMentNode>();
        for (DepartMentNode tree : list) {
            if (tree.getParent_id() == 0) {
                treeList.add(findChildren(tree, list));
            }
        }
        return treeList;
    }

    private static DepartMentNode findChildren(DepartMentNode tree, List<DepartMentNode> list) {
        for (DepartMentNode node : list) {
            if (node.getParent_id().equals(tree.getD_id()) ) {
                if (tree.getChildren() == null) {
                    tree.setChildren(new ArrayList<DepartMentNode>());
                }
                tree.getChildren().add(findChildren(node, list));
            }
        }
        return tree;
    }

}
