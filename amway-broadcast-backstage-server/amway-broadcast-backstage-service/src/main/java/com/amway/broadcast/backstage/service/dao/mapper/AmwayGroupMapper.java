package com.amway.broadcast.backstage.service.dao.mapper;

import com.amway.broadcast.backstage.service.dao.entity.AmwayGroup;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

public interface AmwayGroupMapper  extends BaseMapper<AmwayGroup> {
}
