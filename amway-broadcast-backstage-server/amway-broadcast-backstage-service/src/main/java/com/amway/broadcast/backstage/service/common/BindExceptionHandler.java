package com.amway.broadcast.backstage.service.common;

import com.amway.broadcast.common.base.Result;
import com.amway.broadcast.common.base.ResultUtils;
import com.amway.broadcast.common.handller.BaseExceptionHandler;
import lombok.extern.slf4j.Slf4j;
import org.springframework.validation.BindException;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.MissingServletRequestParameterException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

@Slf4j
@RestControllerAdvice
public class BindExceptionHandler extends BaseExceptionHandler {


    @ExceptionHandler(value = BindException.class)
    public Result validationExceptionHandler(BindException e) {
        BindingResult bindingResult = e.getBindingResult();
        String errorMesssage = "";
        for (FieldError fieldError : bindingResult.getFieldErrors()) {
            errorMesssage += fieldError.getDefaultMessage() + "!";
        }
        List<ObjectError> errors = e.getAllErrors();
        errors.forEach(er->{
            FieldError fieldError = (FieldError)er;
            log.error("Data check failure : object{" + fieldError.getObjectName() + "},field{" + fieldError.getField() +
                    "},errorMessage{" + fieldError.getDefaultMessage() + "}");
        });
        return ResultUtils.error("0","".equals(errorMesssage) ? "请填写正确信息" : errorMesssage);
    }

    /**
     * 校验异常
     */
    @ExceptionHandler(value = ConstraintViolationException.class)
    public Result ConstraintViolationExceptionHandler(ConstraintViolationException ex) {
        Set<ConstraintViolation<?>> constraintViolations = ex.getConstraintViolations();
        Iterator<ConstraintViolation<?>> iterator = constraintViolations.iterator();
        List<String> msgList = new ArrayList<>();
        while (iterator.hasNext()) {
            ConstraintViolation<?> cvl = iterator.next();
            msgList.add(cvl.getMessageTemplate());
        }
        return ResultUtils.error("0",String.join(",",msgList));
    }


    /**
     * 校验异常
     */
    @ExceptionHandler(value = MissingServletRequestParameterException.class)
    public Result missingParameternExceptionHandler(MissingServletRequestParameterException ex) {
        String message = "缺少参数：["+ex.getParameterName()+"]";
        return ResultUtils.error("0",message);
    }
}

