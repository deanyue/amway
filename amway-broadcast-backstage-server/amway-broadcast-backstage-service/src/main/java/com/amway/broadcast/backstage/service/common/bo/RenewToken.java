package com.amway.broadcast.backstage.service.common.bo;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class RenewToken {
    private String api_token;
    private Long expiration;
}
