package com.amway.broadcast.backstage.service.dao.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;
@Data
@TableName(value = "amway_user")
public class AmwayUser implements Serializable{
    private static final long serialVersionUID = 7508819485495153554L;

    @TableId(value="id",type= IdType.AUTO)
    private Integer id;

    @TableField(value = "nickname")
    private String  nickname;

    @TableField(value = "avatar_url")
    private String  avatarUrl;

    @TableField(value = "mobile")
    private String  mobile;

    @TableField(value = "gender")
    private Integer gender;

    @TableField(value = "open_id")
    private String  openId;

    @TableField(value = "member_id")
    private Integer memberId;

    @TableField(value = "create_time")
    private Date createTime;

    @TableField(value = "is_auth")
    private Integer isAuth;
}
