package com.amway.broadcast.backstage.service.dao.mapper;

import com.amway.broadcast.backstage.service.dao.entity.AmwayLog;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

public interface AmwayLogMapper extends BaseMapper<AmwayLog> {
}
