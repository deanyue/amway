package com.amway.broadcast.backstage.service.common.util;

import com.amway.broadcast.backstage.service.api.service.SimpleGenericDataBaseService;
import com.amway.broadcast.backstage.service.common.constant.Constant;
import com.amway.broadcast.backstage.service.dao.entity.AmwayDepartmentMember;
import com.amway.broadcast.backstage.service.dao.entity.AmwayLog;
import com.amway.broadcast.mybatis.autoconfigure.util.WrapperUtils;
import com.vdurmont.emoji.EmojiParser;
import jodd.util.ObjectUtil;
import lombok.extern.slf4j.Slf4j;
import net.logstash.logback.encoder.org.apache.commons.lang3.ObjectUtils;
import net.logstash.logback.encoder.org.apache.commons.lang3.StringUtils;
import org.springframework.cloud.client.ServiceInstance;

import java.util.Date;
@Slf4j
public class LogUtil {


    public static void addLog(SimpleGenericDataBaseService simpleGenericDataBaseService,String membersUserid,String ipAddr,Integer type){
        Date create_time=new Date();
        AmwayDepartmentMember one = null;
        if(StringUtils.isNotBlank(membersUserid)){
            AmwayDepartmentMember amwayDepartmentMember=new AmwayDepartmentMember();
            amwayDepartmentMember.setUserid(membersUserid);
            one = simpleGenericDataBaseService.getOne(WrapperUtils.query(amwayDepartmentMember));
        }
        String content=ObjectUtils.isEmpty(one)? "同步企业微信组织架构日志成功": EmojiParser.parseToHtmlDecimal(one.getName())+"同步企业微信组织架构日志成功";
        AmwayLog build = AmwayLog.builder().content(content).createTime(create_time).ip(ipAddr).type(Constant.LOG_WEIXIN).build();
        log.warn("开始执行添加拉取组织架构日志SQL");
        simpleGenericDataBaseService.save(build);
        log.info("同步日志成功");
    }
}
