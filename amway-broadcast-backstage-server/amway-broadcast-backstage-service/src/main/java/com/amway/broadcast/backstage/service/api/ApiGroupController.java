package com.amway.broadcast.backstage.service.api;

import com.alibaba.fastjson.JSON;
import com.amway.broadcast.backstage.service.api.service.MemberGroupService;
import com.amway.broadcast.backstage.service.common.annotation.BackstageCheck;
import com.amway.broadcast.backstage.service.common.request.AmwayMemberReq;
import com.amway.broadcast.backstage.service.common.request.GroupReq;
import com.amway.broadcast.common.base.Result;
import com.amway.broadcast.common.base.ResultUtils;
import com.amway.broadcast.common.constant.ResultState;
import com.amway.broadcast.common.util.IpUtils;
import com.amway.broadcast.common.util.JwtUtils;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.Date;

@RestController
@CrossOrigin
@Slf4j
@RequestMapping("/vue")
@Api(value = "安利直播部门接口", tags = {"amway"})
@Validated
public class ApiGroupController {

    @Autowired(required = false)
   private MemberGroupService memberGroupService;

    @ApiOperation(value = "部门成员列表", response = Result.class)
    @GetMapping("/memberlist")
    @BackstageCheck
    public Result getDepartmentPage(@NotBlank(message = "token不能为空") @RequestParam("api_token") String api_token,

                                    @NotNull(message = "d_id不能为空")  @RequestParam("d_id") Integer d_id,
                                    @RequestParam(value = "current_page",defaultValue = "1")Integer current_page,
                                    @RequestParam(value = "page_size",defaultValue = "20")Integer page_size,

                                    @RequestParam(value = "name",required = false) String name){
        AmwayMemberReq amwayMemberReq=AmwayMemberReq.builder().api_token(api_token).current_page(current_page).d_id(d_id).page_size(page_size).name(name).build();
        log.info("用户请求部门成员列表，参数："+ JSON.toJSONString(amwayMemberReq)+",账号名:"+ JwtUtils.getUserNameFromToken(api_token));
        return Result.builder().code(ResultState.SUCCESS).data(memberGroupService.getMemberPage(amwayMemberReq)).build();
    }



    @ApiOperation(value = "拉取部门数据", response = Result.class)
    @PostMapping("/getDepartmentData")
    @BackstageCheck
    public Result getDepartmentData(@RequestParam("wxapp_api_token") String wxapp_api_token,String code ,GroupReq groupReq, HttpServletRequest request){
        log.info("用户请求拉取部门数据接口，参数："+ JSON.toJSONString(groupReq)+"，code码是："+code);
        String ipAddr= IpUtils.getIpAddress(request);
        return Result.builder().code(ResultState.SUCCESS).data(memberGroupService.getDepartmentData(wxapp_api_token,code,groupReq,ipAddr)).build();
    }




    @ApiOperation(value = "部门树形图", response = Result.class)
    @GetMapping("/departlist")
    @BackstageCheck
    public Result getDepartmentList(@NotBlank(message = "token不能为空")  @RequestParam("api_token") String api_token){
        log.info("用户请求部门树形图接口，参数："+ api_token+",账号名:"+ JwtUtils.getUserNameFromToken(api_token));
        return Result.builder().code(ResultState.SUCCESS).data(memberGroupService.getMemberList(api_token)).build();
    }

    @ApiOperation(value = "直播群排行榜（查出分享前10名的用户，按照第一名到第十名排好序）", response = Result.class)
    @GetMapping("/ranklist")
    @BackstageCheck
    public Result getRankList(@NotBlank(message = "token不能为空") @RequestParam("api_token") String api_token,
                              @NotNull(message = "group_id不能为空") @RequestParam("group_id") Integer group_id){
        log.info("用户请求直播群排行榜接口，参数api_token："+ api_token+",group_id:"+group_id+",账号名:"+ JwtUtils.getUserNameFromToken(api_token));
        return Result.builder().code(ResultState.SUCCESS).data(memberGroupService.getGroupRankList(api_token,group_id)).build();

    }

    @ApiOperation(value = "直播群列表", response = Result.class)
    @GetMapping("/grouplist")
    @BackstageCheck
    public Result getGroupList(@NotBlank(message = "token不能为空") @RequestParam("api_token") String api_token,
                               @RequestParam(value = "current_page",defaultValue = "1") Integer current_page,
                               @RequestParam(value = "page_size",defaultValue = "20") Integer page_size,
                               @RequestParam(value = "name",required = false) String name,
                               @RequestParam(value = "status",required = false) Integer status,
                               @RequestParam(value = "start_date",required = false) String start_date,
                               @RequestParam(value = "end_date",required = false) String end_date){
        log.info("用户请求直播群列表接口，参数api_token："+ api_token+",current_page:"+current_page+",page_size:"+page_size+",name:"+name+",status:"+status+",start_date:"+start_date+",end_date:"+end_date+",账号名:"+ JwtUtils.getUserNameFromToken(api_token));
        return Result.builder().code(ResultState.SUCCESS).data(memberGroupService.getGroupList(api_token,name,status,start_date,end_date,current_page,page_size)).build();
    }

}
