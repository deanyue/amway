package com.amway.broadcast.backstage.service.common.aspect;

import com.amway.broadcast.backstage.service.common.constant.LoginWhether;
import com.amway.broadcast.common.exception.user.access.NoPermissionToUseTheApiException;
import com.amway.broadcast.common.exception.user.access.UnauthorizedAccessException;
import com.amway.broadcast.common.exception.user.login.UserIdentityVerificationFailedException;
import com.amway.broadcast.common.exception.user.overall.OverallException;
import com.amway.broadcast.common.util.DateUtils;
import com.amway.broadcast.common.util.JwtUtils;
import lombok.extern.slf4j.Slf4j;
import net.logstash.logback.encoder.org.apache.commons.lang3.ObjectUtils;
import net.logstash.logback.encoder.org.apache.commons.lang3.StringUtils;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.redisson.api.RedissonClient;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;
import org.springframework.web.context.request.RequestAttributes;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.Date;
import java.util.concurrent.TimeUnit;

/**
 * @author Lee
 * <p>
 * 接口访问-登录权限访问
 */
@Slf4j
@Aspect
@Order(1)
@Component
public class BackstageAspect {
    @Resource
    private RedissonClient redissonClient;
    @Value("${token.front.time}")
    private int minute;

    /**
     * 切点定义
     */
    @Pointcut("@annotation(com.amway.broadcast.backstage.service.common.annotation.BackstageCheck)")
    private void pointCut() {

    }


    @Around("pointCut()")
    public Object doAround(ProceedingJoinPoint pjp) throws Throwable {
        RequestAttributes requestAttributes = RequestContextHolder.getRequestAttributes();
        HttpServletRequest request = ((ServletRequestAttributes) requestAttributes).getRequest();
        String token = null;
        if(!StringUtils.isBlank(request.getParameter("api_token"))){
            token=request.getParameter("api_token");
        }
//        if(!StringUtils.isBlank(request.getParameter("wxapp_api_token"))){
//            token=request.getParameter("wxapp_api_token");
//        }
        if (!StringUtils.isBlank(token)) {
            try {
                JwtUtils.getClaimsFromToken(token);
            } catch (Exception e) {
                throw new OverallException("无权访问，请联系管理员");
            }
//            String userName = JwtUtils.getUserNameFromToken(token);
            Object o = redissonClient.getBucket(token).get();
            if(ObjectUtils.isEmpty(o)){
                throw new OverallException("请重新登入");
            }

//            long size = redissonClient.getBucket(userName).size();
//            log.info(size+"bucket大小");
//            if(size==0){
//                throw new UserIdentityVerificationFailedException();
//            }else{
//                Date expirationDateFromToken = JwtUtils.getExpirationDateFromToken(token);
//                String expirationToken = DateUtils.getYYYYMMDDHHMMSS(expirationDateFromToken);
//                log.info(expirationToken+"==============================>");
//                Date beforeMinute = DateUtils.beforeHourToNowDate(expirationToken ,minute);
//                log.info("获取过期时间的前"+minute+"分钟"+beforeMinute);
//                //在这个区间请求就刷新token的时间
//                if(DateUtils.belongCalendar(new Date(), beforeMinute, expirationDateFromToken)){
//                    //得到跟新的token和过期时间
//                    String freshToken = JwtUtils.refreshToken(token);
//                    Date expirationDateFromToken1 = JwtUtils.getExpirationDateFromToken(freshToken);
//
//                    //将新的过期时间存到redis中
//                    LoginWhether o = (LoginWhether) redissonClient.getBucket(userName).get();
//                    o.setToken(freshToken);
//                    //设置redis过期时间
//                    Long untilSecond = DateUtils.getUntilSecond(new Date(),expirationDateFromToken1);
//                    redissonClient.getBucket(userName).set(o,untilSecond ,TimeUnit.SECONDS);
//                    log.info(redissonClient.getBucket(userName).get()+"redis存的值");
//                }
//            }
        } else {
            throw new OverallException("无权访问，请联系管理员");
        }
        return pjp.proceed();
    }

}
