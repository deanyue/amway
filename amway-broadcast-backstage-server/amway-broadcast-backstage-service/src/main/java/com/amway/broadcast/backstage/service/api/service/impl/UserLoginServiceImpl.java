package com.amway.broadcast.backstage.service.api.service.impl;

import com.alibaba.fastjson.JSONObject;
import com.amway.broadcast.backstage.service.api.service.UserLoginService;
import com.amway.broadcast.backstage.service.common.bo.LoginBo;
import com.amway.broadcast.backstage.service.common.constant.Constant;
import com.amway.broadcast.backstage.service.common.constant.LoginWhether;
import com.amway.broadcast.backstage.service.common.request.UserInformationReq;
import com.amway.broadcast.backstage.service.dao.entity.AmwayAccount;
import com.amway.broadcast.backstage.service.dao.entity.AmwayLog;
import com.amway.broadcast.backstage.service.domain.AdDomainReq;
import com.amway.broadcast.backstage.service.domain.AdDomainResponse;
import com.amway.broadcast.common.base.Result;
import com.amway.broadcast.common.constant.ResultState;
import com.amway.broadcast.common.exception.user.overall.OverallException;
import com.amway.broadcast.common.util.DateUtils;
import com.amway.broadcast.common.util.JwtUtils;
import com.amway.broadcast.common.util.SHA256;
import com.amway.broadcast.mybatis.autoconfigure.util.WrapperUtils;
import lombok.extern.slf4j.Slf4j;
import net.logstash.logback.encoder.org.apache.commons.lang3.ObjectUtils;
import net.logstash.logback.encoder.org.apache.commons.lang3.StringUtils;
import org.redisson.api.RBucket;
import org.redisson.api.RedissonClient;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import javax.annotation.Resource;
import java.util.Date;
import java.util.concurrent.TimeUnit;

@Slf4j
@Service
public class UserLoginServiceImpl implements UserLoginService {
    @Resource
    private SimpleGenericDataBaseServiceImpl simpleGenericDataBaseService;
    @Resource
    private RedissonClient redissonClient;
    @Value("${LoginCenterConfig.adDomainLoginUrl}")
    private String adDomainLoginUrl;

    @Override
    public Result login(UserInformationReq userInformationReq, String remoteIp) {
        log.info("执行登入获取接口访问业务");
        String encrypt = SHA256.encrypt(userInformationReq.getPassword());
        AmwayAccount amwayAccount = new AmwayAccount();
        amwayAccount.setUserName(userInformationReq.getUsername());
        amwayAccount = simpleGenericDataBaseService.getOne(WrapperUtils.query(amwayAccount));
        log.info("查询出来的用户，{" + amwayAccount + "}");
        boolean isFirst = false;
        if (ObjectUtils.isEmpty(amwayAccount)) {
            log.error("用户不存在");
            if (userInformationReq.getUsername().toLowerCase().indexOf("cn") == 0) {
                amwayAccount = new AmwayAccount();
                HttpHeaders httpHeaders = new HttpHeaders();
                httpHeaders.setContentType(MediaType.APPLICATION_JSON_UTF8);
                RestTemplate restTemplate = new RestTemplate();
                String jsonStr = JSONObject.toJSONString(new AdDomainReq(userInformationReq.getUsername(),userInformationReq.getPassword()));
                log.info("登录时候调用ad域接口的请求参数json={}",jsonStr);
                HttpEntity<String> httpEntity = new HttpEntity<>(jsonStr, httpHeaders);
                ResponseEntity<String> responseEntity = restTemplate.postForEntity(adDomainLoginUrl, httpEntity, String.class);
                log.info("登录时候调用ad域接口的返回值参数={}",responseEntity);

                AdDomainResponse adDomainResponse = null;
                try {
                    adDomainResponse = JSONObject.parseObject(responseEntity.getBody(), AdDomainResponse.class);
                } catch (Exception e) {
                    log.error("登录时候调用ad域接口的返回值参数转换异常",e);
                }

                if (adDomainResponse == null) {
                    throw new OverallException("用户密码错误");
                }
                amwayAccount.setUserName(userInformationReq.getUsername());
                amwayAccount.setPassWord(encrypt);
                amwayAccount.setCreateTime(new Date());
                amwayAccount.setEnabled(1);
                AdDomainResponse.EmbeddedDTO.UserDTO.ProfileDTO user = adDomainResponse.getEmbedded().getUser().getProfile();
                if (user != null) {
                    amwayAccount.setRealName(user.getFirstName() + user.getLastName());
                }
                isFirst = true;
            } else {
                throw new OverallException("用户不存在");
            }
        }
        if (!encrypt.equals(amwayAccount.getPassWord())) {
            log.error("用户密码错误");
            throw new OverallException("用户密码错误");
        }
        if(Constant.ACCOUNT_STOP_STATUS.equals(amwayAccount.getEnabled())){
            throw new OverallException("账号已被禁用,请联系负责人");
        }
        String accessToken = null;
        Date expiration;
        try {
            accessToken = JwtUtils.generateOperationToken(String.valueOf(amwayAccount.getId()), amwayAccount.getUserName(), String.valueOf(1));
            expiration = JwtUtils.getExpirationDateFromToken(accessToken);
            //获取过期时间
            Long untilSecond = DateUtils.getUntilSecond(new Date(), expiration);
            log.info(untilSecond + "时间差");
            redissonClient.getBucket(accessToken).set(accessToken, untilSecond, TimeUnit.SECONDS);
        } catch (Exception e) {
            log.error("生成会话校验码失败");
            throw new OverallException("生成会话校验码失败");
        }
        //跟新数据库中的登入时间
        amwayAccount.setLastLoginTime(System.currentTimeMillis());
        if (isFirst) {
            simpleGenericDataBaseService.save(amwayAccount);
        } else {
            simpleGenericDataBaseService.updateById(amwayAccount);
        }
        log.info("token签发token:{}", accessToken);

        //记录日志
        //create_time
        Date createTime = new Date();
        String content = userInformationReq.getUsername() + "登录成功";

        AmwayLog build = AmwayLog.builder().content(content).createTime(createTime).ip(remoteIp).type(Constant.LOG_LOGIN).build();
        simpleGenericDataBaseService.save(build);
        log.info("后台管理人员登入业务执行完毕");
        return Result.builder().code(ResultState.SUCCESS).msg("登入成功").data(LoginBo.builder().session_id(accessToken).expiration(expiration.getTime()).user_id(String.valueOf(amwayAccount.getId())).realName(amwayAccount.getRealName()).build()).build();
    }
}
