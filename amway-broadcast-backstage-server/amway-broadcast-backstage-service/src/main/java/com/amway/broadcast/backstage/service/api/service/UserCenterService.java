package com.amway.broadcast.backstage.service.api.service;

import com.amway.broadcast.common.base.Result;

public interface UserCenterService {
    /**
     * 小程序用户列表
     * @param apiToken
     * @param currentPage
     * @param pageSize
     * @param nickName
     * @return
     */
    Result userList(String apiToken,Integer currentPage,Integer pageSize,String nickName);
}
