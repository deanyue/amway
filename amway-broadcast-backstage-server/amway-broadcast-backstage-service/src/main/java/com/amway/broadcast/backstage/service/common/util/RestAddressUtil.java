package com.amway.broadcast.backstage.service.common.util;

import com.amway.broadcast.backstage.service.common.constant.Constant;
import com.amway.broadcast.common.constant.WXApiConstant;
import org.apache.commons.lang.StringUtils;

/**
 * 远程地址返回
 */
public class RestAddressUtil {

    /**
     * 获取access_token
     *
     * @param corpid
     * @param corpsecret
     * @return
     */
    public static String getAccessToken(String corpid, String corpsecret) {
        return Constant.GETACCESSTOKEN + "?corpid=" + corpid + "&corpsecret=" + corpsecret;
    }

    /**
     * 获取部门数据
     *
     * @param access_token
     * @param departmentId
     * @return
     */
    public static String getDepartmentData(String access_token, String departmentId) {
        String departUrl = WXApiConstant.LIST_DEPARTMENT + access_token;
        if (!StringUtils.isBlank(departmentId)) {
            departUrl = departUrl + "&id=" + departmentId;
        }
        return departUrl;
    }

    /**
     * 获取部门下员工列表
     *
     * @param access_token
     * @param departmentId
     * @return
     */
    public static String getMemberListByDepartment(String access_token, Integer departmentId, String fetch_child) {
        String memberUrl = WXApiConstant.LIST_USER + access_token + "&department_id=" + departmentId;
        if (StringUtils.isNotBlank(fetch_child)) {
            memberUrl = memberUrl + "&fetch_child=" + fetch_child;
        }
        return memberUrl;
    }
}
