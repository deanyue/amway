package com.amway.broadcast.backstage.service.common.bo;


import lombok.Builder;
import lombok.Data;

import java.io.Serializable;

@Data
@Builder
public class AmwayLogBo implements Serializable {

    private Integer id;

    private String ip;


    private String type;

    private String content;

    private String createTime;
}
