package com.amway.broadcast.backstage.service.common.constant;

import lombok.Data;

@Data
public class QwReturnValue {
    private Integer errcode ;
    private String errmsg;
    private String UserId;
    private String OpenId;
}
