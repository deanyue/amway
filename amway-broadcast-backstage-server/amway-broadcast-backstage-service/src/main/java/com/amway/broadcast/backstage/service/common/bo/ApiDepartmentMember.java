package com.amway.broadcast.backstage.service.common.bo;

import lombok.Builder;
import lombok.Data;

import java.io.Serializable;

@Builder
public class ApiDepartmentMember implements Serializable {
      private static final long serialVersionUID = 3470881553030536328L;
      private Integer id;

      /**
       * 成员UserID
       */
      private String  userid;

      /**
       * 成员名称
       */
      private String  name;

      /**
       * 成员所属部门列表
       */
      private String[]  department;

      /**
       * openid
       */
      private String  open_userid;

      /**
       * 部门内的排序值
       */
      private String[]  order;

    /**
     * 职务
     */
      private String position;

    /**
     * 手机号
     */
      private String mobile;

    /**
     * 性别
     */
      private Integer gender;

    /**
     * 邮箱
     */
      private String email;

    /**
     * 头像
     */
      private String avatar;

    /**
     * 头像缩略图url
     */
      private String thumb_avatar;

    /**
     * 主部门
     */
      private Integer main_department;

    /**
     * 激活状态: 1=已激活，2=已禁用，4=未激活，5=退出企业
     */
      private Integer status;

    /**
     * 初始密码，模拟企业微信登录
     */
      private String password;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getUserid() {
        return userid;
    }

    public void setUserid(String userid) {
        this.userid = userid;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String[] getDepartment() {
        return department;
    }

    public void setDepartment(String[] department) {
        this.department = department;
    }

    public String getOpen_userid() {
        return open_userid;
    }

    public void setOpen_userid(String open_userid) {
        this.open_userid = open_userid;
    }

    public String[] getOrder() {
        return order;
    }

    public void setOrder(String[] order) {
        this.order = order;
    }

    public String getPosition() {
        return position;
    }

    public void setPosition(String position) {
        this.position = position;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public Integer getGender() {
        return gender;
    }

    public void setGender(Integer gender) {
        this.gender = gender;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public String getThumb_avatar() {
        return thumb_avatar;
    }

    public void setThumb_avatar(String thumb_avatar) {
        this.thumb_avatar = thumb_avatar;
    }

    public Integer getMain_department() {
        return main_department;
    }

    public void setMain_department(Integer main_department) {
        this.main_department = main_department;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
