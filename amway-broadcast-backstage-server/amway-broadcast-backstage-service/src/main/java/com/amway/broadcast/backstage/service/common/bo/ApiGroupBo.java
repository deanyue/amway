package com.amway.broadcast.backstage.service.common.bo;

import com.amway.broadcast.backstage.service.dao.entity.AmwayGroup;
import com.baomidou.mybatisplus.annotation.TableField;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

@Data
public class ApiGroupBo extends AmwayGroup{
    /**
     * 进群总个数
     */
    private Integer total_user_count;

    /**
     * 创建群用户姓名
     */
    private String creator_name;

    /**
     * 序号
     */
    private Integer index;


    /**
     * 创建时间
     */
    private String create_time;

}
