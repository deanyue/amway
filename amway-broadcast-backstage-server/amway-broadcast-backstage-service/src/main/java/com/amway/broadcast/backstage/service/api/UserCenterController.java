package com.amway.broadcast.backstage.service.api;

import com.amway.broadcast.backstage.service.api.service.UserCenterService;
import com.amway.broadcast.backstage.service.common.annotation.BackstageCheck;
import com.amway.broadcast.common.base.Result;
import com.amway.broadcast.common.util.JwtUtils;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;

@RestController
@CrossOrigin
@Slf4j
@RequestMapping("/vue")
@Api(value = "用户中心", tags = {"用户中心"})
public class UserCenterController {
    @Resource
    private UserCenterService userCenterService;

    @ApiOperation(value = "小程序用户列表", response = Result.class)
    @GetMapping("userlist")
    @BackstageCheck
    public Result userList(@RequestParam(required = false) String api_token,@RequestParam(value = "current_page",defaultValue = "1") Integer current_page,@RequestParam(value = "page_size",defaultValue = "20") Integer page_size,@RequestParam(required = false) String nickname){
        log.info(JwtUtils.getUserNameFromToken(api_token)+"访问小程序用户列表接口，"+"请求参数：{"+"页码："+current_page+",页面大小："+page_size+"昵称："+nickname+"}");
        return userCenterService.userList(api_token,current_page,page_size,nickname);
    }

}
