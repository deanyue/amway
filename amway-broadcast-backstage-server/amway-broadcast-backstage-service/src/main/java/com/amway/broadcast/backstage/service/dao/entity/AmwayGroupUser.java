package com.amway.broadcast.backstage.service.dao.entity;

import com.amway.broadcast.common.base.BaseAmwayGroupUser;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Builder;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

@Builder
@Data
@TableName(value = "amway_group_user")
public class AmwayGroupUser extends BaseAmwayGroupUser implements Serializable{
    private static final long serialVersionUID = 8765272638469138400L;

    @TableId(value = "id")
    private Integer id;

    @TableField(value = "group_id")
    private Integer groupId;

    @TableField(value = "user_id")
    private Integer userId;

    @TableField(value = "share_user_id")
    private Integer shareUserId;

    @TableField(value = "create_time")
    private Date createTime;

}
