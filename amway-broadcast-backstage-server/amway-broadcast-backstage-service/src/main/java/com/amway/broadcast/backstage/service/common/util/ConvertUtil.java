package com.amway.broadcast.backstage.service.common.util;

import com.alibaba.fastjson.JSON;
import com.amway.broadcast.backstage.service.common.bo.ApiDepartmentMember;
import com.amway.broadcast.backstage.service.common.bo.ApiGetGroupItemBo;
import com.amway.broadcast.backstage.service.common.bo.MemberlistBo;
import com.amway.broadcast.backstage.service.common.constant.Constant;
import com.amway.broadcast.backstage.service.common.request.AccountEdiTaccountReq;
import com.amway.broadcast.backstage.service.dao.entity.AmwayDepartment;
import com.amway.broadcast.backstage.service.dao.entity.AmwayDepartmentMember;
import com.amway.broadcast.backstage.service.dao.mapper.AmwayDepartmentMapper;
import com.amway.broadcast.common.base.Result;
import com.amway.broadcast.common.exception.user.overall.OverallException;
import com.amway.broadcast.common.util.CommonUtils;
import com.amway.broadcast.common.util.MD5Utils;
import com.amway.broadcast.common.util.SHA256;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.sun.org.apache.bcel.internal.generic.RETURN;
import com.vdurmont.emoji.EmojiParser;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.BeanUtils;
import springfox.documentation.spring.web.json.Json;

@Slf4j
public class ConvertUtil {

    /**
     * 转化成部门数据表
     * @param convertValue
     * @return
     */
    public static AmwayDepartment convertDepartment(ApiGetGroupItemBo convertValue){
        AmwayDepartment departmentEntity = AmwayDepartment.builder()
                .orderAm(convertValue.getOrder())
                .departmentId(convertValue.getId())
                .name(convertValue.getName())
                .parentId(convertValue.getParentid()).build();
        return departmentEntity;
    }

    /**
     * 转化成员工数据列表
     * @param amwayDepartmentMapper
     * @param convertValue
     * @param index
     * @return
     */
    public static MemberlistBo convertMemberListBo(AmwayDepartmentMapper amwayDepartmentMapper, AmwayDepartmentMember convertValue, int index){
        MemberlistBo listBo = new MemberlistBo();
        if(!CommonUtils.isEmpty(convertValue)){
            if (null != convertValue.getMainDepartment()) {
                QueryWrapper<AmwayDepartment> queryWrapper=new QueryWrapper<>();
                queryWrapper.eq("department_id",convertValue.getMainDepartment());
                AmwayDepartment amwayDepartment = amwayDepartmentMapper.selectOne(queryWrapper);
                listBo.setDepartment_name(amwayDepartment.getName());
            }
            listBo.setEmail(convertValue.getEmail());
            listBo.setGender(convertValue.getGender() == 0 ? "未定义" : (convertValue.getGender() == 1 ? "男" : "女"));
            listBo.setId(String.valueOf(convertValue.getId()));
            listBo.setMobile(convertValue.getMobile());
            listBo.setName(EmojiParser.parseToUnicode(convertValue.getName()));
            listBo.setPosition(convertValue.getPosition());
            listBo.setIndex(index);
            listBo.setThumb_avatar(convertValue.getThumbAvatar());
            listBo.setStatus(convertValue.getStatusAm());
        }
        return listBo;
    }

    /**
     * 网上的数据向数据库字段转变
     * @param memberItem
     * @return
     */
    public static AmwayDepartmentMember convertDeparmentMemberBo(ApiDepartmentMember memberItem) {
        AmwayDepartmentMember entity = new AmwayDepartmentMember();
        BeanUtils.copyProperties(memberItem, entity);
        log.info("人员name:" + memberItem.getName());
        entity.setName(EmojiParser.parseToHtmlDecimal(entity.getName()));
        String[] orderArr = memberItem.getOrder();
        if (orderArr != null && orderArr.length > 0) {
            entity.setOrderAm(String.join(",", orderArr));
        }
        entity.setMainDepartment(memberItem.getMain_department());
        entity.setThumbAvatar(memberItem.getThumb_avatar());
        entity.setPasswordAm(SHA256.encrypt("eyJpYX"));
        entity.setStatusAm(memberItem.getStatus());
        String[] departmentArr = memberItem.getDepartment();
        if (departmentArr != null && departmentArr.length > 0) {
            entity.setDepartment(String.join(",", departmentArr));
        }
        return entity;
    }

    /**
     * 添加编辑用户密码校验
     */
    public static void  passwordCheck(AccountEdiTaccountReq accountEdiTaccountReq){
        if(!accountEdiTaccountReq.getPassword().matches(Constant.PASSWORD_CHECK)){
            throw new OverallException("密码至少含有大、小写字母、特殊字符和阿拉伯数字3种");
        }
        if(!accountEdiTaccountReq.getPassword().matches(Constant.PASSWORD_LENGTH)){
            throw new OverallException("密码长度至少8位");
        }
    }

}
