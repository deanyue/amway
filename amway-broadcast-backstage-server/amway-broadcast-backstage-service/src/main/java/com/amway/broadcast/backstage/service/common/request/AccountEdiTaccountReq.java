package com.amway.broadcast.backstage.service.common.request;

import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

@Data
public class AccountEdiTaccountReq {
    private String api_token;

    private Integer id;

    @Pattern(regexp = "^([a-z]|[A-Z])[\\w_]{5,11}$", message = "账号要以字母开头，长度6-11之间")
    @NotBlank(message = "账号不能为空")
    private String username;

    private String password;

    @NotBlank(message = "姓名不能为空")
    private String realname;

    @NotNull(message = "是否启用")
    private Integer enabled;

}
