package com.amway.broadcast.backstage.service.api;

import com.amway.broadcast.backstage.service.api.service.UserLoginService;
import com.amway.broadcast.backstage.service.common.request.UserInformationReq;
import com.amway.broadcast.common.base.Result;
import com.amway.broadcast.common.util.IpUtils;
import com.amway.broadcast.common.util.JwtUtils;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

@RestController
@CrossOrigin
@Slf4j
@Api(value = "登入API接口", tags = {"amway登入"})
@RequestMapping("/vue")
public class ApiUserLoginController {
    @Resource
    private UserLoginService userLogin;

    @ApiOperation(value = "登陆", response = Result.class)
    @PostMapping("login")
    public Result login(@Validated UserInformationReq userInformationReq, HttpServletRequest request){
        String remoteIp= IpUtils.getIpAddress(request);
        log.info(userInformationReq.getUsername()+"访问登入接口"+"ip:"+remoteIp);
        return userLogin.login(userInformationReq,remoteIp);
    }
}
