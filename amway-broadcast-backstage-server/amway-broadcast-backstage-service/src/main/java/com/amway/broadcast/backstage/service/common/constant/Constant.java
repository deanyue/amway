package com.amway.broadcast.backstage.service.common.constant;

public class Constant {
    /**
     * 登录日志
     */
    public static final Integer LOG_LOGIN=1;

    /**
     * 同步企业微信组织架构日志
     */
    public static final Integer LOG_WEIXIN=2;

    /**
     * 类型1:男;
     */
    public static final  Integer GLOBAL_TYPR=1;

    /**
     * 默认-1查询所有类型日志
     */
    public static final Integer  SELECT_LOG_TYPE=-1;

    /**
     * 账号的禁用状态
     */
    public static final Integer ACCOUNT_STOP_STATUS=0;

    /**
     * 密码正则校验
     */
    public static  final  String PASSWORD_CHECK="^(?![a-zA-Z]+$)(?![a-z\\d]+$)(?![a-z!@#\\$%]+$)(?![A-Z\\d]+$)(?![A-Z!@#\\$%]+$)(?![\\d!@#\\$%]+$)[a-zA-Z\\d!@#\\$%]+$";

    /**
     * 密码长度校验
     */
    public static  final  String PASSWORD_LENGTH="^.{8,}$";

    /**
     * 直播全局编辑添加校验
     */
    public static  final  String  BROADCAST_CHECK="^[0-9]*[1-9][0-9]*$";
    /**
     * 拉取部门数据获取access_token接口
     */
    public static final String GETACCESSTOKEN="https://qyapi.weixin.qq.com/cgi-bin/gettoken";

    /**
     * 获取访问用户身份地址
     */
    public static final String QW_USER_INFO="https://qyapi.weixin.qq.com/cgi-bin/user/getuserinfo";

    /**
     * 记录定时任务的ip
     */
    public static final String JOBIP="job";

}
