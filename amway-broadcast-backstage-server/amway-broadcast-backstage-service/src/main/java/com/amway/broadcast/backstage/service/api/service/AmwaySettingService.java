package com.amway.broadcast.backstage.service.api.service;

import com.amway.broadcast.backstage.service.common.request.AccountEdiTaccountReq;
import com.amway.broadcast.backstage.service.common.request.AddBroadcastNumReq;
import com.amway.broadcast.common.base.Result;

public interface AmwaySettingService {

    /**
     * 直播全局设置新增/编辑保存
     * @param addBroadcastNumReq
     * @return
     */
    Result settingEedit(AddBroadcastNumReq addBroadcastNumReq);

    /**
     *直播全局设置预览
     * @param apiToken
     * @return
     */
    Result settingVedit(String apiToken);

    /**
     * 系统日志列表
     * @param apiToken
     * @param currentPage
     * @param pageSize
     * @param type
     * @return
     */
    Result logList(String apiToken, Integer currentPage, Integer pageSize, Integer type);

    /**
     * 账户列表
     * @param apiToken
     * @param currentPage
     * @param pageSize
     * @return
     */
    Result accountList(String apiToken, Integer currentPage, Integer pageSize);

    /**
     * 账户详细信息
     * @param apiToken
     * @param id
     * @return
     */
    Result accountInfo(String apiToken, Integer id); //

    /**
     * 账户添加/编辑
     * @param accountEdiTaccountReq
     * @return
     */
    Result editAccount(AccountEdiTaccountReq accountEdiTaccountReq);

}
