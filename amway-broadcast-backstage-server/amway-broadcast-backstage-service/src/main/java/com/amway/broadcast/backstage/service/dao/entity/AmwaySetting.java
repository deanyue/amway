package com.amway.broadcast.backstage.service.dao.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

@Getter
@Setter
@TableName(value = "amway_setting")
public class AmwaySetting implements Serializable {
    private static final long serialVersionUID = 2588260998858063638L;

    @TableId(type = IdType.AUTO )
    private Integer id;

    @TableField(value = "max_group_per_day")
    private Integer maxGroupPerDay;

    @TableField(value = "max_group_total")
    private Integer maxGroupTotal;

    @TableField(value = "open_msg_check")
    private Integer openMsgCheck;

    @TableField(value = "open_video_upload")
    private Integer openVideoUpload;

    @TableField(value = "max_admin_num")
    private Integer maxAdminNum;
}
