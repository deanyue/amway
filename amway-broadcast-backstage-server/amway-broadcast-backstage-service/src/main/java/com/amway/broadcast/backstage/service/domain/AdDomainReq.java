package com.amway.broadcast.backstage.service.domain;

import lombok.Data;

import java.io.Serializable;

/**
 * @author liujch
 * @description
 * @Package com.amway.broadcast.applets.service.dao.entity
 * @date 2020年12月24日 15:33
 */

@Data
public class AdDomainReq implements Serializable {

    private static final long serialVersionUID = 460806293099490140L;
    private String username;

    private String password;

    private Options options;

    public AdDomainReq(String username, String password) {
        this.username = username;
        this.password = password;
        this.options = new Options();
    }

    @Data
    static class Options{
        private boolean multiOptionalFactorEnroll = true;

        private boolean warnBeforePasswordExpired = true;
    }
}
