package com.amway.broadcast.backstage.service.dao.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.io.Serializable;

@Data
@TableName(value = "amway_department_member")
public class AmwayDepartmentMember  implements Serializable {
      private static final long serialVersionUID = 3470881553030536328L;

      @TableId(value="id",type= IdType.AUTO)
      private Long id;

      /**
       * 成员UserID
       */
      @TableField(value = "userid")
      private String  userid;

      /**
       * 成员名称
       */
      @TableField(value = "name")
      private String  name;

      /**
       * 成员所属部门列表
       */
      @TableField(value = "department")
      private String  department;

      /**
       * openid
       */
      @TableField(value = "open_userid")
      private String  openUserid;

      /**
       * 部门内的排序值
       */
      @TableField(value = "order_am")
      private String  orderAm;

    /**
     * 职务
     */
      @TableField(value = "position")
      private String position;

    /**
     * 手机号
     */
     @TableField(value = "mobile")
      private String mobile;

    /**
     * 性别
     */
    @TableField(value = "gender")
      private Integer gender;

    /**
     * 邮箱
     */
    @TableField(value = "email")
      private String email;

    /**
     * 头像
     */
    @TableField(value = "avatar")
      private String avatar;

    /**
     * 头像缩略图url
     */
    @TableField(value = "thumb_avatar")
      private String thumbAvatar;

    /**
     * 主部门
     */
    @TableField(value = "main_department")
      private Integer mainDepartment;

    /**
     * 激活状态: 1=已激活，2=已禁用，4=未激活，5=退出企业
     */
    @TableField(value = "status_am")
      private Integer statusAm;

    /**
     * 初始密码，模拟企业微信登录
     */
    @TableField(value = "password_am")
      private String passwordAm;
}
