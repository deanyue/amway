package com.amway.broadcast.backstage.service.common.bo;

import lombok.Data;

@Data
public class DetailsBo<T> {
    private T info;
//    private RenewToken renewToken;
}
