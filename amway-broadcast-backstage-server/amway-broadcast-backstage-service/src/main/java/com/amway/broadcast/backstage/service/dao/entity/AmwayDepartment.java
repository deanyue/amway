package com.amway.broadcast.backstage.service.dao.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Builder;
import lombok.Data;

import java.io.Serializable;

@Builder
@Data
@TableName(value = "amway_department")
public class AmwayDepartment implements Serializable {
    private static final long serialVersionUID = 2062037714340599801L;

    @TableId(value="id",type= IdType.AUTO)
    private Integer id;

    /**
     * 部门id
     */
    @TableField(value = "department_id")
    private Integer departmentId;

    /**
     * 部门名称
     */
    @TableField(value = "name")
    private String name;

    /**
     * 父部门id
     */
    @TableField(value = "parent_id")
    private Integer parentId;

    /**
     * 在父部门中的次序值
     */
    @TableField(value = "order_am")
    private Integer orderAm;
}
