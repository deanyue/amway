package com.amway.broadcast.backstage.service.common.bo;

import lombok.Data;

import java.io.Serializable;

@Data
public class ApiTokenBo implements Serializable{

    private Integer errcode;
    private String  errmsg;
    private String access_token;
    private Long expires_in;
}
