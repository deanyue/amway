package com.amway.broadcast.backstage.service.common.request;

import lombok.Data;

import javax.validation.constraints.NotBlank;

@Data
public class UserInformationReq {
    @NotBlank(message = "用户账号不能为空")
    private String username;
    @NotBlank(message = "密码不能为空")
    private String password;
}
