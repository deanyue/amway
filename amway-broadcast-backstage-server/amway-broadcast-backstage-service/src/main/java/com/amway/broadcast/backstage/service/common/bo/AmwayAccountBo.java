package com.amway.broadcast.backstage.service.common.bo;

import lombok.Builder;
import lombok.Data;

import java.util.Date;

@Data
@Builder
public class AmwayAccountBo {
    private Integer id;

    private String username;

    private String realname;

    private Integer enabled;

    private String create_time;
}
