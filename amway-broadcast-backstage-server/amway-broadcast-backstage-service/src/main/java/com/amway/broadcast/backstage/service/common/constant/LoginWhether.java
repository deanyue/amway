package com.amway.broadcast.backstage.service.common.constant;

import lombok.Builder;
import lombok.Data;

import java.io.Serializable;

@Data
@Builder
public class LoginWhether implements Serializable {
    private static final long serialVersionUID = -3044639303860592226L;
    private String ip;
    private String token;
}
