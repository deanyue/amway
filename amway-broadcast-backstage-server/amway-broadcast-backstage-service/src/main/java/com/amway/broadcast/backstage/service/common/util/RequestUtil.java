package com.amway.broadcast.backstage.service.common.util;

import com.alibaba.fastjson.JSON;
import com.amway.broadcast.backstage.service.common.bo.ApiDepartmentMember;
import com.amway.broadcast.backstage.service.common.bo.ApiGetDepartmentBo;
import com.amway.broadcast.backstage.service.common.bo.ApiMemberBo;
import com.amway.broadcast.backstage.service.common.bo.ApiTokenBo;
import com.amway.broadcast.backstage.service.common.constant.Constant;
import com.amway.broadcast.backstage.service.common.constant.QwReturnValue;
import com.amway.broadcast.backstage.service.common.request.GroupReq;
import com.amway.broadcast.common.exception.user.overall.OverallException;
import com.amway.broadcast.common.util.CommonUtils;
import com.amway.broadcast.common.util.JwtUtils;
import com.google.gson.Gson;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang.StringUtils;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import java.util.List;
@Slf4j
public class RequestUtil {

    /**
     * 获取access_token
     * @param corpid
     * @param corpsecret
     * @return
     */
    public static ApiTokenBo getTokenBo(String corpid,String corpsecret){
        String accessTokenUrl = RestAddressUtil.getAccessToken(corpid,corpsecret);
        RestTemplate tokenTemplate = new RestTemplate();
        ResponseEntity<String> responseToken = tokenTemplate.exchange(accessTokenUrl, HttpMethod.GET, null, String.class);
        if (responseToken != null && responseToken.getStatusCode() == HttpStatus.OK) {
            log.info("获取access_token返回成功");
            String sessionData = responseToken.getBody();
            Gson gson = new Gson();
            ApiTokenBo tokenBo = gson.fromJson(sessionData, ApiTokenBo.class);
            log.info("获取access_token返回数据:"+JSON.toJSONString(tokenBo));
            return tokenBo;
        }
        else{
            log.error("获取access_token返回失败，无法获取access_token");
            throw new OverallException("无法获取token");
        }
    }

    /**
     * 获取部门数据
     * @param access_token
     * @param groupId
     * @return
     */
    public static ApiGetDepartmentBo getDepartmentBo(String access_token,String groupId){

        String departUrl = RestAddressUtil.getDepartmentData(access_token, groupId);
        RestTemplate restDepart = new RestTemplate();

        ResponseEntity<String> responseEntity = restDepart.exchange(departUrl, HttpMethod.GET, null, String.class);
        if (responseEntity != null && responseEntity.getStatusCode() == HttpStatus.OK) {
            log.info("获取部门数据成功");
            String departData = responseEntity.getBody();
            Gson departgson = new Gson();
            ApiGetDepartmentBo memberBo = departgson.fromJson(departData, ApiGetDepartmentBo.class);
            return memberBo;
        }
        else{
            log.info("无法获取部门数据");
            return null;
        }

    }


    public static List<ApiDepartmentMember> getDepartmentMemberListBo(String access_token,Integer departmentId,String fetchChild) {

        String memberUrl = RestAddressUtil.getMemberListByDepartment(access_token, departmentId, fetchChild);
        RestTemplate departmentTemplate = new RestTemplate();
        //进行网络请求
        ResponseEntity<String> departmentResponse = departmentTemplate.exchange(memberUrl, HttpMethod.GET, null, String.class);
        if (departmentResponse != null && departmentResponse.getStatusCode() == HttpStatus.OK) {
            log.info("获取部门"+departmentId+"下的员工列表成功");
            String returnData = departmentResponse.getBody();
            Gson departGson = new Gson();
            //解析从微信服务器获得的数据并进行解析
            ApiMemberBo memberGroupBo = departGson.fromJson(returnData, ApiMemberBo.class);
            //请求成功
            if (0 == memberGroupBo.getErrcode()) {
                List<ApiDepartmentMember> memberlist = memberGroupBo.getUserlist();
                return memberlist;
            }
        }
        else{
            log.info("获取部门"+departmentId+"下的员工列表失败");
        }
        return null;
    }


    /**
     * 根据code码来判断该用户是否为该企业成员
     */
    public static String  IdentityCheck(ApiTokenBo tokenBo,String code){
        //调用企业微信判断是否是该企业成员
        if(!CommonUtils.isEmpty(tokenBo) && !StringUtils.isBlank(tokenBo.getAccess_token())){
            //?access_token=ACCESS_TOKEN&code=CODE
            String url= Constant.QW_USER_INFO+"?access_token="+tokenBo.getAccess_token()+"&code="+code;
            //开始发起远程调用
            RestTemplate restTemplate = new RestTemplate ();
            //进行网络请求
            ResponseEntity<String> exchange = restTemplate.exchange(url, HttpMethod.GET, null, String.class);
            if (exchange != null && exchange.getStatusCode () == HttpStatus.OK){
                String sessionData = exchange.getBody ();
                Gson gson = new Gson ();
                //解析从微信服务器获得的数据并进行解析
                QwReturnValue qwReturnValue = gson.fromJson(sessionData, QwReturnValue.class);
                if(!Constant.ACCOUNT_STOP_STATUS.equals(qwReturnValue.getErrcode())){
                    log.error("调用企业微信api,验证用户身份失败");
                    throw new OverallException("调用企业微信api,验证用户身份失败");
                }
                if(net.logstash.logback.encoder.org.apache.commons.lang3.StringUtils.isNotBlank( qwReturnValue.getOpenId())){
                    return null;
                }
                return qwReturnValue.getUserId();
            }
        }
        throw new OverallException("调用企业微信api失败");

    }

}
