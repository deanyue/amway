package com.amway.broadcast.backstage.service.domain;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.List;

/**
 * @author liujch
 * @description
 * @Package com.amway.broadcast.backstage.service.dao.entity
 * @date 2020年12月24日 17:03
 */

@NoArgsConstructor
@Data
public class AdDomainResponse implements Serializable {

    private static final long serialVersionUID = -2741308594191423862L;

    @JsonProperty("expiresAt")
    private String expiresAt;
    @JsonProperty("status")
    private String status;
    @JsonProperty("sessionToken")
    private String sessionToken;
    @JsonProperty("_embedded")
    private EmbeddedDTO embedded;
    @JsonProperty("_links")
    private LinksDTO links;

    @NoArgsConstructor
    @Data
    public static class EmbeddedDTO {

        @JsonProperty("user")
        private UserDTO user;

        @NoArgsConstructor
        @Data
        public static class UserDTO {

            @JsonProperty("id")
            private String id;
            @JsonProperty("profile")
            private ProfileDTO profile;

            @NoArgsConstructor
            @Data
            public static class ProfileDTO {
                /**
                 * login : CNVISL65@na.intranet.msd
                 * firstName : Jing
                 * lastName : Yan
                 * locale : en
                 * timeZone : America/Los_Angeles
                 */

                @JsonProperty("login")
                private String login;
                @JsonProperty("firstName")
                private String firstName;
                @JsonProperty("lastName")
                private String lastName;
                @JsonProperty("locale")
                private String locale;
                @JsonProperty("timeZone")
                private String timeZone;
            }
        }
    }

    @NoArgsConstructor
    @Data
    public static class LinksDTO {

        @JsonProperty("cancel")
        private CancelDTO cancel;

        @NoArgsConstructor
        @Data
        public static class CancelDTO {

            @JsonProperty("href")
            private String href;
            @JsonProperty("hints")
            private HintsDTO hints;

            @NoArgsConstructor
            @Data
            public static class HintsDTO {
                @JsonProperty("allow")
                private List<String> allow;
            }
        }
    }
}
