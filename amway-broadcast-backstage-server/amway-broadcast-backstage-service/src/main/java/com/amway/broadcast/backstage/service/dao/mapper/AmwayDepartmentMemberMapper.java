package com.amway.broadcast.backstage.service.dao.mapper;

import com.amway.broadcast.backstage.service.dao.entity.AmwayDepartmentMember;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

public interface AmwayDepartmentMemberMapper  extends BaseMapper<AmwayDepartmentMember> {

    /**
     * 查询部门成员分页
     * @param departmentMemberPage
     * @param d_id
     * @param name
     * @return
     */
    @Select("select * from amway_department_member where 1=1 and case when #{d_id} is not null and #{d_id}<>-1 then CONCAT(',',department,',') like CONCAT('%,',#{d_id},',%') else true end and case when #{name} is not null then `name` like CONCAT('%',#{name},'%') else true end")
    IPage<AmwayDepartmentMember> selectDepartmentMemberPage(Page<AmwayDepartmentMember> departmentMemberPage, @Param(value = "d_id")Integer d_id, @Param(value = "name")String name);
}
