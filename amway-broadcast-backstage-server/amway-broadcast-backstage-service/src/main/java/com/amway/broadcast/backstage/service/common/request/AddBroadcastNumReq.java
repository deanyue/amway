package com.amway.broadcast.backstage.service.common.request;

import lombok.Data;

import javax.validation.constraints.*;

@Data
public class AddBroadcastNumReq {
    private Integer id;

    private String api_token;

    @NotNull(message = "每人每天最多可以开多少场直播次数不能为空")
    @Min(value = 1,message = "开播场次的次数设置不能小于1")
    private Integer max_group_per_day;

    @NotNull (message = "每人累计可以开直播次数不能为空")
    @Min(value = 1,message = "每人累计可以开直播次数设置不能小于1")
    private Integer max_group_total;

    private Integer max_admin_num;

    private Integer open_msg_check;
    private Integer open_video_upload;
}
