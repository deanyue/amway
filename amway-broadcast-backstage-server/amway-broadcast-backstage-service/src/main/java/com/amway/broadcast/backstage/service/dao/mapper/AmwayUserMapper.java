package com.amway.broadcast.backstage.service.dao.mapper;

import com.amway.broadcast.backstage.service.dao.entity.AmwayUser;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

public interface AmwayUserMapper  extends BaseMapper<AmwayUser> {
}
