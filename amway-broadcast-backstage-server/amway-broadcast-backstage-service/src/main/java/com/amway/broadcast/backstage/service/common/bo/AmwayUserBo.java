package com.amway.broadcast.backstage.service.common.bo;

import lombok.Builder;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

@Data
@Builder
public class AmwayUserBo implements Serializable{

    private Integer id;

    private String  nickname;

    private String  avatar_url;

    private String gender;

    /**
     * 组织架构成员ID号
     */
    private Integer member_id;

    private String create_time;

}
