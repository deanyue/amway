package com.amway.broadcast.backstage.service.dao.entity;

import com.baomidou.mybatisplus.annotation.*;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.io.Serializable;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Date;

@Getter
@Setter
@TableName(value = "amway_account")
@ToString
public class AmwayAccount implements Serializable {
    private static final long serialVersionUID = 3688581643423828676L;

    @TableId(value = "id",type = IdType.AUTO)
    private Integer id;

    @TableField(value = "username")
    private String userName;

    @TableField(value = "password_am")
    private String passWord;

    @TableField(value = "realname")
    private String realName;

    @TableField(value = "last_login_time")
    private Long lastLoginTime;

    @TableField(value = "enabled")
    private Integer enabled;

    @TableField(value = "create_time", exist = true)
    private Date createTime;



}
