package com.amway.broadcast.backstage.service.dao.mapper;

import com.amway.broadcast.backstage.service.dao.entity.AmwaySetting;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

public interface AmwaySettingMapper extends BaseMapper<AmwaySetting> {
}
