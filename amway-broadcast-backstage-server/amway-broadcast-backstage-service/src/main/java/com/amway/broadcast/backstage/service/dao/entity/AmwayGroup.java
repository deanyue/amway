package com.amway.broadcast.backstage.service.dao.entity;

import com.amway.broadcast.common.base.BaseAmwayGroup;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;
@Data
@TableName(value = "amway_group")
public class AmwayGroup implements Serializable{
    private static final long serialVersionUID = 9149313529291798474L;

    @TableId(value="id",type= IdType.AUTO)
    private Long id;

    @TableField(value = "member_id")
    private Long memberId;

    @TableField(value = "im_group_id")
    private String imGroupId;

    @TableField(value = "name")
    private String  name;

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone="GMT+8")
    @JsonProperty("start_time")
    @TableField(value = "start_time")
    private Date  startTime;

    @TableField(value = "expire")
    private Integer expire;

    @TableField(value = "limit_type")
    private Integer limitType;

    @TableField(value = "remark")
    private String  remark;

    @JsonProperty("status")
    @TableField(value = "status_am")
    private Integer statusAm;

    @JsonProperty("is_delete")
    @TableField(value = "is_delete")
    private Integer isDelete;

    @TableField(value = "create_time")
    private Date    createTime;
}
