package com.amway.broadcast.backstage.service.common.request;

import lombok.Builder;
import lombok.Data;
@Builder
@Data
public class AmwayMemberReq {

    /**
     * api_token
     */
    private String api_token;

    /**
     * 部门id号 查询全部：-1
     */
    private Integer d_id;

    /**
     * 当前页数
     */
    private Integer current_page;

    /**
     * 每页条数
     */
    private Integer page_size;

    /**
     * 查询条件：姓名
     */
    private String name;
}
