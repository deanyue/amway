package com.amway.broadcast.backstage.service.api.service.impl;

import com.amway.broadcast.backstage.service.api.service.AmwaySettingService;
import com.amway.broadcast.backstage.service.common.bo.*;
import com.amway.broadcast.backstage.service.common.constant.Constant;
import com.amway.broadcast.backstage.service.common.request.AccountEdiTaccountReq;
import com.amway.broadcast.backstage.service.common.request.AddBroadcastNumReq;
import com.amway.broadcast.backstage.service.common.util.ConvertUtil;
import com.amway.broadcast.backstage.service.common.util.NewTokenTime;
import com.amway.broadcast.backstage.service.dao.entity.AmwayAccount;
import com.amway.broadcast.backstage.service.dao.entity.AmwayLog;
import com.amway.broadcast.backstage.service.dao.entity.AmwaySetting;
import com.amway.broadcast.common.base.Result;
import com.amway.broadcast.common.constant.ResultState;
import com.amway.broadcast.common.exception.user.overall.OverallException;
import com.amway.broadcast.common.util.CommonUtils;
import com.amway.broadcast.common.util.DateUtils;
import com.amway.broadcast.common.util.SHA256;
import com.amway.broadcast.mybatis.autoconfigure.util.WrapperUtils;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.vdurmont.emoji.EmojiParser;
import lombok.extern.slf4j.Slf4j;
import net.logstash.logback.encoder.org.apache.commons.lang3.ObjectUtils;
import net.logstash.logback.encoder.org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

@Service
@Slf4j
public class AmwaySettingServiceImpl implements AmwaySettingService {
    @Resource
    private SimpleGenericDataBaseServiceImpl simpleGenericDataBaseService;
    @Resource
    private NewTokenTime newTokenTime;
    @Override
    public Result settingEedit(AddBroadcastNumReq addBroadcastNumReq) {
        log.info("开始执行直播次数限制添加编辑业务层");
        AmwaySetting amwaySetting=new AmwaySetting();
        amwaySetting.setMaxGroupTotal(addBroadcastNumReq.getMax_group_total());
        amwaySetting.setMaxGroupPerDay(addBroadcastNumReq.getMax_group_per_day());
        amwaySetting.setMaxAdminNum(addBroadcastNumReq.getMax_admin_num());
        amwaySetting.setOpenMsgCheck(addBroadcastNumReq.getOpen_msg_check());
        amwaySetting.setOpenVideoUpload(addBroadcastNumReq.getOpen_video_upload());
        if (CommonUtils.isEmpty(addBroadcastNumReq.getId())) {
            //新增
            AmwaySetting amwaySetting1=new AmwaySetting();
            List<AmwaySetting> list = simpleGenericDataBaseService.list(amwaySetting1);
            if(!list.isEmpty()){
                throw new OverallException("添加直播限制条数已上限，请去编辑");
            }
            simpleGenericDataBaseService.save(amwaySetting);
            return Result.builder().code(ResultState.SUCCESS).msg("添加直播次数限制成功").build();
        }

        amwaySetting.setId(addBroadcastNumReq.getId());
        try {
            simpleGenericDataBaseService.updateById(amwaySetting);
        } catch (Exception e) {
            log.error("编辑直播限制失败");
            throw new OverallException("编辑失败");
        }
        log.info("直播次数限制添加编辑业务执行完毕");
        return Result.builder().code(ResultState.SUCCESS).msg("编辑成功").build();
    }

    @Override
    public Result settingVedit(String apiToken) {
        log.info("开始执行预览直播的次数限制业务层");
        AmwaySetting amwaySetting=new AmwaySetting();
        AmwaySetting one = simpleGenericDataBaseService.getOne(WrapperUtils.query(amwaySetting));
        DetailsBo<GlobalSettingsPreview> detailsBo=new DetailsBo();
        if(!ObjectUtils.isEmpty(one)){
            GlobalSettingsPreview build = GlobalSettingsPreview.builder()
                    .max_group_per_day(one.getMaxGroupPerDay())
                    .max_group_total(one.getMaxGroupTotal())
                    .id(one.getId()).max_admin_num(Optional.ofNullable(one.getMaxAdminNum()).orElse(0))
                    .open_msg_check(Optional.ofNullable(one.getOpenMsgCheck()).orElse(0))
                    .open_video_upload(Optional.ofNullable(one.getOpenVideoUpload()).orElse(0))
                    .build();
            detailsBo.setInfo(build);
        }
//        RenewToken renewToken = newTokenTime.tokenTime(apiToken);
//        detailsBo.setRenewToken(renewToken);
        log.info("预览直播的次数限制业务层执行完毕");
        return Result.builder().code(ResultState.SUCCESS).data(detailsBo).build();
    }

    @Override
    public Result logList(String apiToken, Integer currentPage, Integer pageSize, Integer type) {
        log.info("开始执行系统日志列表业务层");
        List <AmwayLogBo> lists=new ArrayList<>();
        Page<AmwayLog> page=new Page(currentPage,pageSize);
        IPage<AmwayLog> page1;
        if(Constant.SELECT_LOG_TYPE.equals(type)){
           page1= simpleGenericDataBaseService.page(page,WrapperUtils.query(AmwayLog.builder().build()).orderByDesc("create_time"));
        }else {
            page1 = simpleGenericDataBaseService.page(page, WrapperUtils.query(AmwayLog.builder().type(type).build()).orderByDesc("create_time"));
        }
        List<AmwayLog> record = page1.getRecords();
        record.forEach(am->{
            AmwayLogBo build = AmwayLogBo.builder()
                    .id(am.getId())
                    .ip(am.getIp())
                    .type(Constant.LOG_LOGIN.equals(am.getType())?"登录日志":"同步日志")
                    .content(EmojiParser.parseToUnicode(am.getContent()))
                    .createTime(DateUtils.getYYYYMMDDHHMMSS(am.getCreateTime()))
                    .build();
            lists.add(build);
        });
//        RenewToken renewToken = newTokenTime.tokenTime(apiToken);
        log.info("系统日志列表业务层执行完毕");
        return new Result(ResultState.SUCCESS,new BasicPage<AmwayLogBo>(lists,page1.getTotal()));
    }

    @Override
    public Result accountList(String apiToken, Integer currentPage, Integer pageSize) {
        log.info("开始执行账户列表列表业务层");
        Page<AmwayAccount> page=new Page(currentPage,pageSize);
        IPage<AmwayAccount> page1 = simpleGenericDataBaseService.page(page,WrapperUtils.query(new AmwayAccount()).orderByDesc("create_time"));

        List<AmwayAccount> records = page1.getRecords();
        List <AmwayAccountBo> lists=new ArrayList<>();
        records.forEach(am->{
            AmwayAccountBo build = AmwayAccountBo.builder()
                    .id(am.getId())
                    .username(am.getUserName())
                    .realname(am.getRealName())
                    .enabled(am.getEnabled())
                    .create_time(DateUtils.getYYYYMMDDHHMMSS(am.getCreateTime()))
                    .build();
            lists.add(build);
        });
//        RenewToken renewToken = newTokenTime.tokenTime(apiToken);
        log.info("账户列表列表业务层执行完毕");
        return new Result(ResultState.SUCCESS,new BasicPage<AmwayAccountBo>(lists,page1.getTotal()));
    }

    @Override
    public Result accountInfo(String apiToken, Integer id) {
        log.info("开始执行账户详情业务层");
        AmwayAccount amwayAccount=new AmwayAccount();
        amwayAccount.setId(id);
        AmwayAccount accountInfo = simpleGenericDataBaseService.getOne(WrapperUtils.query(amwayAccount));
        log.info("根据id查询账户详情：{"+accountInfo+"}");
        if(ObjectUtils.isEmpty(accountInfo)){
            throw new OverallException("该用户不存在");
        }
        AmwayAccountBo build = AmwayAccountBo.builder()
                .id(accountInfo.getId())
                .username(accountInfo.getUserName())
                .realname(accountInfo.getRealName())
                .enabled(accountInfo.getEnabled())
                .build();
        DetailsBo<AmwayAccountBo> detailsBo=new DetailsBo<>();
        detailsBo.setInfo(build);
        log.info("账户详情业务层执行完毕");
        return new Result(ResultState.SUCCESS,detailsBo);
    }

    @Override
    public Result editAccount(AccountEdiTaccountReq accountEdiTaccountReq) {
        log.info("开始执行账户的编辑添加业务层");
        AmwayAccount amwayAccount=new AmwayAccount();
        amwayAccount.setUserName(accountEdiTaccountReq.getUsername());
        if(CommonUtils.isEmpty(accountEdiTaccountReq.getId())){
            //说明是添加用户
            log.info("开始执行添加用户业务");
            if(StringUtils.isBlank(accountEdiTaccountReq.getPassword())){
                throw new OverallException("密码不能为空");
            }
            //查看当前添加的账号名是否存在
            if(ObjectUtils.isNotEmpty(simpleGenericDataBaseService.getOne(WrapperUtils.query(amwayAccount)))){
                throw new OverallException("当前账号已有人在使用");
            }
            //密码校验
            ConvertUtil.passwordCheck(accountEdiTaccountReq);
            amwayAccount.setRealName(accountEdiTaccountReq.getRealname());
            amwayAccount.setPassWord(SHA256.encrypt(accountEdiTaccountReq.getPassword()));
            amwayAccount.setEnabled(accountEdiTaccountReq.getEnabled());
            amwayAccount.setCreateTime(new Date());
            log.warn("开始添加用户到数据库");
            if (!simpleGenericDataBaseService.save(amwayAccount)) {
                log.error("添加用户失败");
                throw new OverallException("添加用户失败");
            }
            log.info("添加用户业务执行完毕");
            return Result.builder().code(ResultState.SUCCESS).msg("添加用户成功").build();
        }else {
            //修改跟新
            log.info("开始执行编辑用户业务");
            amwayAccount.setRealName(accountEdiTaccountReq.getRealname());
            amwayAccount.setEnabled(accountEdiTaccountReq.getEnabled());
            amwayAccount.setId(accountEdiTaccountReq.getId());
            if(StringUtils.isBlank(accountEdiTaccountReq.getPassword())){
                log.warn("开始更新当前没修改密码的用户");
                if(!simpleGenericDataBaseService.updateById(amwayAccount)){
                    log.error("编辑用户失败");
                    throw new OverallException("编辑用户失败");
                }
                return Result.builder().code(ResultState.SUCCESS).msg("编辑用户成功").build();
            }
            log.warn("开始更新修改了密码的用户");
            //密码校验
            ConvertUtil.passwordCheck(accountEdiTaccountReq);
            amwayAccount.setPassWord(SHA256.encrypt(accountEdiTaccountReq.getPassword()));
            if(!simpleGenericDataBaseService.updateById(amwayAccount)){
                log.error("编辑用户失败");
                throw new OverallException("编辑用户失败");
            }
            log.info("编辑用户业务执行完毕");
            return Result.builder().code(ResultState.SUCCESS).msg("编辑用户成功").build();
        }
    }

}
