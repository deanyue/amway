package com.amway.broadcast.backstage.service.common.request;

import lombok.Data;

@Data
public class GroupMemberReq {

    /**
     * 调用接口凭证
     */
    private String access_token;

    /**
     * 获取的部门id
     */
    private Integer department_id;

    /**
     * 1/0：是否递归获取子部门下面的成员
     */
    private Integer fetch_child;

}
