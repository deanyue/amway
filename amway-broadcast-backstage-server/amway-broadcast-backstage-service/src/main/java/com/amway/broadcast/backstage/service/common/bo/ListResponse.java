package com.amway.broadcast.backstage.service.common.bo;

import lombok.Data;

import java.util.List;

@Data
public class ListResponse<T> {

    private List<T> list;
//    private RenewToken renewToken;
}
