package com.amway.broadcast.backstage.service.dao.mapper;

import com.amway.broadcast.backstage.service.common.bo.ApiShareTotalCountBo;
import com.amway.broadcast.backstage.service.dao.entity.AmwayGroupUser;
import com.amway.broadcast.backstage.service.dao.entity.AmwayGroupUserLog;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.List;

public interface AmwayGroupUserLogMapper extends BaseMapper<AmwayGroupUserLog> {

    /**
     *
     * @param groupId
     * @return
     */
    @Select("select A.shareTotalCount,q.nickname  from (select COUNT(*) as shareTotalCount,share_user_id from amway_group_user_log t  " +
            "where t.group_id=#{group_id} and t.share_user_id>0 group by t.share_user_id order by shareTotalCount desc limit 10) A " +
            "LEFT JOIN amway_user q on A.share_user_id=q.id")
    List<ApiShareTotalCountBo> selectGroupUserList(@Param(value = "group_id") Integer groupId);
}
