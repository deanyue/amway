package com.amway.broadcast.backstage.service.common.bo;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 分享排行榜
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class RankListBo {
    /**
     * 姓名
     */
    private String name;

    /**
     * 分享用户人数
     */
    private Integer share_total_count;
}
