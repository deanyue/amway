package com.amway.broadcast.backstage.service.common.bo;

import lombok.Data;

import java.util.List;

@Data
public class ApiMemberBo {

    private Integer errcode;

    private String errmsg;

    private List<ApiDepartmentMember> userlist;
}
