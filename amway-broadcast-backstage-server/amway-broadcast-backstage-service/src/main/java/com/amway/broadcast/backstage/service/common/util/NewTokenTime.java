package com.amway.broadcast.backstage.service.common.util;

import com.amway.broadcast.backstage.service.common.bo.RenewToken;
import com.amway.broadcast.backstage.service.common.constant.LoginWhether;
import com.amway.broadcast.common.util.JwtUtils;
import lombok.extern.slf4j.Slf4j;
import net.logstash.logback.encoder.org.apache.commons.lang3.StringUtils;
import org.redisson.api.RedissonClient;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Component;
import javax.annotation.Resource;
import java.util.Date;

@Slf4j
@Component
public class NewTokenTime {
    @Resource
    private RedissonClient redissonClient;
    public  RenewToken tokenTime(String token){
        String userName = JwtUtils.getUserNameFromToken(token);
        LoginWhether newToken = (LoginWhether) redissonClient.getBucket(userName).get();
        if(token.equals(newToken.getToken())){
            return null;
        }
        //新的过期时间
        Date expirationDateFromToken = JwtUtils.getExpirationDateFromToken(newToken.getToken());
        return RenewToken.builder().api_token(newToken.getToken()).expiration(expirationDateFromToken.getTime()).build();
    }
}
