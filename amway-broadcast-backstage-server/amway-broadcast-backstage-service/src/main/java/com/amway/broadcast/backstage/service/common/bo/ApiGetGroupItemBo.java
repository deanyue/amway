package com.amway.broadcast.backstage.service.common.bo;

import lombok.Data;

@Data
public class ApiGetGroupItemBo {

    private Integer id;
    private String name;
    private String name_en;
    private Integer parentid;
    private Integer order;
}
