package com.amway.broadcast.backstage.service.api.service;

import com.amway.broadcast.backstage.service.common.request.UserInformationReq;
import com.amway.broadcast.common.base.Result;

public interface UserLoginService {
    /**
     * 登入
     * @param userInformationReq
     * @param remoteIp
     * @return
     */
    Result login(UserInformationReq userInformationReq,String remoteIp);
}
