package com.amway.broadcast.backstage.service.common.bo;

import io.swagger.models.auth.In;
import lombok.Data;

import java.io.Serializable;

@Data
public class ApiShareTotalCountBo implements Serializable{

    private String nickname;

    private Integer shareTotalCount;
}
