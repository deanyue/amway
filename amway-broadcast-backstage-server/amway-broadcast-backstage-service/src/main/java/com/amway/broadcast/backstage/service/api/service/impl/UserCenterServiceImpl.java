package com.amway.broadcast.backstage.service.api.service.impl;

import com.amway.broadcast.backstage.service.api.service.SimpleGenericDataBaseService;
import com.amway.broadcast.backstage.service.api.service.UserCenterService;
import com.amway.broadcast.backstage.service.common.bo.AmwayUserBo;
import com.amway.broadcast.backstage.service.common.bo.BasicPage;
import com.amway.broadcast.backstage.service.common.bo.RenewToken;
import com.amway.broadcast.backstage.service.common.constant.Constant;
import com.amway.broadcast.backstage.service.common.util.NewTokenTime;
import com.amway.broadcast.backstage.service.dao.entity.AmwayUser;
import com.amway.broadcast.common.base.Result;
import com.amway.broadcast.common.constant.ResultState;
import com.amway.broadcast.common.util.DateUtils;
import com.amway.broadcast.mybatis.autoconfigure.util.WrapperUtils;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.vdurmont.emoji.EmojiParser;
import net.logstash.logback.encoder.org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;
import lombok.extern.slf4j.Slf4j;
import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;

@Service
@Slf4j
public class UserCenterServiceImpl implements UserCenterService {
    @Resource
    private SimpleGenericDataBaseService simpleGenericDataBaseService;

    @Resource
    private NewTokenTime newTokenTime;
    @Override
    public Result userList(String apiToken,Integer currentPage,Integer pageSize,String nickName) {
        log.info("开始执行获取用户列表业务层");
//        RenewToken renewToken = newTokenTime.tokenTime(apiToken);
        Page<AmwayUser> page=new Page(currentPage,pageSize);
        IPage<AmwayUser> page1;
        AmwayUser amwayUser = new AmwayUser();
        if(StringUtils.isBlank(nickName)){
            page1=simpleGenericDataBaseService.page(page,WrapperUtils.query(amwayUser).orderByDesc("create_time"));
        }else {
            page1 = simpleGenericDataBaseService.page(page, WrapperUtils.query(amwayUser).like("nickname",EmojiParser.parseToHtmlDecimal(nickName)).orderByDesc("create_time"));
        }
        List<AmwayUser> records = page1.getRecords();
        List <AmwayUserBo> lists=new ArrayList<>();
        records.forEach(am->{
            if(StringUtils.isNotBlank(am.getNickname())){
                am.setNickname(EmojiParser.parseToUnicode(am.getNickname()));
            }
            AmwayUserBo build = AmwayUserBo.builder()
                    .id(am.getId())
                    .nickname(am.getNickname())
                    .gender(Constant.GLOBAL_TYPR.equals(am.getGender())?"男":"女")
                    .avatar_url(am.getAvatarUrl())
                    .create_time(DateUtils.getYYYYMMDDHHMMSS(am.getCreateTime()))
                    .member_id(am.getMemberId())
                    .build();
            lists.add(build);
        });
        log.info("获取用户列表业务层执行完毕");
        return new Result(ResultState.SUCCESS,new BasicPage(lists,page1.getTotal()));
    }
}
