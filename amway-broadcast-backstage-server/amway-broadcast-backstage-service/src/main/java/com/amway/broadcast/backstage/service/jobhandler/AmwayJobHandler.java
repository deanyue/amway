package com.amway.broadcast.backstage.service.jobhandler;

import com.amway.broadcast.backstage.service.api.service.impl.MemberGroupServiceImpl;
import com.amway.broadcast.backstage.service.common.request.GroupReq;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

@Slf4j
@Component
public class AmwayJobHandler {


    @Autowired
    private MemberGroupServiceImpl memberGroupService;


    @Scheduled(cron = "0 0 1 * * ?")
//    @PeopleUpdateCheck(type = PeopleUpdateCheck.Type.JOB)
    public void execute() throws Exception {
        log.info("开始定时拉取组织架构 start");
        GroupReq groupReq = new GroupReq();
        try {
            Boolean isSuccessful=memberGroupService.getDepartmentData("ss","code",groupReq,"job");
            if(isSuccessful){
                log.info("定时拉取组织架构 SUCCESS");
            }else{
                log.error("定时拉取组织架构 ERROR");
            }
        }catch (Exception e){
            log.error("定时拉取组织架构中断{}",e.getStackTrace());
        }
    }
}
