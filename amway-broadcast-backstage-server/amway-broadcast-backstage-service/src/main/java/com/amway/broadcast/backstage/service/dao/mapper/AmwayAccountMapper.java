package com.amway.broadcast.backstage.service.dao.mapper;

import com.amway.broadcast.backstage.service.dao.entity.AmwayAccount;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

public interface AmwayAccountMapper extends BaseMapper<AmwayAccount> {
}
