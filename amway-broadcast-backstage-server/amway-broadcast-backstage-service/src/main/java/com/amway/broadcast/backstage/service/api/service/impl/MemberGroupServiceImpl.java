package com.amway.broadcast.backstage.service.api.service.impl;

import com.amway.broadcast.backstage.service.api.service.MemberGroupService;
import com.amway.broadcast.backstage.service.api.service.SimpleGenericDataBaseService;
import com.amway.broadcast.backstage.service.common.bo.*;
import com.amway.broadcast.backstage.service.common.constant.Constant;
import com.amway.broadcast.backstage.service.common.request.AmwayMemberReq;
import com.amway.broadcast.backstage.service.common.request.GroupReq;
import com.amway.broadcast.backstage.service.common.util.*;
import com.amway.broadcast.backstage.service.dao.entity.AmwayDepartment;
import com.amway.broadcast.backstage.service.dao.entity.AmwayDepartmentMember;
import com.amway.broadcast.backstage.service.dao.entity.AmwayGroup;
import com.amway.broadcast.backstage.service.dao.entity.AmwayGroupUserLog;
import com.amway.broadcast.backstage.service.dao.mapper.*;
import com.amway.broadcast.backstage.service.redis.RedisUtils;
import com.amway.broadcast.common.exception.user.overall.OverallException;
import com.amway.broadcast.common.util.CommonUtils;
import com.amway.broadcast.common.util.DateUtils;
import com.amway.broadcast.mybatis.autoconfigure.util.WrapperUtils;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.vdurmont.emoji.EmojiParser;
import lombok.extern.slf4j.Slf4j;
import net.logstash.logback.encoder.org.apache.commons.lang3.StringUtils;
import org.apache.commons.collections4.CollectionUtils;
import org.redisson.api.RedissonClient;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import static com.amway.broadcast.common.enumeration.RedisKeyEnum.SYNC_USER_DEPARTMENT_LOCK;

@Slf4j
@Service
@Transactional
public class MemberGroupServiceImpl implements MemberGroupService {

    @Value("${config.corpid}")
    private String corpid;

    @Value("${config.corpsecret}")
    private String corpsecret;

    //用户身份校验secret
    @Value("${config.secret}")
    private String secret;

    @Resource
    private RedisUtils redisUtils;

    @Autowired(required = false)
    private SimpleGenericDataBaseService simpleGenericDataBaseService;


    @Autowired(required = false)
    private AmwayDepartmentMapper amwayDepartmentMapper;

    @Autowired(required = false)
    private AmwayDepartmentMemberMapper amwayDepartmentMemberMapper;

    @Autowired(required = false)
    private AmwayGroupUserLogMapper amwayGroupUserMapper;

//    @Autowired(required = false)
//    private AmwayGroupUserMapper amwayGroupUserMapper;

    @Autowired(required = false)
    private AmwayGroupMapper amwayGroupMapper;

    @Resource
    private RedissonClient redissonClient;


    @Autowired(required = false)
    private AmwayUserMapper amwayUserMapper;

    @Resource
    private NewTokenTime newTokenTime;

    @Override
    public BasicPage<MemberlistBo> getMemberPage(AmwayMemberReq amwayMemberReq) {
        if (CommonUtils.isEmpty(amwayMemberReq)) {
            log.error("请求体不能为空");
            throw new OverallException("请求体不能为空");
        }
        if (StringUtils.isBlank(amwayMemberReq.getApi_token())) {
            log.error("api_token不能为空");
            throw new OverallException("api_token不能为空");
        }
        if (CommonUtils.isEmpty(amwayMemberReq.getCurrent_page()) || CommonUtils.isEmpty(amwayMemberReq.getPage_size())) {
            log.error("分页参数不能为空");
            throw new OverallException("分页参数不能为空");
        }

        BasicPage<MemberlistBo> pageResult = new BasicPage<>();
        Page<AmwayDepartmentMember> departmentMemberPage = new Page<>(amwayMemberReq.getCurrent_page(), amwayMemberReq.getPage_size());

        //搜索部门员工表
        log.info("执行amwayDepartmentMemberMapper.selectDepartmentMemberPage sql:");
        IPage<AmwayDepartmentMember> resultInfo = amwayDepartmentMemberMapper.selectDepartmentMemberPage(departmentMemberPage, amwayMemberReq.getD_id(), amwayMemberReq.getName());

        List<AmwayDepartmentMember> records = resultInfo.getRecords();

        List<MemberlistBo> resultList = new ArrayList<>();
        if (!CommonUtils.isEmpty(records)) {
            for (int i = 0; i < records.size(); i++) {
                AmwayDepartmentMember recordItem = records.get(i);
                //搜索部门名称，构建部门员工返回对象
                log.info("执行amwayDepartmentMapper sql:");
                MemberlistBo listBo = ConvertUtil.convertMemberListBo(amwayDepartmentMapper, recordItem, i + 1);
                resultList.add(listBo);
            }
        }else{
            log.info("部门员工分页查询结果为空");
        }
        pageResult.setList(resultList);
        pageResult.setTotal_count(resultInfo.getTotal());
        log.info("getMemberPage执行完毕");
        return pageResult;
    }

    @Override
    public Boolean getDepartmentData(String wxapp_api_token,String code,GroupReq groupReq, String ipAddr) {
        if (CommonUtils.isEmpty(groupReq)) {
            log.error("请求体不能为空");
            throw new OverallException("请求体不能为空");
        }
        Object isSync = redisUtils.get(SYNC_USER_DEPARTMENT_LOCK.getPrefix());
        //锁存在正在同步返回true
        if (org.springframework.util.StringUtils.isEmpty(isSync)){
            redisUtils.set(SYNC_USER_DEPARTMENT_LOCK.getPrefix(),"1",SYNC_USER_DEPARTMENT_LOCK.getExpiredTime());
            //获取access_token
            ApiTokenBo tokenBo = RequestUtil.getTokenBo(corpid, corpsecret);
            //获取身份校验access_token
            ApiTokenBo tokenBo1 = RequestUtil.getTokenBo(corpid, secret);
            String membersUserid = null;
//        if (!ipAddr.equals("job")) {
//            //就不是定时框架触发更新，开始判断用户身份是否为企业用户
//            membersUserid= RequestUtil.IdentityCheck(tokenBo1, code);
//            if (net.logstash.logback.encoder.org.apache.commons.lang3.StringUtils.isBlank(membersUserid)) {
//                throw new OverallException("非企业成员,不能拉取数据");
//            }
//        }
            //获取所有部门数据
            if (!CommonUtils.isEmpty(tokenBo) && !StringUtils.isBlank(tokenBo.getAccess_token())) {
                new Thread(()-> syncDepartmentUser(groupReq, ipAddr, tokenBo, membersUserid)).start();
            }
        }
        return true;
    }

    private void syncDepartmentUser(GroupReq groupReq, String ipAddr, ApiTokenBo tokenBo, String membersUserid) {
        //返回部门数据
        try {
            ApiGetDepartmentBo memberBo = RequestUtil.getDepartmentBo(tokenBo.getAccess_token(), groupReq.getId());
            if (0 == memberBo.getErrcode()) {
                List<ApiGetGroupItemBo> departmentlist = memberBo.getDepartment();
                if (!CommonUtils.isEmpty(departmentlist)) {
                    List<Integer> ids = departmentlist.stream().map(ApiGetGroupItemBo::getId).collect(Collectors.toList());
                    LambdaQueryWrapper<AmwayDepartment> departmentWrapper = new LambdaQueryWrapper<AmwayDepartment>();
                    departmentWrapper.in(AmwayDepartment::getDepartmentId, ids);
                    List<AmwayDepartment> amwayDepartmentList = amwayDepartmentMapper.selectList(departmentWrapper);
                    departmentlist.parallelStream().forEach(departmentItem -> {
                        //由于字段不同，重新分装
                        AmwayDepartment amwayDepartment = ConvertUtil.convertDepartment(departmentItem);
//                        log.info(JSON.toJSONString(amwayDepartment));
                        //判断有无存在department_id对应的数据，如果有就不添加
                        //如果拉取的数据没有重复的就根据id查询查询查询后然后一一吧amwayDepartment值赋值给查出来的数据跟新
                        Integer existDepartmentCount = 0;
                        if (CollectionUtils.isNotEmpty(amwayDepartmentList)){
                            existDepartmentCount = (int) amwayDepartmentList.stream().filter(d -> d.getDepartmentId().equals(amwayDepartment.getDepartmentId())).count();
                        }
                        if (existDepartmentCount.equals(0)) {
                            log.info("执行simpleGenericDataBaseService.save sql:");
                            simpleGenericDataBaseService.save(amwayDepartment);
                        }else{
                            log.info("已经找到对应的部门");
                        }
                        //获取部门下的员工数据
                        if (!CommonUtils.isEmpty(departmentItem.getId())) {
                            List<ApiDepartmentMember> memberlist = RequestUtil.getDepartmentMemberListBo(tokenBo.getAccess_token(), departmentItem.getId(), groupReq.getFetch_child());

                            if (!CommonUtils.isEmpty(memberlist)) {
                                List<String> userIdList = memberlist.stream().map(ApiDepartmentMember::getUserid).collect(Collectors.toList());
                                LambdaQueryWrapper<AmwayDepartmentMember> departmentMemberWrapper = new LambdaQueryWrapper<AmwayDepartmentMember>();
                                departmentMemberWrapper.in(AmwayDepartmentMember::getUserid, userIdList);
                                List<AmwayDepartmentMember> memberList = amwayDepartmentMemberMapper.selectList(departmentMemberWrapper);
                                memberlist.forEach(memberItem -> {
                                    //部门成员表
                                    AmwayDepartmentMember entity = ConvertUtil.convertDeparmentMemberBo(memberItem);
                                    log.info("远程查出来的数据"+entity);
                                    String userid = entity.getUserid();
                                    //判断是否有userid相同的数据
                                    Integer existsDepartmentMember = 0;
                                    if (CollectionUtils.isNotEmpty(memberList)){
                                        existsDepartmentMember = (int) memberList.stream().filter(m -> m.getUserid().equals(userid)).count();
                                    }
                                    if (existsDepartmentMember.equals(0)) {
                                        log.info("执行simpleGenericDataBaseService.save sql:");
                                        //插入部门员工表
                                        simpleGenericDataBaseService.save(entity);
                                    }else{
                                        log.warn("数据库中已有员工数据,开始执行跟新操作");
                                        simpleGenericDataBaseService.update(entity,new QueryWrapper<AmwayDepartmentMember>().eq("userid",userid));
                                    }
                                });
                            }
                        }
                        else{
                            log.info("部门信息中没有id");
                        }
                    });
                }
            }
            //记录日志
            log.info("同步微信组织架构数据成功");
            LogUtil.addLog(simpleGenericDataBaseService,membersUserid, ipAddr, Constant.LOG_WEIXIN);
        }catch (Exception e){
            log.error("同步微信组织架构数据失败:{}",e.getMessage());
        }finally {
            redisUtils.delete(SYNC_USER_DEPARTMENT_LOCK.getPrefix());
        }
    }


    @Override
    public ListResponse<DepartMentNode> getMemberList(String api_token) {
        //查询所有部门
        ListResponse<DepartMentNode> listResponse = new ListResponse<>();
        QueryWrapper<AmwayDepartment> wrapper = new QueryWrapper();
        log.info("执行amwayDepartmentMapper.getDepartmentList sql:");
        List<AmwayDepartment> toplist = amwayDepartmentMapper.getDepartmentList();
        List<DepartMentNode> nodelist = new ArrayList<>();
        if(!CommonUtils.isEmpty(toplist)){
            toplist.forEach(item -> {
                nodelist.add(DepartMentNode.builder()
                        .d_id(item.getDepartmentId())
                        .label(item.getName())
                        .parent_id(item.getParentId())
                        .build());
            });
        }else{
            log.info("数据库中无部门数据");
        }
        List<DepartMentNode> resultNodes = TreeUtils.listToTree(nodelist);
        listResponse.setList(resultNodes);
        log.info("获取部门数据成功");
        return listResponse;
    }

    @Override
    public ListResponse<ApiShareTotalCountBo> getGroupRankList(String api_token, Integer group_id) {
        if (CommonUtils.isEmpty(group_id)) {
            log.error("组织id不能为空");
            throw new OverallException("组织id不能为空");
        }
        log.info("执行amwayGroupUserMapper.selectGroupUserList sql:");
        List<ApiShareTotalCountBo> apiShareTotalCountBos = amwayGroupUserMapper.selectGroupUserList(group_id);
        //查询出来的结果中的用户昵称特殊的格式转换
        //定义一个集合
        List<ApiShareTotalCountBo> list=new ArrayList<>();
        for (int i = 0; i < apiShareTotalCountBos.size(); i++) {
            ApiShareTotalCountBo apiShareTotalCountBo = apiShareTotalCountBos.get(i);
            if(StringUtils.isNotBlank(apiShareTotalCountBo.getNickname())){
                apiShareTotalCountBo.setNickname(EmojiParser.parseToUnicode(apiShareTotalCountBo.getNickname()));
            }
            list.add(apiShareTotalCountBo);
        }
        ListResponse<ApiShareTotalCountBo> response = new ListResponse<>();
        response.setList(list);
        log.info("直播群排行榜接口执行成功");
        return response;
    }

    @Override
    public BasicPage<ApiGroupBo> getGroupList(String api_token, String name, Integer status, String start_date, String end_date, Integer current_page, Integer page_size) {
        QueryWrapper<AmwayGroup> query = new QueryWrapper<>();
        if (StringUtils.isNotBlank(start_date) && StringUtils.isNotBlank(end_date)) {
            query.between("create_time", start_date, end_date);
        }
        if (!StringUtils.isBlank(name)) {
            query.like("name",EmojiParser.parseToHtmlDecimal(name));
        }
        if (!CommonUtils.isEmpty(status) && status != -1) {
            query.eq("status_am", status);
        }
        query.orderByDesc("create_time");

        BasicPage<ApiGroupBo> pageResult = new BasicPage<>();
        Page<AmwayGroup> groupPage = new Page<>(current_page, page_size);
        log.info("执行amwayGroupMapper.selectPage sql:");
        IPage<AmwayGroup> resultInfo = amwayGroupMapper.selectPage(groupPage, query);
        //查询所有Group表数据
        List<AmwayGroup> recordList = resultInfo.getRecords();

//        返参集合
        List<ApiGroupBo> resultList = new ArrayList<>();
        if(!CommonUtils.isEmpty(recordList)){
            for (int i = 0; i < recordList.size(); i++) {
                AmwayGroup recordItem = recordList.get(i);
                //直播名格式转换
                recordItem.setName(EmojiParser.parseToUnicode(recordItem.getName()));
                if(StringUtils.isNotBlank(recordItem.getRemark())){
                    recordItem.setRemark(EmojiParser.parseToUnicode(recordItem.getRemark()));
                }
                //totalCount
//                QueryWrapper<AmwayGroupUser> queryUser = new QueryWrapper<>();
//                queryUser.eq("group_id", recordItem.getId());
                log.info("执行amwayGroupUserMapper.selectCount sql:");
                Integer totalCount = amwayGroupUserMapper.selectCount(new QueryWrapper<AmwayGroupUserLog>().eq("group_id", recordItem.getId()));
                //creatorName
                AmwayDepartmentMember amwayDepartmentMember=new AmwayDepartmentMember();
                if(!CommonUtils.isEmpty(recordItem.getMemberId())){
                    amwayDepartmentMember.setId(recordItem.getMemberId());
                    log.info("执行DepartmentMember.selectOne sql:");
                    amwayDepartmentMember = simpleGenericDataBaseService.getOne(WrapperUtils.query(amwayDepartmentMember).select("name"));
                }
                ApiGroupBo resultItem = new ApiGroupBo();
                BeanUtils.copyProperties(recordItem, resultItem);
                resultItem.setIndex(i + 1);
                resultItem.setTotal_user_count(totalCount);
                resultItem.setCreate_time(DateUtils.getYYYYMMDDHHMMSS(recordItem.getCreateTime()));
                if (!CommonUtils.isEmpty(amwayDepartmentMember)&&StringUtils.isNotBlank(amwayDepartmentMember.getName())) {
                    resultItem.setCreator_name(EmojiParser.parseToUnicode(amwayDepartmentMember.getName()));
                }
                resultList.add(resultItem);
            }
        }else{
            log.info("数据库中无group数据");
        }
        pageResult.setList(resultList);
        pageResult.setTotal_count(resultInfo.getTotal());
        log.info("直播群列表接口执行完毕");
        return pageResult;
    }


}
