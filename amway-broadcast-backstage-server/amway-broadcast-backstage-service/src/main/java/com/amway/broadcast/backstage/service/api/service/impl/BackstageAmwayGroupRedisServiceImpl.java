//package com.amway.broadcast.backstage.service.api.service.impl;
//
//import com.amway.broadcast.backstage.service.dao.entity.AmwayGroup;
//import com.amway.broadcast.common.service.AmwayGroupService;
//import org.springframework.stereotype.Service;
//import org.springframework.transaction.annotation.Transactional;
//
//import java.util.List;
//
///**
// * @auther 薛晨
// * @date 2020/12/12
// * @time 11:37
// * @description redis实现
// */
//@Service
//@Transactional(rollbackFor = Exception.class)
//public class BackstageAmwayGroupRedisServiceImpl implements AmwayGroupService<AmwayGroup> {
//    @Override
//    public int insert(AmwayGroup baseAmwayGroup) {
//        return 0;
//    }
//
//    @Override
//    public int delete(String groupId) {
//        return 0;
//    }
//
//    @Override
//    public int update(AmwayGroup baseAmwayGroup) {
//        return 0;
//    }
//
//    @Override
//    public List<AmwayGroup> getListByCondition(AmwayGroup condition) {
//        return null;
//    }
//
//    @Override
//    public AmwayGroup getByGroupId(String groupId) {
//        return null;
//    }
//
//    @Override
//    public AmwayGroup getByImGroupId(String imGroupId) {
//        return null;
//    }
//
//    @Override
//    public List<AmwayGroup> getByMemberId(Long memberId) {
//        return null;
//    }
//}
