package com.amway.broadcast.backstage.service.common.request;

import lombok.Data;

@Data
public class GroupReq {
    //部门id。获取指定部门及其下的子部门（以及及子部门的子部门等等，递归）。 如果不填，默认获取全量组织架构
    private String id;
    // 1/0：是否递归获取子部门下面的成员
    private String fetch_child;
}
