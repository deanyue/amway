package com.amway.broadcast.backstage.service.api.service.impl;

import com.amway.broadcast.backstage.service.dao.entity.AmwayGroupUser;
import com.amway.broadcast.common.service.AmwayGroupUserService;

import java.util.List;

/**
 * @auther 薛晨
 * @date 2020/12/12
 * @time 13:50
 * @description
 */
public class AmwayGroupUserDBServiceImpl implements AmwayGroupUserService<AmwayGroupUser> {
    @Override
    public int insert(AmwayGroupUser baseAmwayGroupUser) {
        return 0;
    }

    @Override
    public int update(AmwayGroupUser baseAmwayGroupUser) {
        return 0;
    }

    @Override
    public List<AmwayGroupUser> getListByCondition(AmwayGroupUser condition) {
        return null;
    }

    @Override
    public AmwayGroupUser getOne(String groupId, String userId) {
        return null;
    }

    @Override
    public List<AmwayGroupUser> getByGroupIdAndStatus(String groupId, Integer quitStatus) {
        return null;
    }
}
