package com.amway.broadcast.backstage.service.api.service;

import com.amway.broadcast.backstage.service.common.bo.*;
import com.amway.broadcast.backstage.service.common.request.AmwayMemberReq;
import com.amway.broadcast.backstage.service.common.request.GroupReq;

public interface MemberGroupService {
    /**
     * 返回部门成员列表
     * @param amwayMemberReq
     * @return
     */
    BasicPage<MemberlistBo> getMemberPage(AmwayMemberReq amwayMemberReq);

    /**
     * 拉取部门及部门员工数据
     * @param groupReq
     * @param ipAddr
     * @return
     */
    Boolean getDepartmentData(String wxapp_api_token,String code,GroupReq groupReq, String ipAddr);

    /**
     * 部门树形图
     * @param api_token
     * @return
     */
    ListResponse<DepartMentNode> getMemberList(String api_token);

    /**
     * 直播群排行榜
     * @param apiToken
     * @param groupId
     * @return
     */
    ListResponse<ApiShareTotalCountBo> getGroupRankList(String apiToken, Integer groupId);

    /**
     * 直播群列表
     * @param apiToken
     * @param name
     * @param status
     * @param startDate
     * @param endDate
     * @param currentPage
     * @param pageSize
     * @return
     */
    BasicPage<ApiGroupBo> getGroupList(String apiToken, String name, Integer status, String startDate, String endDate, Integer currentPage, Integer pageSize);


    //  Boolean getDepartmentMemberData(GroupMemberReq groupMemberReq);
}
