package com.amway.broadcast.backstage.service.common.bo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * 分页参数
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class BasicPage<T> {

    private List<T> list;

    private Long total_count;

//    private RenewToken renewToken;

}
