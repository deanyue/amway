package com.amway.broadcast.backstage.service.common.bo;

import lombok.Builder;
import lombok.Data;

import java.util.List;

@Data
@Builder
public class DepartMentNode {

    private Integer d_id;

    private String label;

    private Integer parent_id;

    private List<DepartMentNode> children;
}

