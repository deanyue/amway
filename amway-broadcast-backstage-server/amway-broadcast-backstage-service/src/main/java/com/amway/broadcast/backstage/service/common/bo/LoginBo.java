package com.amway.broadcast.backstage.service.common.bo;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class LoginBo {
    private String session_id;
    private String realName;
    private String user_id;
    private Long expiration;
}
