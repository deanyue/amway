package com.amway.broadcast.backstage.service.dao.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Builder;
import lombok.Data;
import java.io.Serializable;
import java.util.Date;

@Builder
@Data
@TableName(value = "amway_log")
public class AmwayLog  implements Serializable {
    private static final long serialVersionUID = 3470881553030536328L;
    @TableId(value="id",type= IdType.AUTO)
    private Integer id;

    @TableField(value = "ip")
    private String ip;

    @TableField(value = "type")
    private Integer type;

    @TableField(value = "content")
    private String content;

    @TableField(value = "create_time")
    private Date createTime;
}
