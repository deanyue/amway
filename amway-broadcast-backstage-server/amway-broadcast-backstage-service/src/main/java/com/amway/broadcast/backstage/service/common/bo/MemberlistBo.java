package com.amway.broadcast.backstage.service.common.bo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;


/**
 * 部门成员列表
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class MemberlistBo {

    /**
     * 序号
     */
    private Integer  index;

    /**
     * 成员id号
     */
    private String id;

    /**
     * 姓名
     */
    private String name;

    /**
     * 性别
     */
    private String gender;

    /**
     * 职务
     */
    private String position;

    /**
     * 部门名称
     */
    private String department_name;

    /**
     * 手机号
     */
    private String mobile;

    /**
     * 邮箱
      */
    private String email;


    /**
     * 头像
     */
    private String thumb_avatar;


    /**
     * 状态
     */
    private Integer status;
}
